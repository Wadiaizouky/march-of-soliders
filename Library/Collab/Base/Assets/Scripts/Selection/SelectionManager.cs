﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace MarchOfSoldiers
{
    public class SelectionManager : MonoBehaviour
    {
        #region Fields

        #region Wade
        /* ************************************** */
        public List<SingleCastle> Allcastle;
        public List<SingleCastle> Allgate;

        /// <summary> 
        /// for testing use it
        /// </summary>
        private List<GameObject> AllBoxPositions;
        private List<List<Vector2>> CircleBoxPosition;
        bool Drawablee;
        SingleCastle TargetCastle;


        //return selected Castles
        public List<SingleCastle> GetSelectedCastles()
        {
            return CastlesSelection.Instance.selectedCastles;
        }

        //return selected Soldiers
        public List<GroupOfUnits> GetselectedSoliders()
        {
            // return _selectedSoldiers;
            return UnitsSelection.Instance.selectedGroups;
        }


        /* ************************************** */
        #endregion

        private static SelectionManager _instance = null;

        [Header("Groups")]
        public GroupOfUnits _groupOfPlayerSowrdUnits = null;
        public GroupOfUnits _groupOfEnemySowrdUnits = null;

        [Tooltip("Layers that contains Objects on screen that can be selected."), SerializeField]
        public LayerMask _selectableObjectsLayer;



        [HideInInspector] public Camera mainCamera = null;
        [HideInInspector] public SelectableType currentSelectedType;
        [HideInInspector] public Vector3 mousePos1, mousePos2;
        [HideInInspector] public float lastClickTime = 0;
        [HideInInspector] public GameObject mouseObjectHit;

        public const float MAX_VIRTUAL_SIZE = 10;
        public const int MAX_UNITS_PER_GROUP = 200;
        public const int MAX_SELECTABLE_GROUP = 12;
        public const float DOUBLE_CLICK_DEFERENCE = 0.2f;

        private bool canSelect = true;


        #endregion

        #region Properties

        public static SelectionManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<SelectionManager>();
                return _instance;
            }
        }

        /// <summary>
        /// percent of group used to separate group of units.
        /// </summary>
        public int Percent { get; set; }

        /// <summary>
        /// True while selection box in on drawn. 
        /// </summary>
        public bool DrawSelectionBox { get; set; }

        public List<GroupOfUnits> SelectedGroups { get => UnitsSelection.Instance.selectedGroups; }

        #endregion

        #region MonoBehavior Callbacks

        private void Awake()
        {
            mainCamera = Camera.main;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.T))
                print(UnitsSelection.Instance.selectedGroups.Count);

            UpdateMousePosition();
            UpdateMouseObjectHit();
            UpdateSelection();



            if (Input.GetMouseButtonUp(0))
            {
                canSelect = true;
            }


            // wadi add:
            /* ******************************************************************* */
            //load cicil level if Player have all Castles or loose All his Castles
            CheckWinnerPlayer();

            //Show UI Selver and Gold Coin
            //CalculatSelverAndGoldCoin();

            /////////////////For ai
            //if (!AddSoliderforfirsttime)
            //    AiAddExtraSoliders();

            //for outing soliders with mouse
            DrawBoxPosition drawBox = GetComponent<DrawBoxPosition>();
            AllBoxPositions = drawBox.AllBoxPositions;
            //CircleBoxPosition = drawBox.BoxCirclesPositions;
            Drawablee = drawBox.Drawablee;
            TargetCastle = drawBox.TargetCastle;

            if (Input.GetMouseButtonUp(1) && Drawablee && !IsMouseOverUI())
                CkeckwhatSelecting();



            /* ******************************************************************* */

        }


        #endregion

        #region Selectable

        private void UpdateMousePosition()
        {
            if (Input.GetMouseButtonDown(0) && canSelect)
            {
                if (IsMouseOverUI())
                {
                    canSelect = false;
                }
                mousePos1 = mainCamera.ScreenToViewportPoint(Input.mousePosition);
            }
            if (Input.GetMouseButtonUp(0))
            {
                mousePos2 = mainCamera.ScreenToViewportPoint(Input.mousePosition);
            }
        }

        public bool IsMouseOverUI()
        {
            return EventSystem.current.IsPointerOverGameObject();
        }

        private void UpdateMouseObjectHit()
        {
            mouseObjectHit = null;
            if (!Input.GetMouseButtonDown(1))
            {
                Vector2 worldPoint = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero, Mathf.Infinity);
                if (hit) mouseObjectHit = hit.collider.gameObject;
            }
        }

        private void UpdateSelection()
        {
            if (Input.GetMouseButtonDown(0) && canSelect)
            {
                DrawSelectionBox = true;
                GameObject hit = GetHit(Input.mousePosition);
                if (hit)
                {
                    SelectUnit(hit);
                }
                //else if (!Input.GetKey("left ctrl"))
                //{
                //    ClearSelection();
                //}

            }
            if (Input.GetMouseButtonUp(0))
            {
                DrawSelectionBox = false;
                if (mousePos1 != mousePos2 && canSelect)
                    SelectMultipleUnits();
            }
        }

        private GameObject GetHit(Vector3 pos)
        {
            Vector2 worldPoint = mainCamera.ScreenToWorldPoint(pos);

            SingleUnit unit = UnitsRaycasting.Instance.HitSolider(worldPoint);
            if (unit != null)
            {
                return unit.gameObject;
            }

            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero, Mathf.Infinity, _selectableObjectsLayer);
            if (hit)
            {
                return hit.collider.gameObject;
            }

            return null;
        }


        private void SelectUnit(GameObject hit)
        {
            Selectable selectableObject = hit.GetComponent<Selectable>();

            if (!selectableObject) return;

            switch (selectableObject.SelectableType)
            {
                case SelectableType.Soldier:
                    {
                        UnitsSelection.Instance.OnSelectionSoldier(selectableObject);
                        break;
                    }
                case SelectableType.Castle:
                    {
                        if(selectableObject.GetComponent<SingleCastle>().isGate)
                        {
                            GatesSelection.Instance.OnSelectionGates(selectableObject);
                        }
                        else
                        {
                            CastlesSelection.Instance.OnSelectionCastle(selectableObject);
                        }
                        
                        break;
                    }

                default:
                    break;
            }
        }

        private void SelectMultipleUnits()
        {
            // create rectangle to select objects.
            Rect selectionBox = new Rect(
                x: mousePos1.x,
                y: mousePos1.y,
                width: mousePos2.x - mousePos1.x,
                height: mousePos2.y - mousePos1.y);

            bool selectMultipleUnits = UnitsSelection.Instance.SelectMultipleSoldiers(ref selectionBox);

            if (selectMultipleUnits) return;

            bool selectMultipleCastles = CastlesSelection.Instance.SelectMultipleCastles(ref selectionBox);

            if (selectMultipleCastles) return;

            bool selectMultipleGates = GatesSelection.Instance.SelectMultipleGates(ref selectionBox);

            if (selectMultipleGates) return;
        }

        public void ClearSelection()
        {
            UnitsSelection.Instance.ClearUnitsSelection();
            CastlesSelection.Instance.ClearCastlesSelection();
            GatesSelection.Instance.ClearGatesSelection();

            currentSelectedType = SelectableType.None;
        }

        public void AddSelectable(Selectable unit, SelectableType type)
        {
            switch (type)
            {
                case SelectableType.None:
                    return;
                case SelectableType.Soldier:
                    UnitsSelection.Instance.selectableSoldiers.Add(unit as SingleUnit);
                    break;
                case SelectableType.Castle:
                    {
                        if (unit.GetComponent<SingleCastle>().isGate)
                        {
                            GatesSelection.Instance.selectableGates.Add(unit as SingleCastle);
                        }
                        else
                        {
                            CastlesSelection.Instance.selectableCastles.Add(unit as SingleCastle);
                        }

                        break;
                    }
                default:
                    break;
            }
        }

        public void RemoveSelectable(Selectable unit, SelectableType type)
        {
            switch (type)
            {
                case SelectableType.None:
                    return;
                case SelectableType.Soldier:
                    UnitsSelection.Instance.selectableSoldiers.Remove(unit as SingleUnit);
                    break;
                case SelectableType.Castle:
                    {
                        if (unit.GetComponent<SingleCastle>().isGate)
                        {
                            GatesSelection.Instance.selectableGates.Add(unit as SingleCastle);
                        }
                        else
                        {
                            CastlesSelection.Instance.selectableCastles.Add(unit as SingleCastle);
                        }

                        break;
                    }
                default:
                    break;
            }
        }

        #endregion

        #region WadeeFunctions

        /// <summary>wade adds
        /// Great Groups when select Castles and press I
        /// </summary>
        public void GreatGroupFromCastels()
        {



            //List<GroupOfUnits> groupOfUnits = new List<GroupOfUnits>();
            for (int i = 0; i < CastlesSelection.Instance.selectedCastles.Count; i++)
            {
                if (CastlesSelection.Instance.selectedCastles[i] != AllBoxPositions[i].GetComponent<SingleCastle>())
                {
                    int count = int.Parse(CastlesSelection.Instance.selectedCastles[i].transform.GetChild(1).gameObject.name);
                    int groupsize = Selectedpercentage();

                    //CreateGroupParameters group = new CreateGroupParameters();
                    //group.GroupOwner = GroupOwner.Player;
                    //group.GroupType = GroupType.Sword;
                    //group.Position = AllBoxPositions[i].transform.position;
                    //group.SourceCastle = _selectedCastles[i];
                    //group.GroupSize = groupsize;
                    //if (TargetCastle != null)
                    //    group.TargetCastle = TargetCastle;

                    ////////
                    List<Vector2> CirclePosition = new List<Vector2>();
                    if (TargetCastle == null)
                    {
                        foreach (Circle circle in AllBoxPositions[i].GetComponent<BoxPosition>().BoxCirclesPositios)
                        {
                            CirclePosition.Add(circle.transform.position);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < 20; j++)
                            CirclePosition.Add(TargetCastle.transform.position);
                    }

                    Vector2 Mouseposition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    GroupOfUnits g;
                    if (count > groupsize)
                    {
                        //group.GroupSize = groupsize;
                        //CreateGroup(groupsize, AllBoxPositions[i].transform.position, GroupType.Sword, GroupOwner.Player, _selectedCastles[i]);
                        //g = CreateGroup(group);
                        if (CastlesSelection.Instance.selectedCastles[i].tag == "Player")
                            g = UnitsSpawner.Instance.CreateGroup(groupsize, CirclePosition, GroupType.Sword, GroupOwner.Player, Mouseposition, CastlesSelection.Instance.selectedCastles[i], TargetCastle);
                    }
                    else
                    {
                        //group.GroupSize = count;
                        //CreateGroup(count, AllBoxPositions[i].transform.position, GroupType.Sword, GroupOwner.Player, _selectedCastles[i]);
                        //g= CreateGroup(group);
                        if (CastlesSelection.Instance.selectedCastles[i].tag == "Player")
                            g = UnitsSpawner.Instance.CreateGroup(count, CirclePosition, GroupType.Sword, GroupOwner.Player, Mouseposition, CastlesSelection.Instance.selectedCastles[i], TargetCastle);

                    }


                    //groupOfUnits.Add(g);
                    //MoveMultipleGroups(groupOfUnits, group.TargetCastle.transform.position, false, group.TargetCastle);


                }

            }
            //if (TargetCastle != null)
            //{
            //    MoveMultipleGroups(groupOfUnits, TargetCastle.transform.position, false, TargetCastle);

            //}
        }

        /// <summary>wade adds
        /// Move All Selected Groups
        /// </summary>
        public void MoveAllSelectedGroups()
        {

            for (int i = 0; i < UnitsSelection.Instance.selectedGroups.Count; i++)
            {
                List<Vector2> CirclePosition = new List<Vector2>();
                if (TargetCastle == null)
                {
                    foreach (Circle circle in AllBoxPositions[i].GetComponent<BoxPosition>().BoxCirclesPositios)
                    {
                        CirclePosition.Add(circle.transform.position);
                    }
                }
                else
                {
                    for (int j = 0; j < 20; j++)
                        CirclePosition.Add(TargetCastle.transform.position);
                }

                UnitsOperations.Instance.MoveGroups(UnitsSelection.Instance.selectedGroups[i], CirclePosition, TargetCastle);
            }
        }


        /// <summary>wade adds
        /// check if select Castle or Soliders
        /// </summary>
        public void CkeckwhatSelecting()
        {

            if (UnitsSelection.Instance.selectedGroups.Count > 0)
                MoveAllSelectedGroups();
            else if (mouseObjectHit == null || mouseObjectHit.tag != "Map Border")
                GreatGroupFromCastels();
        }

        // <summary>wade adds
        // return the percentage selected by player in UI
        // </summary>
        public int Selectedpercentage()
        {
            return NumOfUnits_UI.Instance.GetNumOfUnits();
        }

        //// Ai
        //void AiAddExtraSoliders()
        //{
        //    List<SingleCastle> AiCastles = new List<SingleCastle>();
        //    int AiCastleCount = 0;
        //    foreach (SingleCastle cast in Allcastle)
        //    {
        //        if (cast.GetComponent<CastelArmyUICount>().isEnumyCastle)
        //        {
        //            AiCastles.Add(cast);
        //            AiCastleCount++;
        //        }
        //    }

        //    if (AiCastleCount == 4)
        //    {
        //        AddSoliderforfirsttime = true;
        //        foreach (SingleCastle cast in AiCastles)
        //        {
        //            CastelArmyUICount ca = cast.GetComponent<CastelArmyUICount>();
        //            cast.transform.GetChild(2).gameObject.transform.GetChild(3).gameObject.active = true;
        //            ca.IsExtraSoliders = true;
        //        }
        //    }
        //}
        /// <summary>wade adds
        /// load cicil level if Player have all Castles or loose All his Castles
        /// </summary>
        public void CheckWinnerPlayer()
        {
            if (CastlesSelection.Instance.selectableCastles.Count == Allcastle.Count || CastlesSelection.Instance.selectableCastles.Count == 0)
                SceneManager.LoadScene("Civil war");
        }



        /////////////
        /////<summary>wade adds
        ///// Show Selver And Gold UI Coin
        ///// </summary>
        //public void MyCorotine()
        //{
        //    StartCoroutine("GoldAndSelverCalc");
        //}
        //IEnumerator GoldAndSelverCalc()
        //{
        //    if (KindroomLevel == 1)
        //    {
        //        gold += (float)25 * _selectableCastles.Count / 100;
        //        SelverText.text = (25 * _selectableCastles.Count).ToString();
        //        GoldText.text = gold.ToString();
        //    }
        //    else if (KindroomLevel == 2)
        //    {
        //        gold += (float)35 * _selectableCastles.Count / 100;
        //        SelverText.text = (35 * _selectableCastles.Count).ToString();
        //        GoldText.text = gold.ToString();
        //    }

        //    yield return new WaitForSeconds(1);
        //    MyCorotine();
        //}

        /// </summary>
        ////This add 1 solider to Castle after 8 second..
        ///// </summary>
        //public void MyExraCorotine()
        //{
        //    StartCoroutine("Extra1SolidersIncris");
        //}
        //IEnumerator Extra1SolidersIncris()
        //{
        //    yield return new WaitForSeconds(8);
        //    float gold = KindroomManger.Instance.gold;
        //    foreach (SingleCastle castle in _selectableCastles)
        //    {
        //        if (gold >= 2.0f)
        //        {
        //            castle.GetComponent<CastelArmyUICount>().MyExraCorotine();
        //            gold -= 2.0f;
        //        }
        //    }

        //    MyExraCorotine();
        //}
        #endregion
    }
}