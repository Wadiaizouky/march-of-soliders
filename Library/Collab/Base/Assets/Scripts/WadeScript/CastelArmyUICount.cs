﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEditor;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

namespace MarchOfSoldiers
{
    public class CastelArmyUICount : MonoBehaviour
    {

        // Use this for initialization
        //number of Sword Solider
        public int sward { get; set; }
        public GameObject p1;
        bool sw = true;
        public Text SwardText;
        int ExtraSword;
        public GameObject ExraTextFor1Solider;

        //number of spear Solider
        int spear;
        public GameObject p2;
        bool s = false;

        //number of horse Solider
        int horse;
        public GameObject p3;
        bool h = false;

        bool refresh = false;

        public bool iswhitecastel;
        public bool isEnumyCastle;

        public GameObject particSys;
        //public Texture whitecastelTex;
        GameObject castelllNu;
        public Sprite Realcasteltex;
        public Sprite Enemycasteltex;
        public int i;

        public List<Button> lbuttoms; //UI left buttons

        public List<GameObject> AllOtherCastles;

        //what the Castle result selver and gold in seconds..
        [SerializeField]
        public int Selver { get; private set; }
        [SerializeField]
        public double Gold { get; private set; }

        //Etra 500 Soliders for ai
        public int ExtraiSoliders;
        public Text ExtraSolid;
        public bool IsExtraSoliders { get; set; }

        public int SoldierCounter;


        private static CastelArmyUICount _instance = null;


        public static CastelArmyUICount Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<CastelArmyUICount>();
                return _instance;
            }
        }

        void Start()
        {
            if (!iswhitecastel && !isEnumyCastle)
            {
                GetComponent<Animator>().SetBool("IsNeutarl_Castle", false);
                GetComponent<Animator>().SetBool("IsBlue_Castle", false);
                GetComponent<Animator>().SetBool("IsPlayer_Castle", true);
            }
            else if (iswhitecastel)
                GetComponent<Animator>().SetBool("IsNeutarl_Castle", true);
            else
                GetComponent<Animator>().SetBool("IsBlue_Castle", true);

            ExtraiSoliders = 500;
            IsExtraSoliders = false;

            Selver = 0;
            Gold = 0.0f;
            //Get the real number of Sword solider
            castelllNu = transform.GetChild(1).gameObject;

            //test if the Castle is Player Castle or no
            //if yes set the nuber of Sword solider 8
            //if no set a random value betwen 5 and 10 .
            if (!iswhitecastel)
                sward = 40;
            else
                sward = Random.Range(5, 10);



            spear = 0;
            horse = 0;
            StartCoroutine(Incris());

            ExtraSword = 1;
            //CAstleEffects (for Effects)
            ParticleSystem particleObject = particSys.GetComponent<ParticleSystem>();
            particleObject.Stop();


            SoldierCounter = 1;
        }

        // Update is called once per frame
        void Update()
        {
            //UI Extra Soliders for ai
            ExtraSolid.text = ExtraiSoliders.ToString();


            //UI Sward above the Castle
            SwardText.text = sward.ToString();

            castelllNu.name = sward.ToString();

            //when click i to output soliders from castle u should decric the number of sloliders in casle ..
            if (Input.GetMouseButtonUp(1) && DrawBoxPosition.Instance.Drawablee && DrawBoxPosition.Instance.TargetCastle != GetComponent<SingleCastle>())
            {
                if (transform.GetChild(0).gameObject.active)
                {
                    if (SelectionManager.Instance.mouseObjectHit == null
                        || SelectionManager.Instance.mouseObjectHit.tag != "Map Border")
                    {
                        int count = int.Parse(transform.GetChild(1).gameObject.name);
                        int groupsize = Selectedpercentage();

                        if (count > groupsize)
                            sward = sward - groupsize;
                        else
                            sward = 0;
                    }
                }
            }


            //this blok is not used now ........
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay((Vector2)Input.mousePosition), out hit))
                {
                    if (hit.collider.gameObject.tag == "sw")
                    {
                        sw = true;
                        s = false;
                        h = false;

                    }
                    if (hit.collider.gameObject.tag == "s")
                    {
                        sw = false;
                        s = true;
                        h = false;

                    }
                    if (hit.collider.gameObject.tag == "h")
                    {
                        sw = false;
                        s = false;
                        h = true;

                    }
                    if (hit.collider.gameObject.tag == "refresh")
                    {
                        refresh = !refresh;
                    }
                }
            }



        }

        //when Solider collider hit castle collider
        public void OnCollisionWithUnit(GameObject other)
        {
            SingleUnit solider = other.gameObject.GetComponent<SingleUnit>();
            ////Get the castle pressed by player
            if (other.gameObject.tag == "Player" && solider != null)
            {
                GroupOfUnits curr = solider.Group;
                SingleCastle currentcastlepressed = curr.targetCastle;


                int size = solider.VirtualValue;
                ////check if the curent castle is castle pressed by player
                if (currentcastlepressed != null && currentcastlepressed.gameObject == this.gameObject)
                {

                    //CastleEffects ( use Effect )
                    //StartCoroutine(GreatCastleEffects());

                    //you have 2 choise
                    //1. if Castle hited is Player Castle 
                    //2. if Castle hited is Army Castle
                    //Player Castle
                    if (iswhitecastel || isEnumyCastle)
                    {
                        if (IsExtraSoliders && ExtraiSoliders > 0)
                        {
                            ExtraiSoliders -= size;
                            if (ExtraiSoliders < 0)
                                ExtraiSoliders = 0;
                        }
                        else
                            sward -= size;
                    }
                    //else if (other.gameObject.tag == "Spear")
                    //    spear++;
                    //else
                    //    horse++;


                    //Army Castle
                    else
                    {
                        sward += size;
                    }

                    //Destroy Solider

                    /* ************************************************** */
                    Destroy(other.transform.gameObject);
                    /* ************************************************** */

                    if (sward <= -1)
                        ChangeCastleSprite("Player");

                }

            }
            else if (other.gameObject.tag == "Enemy" && solider != null)
            {
                GroupOfUnits curr = solider.Group;
                SingleCastle currentcastlepressed = curr.targetCastle;


                int size = solider.VirtualValue;
                ////check if the curent castle is castle pressed by player
                if (currentcastlepressed != null && currentcastlepressed.gameObject == this.gameObject)
                {

                    //CastleEffects ( use Effect )
                    //StartCoroutine(GreatCastleEffects());

                    //you have 2 choise
                    //1. if Castle hited is Player Castle 
                    //2. if Castle hited is Army Castle
                    //Player Castle
                    if (iswhitecastel || !isEnumyCastle)
                    {
                        sward -= size;
                    }
                    //else if (other.gameObject.tag == "Spear")
                    //    spear++;
                    //else
                    //    horse++;


                    //Army Castle
                    else
                    {
                        sward += size;
                    }

                    //Destroy Solider

                    /* ************************************************** */
                    Destroy(other.transform.gameObject);
                    /* ************************************************** */
                    if (sward <= -1)
                    {
                        ChangeCastleSprite("Enemy");
                        CastlesSelection.Instance.DeselectSingleCastle(GetComponent<SingleCastle>());

                    }
                }
            }
        }
        public void MyCorotine()
        {
            StartCoroutine(Incris());
        }

        //this for incris Solider in Castle after 1 second and repeat that ..
        IEnumerator Incris()
        {
            //GameObject chil = transform.GetChild(3).gameObject;
            if (!iswhitecastel)
            {
                if (!refresh)
                {
                    //if (sw)
                    //    sward+= SoldierCounter;
                    //else if (s)
                    //    spear++;
                    //else if (h)
                    //    horse++;
                    if (sw && isEnumyCastle)
                        sward += SoldierCounter;
                    else
                        sward++;
                }
            }

            //Player Castle
            if (!iswhitecastel & !isEnumyCastle)
            {
                //result is 25 selver in 1 second
                Selver += 25;
                Gold = (double)Selver / 100;
            }
            //Number.name = Solidercoun.ToString();
            yield return new WaitForSecondsRealtime(1);
            MyCorotine();

        }

        //CastleEffects method
        IEnumerator GreatCastleEffects()
        {

            ParticleSystem particleObject = particSys.GetComponent<ParticleSystem>();
            particleObject.Play();
            yield return new WaitForSecondsRealtime(3);
            particleObject.Stop();

        }


        //This add 1 solider to Castle after 8 second..
        public void MyExraCorotine()
        {
            GameObject Extex = Instantiate(ExraTextFor1Solider, transform.GetChild(3).position, Quaternion.identity);
            sward += ExtraSword;
            //StartCoroutine("Extra1SolidersIncris");
        }

        /// <summary>wade adds
        /// return the percentage selected by player in UI
        /// </summary>
        public int Selectedpercentage()
        {
            int descount = 200;
            Color32 colRed = new Color32(255, 90, 90, 255);
            for (int i = 0; i < 4; i++)
            {
                Image im = lbuttoms[i].GetComponent<Image>();
                if (im.color == colRed)
                {
                    switch (i)
                    {
                        case 0: descount = 50; break;
                        case 1: descount = 100; break;
                        case 2: descount = 150; break;
                        case 3: descount = 200; break;
                    }
                }
            }
            return descount;
        }

        ///// <summary>wade adds
        ///// NearlyCastle to the main Castle
        ///// </summary>
        //public void creatgrouptoNearlyCastle()
        //{
        //    GameObject NearCastle = NearlyCastle();
        //    //CreateGroupParameters group = new CreateGroupParameters();
        //    //group.GroupOwner = GroupOwner.Enemy;
        //    //group.GroupType = GroupType.Sword;
        //    //group.Position = NearCastle.transform.position;
        //    //group.SourceCastle = GetComponent<SingleCastle>();
        //    //group.TargetCastle = NearCastle.GetComponent<SingleCastle>();
        //    //group.GroupSize = sward;
        //    //sward = sward - 50;
        //    //List<GroupOfUnits> groupOfUnits = new List<GroupOfUnits>();
        //    //GroupOfUnits g = SelectionManager.Instance.CreateGroup(group);
        //    //groupOfUnits.Add(g);
        //    //SelectionManager.Instance.MoveMultipleGroups(groupOfUnits, group.TargetCastle.transform.position, false, group.TargetCastle);
        //    List<Vector2> CirclePosition = new List<Vector2>();
        //    for (int j = 0; j < 20; j++)
        //        CirclePosition.Add(NearCastle.transform.position);
        //    GroupOfUnits g;
        //    g = SelectionManager.Instance.CreateGroup(sward, CirclePosition, GroupType.Sword, GroupOwner.Enemy, NearCastle.transform.position, GetComponent<SingleCastle>(), NearCastle.GetComponent<SingleCastle>());

        //    sward = sward - 50;
        //}
        ///// <summary>wade adds
        ///// Calculate the near castle
        ///// </summary>
        //public GameObject NearlyCastle()
        //{
        //    GameObject castle = AllOtherCastles[0];
        //    float dis = Vector3.Distance(transform.position, AllOtherCastles[0].transform.position);
        //    for (i = 1; i < AllOtherCastles.Count; i++)
        //    {
        //        if (!AllOtherCastles[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
        //        {
        //            float distance = Vector3.Distance(transform.position, AllOtherCastles[i].transform.position);
        //            if (dis > distance)
        //            {
        //                dis = distance;
        //                castle = AllOtherCastles[i];
        //            }
        //        }

        //    }
        //    return castle;
        //}

        public void ChangeCastleSprite(string tag)
        {
            if (tag == "Player")
            {
                GetComponent<Animator>().SetBool("IsPlayer_Castle", true);
                GetComponent<Animator>().SetBool("IsBlue_Castle", false);
                if (iswhitecastel)
                    GetComponent<Animator>().SetBool("IsNeutarl_Castle", false);

                transform.gameObject.tag = "Player";
                transform.gameObject.layer = 9;
                sward *= -1;
                iswhitecastel = false;
                isEnumyCastle = false;
                //SpriteRenderer sp = GetComponent<SpriteRenderer>();
                //sp.sprite = Realcasteltex;

                SelectionManager.Instance.AddSelectable(gameObject.GetComponent<SingleCastle>(), SelectableType.Castle);

            }
            else
            {
                GetComponent<Animator>().SetBool("IsPlayer_Castle", false);
                GetComponent<Animator>().SetBool("IsBlue_Castle", true);
                //GetComponent<Animator>().SetBool("Level0", false);
                if (iswhitecastel)
                    GetComponent<Animator>().SetBool("IsNeutarl_Castle", false);


                transform.gameObject.tag = "Enemy";
                transform.gameObject.layer = 9;
                sward *= -1;
                iswhitecastel = false;
                isEnumyCastle = true;
                //SpriteRenderer sp = GetComponent<SpriteRenderer>();
                //sp.sprite = Enemycasteltex;

                SelectionManager.Instance.RemoveSelectable(gameObject.GetComponent<SingleCastle>(), SelectableType.Castle);

            }
        }


    }
}