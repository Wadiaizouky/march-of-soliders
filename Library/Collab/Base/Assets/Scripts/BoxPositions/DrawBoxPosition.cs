﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;
using Photon.Pun;

public class DrawBoxPosition : MonoBehaviourPunCallbacks
{
    public GameObject BoxPos;

    public Dictionary<int, GameObject> BoxPos1ByPlayers = new Dictionary<int, GameObject>();
    public Dictionary<int, List<GameObject>> AllBoxPositionsByPlayers = new Dictionary<int, List<GameObject>>();

    public Dictionary<int, bool> DrawableeByPlayers = new Dictionary<int, bool>();
    public Dictionary<int, bool> IsDrawingByPlayers = new Dictionary<int, bool>();


    public Dictionary<int, SingleCastle> TargetCastleByPlayers = new Dictionary<int, SingleCastle>();

    private Vector3 startMouseClickPos;

    private Vector3 prevMousePosition;
    private bool validCirclesChecked = false;

    private static DrawBoxPosition _instance = null;
    public static DrawBoxPosition Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<DrawBoxPosition>();
            return _instance;
        }
    }

    private void Awake()
    {
        prevMousePosition = Input.mousePosition;
    }


    void Start()
    {
        DrawableeByPlayers.Add(1, false);
        IsDrawingByPlayers.Add(1, false);

        DrawableeByPlayers.Add(2, false);
        IsDrawingByPlayers.Add(2, false);

        BoxPos1ByPlayers.Add(1, null);
        BoxPos1ByPlayers.Add(2, null);

        AllBoxPositionsByPlayers.Add(1, new List<GameObject>());
        AllBoxPositionsByPlayers.Add(2, new List<GameObject>());

        TargetCastleByPlayers.Add(1, null);
        TargetCastleByPlayers.Add(2, null);

        //Test
        DrawableeByPlayers.Add(-1, false);
        IsDrawingByPlayers.Add(-1, false);
        BoxPos1ByPlayers.Add(-1, null);
        AllBoxPositionsByPlayers.Add(-1, new List<GameObject>());
        TargetCastleByPlayers.Add(-1, null);
    }

    void Update()
    {
        if (PasueController.Instance.IsPasued)
        {
            return;
        }
        if (Input.GetKey(KeyCode.P))
        {
            TestRecord();
            return;
        }
        List<GroupOfUnits> SelectedSoliders = SelectionManager.Instance.GetSelectedSolidersForLocalPlayer();
        List<SingleCastle> SelectedCastles = SelectionManager.Instance.GetSelectedCastlesForLocalPlayer();

        if ((Input.GetKey(KeyCode.Mouse1) & !Input.GetKey(KeyCode.Mouse0)) | Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                startMouseClickPos = Input.mousePosition;
            }
            if (SelectedCastles.Count > 0 && BoxPos1ByPlayers[PlayersManager.Instance.GetLocalPlayerNumber()] == null)
            {
                if (!PhotonNetwork.IsConnected)
                {
                    DrawBoxPositions(PlayersManager.Instance.GetLocalPlayerNumber(), SelectedCastles.Count, Camera.main.ScreenToWorldPoint(Input.mousePosition));
                }
                else
                {
                    photonView.RPC("DrawBoxPositions", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber(), SelectedCastles.Count, Camera.main.ScreenToWorldPoint(Input.mousePosition));
                }
            }
            else if (BoxPos1ByPlayers[PlayersManager.Instance.GetLocalPlayerNumber()] == null)
            {
                if (!PhotonNetwork.IsConnected)
                {
                    DrawBoxPositions(PlayersManager.Instance.GetLocalPlayerNumber(), SelectedSoliders.Count, Camera.main.ScreenToWorldPoint(Input.mousePosition));
                }
                else
                {
                    photonView.RPC("DrawBoxPositions", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber(), SelectedSoliders.Count, Camera.main.ScreenToWorldPoint(Input.mousePosition));
                }
            }
        }
        else if (BoxPos1ByPlayers[PlayersManager.Instance.GetLocalPlayerNumber()] != null)
        {
            if (!PhotonNetwork.IsConnected)
            {
                Clear(PlayersManager.Instance.GetLocalPlayerNumber());
            }
            else
            {
                photonView.RPC("Clear", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber());
            }
        }

        if (prevMousePosition != Input.mousePosition || Input.GetKeyDown(KeyCode.Mouse1) || validCirclesChecked)
        {
            if (!PhotonNetwork.IsConnected)
            {
                RotateWithMouse(PlayersManager.Instance.GetLocalPlayerNumber(), Input.mousePosition - startMouseClickPos);
                CheckValidSolidersSorting(PlayersManager.Instance.GetLocalPlayerNumber());
            }
            else
            {
                if (BoxPos1ByPlayers[PlayersManager.Instance.GetLocalPlayerNumber()] != null)
                {
                    photonView.RPC("RotateWithMouse", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber(), Input.mousePosition - startMouseClickPos);
                }
                if (BoxPos1ByPlayers[PlayersManager.Instance.GetLocalPlayerNumber()] != null
                    || DrawableeByPlayers[PlayersManager.Instance.GetLocalPlayerNumber()] != false)
                {
                    photonView.RPC("CheckValidSolidersSorting", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber());
                }
            }

            prevMousePosition = Input.mousePosition;

            if (validCirclesChecked == true)
                validCirclesChecked = false;
            else if (Input.GetKeyDown(KeyCode.Mouse1))
                validCirclesChecked = true;
        }


        DrawLine(PlayersManager.Instance.GetLocalPlayerNumber());
    }

    private void TestRecord()
    {
        List<GroupOfUnits> SelectedSoliders = UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(-1);
        List<SingleCastle> SelectedCastles = CastlesSelection.Instance.GetSelectedCastlesByPlayerNum(-1);

        if (Input.GetKey(KeyCode.Mouse1) & !Input.GetKey(KeyCode.Mouse0) | Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                startMouseClickPos = Input.mousePosition;
            }
            if (SelectedCastles.Count > 0 && BoxPos1ByPlayers[-1] == null)
            {
                DrawBoxPositions(-1, SelectedCastles.Count, Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }
            else if (BoxPos1ByPlayers[-1] == null)
            {
                DrawBoxPositions(-1, SelectedSoliders.Count, Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }
        }
        else if (BoxPos1ByPlayers[-1] != null)
        {
            Clear(-1);
        }

        if (prevMousePosition != Input.mousePosition || Input.GetKeyDown(KeyCode.Mouse1) || validCirclesChecked)
        {
            RotateWithMouse(-1, Input.mousePosition - startMouseClickPos);
            CheckValidSolidersSorting(-1);

            prevMousePosition = Input.mousePosition;

            if (validCirclesChecked == true)
                validCirclesChecked = false;
            else if (Input.GetKeyDown(KeyCode.Mouse1))
                validCirclesChecked = true;
        }


        DrawLine(-1);
    }

    [PunRPC]
    public void Clear(int playerNubmer)
    {
        Destroy(BoxPos1ByPlayers[playerNubmer]);
        AllBoxPositionsByPlayers[playerNubmer].Clear();
        IsDrawingByPlayers[playerNubmer] = false;
        if (PlayersManager.Instance.GetLocalPlayerNumber() == playerNubmer)
            validCirclesChecked = false;
    }

    [PunRPC]
    public void DrawBoxPositions(int playerNumber, int CastleNumber, Vector3 mousePositionWordSpace)
    {
        CastleNumber = Mathf.Min(12, CastleNumber);

        if (BoxPos1ByPlayers[playerNumber] == null)
        {
            BoxPos1ByPlayers[playerNumber] = new GameObject("BoxPos1");

            if (PhotonNetwork.IsConnected)
            {
                if (PlayersManager.Instance.GetLocalPlayerNumber() != playerNumber)
                {
                    BoxPos1ByPlayers[playerNumber].SetActive(false);
                }
            }

            // check if player choose to move toward a castle
            RaycastHit2D hit = Physics2D.Raycast(
                origin: mousePositionWordSpace,
                direction: Vector2.zero,
                distance: 100);

            // if destination is CASTLE: don't add offset to destination, just move to Center of castle
            if (hit && hit.collider.gameObject.transform.childCount > 4)
            {
                IsDrawingByPlayers[playerNumber] = false;
                for (int j = 1; j <= CastleNumber; j++)
                {
                    AllBoxPositionsByPlayers[playerNumber].Add(hit.collider.gameObject);
                }
                GameObject hitCastle = hit.collider.gameObject;
                TargetCastleByPlayers[playerNumber] = hitCastle.GetComponent<SingleCastle>();
            }
            else
            {
                IsDrawingByPlayers[playerNumber] = true;
                Vector3 CurrentBoxPosition = new Vector3(mousePositionWordSpace.x, mousePositionWordSpace.y, 0f);
                BoxPos1ByPlayers[playerNumber].transform.position = CurrentBoxPosition;

                GameObject Box;
                int i = CastleNumber;

                int XNum = 0;
                while (i > 0)
                {
                    Box = Instantiate(BoxPos, CurrentBoxPosition, Quaternion.identity);
                    AllBoxPositionsByPlayers[playerNumber].Add(Box);

                    if (XNum == 2 | XNum == 5 | XNum == 8)
                        CurrentBoxPosition = new Vector3(mousePositionWordSpace.x, CurrentBoxPosition.y - 4.4f, 0f);

                    else
                    {
                        CurrentBoxPosition = new Vector3(CurrentBoxPosition.x + 6.4f, CurrentBoxPosition.y, 0f);
                    }
                    i--;
                    XNum++;
                }
                if (XNum > 1)
                    BoxPos1ByPlayers[playerNumber].transform.position = AllBoxPositionsByPlayers[playerNumber][1].transform.position; // AllBoxPositions[1].transform.position;


                foreach (GameObject g in AllBoxPositionsByPlayers[playerNumber])
                    g.transform.SetParent(BoxPos1ByPlayers[playerNumber].transform);

                TargetCastleByPlayers[playerNumber] = null;
            }
        }
    }

    [PunRPC]
    public void RotateWithMouse(int playerNumber, Vector3 dir)
    {
        if (BoxPos1ByPlayers[playerNumber] != null)
        {
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            BoxPos1ByPlayers[playerNumber].transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    [PunRPC]
    public void CheckValidSolidersSorting(int playerNumber)
    {
        if (BoxPos1ByPlayers[playerNumber] != null)
        {
            DrawableeByPlayers[playerNumber] = true;
            if (IsDrawingByPlayers[playerNumber])
            {
                foreach (GameObject g in AllBoxPositionsByPlayers[playerNumber])
                {
                    if (g.GetComponent<BoxPosition>().Drawable() == false)
                    {
                        DrawableeByPlayers[playerNumber] = false;
                    }
                }
            }
        }
        else
        {
            DrawableeByPlayers[playerNumber] = false;
        }
    }

    public void DrawLine(int playerNubmer)
    {
        if (CastlesSelection.Instance.GetAllSelectedByPlayerNum(playerNubmer).Count > 0 && AllBoxPositionsByPlayers[playerNubmer].Count > 0)
        {
            for (int i = 0; i < AllBoxPositionsByPlayers[playerNubmer].Count; i++)
            {
                Debug.DrawLine(CastlesSelection.Instance.GetAllSelectedByPlayerNum(playerNubmer)[i].transform.position, AllBoxPositionsByPlayers[playerNubmer][i].transform.position, Color.green);
            }
        }
    }
}
