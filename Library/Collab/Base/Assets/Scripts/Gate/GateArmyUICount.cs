﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;
using UnityEngine.UI;

public class GateArmyUICount : MonoBehaviour
{



    //number of Sword Solider
    public int sward { get; set; }
    public GameObject p1;
    bool sw = true;
    public Text SwardText;
    int ExtraSword;

    public bool IsPlayerGate;
    public bool IsEnemyGate;

    public GameObject particSys;
    //public Texture whitecastelTex;
    GameObject castelllNu;
    public Sprite PlayerGatetex;
    public Sprite EnemyGatetex;
    public int i;

    public List<GameObject> AllOtherCastles;



    private static GateArmyUICount _instance = null;


    public static GateArmyUICount Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GateArmyUICount>();
            return _instance;
        }
    }

    void Start()
    {

        //Get the real number of Sword solider
        castelllNu = transform.GetChild(1).gameObject;


        sward = 75;
    



       
    }

    // Update is called once per frame
    void Update()
    {
     


        //UI Sward above the Castle
        SwardText.text = sward.ToString();

        castelllNu.name = sward.ToString();

        //when click i to output soliders from castle u should decric the number of sloliders in casle ..
        if (Input.GetMouseButtonUp(1) && DrawBoxPosition.Instance.Drawablee && DrawBoxPosition.Instance.TargetCastle != GetComponent<SingleCastle>())
        {
            if (transform.GetChild(0).gameObject.active)
            {
                int count = int.Parse(transform.GetChild(1).gameObject.name);
                int groupsize = Selectedpercentage();

                if (count > groupsize)
                    sward = sward - groupsize;
                else
                    sward = 0;
            }
        }

    }

    //when Solider collider hit castle collider
    public void OnCollisionWithUnit(GameObject other)
    {
        SingleUnit solider = other.gameObject.GetComponent<SingleUnit>();
        ////Get the castle pressed by player
        if (other.gameObject.tag == "Player" && solider != null)
        {
            GroupOfUnits curr = solider.Group;
            SingleCastle currentcastlepressed = curr.targetCastle;


            int size = solider.VirtualValue;
            ////check if the curent castle is castle pressed by player
            if (currentcastlepressed != null && currentcastlepressed.gameObject == this.gameObject)
            {

                //CastleEffects ( use Effect )
                //StartCoroutine(GreatCastleEffects());

                //you have 2 choise
                //1. if Castle hited is Player Castle 
                //2. if Castle hited is Army Castle
                //Player Castle
                if (IsEnemyGate || (!IsEnemyGate && !IsPlayerGate))
                        sward -= size;

                //Army Castle
                else
                {
                    sward += size;
                }

                //Destroy Solider

                /* ************************************************** */
                Destroy(other.transform.gameObject);
                /* ************************************************** */

                if (sward <= -1)
                    ChangeCastleSprite("Player");
            }

        }
        else if (other.gameObject.tag == "Enemy" && solider != null)
        {
            GroupOfUnits curr = solider.Group;
            SingleCastle currentcastlepressed = curr.targetCastle;


            int size = solider.VirtualValue;
            ////check if the curent castle is castle pressed by player
            if (currentcastlepressed != null && currentcastlepressed.gameObject == this.gameObject)
            {

                //CastleEffects ( use Effect )
                //StartCoroutine(GreatCastleEffects());

                //you have 2 choise
                //1. if Castle hited is Player Castle 
                //2. if Castle hited is Army Castle
                //Player Castle
                if (IsPlayerGate)
                    sward -= size;
                //Army Castle
                else
                    sward += size;


                //Destroy Solider

                /* ************************************************** */
                Destroy(other.transform.gameObject);
                /* ************************************************** */
                if (sward <= -1)
                {
                    ChangeCastleSprite("Enemy");
                    GatesSelection.Instance.DeselectSingleGate(GetComponent<SingleCastle>());
                }
            }
        }
    }





    /// <summary>wade adds
    /// return the percentage selected by player in UI
    /// </summary>
    public int Selectedpercentage()
    {
        return NumOfUnits_UI.Instance.GetNumOfUnits();
    }


    public void ChangeCastleSprite(string tag)
    {
        if (tag == "Player")
        {
            transform.gameObject.tag = "Player";
            transform.gameObject.layer = 9;
            sward *= -1;
            IsEnemyGate = false;
            IsPlayerGate = true;
            SpriteRenderer sp = GetComponent<SpriteRenderer>();
            sp.sprite = PlayerGatetex;

            SelectionManager.Instance.AddSelectable(gameObject.GetComponent<SingleCastle>(), SelectableType.Castle);
        }
        else
        {
            transform.gameObject.tag = "Enemy";
            transform.gameObject.layer = 9;
            sward *= -1;
            IsEnemyGate = true;
            IsPlayerGate = false;
            SpriteRenderer sp = GetComponent<SpriteRenderer>();
            sp.sprite = EnemyGatetex;

            SelectionManager.Instance.RemoveSelectable(gameObject.GetComponent<SingleCastle>(), SelectableType.Castle);
        }
    }


}



