﻿using Photon.Pun;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class SingleCastle : Selectable
    {
        [Tooltip("Used to offset castle center when group move toward the castle.")]
        [SerializeField] private float _castleOffset = 2.5f;

        [Tooltip("Gameobject represent castle door position")]
        [SerializeField] private Transform _Door = null;

        public float CastleOffset => _castleOffset;
        public Transform Door => _Door;

        public bool isGate = false;


        private void Awake() //temp
        {
            if (PhotonNetwork.IsConnected)
            {
                OwnerType = OwnerType.Player;
            }

            if (!isGate)
            {
                CastelArmyUICount castelArmyUI = GetComponent<CastelArmyUICount>();

                if (castelArmyUI.iswhitecastel)
                {
                    OwnerType = OwnerType.None;
                }
                if (!castelArmyUI.iswhitecastel && !castelArmyUI.isEnumyCastle) // Player
                {
                    OwnerType = OwnerType.Player;
                    if (!PhotonNetwork.IsConnected)
                    {
                        PlayerOwnerNumber = 1;
                    }
                }
                if (!castelArmyUI.iswhitecastel && castelArmyUI.isEnumyCastle) //Enemy
                {
                    OwnerType = OwnerType.Enemy;
                    PlayerOwnerNumber = -1;
                }
            }
            else
            {
                GateArmyUICount gateArmyUI = GetComponent<GateArmyUICount>();

                if (!gateArmyUI.IsEnemyGate) // Player
                {
                    OwnerType = OwnerType.Player;
                    if (!PhotonNetwork.IsConnected)
                    {
                        PlayerOwnerNumber = 1;
                    }
                }
                if (gateArmyUI.IsPlayerGate) //Enemy
                {
                    OwnerType = OwnerType.Enemy;
                    PlayerOwnerNumber = -1;
                }
            }

        }
    }
}