﻿using UnityEngine;
using System.Collections.Generic;

public enum GroupType { Sword, Spear, Horse }
public enum _offsetType {None, Center, Commander }
public class GroupOfUnits : MonoBehaviour
{
    #region Fields

    [SerializeField] private GroupType _groupType;
    [SerializeField] private GameObject _unitPrefab;
    [SerializeField] private GameObject _groupSizeUI;

    [SerializeField, Tooltip("do you want units to move around it's commander or Center point? or none")]
    private _offsetType _offsetType;

    [SerializeField, Tooltip("Used to separate units when they instantiated.")]
    private Vector2 _unitInstantiateOffset = new Vector2(0.2f, 0.23f);

    [SerializeField, Tooltip("how many units can be in each raw when group instantiated")]
    private int _unitGroupBoundaryRows = 4;

    [SerializeField, Tooltip("how many units can be in each Col when group instantiated")]
    private int _unitGroupBoundaryCols = 5;

    private readonly List<SingleUnit> _units = new List<SingleUnit>();

    #endregion

    #region Properties

    /// <summary>
    /// Point in the middle of units inside the group 
    /// </summary>
    public Vector3 Center { get; set; }

    /// <summary>
    /// is this group currently selected?
    /// </summary>
    public bool IsSelected { get; set; }

    /// <summary>
    /// unit that is the commander for this group
    /// </summary>
    public SingleUnit Commander { get; set; }

    /// <summary>
    /// Count of additional units in the group that is not appear as units in map.
    /// </summary>
    public int BonusUnits { get; set; }

    /// <summary>
    /// array hold position for every unit, used to give every unit a unique position when the group created.
    /// </summary>
    public Vector2[,] UnitInstantiatePositions { get; private set; }

    /// <summary>
    /// unit to instantiate when this group get created
    /// </summary>
    public GameObject UnitPrefab => _unitPrefab;

    /// <summary>
    /// UI Prefab represent number of unit text.
    /// </summary>
    public GameObject GroupSizeUI => _groupSizeUI;

    public GroupType GroupType => _groupType;

    /// <summary>
    /// list of every VISIBLE unit inside the group.
    /// </summary>
    public List<SingleUnit> Units => _units;

    public Vector3 Goal { get; set; }

    #endregion

    private void Awake()
    {
        UnitsPositionInitializer();
    }

    /// <summary>
    /// initialize unit position array and calculate each unit position inside it.
    /// </summary>
    private void UnitsPositionInitializer()
    {
        // initialize positions array.
        UnitInstantiatePositions = new Vector2[_unitGroupBoundaryRows, _unitGroupBoundaryCols];

        // calculate the instantiate position for every unit in the group.
        for (int i = 0; i < UnitInstantiatePositions.GetLength(0); i++)
        {
            for (int j = 0; j < UnitInstantiatePositions.GetLength(1); j++)
            {
                UnitInstantiatePositions[i, j] = new Vector2(j * _unitInstantiateOffset.x, -i * _unitInstantiateOffset.y);
            }
        }
    }

    /// <summary>
    /// Add unit to this group, and mark it's group as THIS.
    /// </summary>
    /// <param name="unit"></param>
    public void AddUnit(SingleUnit unit)
    {
        if (!unit)
        {
            Debug.LogError("make sure to pass valid unit to add");
            return;
        }

        _units.Add(unit);
        unit.Group = this;
    }

    /// <summary>
    /// Remove Unit from Group.
    /// </summary>
    /// <param name="unit"></param>
    public void RemoveUnit(SingleUnit unit)
    {
        if(!unit)
        {
            Debug.LogError("make sure to pass valid unit to remove");
            return;
        }

        _units.Remove(unit);
        unit.Group = null;
    }

    /// <summary>
    /// return whole size of unit's inside this group.
    /// </summary>
    /// <returns></returns>
    public int GetSize() => _units.Count + BonusUnits;

    /// <summary>
    /// Calculate Center Point in the group.
    /// </summary>
    public void CalculateGroupCenter()
    {
        foreach (var unit in _units)
            Center += unit.transform.position;

        Center /= _units.Count;
    }

    /// <summary>
    /// Calculate which unit will be the commander for the group.
    /// </summary>
    public void SetGroupCommander()
    {
        // if count more than 5, commander will be the middle one in the first row
        if (_units.Count > 5)
            Commander = _units[2];

        // otherwise, there is only one row, pick the middle unit.
        else
            Commander = _units[_units.Count / 2];
    }

    /// <summary>
    /// Calculate offset for unit.
    /// </summary>
    /// <param name="movementOffset"></param>
    public void SetUnitGoal()
    {
        // iterate over every unit in the group
        foreach (var unit in _units)
        {
            if (_offsetType == _offsetType.Center)
                unit.Goal = Center - unit.transform.position;

            else if(_offsetType == _offsetType.Commander)
                unit.Goal = Commander.transform.position - unit.transform.position;
        }
    }

    /// <summary>
    /// Called when group get selected.
    /// </summary>
    public void OnSelected()
    {
        // mark the group as selected
        IsSelected = true;

        // select every unit in it
        foreach (var unit in _units)
        {
            if (unit)
                unit.OnSelected();

            else
                Debug.LogError("make sure group has a valid reference to every unit inside it");
        }
    }

    /// <summary>
    /// Called When group get deselected.
    /// </summary>
    public void OnDeselected()
    {
        // deselect the group
        IsSelected = false;

        // deselect every unit in it
        foreach (var unit in _units)
        {
            if (unit)
                unit.OnDeselected();

            else
                Debug.LogError("make sure group has a valid reference to every unit inside it");
        }
    }

    /// <summary>
    /// Move every unit to given point and offset the destination if necessary.
    /// </summary>
    /// <param name="destination"></param>
    /// <param name="offset"></param>
    public void Move(Vector3 destination, bool offset)
    {
        foreach (var unit in Units)
            unit.Move(offset ? destination + Goal : destination, offset);
    }

}
