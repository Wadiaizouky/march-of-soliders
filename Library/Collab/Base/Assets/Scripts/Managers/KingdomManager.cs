﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MarchOfSoldiers;
using System;

namespace MarchOfSoldiers
{
    public class KingdomManager : Singleton<KingdomManager>
    {
        public int maxUpgradeNum = 3;
        //[HideInInspector] public int kingdomLevel;
        [HideInInspector] public int AllPlayerFarms;
        [HideInInspector] public int AllFarmssilver;
        [HideInInspector] public int AllFarmLost;


        private float updateGoldAndSilverEvery = 1.0f;

        public delegate void onKingdomUpgrade();
        public event onKingdomUpgrade OnKingdomUpgrade;

        //Map Level
        public int MapLevel;

        void Start()
        {
            PlayersManager.Instance.LocalPlayer.kingdomLevel = 0;
            
            PlayersManager.Instance.LocalPlayer.silver = 0;
            if (MapLevel == 4 | MapLevel == 5 | MapLevel == 6)
                PlayersManager.Instance.LocalPlayer.gold = 50.0f;
            else
                PlayersManager.Instance.LocalPlayer.gold = 0.0f;

            StartCoroutine("UpdateGoldAndSilver");
        }

        private IEnumerator UpdateGoldAndSilver()
        {

            while (true)
            {
                if (!PasueController.Instance.IsPasued)
                {
                    List<SingleCastle> _selectableCastles = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());

                    float castleProduce = 0.0f;
                    switch (PlayersManager.Instance.LocalPlayer.kingdomLevel)
                    {
                        case 0:
                            castleProduce = 25.0f;
                            break;
                        case 1:
                            castleProduce = 35.0f;
                            break;
                        case 2:
                            castleProduce = 50.0f;
                            break;
                        case 3:
                            castleProduce = 75.0f;
                            break;
                    }
                    AllFarmssilver = GetFarmsCount() * 15;
                    PlayersManager.Instance.LocalPlayer.silver = (int)castleProduce * _selectableCastles.Count + AllFarmssilver;
                    PlayersManager.Instance.LocalPlayer.gold += ((castleProduce * (_selectableCastles.Count * 1.0f)) + (15 * (GetFarmsCount() * 1.0f))) / 100;
                }
                yield return new WaitForSeconds(updateGoldAndSilverEvery);
            }
        }

        public bool CanUpgrade(int level)
        {
            if (PlayersManager.Instance.LocalPlayer.kingdomLevel >= level) return false;
            if (level - PlayersManager.Instance.LocalPlayer.kingdomLevel > 1) return false;

            int price = GetUpgradePricing(level);
            return PlayersManager.Instance.LocalPlayer.gold >= price;
        }

        public void UpgradeKingdom()
        {

            if (!CanUpgrade(PlayersManager.Instance.LocalPlayer.kingdomLevel + 1)) return;
            if (PlayersManager.Instance.LocalPlayer.kingdomLevel == 3) return;

            PlayersManager.Instance.LocalPlayer.kingdomLevel++;

            PlayersManager.Instance.LocalPlayer.gold -= GetUpgradePricing(PlayersManager.Instance.LocalPlayer.kingdomLevel);

            if (OnKingdomUpgrade != null)
                OnKingdomUpgrade();


            ChangeCastlesSpriteLevel();

            ChangeFarmCountWithKingdomLevel();
        }

        public int GetUpgradePricing(int level)
        {
            int price = 0;
            switch (level)
            {
                case 1:
                    price = 150;
                    break;
                case 2:
                    price = 300;
                    break;
                case 3:
                    price = 750;
                    break;
            }

            return price;
        }

        //wade add
        //Change All Sprite Player Castle to new Level 
        public void ChangeCastlesSpriteLevel()
        {
            switch (PlayersManager.Instance.LocalPlayer.kingdomLevel)
            {
                case 1:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {
                        if (Castle.GetComponent<Animator>() == null) return;
                        Castle.GetComponent<Animator>().SetBool("Level0", false);
                        Castle.GetComponent<Animator>().SetBool("Level1", true);
                    }
                    break;
                case 2:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {
                        if (Castle.GetComponent<Animator>() == null) return;
                        Castle.GetComponent<Animator>().SetBool("Level1", false);
                        Castle.GetComponent<Animator>().SetBool("Level2", true);
                    }
                    break;
                case 3:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {
                        if (Castle.GetComponent<Animator>() == null) return;
                        Castle.GetComponent<Animator>().SetBool("Level2", false);
                        Castle.GetComponent<Animator>().SetBool("Level3", true);
                    }
                    break;

            }
        }

        private void Update()
        {
            AllPlayerFarms = GetFarmsCount();
            AllFarmLost = GetFarmLosed();
        }


        #region Farm

        //Change Number of Farms valid to Player Build it like ..(KindomLevel1 = No.Farm =1;KindomLevel2 = No.Farm=2...)
        void ChangeFarmCountWithKingdomLevel()
        {
            switch (PlayersManager.Instance.LocalPlayer.kingdomLevel)
            {
                case 1:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {
                        if (!Castle.GetComponent<CastelArmyUICount>().isEnumyCastle && !Castle.GetComponent<CastelArmyUICount>().iswhitecastel)
                            Castle.GetComponent<CastelArmyUICount>().FarmCountvalid += 1;

                        ActivatFarmButtom();
                    }
                    break;
                case 2:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {
                        if (!Castle.GetComponent<CastelArmyUICount>().isEnumyCastle && !Castle.GetComponent<CastelArmyUICount>().iswhitecastel)
                            Castle.GetComponent<CastelArmyUICount>().FarmCountvalid += 2;
                    }
                    break;
                case 3:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {

                        if (!Castle.GetComponent<CastelArmyUICount>().isEnumyCastle && !Castle.GetComponent<CastelArmyUICount>().iswhitecastel)
                            Castle.GetComponent<CastelArmyUICount>().FarmCountvalid += 3;
                    }
                    break;
            }
        }

        //Active Farm Buttom to allow Player Build Farms
        void ActivatFarmButtom()
        {
            foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
            {
                CastelArmyUICount castelArmy = Castle.GetComponent<CastelArmyUICount>();
                if (castelArmy == null) continue;
                if (!Castle.GetComponent<CastelArmyUICount>().isEnumyCastle && !Castle.GetComponent<CastelArmyUICount>().iswhitecastel)
                    Castle.transform.GetChild(2).transform.GetChild(4).gameObject.SetActive(true);
            }
        }

        //Return All Player Farms
        public int GetFarmsCount()
        {
            int FarmCountt = 0;
            foreach (SingleCastle Castle in CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()))
            {
                CastelArmyUICount castelArmy = Castle.GetComponent<CastelArmyUICount>();
                //if (castelArmy == null) continue;
                //if (!castelArmy.isEnumyCastle && !castelArmy.iswhitecastel)
                FarmCountt += castelArmy.FarmCount;
            }
            return FarmCountt;
        }

        //Return All Player Farm Losed
        public int GetFarmLosed()
        {
            int FarmLosed = 0;
            foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
            {
                //CastelArmyUICount castelArmy = Castle.GetComponent<CastelArmyUICount>();
                //if (castelArmy == null) continue;
                FarmLosed += Castle.GetComponent<CastelArmyUICount>().FarmLosed;
            }
            return FarmLosed;

        }
        #endregion


    }
}