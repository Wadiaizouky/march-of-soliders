﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace MarchOfSoldiers
{
    public class Player : MonoBehaviourPun
    {
        public int playerNumber;
        public string playerName;
        public int playerTeam;
        public string playerColor;
        public bool isMasterClient;
        public bool isLocalPlayer;

        public float Gold { get { return gold; } set { CheckUpdateProp("gold", value); } }
        public int Silver { get { return silver; } set { CheckUpdateProp("silver", value); } }
        public int KingdomLevel { get { return kingdomLevel; } set { CheckUpdateProp("kingdomLevel", value); } }
        public int ReinforcementLevel { get { return reinforcementLevel; } set { CheckUpdateProp("reinforcementLevel", value); } }
        public int BlacksmithLevel { get { return blacksmithLevel; } set { CheckUpdateProp("blacksmithLevel", value); } }
        public int FarmsNumber { get { return farmsNumber; } set { CheckUpdateProp("farmsNumber", value); } }
        public int FarmsLost { get { return farmsLost; } set { CheckUpdateProp("farmsLost", value); } }
        public int FarmsBonusSilver { get { return farmsBonusSilver; } set { CheckUpdateProp("farmsBonusSilver", value); } }

        private float gold;
        private int silver;
        private int kingdomLevel;
        private int reinforcementLevel;
        private int blacksmithLevel;
        private int farmsNumber;
        private int farmsLost;
        private int farmsBonusSilver;


        private Dictionary<string, object> prevPropsValues = new Dictionary<string, object>();

        private void Awake()
        {
            if (!PhotonNetwork.IsConnected) return;

            if (photonView.IsMine)
            {
                photonView.RPC("AddPlayerToPlayersManager", RpcTarget.All, PhotonNetwork.LocalPlayer.ActorNumber);
            }
        }

        [PunRPC]
        public void AddPlayerToPlayersManager(int actorNumber)
        {
            PlayersManager.Instance.AddPlayer(this, actorNumber);
        }

        public void CheckUpdateProp(string propName, object value)
        {
            if (prevPropsValues.ContainsKey(propName) && value.Equals(prevPropsValues[propName])) return;

            if (PhotonNetwork.IsConnected)
            {
                photonView.RPC("UpdateProp", RpcTarget.All, propName, value);
            }
            else
            {
                UpdateProp(propName, value);
            }

            if (!prevPropsValues.ContainsKey(propName))
            {
                prevPropsValues.Add(propName, value);
            }

            prevPropsValues[propName] = value;
        }

        [PunRPC]
        public void UpdateProp(string propName, object value)
        {
            switch (propName)
            {
                case "gold":
                    gold = (float)value;
                    break;
                case "silver":
                    silver = (int)value;
                    break;
                case "kingdomLevel":
                    kingdomLevel = (int)value;
                    break;
                case "reinforcementLevel":
                    reinforcementLevel = (int)value;
                    break;
                case "blacksmithLevel":
                    blacksmithLevel = (int)value;
                    break;
                case "farmsNumber":
                    farmsNumber = (int)value;
                    break;
                case "farmsLost":
                    farmsLost = (int)value;
                    break;
                case "farmsBonusSilver":
                    farmsBonusSilver = (int)value;
                    break;
            }

        }
    }
}
