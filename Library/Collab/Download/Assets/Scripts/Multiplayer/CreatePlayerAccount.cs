﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;

public class ConnectingToMasterServer : MonoBehaviourPunCallbacks
{
    void Start()
    {
        if (!PhotonNetwork.IsConnected)
            PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("Connect Succes to Master Server ..");
        PhotonNetwork.JoinLobby();
    }
    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        Debug.Log("Lobby joined ..");
        RoomOptions roomOptions = new RoomOptions();

        PhotonNetwork.JoinOrCreateRoom("Lobby", roomOptions, TypedLobby.Default);
        PhotonNetwork.NickName = "Player Name";
        PhotonNetwork.LoadLevel("Lobby Multiplayer");
    }

    void Update() {
        if (!PhotonNetwork.IsConnected)
            PhotonNetwork.ConnectUsingSettings();
    }
}
