﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using MarchOfSoldiers;

public class UIMenuStart : MonoBehaviour
{
    // Start is called before the first frame update
   public List<Button> buttonslist;//for translate buttons

    public int Currentlevel = 1;

    public Text Panltext;


    //
    public GameObject Button;
    public GameObject Content;

    public SaveLoadLevels Saveloadlevel;
    public SaveLoadScenes SaveloadScene;



    //Start scene Buttons
    public void OnStartt() {
        SceneManager.LoadScene("New Game scene");
    }

    public void OnQuit()
    {
        Application.Quit();
    }

    public void OQuitGame()
    {
        SceneManager.LoadScene("Civil war");
    }
    //New Game scene Buttons
    public void OnStartGame()
    {
        SceneManager.LoadScene("Camping");
    }

    public void OnBack1()
    {
        SceneManager.LoadScene("Start scene");
    }

    //Camping Buttons
    public void OnCivilWar()
    {
        SceneManager.LoadScene("Civil war");
    }

    public void OnBack2()
    {
        SceneManager.LoadScene("New Game scene");
    }

    public void OnBackToLobby()
    {
        SceneManager.LoadScene("Lobby Multiplayer");
    }


    //Civil war Buttons
    public void OnBack3()
    {
        SceneManager.LoadScene("Camping");
    }


    public void OnCancel()
    {
        SceneManager.LoadScene("Start scene");
    }

    public void OnCancel2()
    {
        SceneManager.LoadScene("Login");
    }


    public void Onlevel1()
    {
        SceneManager.LoadScene("Level 1");
    }
    public void Onleve2()
    {
        SceneManager.LoadScene("Level 2");
    }
    public void Onlevel3()
    {
        SceneManager.LoadScene("Level 3");
    }
    public void Onlevel4()
    {
        SceneManager.LoadScene("Level 4");
    }
    public void Onlevel5()
    {
        SceneManager.LoadScene("Level 5");
    }
    public void Onlevel6()
    {
        SceneManager.LoadScene("Level 6 (kill The Lord -Bonja");
    }

    //
    public void Onl1()
    {
        Panltext.text = "Level 1 Civil war : it's agame a gain the AI ..Not hard game but easy..";
    }
    public void Onl2()
    {
        Panltext.text = "Level 2 Civil war : it's agame a gain the AI ";
    }
    public void Onl3()
    {
        Panltext.text = "Level 3 Civil war : it's agame a gain the AI ..Not hard game but easy..";
    }
    public void Onl4()
    {
        Panltext.text = "Level 4 Civil war : it's agame a gain the AI ..Not hard game but easy..";
    }
    public void Onl5()
    {
        Panltext.text = "Level 5 Civil war : it's agame a gain the AI .. .";
    }
    public void Onl6()
    {
        Panltext.text = "Level 6 Civil war : it's agame a gain the AI ..Hard game";
    }


    ///////////////
    public void OnContinue() {
        Currentlevel++;
        if(Currentlevel == 10)
            SceneManager.LoadScene("Civil war");

        //if(Currentlevel > 6)
        //    SceneManager.LoadScene("Civil war");
        //else
        SceneManager.LoadScene("Level "+Currentlevel);
    }

    public void OnRestart() {
        SceneManager.LoadScene("Level "+Currentlevel);
    }

    //
    public void OnResume()
    {
        PasueController.Instance.DisActivaPasuePanle();
    }

    //////////////**Multiplayer**////////////////

    public void OnCreateGame()
    {
        SceneManager.LoadScene("CreatGame");
    }

    public void OnCreate()
    {
        SceneManager.LoadScene("Ready For Playing");
    }

    ////// Multiplayer**
    public void OnMultiplayer()
    {
        SceneManager.LoadScene("Login");
    }

    public void OnCredit()
    {
        SceneManager.LoadScene("Gredit Scene");
    }

    public void OnCreateNickname()
    {
        SceneManager.LoadScene("Create Player Account");
    }
    /////
    
    //// Scrool View
    
    public void Scroll()
    {
        GameObject Copy = Instantiate(Button);
        AddToScrool(Content,Copy);
    }

    public void DistroyScroll()
    {
        for (int i = Content.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(Content.transform.GetChild(i).gameObject);
        }
    }

    public void AddToScrool(GameObject Contentt, GameObject item)
    {
        item.transform.parent = Contentt.transform;
        Contentt.transform.localPosition = Vector3.zero;
    }

    ////
    public void OnLoad()
    {
        SceneManager.LoadScene("Load_Scene");
    }

    public void OnReset()
    {
        Saveloadlevel.Reset_Levels();
        SaveloadScene.Reset_Scenes();
    }
}
