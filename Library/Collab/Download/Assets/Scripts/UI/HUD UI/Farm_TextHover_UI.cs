﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;
using UnityEngine.UI;

public class Farm_TextHover_UI : MonoBehaviour
{
    private Text HoverText;

    void Start()
    {
        HoverText = GetComponent<Text>();
    }


    void Update()
    {
        HoverText.text = "Farm Cost 5 Gold \n" +
        "Farm Production 15 Silvers \n" +
            "Available Farms For Construction "+ PlayersManager.Instance.LocalPlayer.FarmsLost+" Farms \n" +
            "The Kingdoom has " + PlayersManager.Instance.LocalPlayer.FarmsNumber + " Farms \n" +
            "Bonus Silvers " + PlayersManager.Instance.LocalPlayer.FarmsBonusSilver+ "\n" +
            "Kindoom Lost "+ PlayersManager.Instance.LocalPlayer.FarmsLost + " Farms";
    }
}
