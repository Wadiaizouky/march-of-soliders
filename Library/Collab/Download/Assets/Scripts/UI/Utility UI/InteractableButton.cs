﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InteractableButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject onStatic;
    public GameObject onHover;
    public GameObject onActive;
    public GameObject LockedIcon;

    public bool lockButton = false;

    [HideInInspector] public bool isActivate = true;
    private Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
        if(lockButton)
        {
            LockButton();
        }
    }

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        if (!isActivate) return;
        onStatic.SetActive(false);
        onHover.SetActive(true);
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        if (!isActivate) return;
        onStatic.SetActive(true);
        onHover.SetActive(false);
    }

    public void ActiveButton()
    {
        if (isActivate) return;
        onStatic.SetActive(true);
        onHover.SetActive(false);
        onActive.SetActive(false);
        LockedIcon.SetActive(false);
        button.enabled = true;
        isActivate = true;
    }

    public void DeactivateButton()
    {
        if (!isActivate) return;
        onStatic.SetActive(false);
        onHover.SetActive(false);
        onActive.SetActive(true);
        LockedIcon.SetActive(false);
        button.enabled = false;
        isActivate = false;
    }

    public void LockButton()
    {
        onStatic.SetActive(false);
        onHover.SetActive(false);
        onActive.SetActive(false);
        LockedIcon.SetActive(true);
        button.enabled = false;
        isActivate = false;
        lockButton = true;
    }
}
