﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MarchOfSoldiers
{
    public class CastlesCount_UI : MonoBehaviour
    {
        public Text playerCastleNumber;
        public Text armyCastleNumber;

        private void FixedUpdate()
        {
            UpdateUI();
        }

        private void UpdateUI()
        {
            playerCastleNumber.text = CastlesSelection.Instance.selectableCastles.Count.ToString();
            int NumberofArmyCastle = SelectionManager.Instance.Allcastle.Count - CastlesSelection.Instance.selectableCastles.Count;
            armyCastleNumber.text = NumberofArmyCastle.ToString();
        }

    }
}