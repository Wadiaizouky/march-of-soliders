﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;
using UnityEngine.UI;


namespace MarchOfSoldiers
{
    public class PasueController : MonoBehaviour
    {
        public GameObject PasuePanl;
        public bool IsPasued { get; set; }

        public List<GameObject> UI_Pasue_Buttons;

        private static PasueController _instance = null;
        public static PasueController Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<PasueController>();
                return _instance;
            }
        }

        //Load Save Buttons
        public GameObject SaveButton;
        public GameObject LoadButton;

        public GameObject ScrolviewContent;
        public GameObject ScrolviewContent2;

        public Button SaveName;

        public string SaveName_Clicked = "";

        void Start()
        {
            IsPasued = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PasuePanl.SetActive(true);
                IsPasued = true;

                //StartCoroutine(Active_UI_Buttons());
            }
        }


        public void DisActivaPasuePanle()
        {
            PasuePanl.SetActive(false);
            IsPasued = false;
            //StartCoroutine(DisActive_UI_Buttons());
        }

        IEnumerator Active_UI_Buttons()
        {
            int i = 0;
            while (true)
            {
                yield return new WaitForSeconds(0.05f);
                UI_Pasue_Buttons[i].SetActive(true);
                i++;
                if (i == UI_Pasue_Buttons.Count)
                    break;
            }
        }

        IEnumerator DisActive_UI_Buttons()
        {
            int i = 0;
            while (true)
            {
                yield return new WaitForSeconds(0.05f);
                UI_Pasue_Buttons[i].SetActive(false);
                i++;
                if (i == UI_Pasue_Buttons.Count)
                    break;
            }
        }

        public void OnLoad()
        {
            for (int i = 0; i < ScrolviewContent.transform.childCount; i++)
            {
                Destroy(ScrolviewContent.transform.GetChild(i).gameObject);
            }

            
            LoadButton.SetActive(true);

            //Return all Saved Levels
            string[] AllSaveLevels = SaveScenesManger.AllSavedLoad();
            for (int i = 0; i < AllSaveLevels.Length; i++)
            {
                Button Save_Name = Instantiate(SaveName);
                Save_Name.name = AllSaveLevels[i];
                Save_Name.transform.GetChild(0).gameObject.name = AllSaveLevels[i].Substring(70);


                Save_Name.transform.SetParent(ScrolviewContent.transform);
            }
        }

        public void OnSave()
        {
            for (int i = 0; i < ScrolviewContent2.transform.childCount; i++)
            {
                Destroy(ScrolviewContent2.transform.GetChild(i).gameObject);
            }


            SaveButton.SetActive(true);

            //Return all Saved Levels
            string[] AllSaveLevels = SaveScenesManger.AllSavedLoad();
            for (int i = 0; i < AllSaveLevels.Length; i++)
            {
                Button Save_Name = Instantiate(SaveName);
                Save_Name.name = AllSaveLevels[i];
                Save_Name.transform.GetChild(0).gameObject.name = AllSaveLevels[i].Substring(70);


                Save_Name.transform.SetParent(ScrolviewContent2.transform);
            }
        }
        }
    }