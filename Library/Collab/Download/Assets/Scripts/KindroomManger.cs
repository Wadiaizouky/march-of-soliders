﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MarchOfSoldiers;

public class KindroomManger : MonoBehaviour
{
    //Kindroom Player Level 
    public int KindroomLevel { get; set; }
    //All Player Selver and Gold coin
    public Text SelverText;
    public int Selver { get; set; }
    public Text GoldText;
    public float gold { get; set; }

    private static KindroomManger _instance = null;


    public static KindroomManger Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<KindroomManger>();
            return _instance;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        KindroomLevel = 1;
        //Start Calculat Selver and gold for all Player Castles.
        StartCoroutine("GoldAndSelverCalc");

        //init Selver and gold
        Selver = 0;
        gold = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// Show Selver And Gold UI Coin
    public void MyCorotine()
    {
        StartCoroutine("GoldAndSelverCalc");
    }
    IEnumerator GoldAndSelverCalc()
    {
        List<SingleCastle> _selectableCastles = new List<SingleCastle>();
        _selectableCastles = SelectionManager.Instance._selectableCastles;
        if (KindroomLevel == 1)
        {
            gold += (float)25 * _selectableCastles.Count / 100;
            SelverText.text = (25 * _selectableCastles.Count).ToString();
            GoldText.text = gold.ToString();
        }
        else if (KindroomLevel == 2)
        {
            gold += (float)35 * _selectableCastles.Count / 100;
            SelverText.text = (35 * _selectableCastles.Count).ToString();
            GoldText.text = gold.ToString();
        }

        yield return new WaitForSeconds(1);
        MyCorotine();
    }
}
