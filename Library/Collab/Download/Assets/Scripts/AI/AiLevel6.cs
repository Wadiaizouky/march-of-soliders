﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MarchOfSoldiers;

public class AiLevel6 : MonoBehaviour
{
    //All Castle in the game
    public List<GameObject> AllCastle;

    //Ai Castles
    public List<GameObject> AiCastles;

    //Other Castles
    public List<GameObject> OtherCastles;

    //Main ai Castle
    public GameObject MainAiCastle1;
    int MainAiCastle1Count = 0;
    //Main ai Castle
    public GameObject MainAiCastle2;
    int MainAiCastle2Count = 0;

    //Gates
    public List<GameObject> Gates;

    #region wall1
    //Castles in wall1
    public List<GameObject> first5Castles;
    public List<GameObject> first3Castles;
    //Main AI Castle
    public GameObject MainAiCastlewall1;
    //Castle in the corner
    public GameObject CornerCastle;
    //Deffind Castle
    public GameObject DeffindedCastle;
    //Castle or wall Attack by Player
    private GameObject AttackeCastle;
    #endregion

    #region wall2
    public List<GameObject> first6AiCastleWall2;
    public GameObject MainAiCastlewall2;

    public List<GameObject> DeffindedWall2Castles;
    #endregion

    //Main Player Castle
    public GameObject MainPlayerCastle;

    bool StartAiRun;

    bool StartCounter = false;


    bool SentSoldierforfirsttime = false;

    //Win Panel
    public GameObject Win_Panel;
    //Defeat Panel
    public GameObject Defeat_Panel;

    //Win Effects
    public GameObject Win_Effects;

    void Start()
    {
        StartAiRun = false;

        AttackeCastle = null;
    }

    void Update()
    {
        if (!PasueController.Instance.IsPasued)
        {
            //Calculat Ai and Other Castles.
            CalCastles();

            AiControler();

            CheckWinnerPlayer();
        }
    }

    //Cotrol Ai in the game
    public void AiControler()
    {
        if (!StartAiRun)
        {
            foreach (GameObject castle in AiCastles)
                if (castle.GetComponent<CastelArmyUICount>().sward >= 10)
                {
                    if (MainAiCastle1Count < 5 && castle == MainAiCastle1)
                    {
                        creatgrouptoNearlyCastle(castle, 10);
                        MainAiCastle1Count++;
                    }
                    else if (MainAiCastle2Count < 6 && castle == MainAiCastle2)
                    {
                        creatgrouptoNearlyCastle(castle, 10);
                        MainAiCastle2Count++;
                    }
                }
        }
        //Start Ai Algorithm
        else
        {
            Runwall1Algorithm();

            if (!Gates[1].GetComponent<GateArmyUICount>().IsEnemyGate)
                Runwll2Algorithm();
        }

        //Start Run Ai
        if (AiCastles.Count == 12 && !StartAiRun)
            StartAiRun = true;
    }

    //Algorithm for wall 1
    void Runwall1Algorithm()
    {
        ////if Player have one of this Castle .. the Castle will Sent armys to it 
        if (DeffindedCastle.GetComponent<CastelArmyUICount>().sward >= 7 && DeffindedCastle.GetComponent<CastelArmyUICount>().isEnumyCastle && AttackeCastle != null)
            StartCoroutine(RunAttack(AttackeCastle, DeffindedCastle));

        //Check if Ai have all 5 Castle .
        if (AttackeCastle == null)
            AttackeCastle = CheckAiCastles();

        //checkCastles();

        //Attack to Main Player Castle if condition is true
        //Round Attack
        if (MainAiCastlewall1.GetComponent<CastelArmyUICount>().sward >= 100 && MainAiCastlewall1.GetComponent<CastelArmyUICount>().isEnumyCastle)
        {
            if (AttackeCastle == null)
            {
                int RandomCastle = Random.Range(0, OtherCastles.Count);
                if (!OtherCastles[RandomCastle].GetComponent<CastelArmyUICount>().iswhitecastel)
                    CreatGroupFromCastleToCastle(MainAiCastlewall1, OtherCastles[RandomCastle], 100);
            }
            else
                StartCoroutine(RunAttack(AttackeCastle, MainAiCastlewall1));

        }

        //this for sent 5Soldiers from Castle to mainCastle and Gate
        if (!StartCounter)
        {
            StartCoroutine(Incris());
            StartCounter = true;
        }


    }

    ////Algorithm for wall 2
    //void Runwll2Algorithm()
    //{
    //    if (first6AiCastleWall2[0].GetComponent<CastelArmyUICount>().sward < 251)
    //       StartCoroutine(Runwll2AlgorithmSec1());
    //    else
    //        StartCoroutine(Runwll2AlgorithmSec2());
    //}
    //Algorithm for wall 2 (Section 1)
    void Runwll2Algorithm()
    {
        if (!SentSoldierforfirsttime)
        {
            StartCoroutine(SentSoldiesToGate());
            SentSoldierforfirsttime = true;

        }

        //Attack to Main Player Castle if condition is true
        if (MainAiCastlewall2.GetComponent<CastelArmyUICount>().sward >= 200 && MainAiCastlewall2.GetComponent<CastelArmyUICount>().isEnumyCastle)
        {
            if (!Gates[1].GetComponent<GateArmyUICount>().IsEnemyGate)
                CreatGroupFromCastleToCastle(MainAiCastlewall2, Gates[1], 200);
        }
    }
    ////Algorithm for wall  (Section 2)
    //IEnumerator Runwll2AlgorithmSec2()
    //{

    //    yield return new WaitForSeconds(0.1f);
    //}

    //////Check if 5 Ai Castle is mine ..
    //void checkCastles()
    //{

    //    foreach (GameObject Castle in first5Castles)
    //    {
    //        if (!Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
    //        {
    //            if (AttackeCastle == MainPlayerCastle && AttackeCastle != Castle)
    //            {
    //                AttackeCastle = Castle;
    //                StartCoroutine(RunAttack(Castle,DeffindedCastle));
    //            }
    //            //CreatGroupFromCastleToCastle(DeffindedCastle, Castle, DeffindedCastle.GetComponent<CastelArmyUICount>().sward / 2);
    //        }
    //    }

    //    if (!Gates[0].GetComponent<GateArmyUICount>().IsEnemyGate)
    //        if (AttackeCastle != Gates[0])
    //        {
    //            AttackeCastle = Gates[0];
    //            StartCoroutine(RunAttack(Gates[0], DeffindedCastle));
    //        }
    //}


    GameObject CheckAiCastles()
    {
        GameObject ReturnedCastle = null;
        foreach (GameObject Castle in first5Castles)
        {
            if (!Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                //if (AttackeCastle == MainPlayerCastle && AttackeCastle != Castle)
                //{
                ReturnedCastle = Castle;
                    //StartCoroutine(RunAttack(Castle, DeffindedCastle));
                //}
                //CreatGroupFromCastleToCastle(DeffindedCastle, Castle, DeffindedCastle.GetComponent<CastelArmyUICount>().sward / 2);
            }
        }

        if (!Gates[0].GetComponent<GateArmyUICount>().IsEnemyGate)
            //if (AttackeCastle != Gates[0])
            //{
            ReturnedCastle = Gates[0];
        //StartCoroutine(RunAttack(Gates[0], DeffindedCastle));
        //}

        return ReturnedCastle;
    }

    //if Player have one ai wall Castle 
    IEnumerator RunAttack(GameObject Castle,GameObject CurrentCastle)
    {
        while (true)
        {
            if (!CurrentCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                break;

            CreatGroupFromCastleToCastle(CurrentCastle, Castle, CurrentCastle.GetComponent<CastelArmyUICount>().sward);
            if (Castle.GetComponent<SingleCastle>().isGate)
            {
                if (Castle.GetComponent<GateArmyUICount>().IsEnemyGate)
                    break;
            }
            else
            {
                if (Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                    break;
            }

            yield return new WaitForSeconds(5f);
        }
        AttackeCastle = null;
    }


    //Create group from Castle toword Castle
    void CreatGroupFromCastleToCastle(GameObject CurrentCastle, GameObject TargerCastle, int groupSize)
    {
        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(TargerCastle.transform.position);
        GroupOfUnits g;
        //int swordcount = CurrentCastle.GetComponent<CastelArmyUICount>().sward;
        g = UnitsSpawner.Instance.CreateGroup(groupSize, CirclePosition, GroupType.Sword, OwnerType.Enemy, -1, TargerCastle.transform.position, CurrentCastle.GetComponent<SingleCastle>(), TargerCastle.GetComponent<SingleCastle>());

        CurrentCastle.GetComponent<CastelArmyUICount>().sward -= groupSize;
    }

    //Create group from Gate toword Castle
    void CreatGroupFromGateToCastle(GameObject CurrentGate, GameObject TargerCastle, int groupSize)
    {
        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(TargerCastle.transform.position);
        GroupOfUnits g;
        //int swordcount = CurrentCastle.GetComponent<CastelArmyUICount>().sward;
        g = UnitsSpawner.Instance.CreateGroup(groupSize, CirclePosition, GroupType.Sword, OwnerType.Enemy, -1, TargerCastle.transform.position, CurrentGate.GetComponent<SingleCastle>(), TargerCastle.GetComponent<SingleCastle>());

        CurrentGate.GetComponent<GateArmyUICount>().sward -= groupSize;
    }
    /// NearlyCastle to the main Castle
    /// </summary>
    public void creatgrouptoNearlyCastle(GameObject Castle, int Soliders_Number)
    {
        GameObject NearCastle = NearlyCastle(Castle);

        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(NearCastle.transform.position);
        GroupOfUnits g;
        int swordcount = Castle.GetComponent<CastelArmyUICount>().sward;
        g = UnitsSpawner.Instance.CreateGroup(swordcount, CirclePosition, GroupType.Sword, OwnerType.Enemy, -1, NearCastle.transform.position, Castle.GetComponent<SingleCastle>(), NearCastle.GetComponent<SingleCastle>());

        Castle.GetComponent<CastelArmyUICount>().sward -= Soliders_Number;
    }

    /// <summary>wade adds
    /// Calculate the near castle
    /// </summary>
    public GameObject NearlyCastle(GameObject Castle)
    {
        GameObject castle = OtherCastles[0];
        float dis = Vector3.Distance(Castle.transform.position, OtherCastles[0].transform.position);
        for (int i = 1; i < OtherCastles.Count; i++)
        {
            float distance = Vector3.Distance(Castle.transform.position, OtherCastles[i].transform.position);
            if (dis > distance)
            {
                dis = distance;
                castle = OtherCastles[i];
            }

        }
        return castle;
    }

    //Sent 5 Soliders from 3 Castles After 5 Seconds.
    public void MyExraCorotine()
    {
        StartCoroutine(Incris());
    }
    IEnumerator Incris()
    {
        yield return new WaitForSeconds(5);
        if (!PasueController.Instance.IsPasued)
        {
            //sent 5 soldies from all 3 castle to main Castle in wall 1
            //if (MainAiCastlewall1.GetComponent<CastelArmyUICount>().isEnumyCastle)
            //{
                //For each Castle in List main 4 Castle..
                foreach (GameObject Castle in first3Castles)
                    if (Castle.GetComponent<CastelArmyUICount>().sward >= 5 && Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                    {
                        if(AttackeCastle == null)
                             CreatGroupFromCastleToCastle(Castle, MainAiCastlewall1, 5);
                        else
                        CreatGroupFromCastleToCastle(Castle, AttackeCastle, 5);
                    }
            //}

            //sent 5 Sodiers from corner Castle to Gate
            if (CornerCastle.GetComponent<CastelArmyUICount>().sward >= 5 && CornerCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                CreatGroupFromCastleToCastle(CornerCastle, Gates[0], 5);
        }
        MyExraCorotine();

    }


    //Sent 1000 Soldier from Castle To Gate for one time
    IEnumerator SentSoldiesToGate()
    {
        foreach (GameObject Castle in first6AiCastleWall2)
        {
            if (Castle.GetComponent<CastelArmyUICount>().isEnumyCastle && !Gates[1].GetComponent<GateArmyUICount>().IsEnemyGate)
                CreatGroupFromCastleToCastle(Castle, Gates[1], 10);

            yield return new WaitForSeconds(1);
        }

        //
        if(first6AiCastleWall2[2].GetComponent<CastelArmyUICount>().sward < 251)
           StartCoroutine(SentSoldiesfrom5CastletomainCastle());
        else
            StartCoroutine(SentSoldiesfrom50CastletomainCastle());
    }

    //Sent 5 Soldiers from all Castle in wall 2 to main ai Castle (1)
    public void MyExraCorotinesent()
    {
        StartCoroutine(SentSoldiesfrom5CastletomainCastle());
    }
    IEnumerator SentSoldiesfrom5CastletomainCastle()
    {
        //test if all 6 castles in wall2 is ai
        TestWall2Castle();

        yield return new WaitForSeconds(10);
        if (!PasueController.Instance.IsPasued)
        {
            if (MainAiCastlewall2.GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                //For each Castle in List main 4 Castle..
                foreach (GameObject Castle in first6AiCastleWall2)
                    if (Castle.GetComponent<CastelArmyUICount>().sward >= 5 && Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                        CreatGroupFromCastleToCastle(Castle, MainAiCastlewall2, 5);
            }
        }
        MyExraCorotinesent();
    }

    //Sent 5 Soldiers from all Castle in wall 2 to main ai Castle (2)
    public void MyExraCorotinesent2()
    {
        StartCoroutine(SentSoldiesfrom5CastletomainCastle());
    }
    IEnumerator SentSoldiesfrom50CastletomainCastle()
    {
        //test if all 6 castles in wall2 is ai
        TestWall2Castle();

        yield return new WaitForSeconds(10);
        if (!PasueController.Instance.IsPasued)
        {
            if (MainAiCastlewall2.GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                //For each Castle in List main 4 Castle..
                foreach (GameObject Castle in first6AiCastleWall2)
                    if (Castle.GetComponent<CastelArmyUICount>().sward >= 50 && Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                        CreatGroupFromCastleToCastle(Castle, MainAiCastlewall2, 50);
            }
        }
        MyExraCorotinesent2();
    }

    //Test if all Castles in wall2 are Ai
    void TestWall2Castle()
    {
        foreach(GameObject castle in first6AiCastleWall2)
        {
            if (!castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                foreach (GameObject Cas in first6AiCastleWall2)
                    if (Cas.GetComponent<CastelArmyUICount>().isEnumyCastle)
                        CreatGroupFromCastleToCastle(Cas, castle, Cas.GetComponent<CastelArmyUICount>().sward);
                if(Gates[2].GetComponent<GateArmyUICount>().IsEnemyGate)
                        CreatGroupFromGateToCastle(Gates[2], castle, Gates[2].GetComponent<GateArmyUICount>().sward/2);
            }
        }

        if(!Gates[2].GetComponent<GateArmyUICount>().IsEnemyGate)
        {
            foreach (GameObject Cas in first6AiCastleWall2)
                if (Cas.GetComponent<CastelArmyUICount>().isEnumyCastle)
                    CreatGroupFromCastleToCastle(Cas, Gates[2], Cas.GetComponent<CastelArmyUICount>().sward);
        }
    }

    //Calculat Casttle(Ai + other Castles)
    void CalCastles()
    {
        List<GameObject> AiCas = new List<GameObject>();
        int Aicast = 0;
        List<GameObject> OtherCas = new List<GameObject>();
        int OtherCast = 0;

        foreach (GameObject cast in AllCastle)
        {
            if (cast.GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                AiCas.Add(cast);
                Aicast++;
            }
            else
            {
                OtherCas.Add(cast);
                OtherCast++;
            }
        }


        AiCastles = AiCas;
        OtherCastles = OtherCas;
    }
    /// <summary>wade adds
    /// load cicil level if Player have all Castles or loose All his Castles
    /// </summary>
    public void CheckWinnerPlayer()
    {
        //Defeat Player case
        if (AiCastles.Count == AllCastle.Count)
        {
            Defeat_Panel.SetActive(true);
            PasueController.Instance.IsPasued = true;
        }
        //Win Player case
        else if (AiCastles.Count == 0)
        {
            Win_Panel.SetActive(true);
            PasueController.Instance.IsPasued = true;
            //Win_Effects.SetActive(true);
        }
        //Debug.Log(CastlesSelection.Instance.selectableCastles.Count + "," + Allcastle.Count);
    }
}
