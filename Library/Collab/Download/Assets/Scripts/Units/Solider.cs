﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;

public enum TroopType { Sword, Spear, Horse }

/// <summary>
/// work on selectable object and control it's behavior when it get selected. 
/// </summary>
public class Solider : MonoBehaviour
{
    [SerializeField] protected TroopType _soliderType;
    [SerializeField] private string _targetTag = "Enemy";
    [SerializeField] private float _health;
    [SerializeField] private float _damage;

    // components
    protected Animator _animator = null;
    protected NavMeshAgent _agent = null;
    protected Rigidbody2D _rigidbody = null;
    protected IsometricCharacterRenderer _characterRenderer = null;

    private bool _isAttacking;
    private GameObject _target;
    private readonly Queue<GameObject> _targets = new Queue<GameObject>();

    #region Properties
    public TroopType SoliderType { get => _soliderType; set => _soliderType = value; }
    public float Health { get => _health; set => _health = value; }
    public float Damage { get => _damage; set => _damage = value; }
    public bool IsSelected { get; set; }
    public bool IsDead { get; set; }

    //public bool GoalReached { get => _goalReached; set => _goalReached = value; }
    public Vector3 Destination { get; set; }

    #endregion
    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _characterRenderer = GetComponent<IsometricCharacterRenderer>();

        _agent.updateRotation = false;
        _agent.updateUpAxis = false;
    }

    private void Update()
    {
        // control solider animation
        _characterRenderer.SetDirection(_agent.desiredVelocity);

        // we don't have current target but we still have some in the queue, fetch one and use it as target.
        if (_target == null && _targets.Count > 0)
            _target = _targets.Dequeue();

        // if we have target, move toward it.
        if (_target)
            HeadTowardTarget();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if enemy enter troop range, add it to targets queue.
        if (collision.CompareTag(_targetTag))
            _targets.Enqueue(collision.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(_targetTag) && _target)
            Attack();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="currentSolider"></param>
    /// <param name="enemySolider"></param>
    /// <returns></returns>
    protected virtual float CalculateDamage(TroopType currentSolider, TroopType enemySolider)
    {
        float bonus = 0;

        if (currentSolider == TroopType.Sword && enemySolider == TroopType.Horse)
            bonus = Damage * 0.5f;

        else if (currentSolider == TroopType.Spear && enemySolider == TroopType.Sword)
            bonus = Damage * 0.5f;

        else if (currentSolider == TroopType.Horse && enemySolider == TroopType.Spear)
            bonus = Damage * 0.5f;

        return Damage - bonus;
    }

    private void HeadTowardTarget()
    {
        _agent.isStopped = false;
        if (!_isAttacking)
            _agent.SetDestination(_target.transform.position);
    }

    private void Attack() { }
}
