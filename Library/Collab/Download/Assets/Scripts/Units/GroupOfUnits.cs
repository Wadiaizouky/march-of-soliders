﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace MarchOfSoldiers
{
    public enum GroupType { Sword, Spear, Horse }

    public enum GroupOwner { Player, Enemy }

    public class GroupOfUnits : MonoBehaviour
    {
        #region Fields

        [SerializeField] private GroupType _groupType;
        [SerializeField] private SingleUnit _unitPrefab;
        [SerializeField] private GameObject _groupSizeUI;

        [SerializeField, Tooltip("Used to separate units when they instantiated.")]
        private Vector2 _unitInstantiateOffset = new Vector2(0.2f, 0.23f);

        [SerializeField, Tooltip("how many units can be in each raw when group instantiated")]
        private int _unitGroupBoundaryRows = 4;

        [SerializeField, Tooltip("how many units can be in each Col when group instantiated")]
        private int _unitGroupBoundaryCols = 5;

        private readonly List<SingleUnit> _units = new List<SingleUnit>();

        #endregion

        #region Properties

        /// <summary>
        /// Point in the middle of units inside the group 
        /// </summary>
        public Vector3 Center { get; set; }

        /// <summary>
        /// is this group currently selected?
        /// </summary>
        public bool IsSelected { get; set; }

        /// <summary>
        /// unit that is the commander for this group
        /// </summary>
        public SingleUnit Commander { get; set; }

        /// <summary>
        /// array hold position for every unit, used to give every unit a unique position when the group created.
        /// </summary>
        public Vector2[,] UnitInstantiatePositions { get; private set; }

        public SingleCastle TargetCastle { get; set; }

        /// <summary>
        /// unit to instantiate when this group get created
        /// </summary>
        public SingleUnit UnitPrefab => _unitPrefab;

        /// <summary>
        /// UI Prefab represent number of unit text.
        /// </summary>
        public GameObject GroupSizeUI => _groupSizeUI;

        public FollowTarget FollowTarget { get; set; }

        public Text GroupSizeText { get; set; }

        public GroupType GroupType => _groupType;

        /// <summary>
        /// list of every VISIBLE unit inside the group.
        /// </summary>
        public List<SingleUnit> Units => _units;

        public Vector3 Goal { get; set; }

        public int GroupSize { get; set; }

        #endregion

        private void Awake()
        {
            UnitsPositionInitializer();
        }

        private void OnDestroy()
        {
            SelectionManager selectionManager = SelectionManager.Instance;
            if (selectionManager)
            {
                selectionManager.SelectedGroups.Remove(this);
                selectionManager.Invalidate();
            }

        }

        /// <summary>
        /// initialize unit position array and calculate each unit position inside it.
        /// </summary>
        private void UnitsPositionInitializer()
        {
            // initialize positions array.
            UnitInstantiatePositions = new Vector2[_unitGroupBoundaryRows, _unitGroupBoundaryCols];

            // calculate the instantiate position for every unit in the group.
            for (int i = 0; i < UnitInstantiatePositions.GetLength(0); i++)
            {
                for (int j = 0; j < UnitInstantiatePositions.GetLength(1); j++)
                {
                    UnitInstantiatePositions[i, j] = new Vector2(j * _unitInstantiateOffset.x, -i * _unitInstantiateOffset.y);
                }
            }
        }

        public void AddUnit(SingleUnit unit)
        {
            if (!unit)
            {
                Debug.LogError("make sure to pass valid unit to add");
                return;
            }

            _units.Add(unit);
            unit.JoinGroup(this);
        }

        public void RemoveUnit(SingleUnit unit)
        {
            Units.Remove(unit);
            // group has no unit in it, just destroy it.
            if (Units.Count <= 0)
                Destroy(gameObject);

            // update group commander if it would be destroyed.
            if (Commander == this)
            {
                //UpdateGroupCommander();
            }

            UpdateGroupSize(this, unit.VirtualValue);
        }

        public void UpdateGroupSize(GroupOfUnits group, int lossingSize)
        {
            if (group && lossingSize > 0)
            {
                group.GroupSize -= lossingSize;
                //group.GroupSizeText.text = group.GroupSize.ToString();
            }
        }

        private void UpdateGroupCommanderUnit(SingleUnit unit)
        {
            if (Commander == this)
            {
                SetGroupCommander();
                FollowTarget.Target = Commander.transform;
            }
        }

        public void SetGroupCommander()
        {
            if (_units == null || _units.Count == 0)
            {
                Debug.LogError("Can't set group commander");
                return;
            }
            // if count more than 5, commander will be the middle one in the first row
            Commander = _units.Count > 5 ? _units[2] : _units[_units.Count / 2];

            Commander.IsCommander = true;
        }

        /// <summary>
        /// Calculate offset for unit based on a given position.
        /// </summary>
        /// <param name="unitsPositions">position to calculate offset from</param>
        public void SetUnitsGoal(List<Vector2> unitsPositions)
        {
            for (int i = 0; i < Units.Count; i++)
                Units[i].Unit_Movement.CalculatePositionGoal(unitsPositions[i]);
        }

        /// <summary>
        /// Called when group get selected.
        /// </summary>
        public void OnSelected()
        {
            // mark the group as selected
            IsSelected = true;

            // select every unit in it
            foreach (var unit in _units)
            {
                if (unit)
                    unit.OnSelected();

                else
                    Debug.LogError("make sure group has a valid reference to every unit inside it");
            }
        }

        /// <summary>
        /// Called When group get deselected.
        /// </summary>
        public void OnDeselected()
        {
            // deselect the group
            IsSelected = false;

            // deselect every unit in it
            foreach (var unit in _units)
            {
                if (unit)
                    unit.OnDeselected();

                else
                    Debug.LogError("make sure group has a valid reference to every unit inside it");
            }
        }

        /// <summary>
        /// Move every unit to given point and offset the destination if necessary.
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="offset"></param>
        public void Move(Vector3 destination, bool offset)
        {
            foreach (SingleUnit unit in Units)
                unit.Unit_Movement.Move(destination, offset);
        }

        /// <summary>
        /// Create label that show number of units above the group. 
        /// </summary>
        /// <param name="group">group to set label above</param>
        /// <param name="groupSize">number of units within the group</param>
        public void CreateUnitsUILabel(int groupSize)
        {
            // create group UI label
            GameObject groupUI = Instantiate(_groupSizeUI, transform);

            if (groupUI)
            {
                // Fetch group follow target component 
                FollowTarget = groupUI.GetComponent<FollowTarget>();

                FollowTarget.Target = Commander.transform;

                // fetch text from UI
                GroupSizeText = groupUI.GetComponentInChildren<Text>();

                // set groupUI text so it render group size.
                GroupSizeText.text = groupSize.ToString();
            }

            else
                Debug.LogError("GroupSizeUI didn't instantiated correctly");
        }

    }
}