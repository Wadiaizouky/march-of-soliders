﻿using UnityEngine;

public class SwordSolider : Solider
{
    private bool _isDead = false;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(_targetTag) && _target)
            Attack();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(_targetTag))
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        // we don't have current target but we still have some in the queue, fetch one and use it as target.
        if (_target == null && _targets.Count > 0) 
            _target = _targets.Dequeue();

        // if we have target, move toward it.
        if (_target)
            HeadTowardTarget();
    }

    /// <summary>
    /// move toward enemy target
    /// </summary>
    /// <param name="target">target position </param>
    private void HeadTowardTarget()
    {
        _agent.SetDestination(_target.transform.position);
    }

    private void Attack()
    {         
        //SwordSolider solider = _target.GetComponent<SwordSolider>();

        // already attacking, don't attack again.
        //if (_isAttacking || solider.IsDead)
            //return;

        // calculate damage
        //float damage = CalculateDamage(_soliderType, solider.SoliderType);

        // reduce enemy health
        //if(solider.Health > 0)
            //solider.Health -= damage;

        // mark this troop as attacking.
        _isAttacking = true;

        // health drop down to 0, death.
        //if (solider.Health <= 0)
            //KillEnemy(solider);
    }

    private void KillEnemy(SwordSolider solider)
    {
        // troop can attack again after killing enemy
        _isAttacking = false;

        // mark killed enemy as dead so he can't attack again until he destroyed.
        solider.IsDead = true;

        // destroy killed enemy.
        Destroy(_target.gameObject, 0.01f);
    }
}
