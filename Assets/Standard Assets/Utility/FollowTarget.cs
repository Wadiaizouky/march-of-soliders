using UnityEngine;


public class FollowTarget : MonoBehaviour
{
    public Transform Target { get; set; }
    private Vector3 offset = new Vector3(0f, 2.5f, 0f);


    private void LateUpdate()
    {
        if(Target)
            transform.position = Target.position + offset;
    }
}

