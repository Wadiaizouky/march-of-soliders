﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MarchOfSoldiers;

public class AiLevel5 : MonoBehaviour
{
    //All Castle in the game
    public List<GameObject> AllCastle;

    //Ai Castles
    private List<GameObject> AiCastles;

    //Other Castles
    private List<GameObject> OtherCastles;

    //First 4 Castle
    public List<GameObject> First4Castle;

    
    //Main Ai Castle
    public GameObject MainAiCastle;

    //Main Player Castle
    public GameObject MainPlayerCastle;

    bool StartAiRun;

    //For sent 6 Soliders After 10 Seconds..
    private bool StartCounter = false;

    //if Player have one ai wall Castle
    private bool PlayerAttackTowall = false;
    //Castle or wall Attack by Player
    private GameObject AttackeCastle;

    //if 150 Soldies add for Ai Castles
    private bool AddSoliderforfirsttime;

    //Win Panel
    public GameObject Win_Panel;
    //Defeat Panel
    public GameObject Defeat_Panel;

    public AudioSource WinSound;
    public AudioSource LostSound;
    public AudioSource LevelSound;

    //Win Effects
    public GameObject Win_Effects;

    public SaveLoadLevels Saveloadlevel;


    void Start()
    {
        StartAiRun = false;

        AddSoliderforfirsttime = false;

        MainAiCastle.GetComponent<CastelArmyUICount>().ExtraiSoliders = 250;
        MainAiCastle.GetComponent<CastelArmyUICount>().IsExtraSoliders = true;
        MainAiCastle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;

        AttackeCastle = MainPlayerCastle;
    }

    private void Update()
    {
        if (!PasueController.Instance.IsPasued)
        {
            //Calculate Ai and other Castle
            CalCastles();

            AiControler();

            CheckWinnerPlayer();
        }
    }
    //Cotrol Ai in the game
    public void AiControler()
    {
        if (!StartAiRun)
        {
            foreach (GameObject castle in AiCastles)
                if (castle.GetComponent<CastelArmyUICount>().sward >= 10)
                    creatgrouptoNearlyCastle(castle,10);
        }

        //Run Ai
        else
        {
            TestMainAiCastle();
            //Start sent 6 Soliders to Main Castle
            if (!StartCounter)
            {
                StartCoroutine(Incris());
                StartCounter = true;
            }

            //this for Ad 150 Sodiers to first 4 Ai Castles
            if (!AddSoliderforfirsttime)
            {
                AddSoliderforfirsttime = true;
                AddSoliderforfirsttim();
            }
        }
        //Start Run Ai
        if (AiCastles.Count == 5 && !StartAiRun)
            StartAiRun = true;
    }

    /// NearlyCastle to the main Castle
    /// </summary>
    public void creatgrouptoNearlyCastle(GameObject Castle, int Soliders_Number)
    {
        GameObject NearCastle = NearlyCastle(Castle);

        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(NearCastle.transform.position);
        GroupOfUnits g;
        int swordcount = Castle.GetComponent<CastelArmyUICount>().sward;
        g = UnitsSpawner.Instance.CreateGroup(swordcount, CirclePosition, GroupType.Sword, OwnerType.Enemy, -1, NearCastle.transform.position, Castle.GetComponent<SingleCastle>(), NearCastle.GetComponent<SingleCastle>());

        Castle.GetComponent<CastelArmyUICount>().sward -= Soliders_Number;
    }

    /// <summary>wade adds
    /// Calculate the near castle
    /// </summary>
    public GameObject NearlyCastle(GameObject Castl)
    {
            GameObject castle = OtherCastles[0];
            float dis = Vector3.Distance(Castl.transform.position, OtherCastles[0].transform.position);
            for (int i = 1; i < OtherCastles.Count; i++)
            {
                float distance = Vector3.Distance(Castl.transform.position, OtherCastles[i].transform.position);
                if (dis > distance)
                {
                    dis = distance;
                    castle = OtherCastles[i];
                }

            }
            return castle;
       // }

    }


    //Sent 6 Soliders After 10 Seconds.
    public void MyExraCorotine()
    {
        StartCoroutine(Incris());
    }
    IEnumerator Incris()
    {
        yield return new WaitForSeconds(10);
        if (!PasueController.Instance.IsPasued && !PlayerAttackTowall)
        {
            if (MainAiCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                //For each Castle in List main 4 Castle..
                foreach (GameObject Castle in First4Castle)
                    if (Castle.GetComponent<CastelArmyUICount>().sward >= 6 && Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                        CreatGroupFromCastleToCastle(Castle, MainAiCastle, 6);
            }
        }
        MyExraCorotine();

    }

    ////Sent 6 Soliders To Main Castle
    //void SentSolidersToMainCastle(GameObject Castle)
    //{

    //    List<Vector2> CirclePosition = new List<Vector2>();
    //    for (int j = 0; j < 20; j++)
    //        CirclePosition.Add(MainAiCastle.transform.position);
    //    GroupOfUnits g;
    //    int swordcount = 6;
    //    g = UnitsSpawner.Instance.CreateGroup(swordcount, CirclePosition, GroupType.Sword, GroupOwner.Enemy, MainAiCastle.transform.position, Castle.GetComponent<SingleCastle>(), MainAiCastle.GetComponent<SingleCastle>());

    //    Castle.GetComponent<CastelArmyUICount>().sward -= 6;
    //}

    //
    void TestMainAiCastle()
    {
        //Check if Ai have all 5 Castle .
        checkCastles();

        //Attack to Main Player Castle if condition is true
        //Round Attack
        if (MainAiCastle.GetComponent<CastelArmyUICount>().sward >= 200 && MainAiCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
        {
            //if(!MainPlayerCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
            //       CreatGroupFromCastleToCastle(MainAiCastle, MainPlayerCastle, 200);
            //else
            //    creatgrouptoNearlyCastle(MainAiCastle,50);
            int RandomCastle = Random.Range(0, OtherCastles.Count);
            if(!OtherCastles[RandomCastle].GetComponent<CastelArmyUICount>().iswhitecastel)
                CreatGroupFromCastleToCastle(MainAiCastle, OtherCastles[RandomCastle], 200);
        }

        //Check if Ai haved Main Player Castle
        foreach (GameObject Castle in AiCastles)
        {
                if (!First4Castle.Contains(Castle) && Castle!=MainAiCastle && Castle.GetComponent<CastelArmyUICount>().sward >= 50)
                        creatgrouptoNearlyCastle(Castle,50);
        }
    }

    //Check if 5 Ai Castle is mine ..
    void checkCastles()
    {
        //if Player have the Main Ai Castle.. Ai will attack on it..
        if (!MainAiCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
        {
            if (AttackeCastle != MainAiCastle)
            {
                AttackeCastle = MainAiCastle;
                StartCoroutine(RunAttack(MainAiCastle));
            }
        }

        //foreach (GameObject Castle in First4Castle)
        //    if(Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
        //        CreatGroupFromCastleToCastle(Castle,MainAiCastle,Castle.GetComponent<CastelArmyUICount>().sward/2);        

        foreach (GameObject Castle in First4Castle)
        {
            if (!Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                if (AttackeCastle != Castle)
                {
                    AttackeCastle = Castle;
                    StartCoroutine(RunAttack(Castle));
                }
            }

            //{
            //    foreach (GameObject Cas in First4Castle)
            //        if (Cas.GetComponent<CastelArmyUICount>().isEnumyCastle)
            //            CreatGroupFromCastleToCastle(Cas, Castle, Cas.GetComponent<CastelArmyUICount>().sward / 2);
            //}
        }
    }

    //if Player have one ai wall Castle 
    IEnumerator RunAttack(GameObject Castle)
    {
        PlayerAttackTowall = true;
        while (true)
        {
            foreach (GameObject Cas in First4Castle)
            {
                if (Cas.GetComponent<CastelArmyUICount>().isEnumyCastle)
                    CreatGroupFromCastleToCastle(Cas, Castle, Cas.GetComponent<CastelArmyUICount>().sward);
            }
            if (Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                break;
            yield return new WaitForSeconds(5f);
        }
        PlayerAttackTowall = false;
        AttackeCastle = MainPlayerCastle;

    }

    //Create group from Castle toword Castle
    void CreatGroupFromCastleToCastle(GameObject CurrentCastle,GameObject TargerCastle,int groupSize)
    {
        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(TargerCastle.transform.position);
        GroupOfUnits g;
        //int swordcount = CurrentCastle.GetComponent<CastelArmyUICount>().sward;
        g = UnitsSpawner.Instance.CreateGroup(groupSize, CirclePosition, GroupType.Sword, OwnerType.Enemy, -1, TargerCastle.transform.position, CurrentCastle.GetComponent<SingleCastle>(), TargerCastle.GetComponent<SingleCastle>());

        CurrentCastle.GetComponent<CastelArmyUICount>().sward -= groupSize;
    }

    //Turn on AddSoliderforfirsttime
    void AddSoliderforfirsttim()
    {

        AddSoliderforfirsttime = true;
        foreach (GameObject cast in First4Castle)
        {
            CastelArmyUICount ca = cast.GetComponent<CastelArmyUICount>();
            cast.transform.GetChild(2).gameObject.transform.GetChild(2).transform.GetChild(2).gameObject.active = true;
            ca.IsExtraSoliders = true;
            cast.GetComponent<CastelArmyUICount>().ExtraiSoliders = 150;
        }
    }

    //Calculat Casttle(Ai + other Castles)
    void CalCastles()
    {
        List<GameObject> AiCas = new List<GameObject>();
        int Aicast = 0;
        List<GameObject> OtherCas = new List<GameObject>();
        int OtherCast = 0;

        foreach (GameObject cast in AllCastle)
        {
            if (cast.GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                AiCas.Add(cast);
                Aicast++;
            }
            else
            {
                OtherCas.Add(cast);
                OtherCast++;
            }
        }


        AiCastles = AiCas;
        OtherCastles = OtherCas;
    }

    /// <summary>wade adds
    /// load cicil level if Player have all Castles or loose All his Castles
    /// </summary>
    public void CheckWinnerPlayer()
    {
        //Defeat Player case
        if (AiCastles.Count == AllCastle.Count)
        {
            Defeat_Panel.SetActive(true);
            PasueController.Instance.IsPasued = true;
            LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
            LostSound.Play();

        }
        //Win Player case
        else if (AiCastles.Count == 0)
        {
            Win_Panel.SetActive(true);
            PasueController.Instance.IsPasued = true;
            //Win_Effects.SetActive(true);
            int MapLevel = 5;
            Saveloadlevel.AddLevel(MapLevel);
            PlayerPrefs.SetInt(MapLevel.ToString(), 1);
            LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
            WinSound.Play();

        }
        //Debug.Log(CastlesSelection.Instance.selectableCastles.Count + "," + Allcastle.Count);
    }
}
