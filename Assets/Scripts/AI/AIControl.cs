﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MarchOfSoldiers;

public class AIControl : MonoBehaviour
{
    //Map Level
    private int MapLevel;

    //All Castle in the game
    public List<GameObject> AllCastle;

    //Ai Castles
    private List<GameObject> AiCastles;

    //Other Castles
    private List<GameObject> OtherCastles;

    //for level 1 (Castle in map level 1 has 100 soliders in the first
    //public GameObject SoloCastle;

    //first Caslte for atack 10 sliders
    public List<GameObject> FirstCastles;

    //Main ai Castle
    public GameObject MainAiCastle;
    bool SetMainAiValue = false;
    bool RunDefens = false;

    AiLevel4 Ailevel4 = new AiLevel4();

    //Win Panel
    public GameObject Win_Panel;
    //Defeat Panel
    public GameObject Defeat_Panel;

    //Win Effects
    public GameObject Win_Effects;

    public SaveLoadLevels Saveloadlevel;

    public AudioSource WinSound;
    public AudioSource LostSound;
    public AudioSource LevelSound;

    private void Start()
    {
        MapLevel = KingdomManager.Instance.MapLevel;

        StartCoroutine(AttackWithCounter(MainAiCastle,10, FirstCastles.Count));


    }

    void Update()
    {
        if (!PasueController.Instance.IsPasued)
        {
            if (!SetMainAiValue)
            {
                if (MapLevel == 1)
                    MainAiCastle.GetComponent<CastelArmyUICount>().sward = 75;
                SetMainAiValue = true;
            }
            //MapLevel = KingdomManager.Instance.MapLevel;
            //Debug.Log(MapLevel);
            //Calculat Ai and Other Castles.
            CalCastles();
            //if (MapLevel == 1 | MapLevel == 2 | MapLevel == 3)
            //{

            if (MapLevel == 2)
            {
                if (!TestFirstCastles(FirstCastles) && RunDefens)
                {
                    StartCoroutine(AttackWithCounter(MainAiCastle, 10, 1));
                    foreach (GameObject Castle in FirstCastles)
                    {
                        if(Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                        StartCoroutine(AttackWithCounter(Castle, 10, 1));
                    }

                }
            }

            if (MapLevel == 3)
                {
                    foreach (GameObject castl in AiCastles)
                        if (castl.GetComponent<CastelArmyUICount>().sward >= 50 /*&& castl != SoloCastle*/)
                            creatgrouptoNearlyCastle(castl, 50);
                }
                //}
            //}
            //check if Map is Map Level 4
            if (MapLevel == 4)
            {
                Ailevel4.AllCastle = AllCastle;
                Ailevel4.AiCastles = AiCastles;
                Ailevel4.OtherCastles = OtherCastles;
                Ailevel4.FirstCastles = FirstCastles;
                Ailevel4.MainAiCastle = MainAiCastle;

                Ailevel4.AiControler();
            }

            CheckWinnerPlayer();
        }
    }

    void CreatGroupFromCastleToCastle(GameObject CurrentCastle, GameObject TargerCastle, int groupSize)
    {
        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(TargerCastle.transform.position);
        GroupOfUnits g;
        //int swordcount = CurrentCastle.GetComponent<CastelArmyUICount>().sward;
        g = UnitsSpawner.Instance.CreateGroup(groupSize, CirclePosition, GroupType.Sword, OwnerType.Enemy, -1, TargerCastle.transform.position, CurrentCastle.GetComponent<SingleCastle>(), TargerCastle.GetComponent<SingleCastle>());

        CurrentCastle.GetComponent<CastelArmyUICount>().sward -= groupSize;
    }

    /// NearlyCastle to the main Castle
    /// </summary>
    public void creatgrouptoNearlyCastle(GameObject Castle,int Soliders_Number)
    {
        GameObject NearCastle = NearlyCastle(Castle);

        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(NearCastle.transform.position);
        GroupOfUnits g;
        int swordcount;
        if (Soliders_Number != 10)
            swordcount = Castle.GetComponent<CastelArmyUICount>().sward;
        else
            swordcount = 10;
        g = UnitsSpawner.Instance.CreateGroup(swordcount, CirclePosition, GroupType.Sword, OwnerType.Enemy, -1, NearCastle.transform.position, Castle.GetComponent<SingleCastle>(), NearCastle.GetComponent<SingleCastle>());

        Castle.GetComponent<CastelArmyUICount>().sward -= Soliders_Number;
    }
    /// <summary>wade adds
    /// Calculate the near castle
    /// </summary>
    public GameObject NearlyCastle(GameObject Castle)
    {
        GameObject castle = OtherCastles[0];
        float dis = Vector3.Distance(Castle.transform.position, OtherCastles[0].transform.position);
        for (int i = 1; i < OtherCastles.Count; i++)
        {
            float distance = Vector3.Distance(Castle.transform.position, OtherCastles[i].transform.position);
            if (dis > distance)
            {
                dis = distance;
                castle = OtherCastles[i];
            }

        }
        return castle;
    }

    //Calculat Casttle(Ai + other Castles)
    void CalCastles()
    {
        List<GameObject> AiCas = new List<GameObject>();
        int Aicast = 0;
        List<GameObject> OtherCas = new List<GameObject>();
        int OtherCast = 0;

        foreach (GameObject cast in AllCastle)
        {
            if (cast.GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                AiCas.Add(cast);
                Aicast++;
            }
            else
            {
                OtherCas.Add(cast);
                OtherCast++;
            }
        }


        AiCastles = AiCas;
        OtherCastles = OtherCas;
    }


    //check if all first castles is ai castle
    bool TestFirstCastles(List<GameObject> Castles)
    {
        bool result = true;
        foreach (GameObject castl in FirstCastles)
        {
            if (!castl.GetComponent<CastelArmyUICount>().isEnumyCastle)
                result = false;
        }
        return result;
    }


    //
    IEnumerator AttackWithCounter(GameObject Castle, int Counter,int steps)
    {
        while (steps >= 0)
        {
            if (Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                if (Castle.GetComponent<CastelArmyUICount>().sward >= 10)
                    creatgrouptoNearlyCastle(Castle, 10);
            }
            steps--;
            yield return new WaitForSeconds(Counter);         
        }
        RunDefens = true;
    }

    public void CheckWinnerPlayer()
    {

        //Defeat Player case
        if (AiCastles.Count == AllCastle.Count)
        {
            Defeat_Panel.SetActive(true);
            PasueController.Instance.IsPasued = true;
            LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
            //LevelSound.Stop();
            LostSound.Play();
        }
        //Win Player case
        else if (AiCastles.Count == 0)
        {
            Win_Panel.SetActive(true);
            PasueController.Instance.IsPasued = true;
            //Win_Effects.SetActive(true);
            Saveloadlevel.AddLevel(MapLevel);
            PlayerPrefs.SetInt(MapLevel.ToString(), 1);
            LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
            //LevelSound.Stop();
            WinSound.Play();
        }
        //Debug.Log(CastlesSelection.Instance.selectableCastles.Count + "," + Allcastle.Count);
    }
}
