﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class AiLevel8 : AiLevel
    {
        public SingleCastle village;
        public List<SingleCastle> specificTargetCastles;
        public List<SingleCastle> mainPlayerCastles;
        public CastleGroup enemyGroup1;
        public CastleGroup enemyGroup2;

        private bool isVillageOwnPlayer = false;
        private bool startAttackOccupiedCastle = false;

        private SingleCastle occupiedCastle;



        private void Awake()
        {
            enemyGroup1.mainCastle.GetComponent<CastelArmyUICount>().StopSoldiersPreduce(20);
            foreach (SingleCastle castle in enemyGroup1.otherCastles)
            {
                castle.GetComponent<CastelArmyUICount>().StopSoldiersPreduce(20);
            }

            enemyGroup2.mainCastle.GetComponent<CastelArmyUICount>().StopSoldiersPreduce(20);
            foreach (SingleCastle castle in enemyGroup2.otherCastles)
            {
                castle.GetComponent<CastelArmyUICount>().StopSoldiersPreduce(20);
            }
        }


        private void Start()
        {
            StartCoroutine(UpdaterStateForEnemyGroup1());
            StartCoroutine(UpdaterStateForEnemyGroup2());
            Invoke("IncreaseSoliderCounter", 2f);
        }

        private void IncreaseSoliderCounter()
        {
            enemyGroup1.mainCastle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
            enemyGroup2.mainCastle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
        }

        private void Update()
        {
            if (PasueController.Instance.IsPasued) return;

            UpdatePlayerCastleState();

            occupiedCastle = CheckIfMainEnemyCastlesIsOccupied();
            if (occupiedCastle != null)
            {
                if (!startAttackOccupiedCastle)
                {
                    startAttackOccupiedCastle = true;
                    StartCoroutine(AttackOccupiedCastleUpdater());
                }
            }

            if (startAttackOccupiedCastle)
            {
                startAttackOccupiedCastle = false;
            }

            if (enemyGroup1.mainCastle.GetComponent<CastelArmyUICount>().sward >= 200)
            {
                EnemyGroup1Attack();
            }
            if (enemyGroup2.mainCastle.GetComponent<CastelArmyUICount>().sward >= 150)
            {
                EnemyGroup2Attack();
            }

            CheckWinnerPlayer(8);
        }

        private void UpdatePlayerCastleState()
        {
            if (village.OwnerType == OwnerType.Player && !isVillageOwnPlayer)
            {
                isVillageOwnPlayer = true;
                foreach (SingleCastle castle in mainPlayerCastles)
                {
                    castle.GetComponent<CastelArmyUICount>().SoldierPlayerCounter = 2;
                }
            }
            else if (village.OwnerType != OwnerType.Player && isVillageOwnPlayer)
            {
                isVillageOwnPlayer = false;
                foreach (SingleCastle castle in mainPlayerCastles)
                {
                    castle.GetComponent<CastelArmyUICount>().SoldierPlayerCounter = 1;
                }
            }
        }

        private SingleCastle CheckIfMainEnemyCastlesIsOccupied()
        {
            if (enemyGroup1.mainCastle.OwnerType == OwnerType.Player) return enemyGroup1.mainCastle;
            foreach (SingleCastle castle in enemyGroup1.otherCastles)
            {
                if (castle.OwnerType == OwnerType.Player) return castle;
            }

            if (enemyGroup2.mainCastle.OwnerType == OwnerType.Player) return enemyGroup2.mainCastle;
            foreach (SingleCastle castle in enemyGroup2.otherCastles)
            {
                if (castle.OwnerType == OwnerType.Player) return castle;
            }

            return null;
        }

        private IEnumerator AttackOccupiedCastleUpdater()
        {
            while (startAttackOccupiedCastle)
            {
                while (PasueController.Instance.IsPasued) yield return null;

                if (enemyGroup1.mainCastle.OwnerType != OwnerType.Player)
                {
                    CreatGroupFromCastleToCastle(enemyGroup1.mainCastle, occupiedCastle, 5);
                    yield return new WaitForSeconds(5);
                }
                foreach (SingleCastle castle in enemyGroup1.otherCastles)
                {
                    if (castle.OwnerType == OwnerType.Player) continue;
                    CreatGroupFromCastleToCastle(castle, occupiedCastle, 5);
                    yield return new WaitForSeconds(5);
                }

                if (enemyGroup2.mainCastle.OwnerType != OwnerType.Player)
                {
                    CreatGroupFromCastleToCastle(enemyGroup2.mainCastle, occupiedCastle, 5);
                    yield return new WaitForSeconds(5);
                }
                foreach (SingleCastle castle in enemyGroup2.otherCastles)
                {
                    if (castle.OwnerType == OwnerType.Player) continue;
                    CreatGroupFromCastleToCastle(castle, occupiedCastle, 5);
                    yield return new WaitForSeconds(5);
                }

                yield return new WaitForSeconds(5);
            }
        }

        private IEnumerator UpdaterStateForEnemyGroup1()
        {
            while (true)
            {
                yield return new WaitForSeconds(10f);
                while (PasueController.Instance.IsPasued) yield return null;
                while (startAttackOccupiedCastle) yield return null;

                foreach (SingleCastle castle in enemyGroup1.otherCastles)
                {
                    if (castle.OwnerType == OwnerType.Player) continue;
                    CreatGroupFromCastleToCastle(castle, enemyGroup1.mainCastle, 5);
                }
            }
        }

        private IEnumerator UpdaterStateForEnemyGroup2()
        {
            while (true)
            {
                yield return new WaitForSeconds(5f);
                while (PasueController.Instance.IsPasued) yield return null;
                while (startAttackOccupiedCastle) yield return null;

                foreach (SingleCastle castle in enemyGroup2.otherCastles)
                {
                    if (castle.OwnerType == OwnerType.Player) continue;
                    CreatGroupFromCastleToCastle(castle, enemyGroup2.mainCastle, 5);
                }
            }
        }

        private void EnemyGroup1Attack()
        {
            if (enemyGroup1.mainCastle.OwnerType == OwnerType.Player) return;

            if (village.OwnerType == OwnerType.Player)
            {
                CreatGroupFromCastleToCastle(enemyGroup1.mainCastle, village, 200);
            }
            else
            {
                List<SingleCastle> playerCastles = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
                SingleCastle randCastle = playerCastles[UnityEngine.Random.Range(0, playerCastles.Count)];
                CreatGroupFromCastleToCastle(enemyGroup1.mainCastle, randCastle, 200);
            }
        }

        private void EnemyGroup2Attack()
        {
            if (enemyGroup2.mainCastle.OwnerType == OwnerType.Player || specificTargetCastles.Count == 0) return;

            foreach (SingleCastle targetCastle in specificTargetCastles)
            {
                if (targetCastle.OwnerType == OwnerType.Player)
                {
                    CreatGroupFromCastleToCastle(enemyGroup2.mainCastle, targetCastle, 150);
                    return;
                }
            }
            SingleCastle playerCastle = GetCloserPlayerCastle(enemyGroup2.mainCastle);
            CreatGroupFromCastleToCastle(enemyGroup2.mainCastle, playerCastle, 150);
        }

        private SingleCastle GetCloserPlayerCastle(SingleCastle castle)
        {
            List<SingleCastle> playerCastles = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
            float closerDis = float.MaxValue;
            SingleCastle closerCastle = null;
            foreach (SingleCastle playerCastle in playerCastles)
            {
                float dis = Vector3.Distance(playerCastle.transform.position, castle.transform.position);
                if (dis < closerDis)
                {
                    closerDis = dis;
                    closerCastle = playerCastle;
                }
            }
            return closerCastle;
        }

    }
}

