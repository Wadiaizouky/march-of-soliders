﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public abstract class AiLevel : MonoBehaviour
    {
        public GameObject Win_Panel;
        public GameObject Defeat_Panel;

        public SaveLoadLevels Saveloadlevel;

        public AudioSource WinSound;
        public AudioSource LostSound;
        public AudioSource LevelSound;

        protected void CreatGroupFromCastleToCastle(SingleCastle currentCastle, SingleCastle targerCastle, int groupSize)
        {
            if (targerCastle == null) return;
            List<Vector2> circlePositions = new List<Vector2>();
            for (int j = 0; j < 20; j++)
            {
                circlePositions.Add(targerCastle.transform.position);
            }
            if (currentCastle.isGate)
            {
                groupSize = Mathf.Min(groupSize, currentCastle.GetComponent<GateArmyUICount>().sward);

                UnitsSpawner.Instance.CreateGroup(groupSize, circlePositions, GroupType.Sword, currentCastle.OwnerType, currentCastle.PlayerOwnerNumber, targerCastle.transform.position, currentCastle, targerCastle);
                currentCastle.GetComponent<GateArmyUICount>().sward -= groupSize;
            }
            else if (currentCastle.isVillage)
            {
                groupSize = Mathf.Min(groupSize, currentCastle.GetComponent<VillageArmyUICount>().sward);

                UnitsSpawner.Instance.CreateGroup(groupSize, circlePositions, GroupType.Sword, currentCastle.OwnerType, currentCastle.PlayerOwnerNumber, targerCastle.transform.position, currentCastle, targerCastle);
                currentCastle.GetComponent<VillageArmyUICount>().sward -= groupSize;
            }
            else
            {
                groupSize = Mathf.Min(groupSize, currentCastle.GetComponent<CastelArmyUICount>().sward);

                UnitsSpawner.Instance.CreateGroup(groupSize, circlePositions, GroupType.Sword, currentCastle.OwnerType, currentCastle.PlayerOwnerNumber, targerCastle.transform.position, currentCastle, targerCastle);
                currentCastle.GetComponent<CastelArmyUICount>().sward -= groupSize;
            }

            // Test
            if (MinimapManger.Instance.TowerNumber == 4 && currentCastle.OwnerType == OwnerType.Enemy && targerCastle.OwnerType == OwnerType.Player
                && !targerCastle.isGate && !targerCastle.isTower && !targerCastle.isVillage)
                targerCastle.GetComponent<CastelArmyUICount>().StartChangeMinimapTower4();

        }

        protected IEnumerator UpdateStateForEnemyGroup(CastleGroup enemyGroup, float rate, int soliderNumber)
        {
            while (true)
            {
                yield return new WaitForSeconds(rate);
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (SingleCastle castle in enemyGroup.otherCastles)
                {
                    if (castle.OwnerType != OwnerType.Enemy) continue;
                    CreatGroupFromCastleToCastle(castle, enemyGroup.mainCastle, soliderNumber);
                }
            }
        }

        protected IEnumerator UpdateStateForEnemyGroupWithAttackStatus(CastleGroup enemyGroup, float rate, int soliderNumber, int limitAttack)
        {
            while (true)
            {
                yield return new WaitForSeconds(rate);
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (SingleCastle castle in enemyGroup.otherCastles)
                {
                    if (castle.OwnerType != OwnerType.Enemy) continue;
                    if (castle.GetComponent<CastelArmyUICount>().sward > limitAttack && enemyGroup.groupStatus == CastleGroup.GroupStatus.Attack) continue;

                    CreatGroupFromCastleToCastle(castle, enemyGroup.mainCastle, soliderNumber);
                }
            }
        }

        protected void EnemyCastleRandomAttack(SingleCastle enemyCastle, int soliderNumber)
        {
            if (enemyCastle.OwnerType == OwnerType.Player) return;

            List<SingleCastle> playerCastles = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
            SingleCastle randCastle = playerCastles[UnityEngine.Random.Range(0, playerCastles.Count)];
            CreatGroupFromCastleToCastle(enemyCastle, randCastle, soliderNumber);
        }

        protected void EnemyGroupRandomAttackWithVilliages(SingleCastle enemyCastle, int soliderNumber)
        {
            if (enemyCastle.OwnerType != OwnerType.Enemy) return;

            List<SingleCastle> playerCastles = new List<SingleCastle>(CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber())); ;
            playerCastles.AddRange(CastlesSelection.Instance.GetSelectableVillagesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()));

            SingleCastle randCastle = playerCastles[UnityEngine.Random.Range(0, playerCastles.Count)];
            CreatGroupFromCastleToCastle(enemyCastle, randCastle, soliderNumber);
        }

        protected void EnemyGroupRandomAttackTowers(SingleCastle enemyCastle, int soliderNumber)
        {
            if (enemyCastle.OwnerType != OwnerType.Enemy) return;

            List<SingleCastle> playerTowers = new List<SingleCastle>(CastlesSelection.Instance.GetSelectableTowersByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber())); ;

            SingleCastle randCastle = playerTowers[UnityEngine.Random.Range(0, playerTowers.Count)];
            CreatGroupFromCastleToCastle(enemyCastle, randCastle, soliderNumber);
        }

        protected void EnemyGroupRandomAttack(CastleGroup enemyGroup, int soliderNumber)
        {
            if (enemyGroup.mainCastle.OwnerType != OwnerType.Enemy) return;

            List<SingleCastle> playerCastles = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
            SingleCastle randCastle = playerCastles[UnityEngine.Random.Range(0, playerCastles.Count)];
            CreatGroupFromCastleToCastle(enemyGroup.mainCastle, randCastle, soliderNumber);
        }

        protected void EnemyGroupRandomAttackWithVilliages(CastleGroup enemyGroup, int soliderNumber)
        {
            if (enemyGroup.mainCastle.OwnerType != OwnerType.Enemy) return;

            List<SingleCastle> playerCastles = new List<SingleCastle>(CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber())); ;
            playerCastles.AddRange(CastlesSelection.Instance.GetSelectableVillagesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()));

            SingleCastle randCastle = playerCastles[UnityEngine.Random.Range(0, playerCastles.Count)];
            CreatGroupFromCastleToCastle(enemyGroup.mainCastle, randCastle, soliderNumber);
        }

        public void CheckWinnerPlayer(int MapLevel)
        {
            int playerCastlesNumber = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()).Count;
            int playerVilliagesNumber = CastlesSelection.Instance.GetSelectableVillagesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()).Count;
            int playerGatesNumber = CastlesSelection.Instance.selectableGates.Count;


            if (playerCastlesNumber == 0 && playerVilliagesNumber == 0
               && CastlesSelection.Instance.selectableGates.Count == 0)
            {
                Defeat_Panel.SetActive(true);
                PasueController.Instance.IsPasued = true;
                LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
                LostSound.Play();

            }
            else if (playerCastlesNumber == SelectionManager.Instance.Allcastle.Count
                && playerGatesNumber == SelectionManager.Instance.Allgate.Count)
            {
                Win_Panel.SetActive(true);
                PasueController.Instance.IsPasued = true;

                if (MapLevel != 15)
                    Saveloadlevel.AddLevel(MapLevel);
                PlayerPrefs.SetInt(MapLevel.ToString(), 1);
                LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
                WinSound.Play();

            }
        }

        public void CheckWinnerPlayer(int MapLevel, List<SingleCastle> mapCastles)
        {
            int playerCastlesNumber = 0;
            int enemyCastlesNumber = 0;

            foreach (SingleCastle castle in mapCastles)
            {
                if (castle.OwnerType == OwnerType.Player)
                {
                    playerCastlesNumber++;
                }
                else if (castle.OwnerType == OwnerType.Enemy)
                {
                    enemyCastlesNumber++;
                }
            }

            if (enemyCastlesNumber == mapCastles.Count)
            {
                Defeat_Panel.SetActive(true);
                PasueController.Instance.IsPasued = true;
                LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
                LostSound.Play();

            }
            else if (playerCastlesNumber == mapCastles.Count)
            {
                Win_Panel.SetActive(true);
                PasueController.Instance.IsPasued = true;

                if (MapLevel != 15)
                    Saveloadlevel.AddLevel(MapLevel);
                PlayerPrefs.SetInt(MapLevel.ToString(), 1);
                LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
                WinSound.Play();
            }
        }

        [Serializable]
        public class CastleGroup
        {
            public enum GroupStatus { Idle, Attack }

            public SingleCastle mainCastle;
            public List<SingleCastle> otherCastles;
            [HideInInspector] public GroupStatus groupStatus = GroupStatus.Idle;
        }
    }
}
