﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class AiLevel6_V2 : AiLevel
    {
        public CastleGroup mainEnemyGroup;
        public SingleCastle mainEnemyGroupGate;
        private CastelArmyUICount mainEnemyGroupMainCastleArmy;
        private List<CastelArmyUICount> mainEnemyGroupCastlesArmy = new List<CastelArmyUICount>();
        private bool mainEnemyGroupAttack = false;
        private SingleCastle mainEnemyGroupOccupiedCastle;
        private bool mainEnemyGroupAi = false;
        //-------------------------------------
        public SingleCastle enemyGroupBackGate;
        public SingleCastle enemyGroupFrontGate;

        public List<SingleCastle> firstCastlesStage;

        public CastleGroup enemyGroup1;
        public CastleGroup enemyGroup2;

        public SingleCastle firstCastle;


        private bool startAttackOccupiedCastle = false;
        private SingleCastle occupiedCastle;

        //public SaveLoadLevels Saveloadlevel;


        private void Awake()
        {
            mainEnemyGroupMainCastleArmy = mainEnemyGroup.mainCastle.GetComponent<CastelArmyUICount>();
            foreach (SingleCastle castle in mainEnemyGroup.otherCastles)
            {
                mainEnemyGroupCastlesArmy.Add(castle.GetComponent<CastelArmyUICount>());
            }
        }

        private void Start()
        {
            Invoke("SetCastlesSoliderNumber", 1f);
            Invoke("IncreaseSoliderCounter", 2f);

            StartCoroutine(FirstCastleUpdater());
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup1, 5f, 5, 10000));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup2, 5f, 5, 10000));
        }

        private void SetCastlesSoliderNumber()
        {
            mainEnemyGroupGate.GetComponent<GateArmyUICount>().sward = 750;
            enemyGroupBackGate.GetComponent<GateArmyUICount>().sward = 75;
            enemyGroupFrontGate.GetComponent<GateArmyUICount>().sward = 250;

        }
        private void IncreaseSoliderCounter()
        {
            mainEnemyGroup.mainCastle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
            foreach (SingleCastle castle in mainEnemyGroup.otherCastles)
            {
                castle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
            }
        }

        private void Update()
        {
            if (PasueController.Instance.IsPasued) return;

            CheckWinnerPlayer(6);

            if (enemyGroup1.mainCastle.GetComponent<CastelArmyUICount>().sward > 100)
            {
                EnemyCastleRandomAttack(enemyGroup1.mainCastle, 100);
            }

            occupiedCastle = CheckIfEnemyCastlesIsOccupied();
            if (occupiedCastle != null)
            {
                if (!startAttackOccupiedCastle)
                {
                    startAttackOccupiedCastle = true;
                    FirstAttackOccupiedCastle();
                    StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup1, 5, 5f));
                    StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup2, 5, 5f));
                }
            }
            else
            {
                startAttackOccupiedCastle = false;
            }


            if (mainEnemyGroupMainCastleArmy.sward > 1000)
            {
                mainEnemyGroupMainCastleArmy.SoldierEnemyCounter = 0;
            }
            else
            {
                mainEnemyGroupMainCastleArmy.SoldierEnemyCounter = 2;
            }

            foreach (CastelArmyUICount castleArmy in mainEnemyGroupCastlesArmy)
            {
                if (castleArmy.sward > 500)
                {
                    castleArmy.SoldierEnemyCounter = 0;
                }
                else
                {
                    castleArmy.SoldierEnemyCounter = 2;
                }
            }


            if (!mainEnemyGroupAi && enemyGroupBackGate.OwnerType == OwnerType.Player)
            {

                mainEnemyGroupAi = true;
                StartCoroutine(MainEnemyCastleUpdater());
                StartCoroutine(MainEnemyGateUpdater());
                StartCoroutine(MainCastleAttackUpdater());
            }

            if (!mainEnemyGroupAi) return;

            //if (mainEnemyGroupMainCastleArmy.sward > 200)
            //{
            //    EnemyCastleRandomAttack(mainEnemyGroupMainCastleArmy.singleCastle, 200);

            //}

            mainEnemyGroupOccupiedCastle = CheckIfMainEnemyGroupCastlesIsOccupied();
            if (mainEnemyGroupOccupiedCastle != null)
            {
                if (!mainEnemyGroupAttack)
                {
                    mainEnemyGroupAttack = true;
                    StartCoroutine(AttackOccupiedCastleUpdaterMainGroup());
                }
            }
            else
            {
                mainEnemyGroupAttack = false;
            }
        }


        private IEnumerator FirstCastleUpdater()
        {
            bool attack = false;
            while (true)
            {
                yield return new WaitForSeconds(10f);
                if (enemyGroup1.groupStatus == CastleGroup.GroupStatus.Attack) continue;
                if (firstCastle.OwnerType != OwnerType.Enemy) continue;

                attack = false;

                foreach (SingleCastle castle in enemyGroup1.otherCastles)
                {
                    if (castle.OwnerType != OwnerType.Enemy)
                    {
                        CreatGroupFromCastleToCastle(firstCastle, castle, 10);
                        attack = true;
                        break;
                    }
                }

                if (attack) continue;

                if (enemyGroup1.mainCastle.OwnerType != OwnerType.Enemy)
                {
                    CreatGroupFromCastleToCastle(firstCastle, enemyGroup1.mainCastle, 10);
                    attack = true;
                    continue;
                }

                if (attack) continue;

                foreach (SingleCastle castle in enemyGroup2.otherCastles)
                {
                    if (castle.OwnerType != OwnerType.Enemy)
                    {
                        CreatGroupFromCastleToCastle(firstCastle, castle, 10);
                        break;
                    }
                }

            }
        }

        private SingleCastle CheckIfEnemyCastlesIsOccupied()
        {
            foreach (SingleCastle castle in firstCastlesStage)
            {
                if (castle.OwnerType == OwnerType.Player) return castle;
            }

            return null;
        }

        private void FirstAttackOccupiedCastle()
        {
            foreach (SingleCastle castle in firstCastlesStage)
            {
                if (castle.OwnerType == OwnerType.Player) continue;

                if (castle.isGate)
                {
                    CreatGroupFromCastleToCastle(castle, occupiedCastle, castle.GetComponent<GateArmyUICount>().sward);
                }
                else
                {
                    CreatGroupFromCastleToCastle(castle, occupiedCastle, castle.GetComponent<CastelArmyUICount>().sward);
                }
            }
        }

        private IEnumerator MainEnemyCastleUpdater()
        {
            List<CastelArmyUICount> armyCastles = new List<CastelArmyUICount>();
            foreach (SingleCastle castle in mainEnemyGroup.otherCastles)
            {
                armyCastles.Add(castle.GetComponent<CastelArmyUICount>());
            }

            while (true)
            {
                yield return new WaitForSeconds(10f);
                while (PasueController.Instance.IsPasued) yield return null;
                while (mainEnemyGroupAttack) yield return null;

                foreach (CastelArmyUICount castleArmy in armyCastles)
                {
                    if (castleArmy.singleCastle.OwnerType == OwnerType.Player) continue;

                    if (castleArmy.sward < 150)
                    {
                        CreatGroupFromCastleToCastle(castleArmy.singleCastle, mainEnemyGroup.mainCastle, 10);
                    }
                    else if (castleArmy.sward > 251)
                    {
                        CreatGroupFromCastleToCastle(castleArmy.singleCastle, mainEnemyGroup.mainCastle, 50);
                    }
                }
            }
        }

        private IEnumerator MainCastleAttackUpdater()
        {

            while (true)
            {
                yield return new WaitForSeconds(20f);
                while (PasueController.Instance.IsPasued) yield return null;
                while (mainEnemyGroupGate.OwnerType != OwnerType.Enemy) yield return null;
                while (mainEnemyGroupMainCastleArmy.sward < 200) yield return null;

                EnemyCastleRandomAttack(mainEnemyGroupMainCastleArmy.singleCastle, 200);
            }
        }

        private IEnumerator MainEnemyGateUpdater()
        {
            bool sendingSolider = false;
            GateArmyUICount gateArmy = mainEnemyGroupGate.GetComponent<GateArmyUICount>();
            while (true)
            {
                yield return new WaitForSeconds(10f);
                while (PasueController.Instance.IsPasued) yield return null;
                while (mainEnemyGroupAttack) yield return null;

                if (gateArmy.sward > 1101) sendingSolider = true;
                if (gateArmy.sward <= 1001) sendingSolider = false;

                if (mainEnemyGroupGate.OwnerType == OwnerType.Player) continue;
                if (!sendingSolider) continue;

                CreatGroupFromCastleToCastle(mainEnemyGroupGate, mainEnemyGroup.mainCastle, 50);
            }
        }

        private SingleCastle CheckIfMainEnemyGroupCastlesIsOccupied()
        {
            if (mainEnemyGroup.mainCastle.OwnerType == OwnerType.Player) return mainEnemyGroup.mainCastle;
            if (mainEnemyGroupGate.OwnerType == OwnerType.Player) return mainEnemyGroupGate;

            foreach (SingleCastle castle in mainEnemyGroup.otherCastles)
            {
                if (castle.OwnerType == OwnerType.Player) return castle;
            }

            return null;
        }

        private IEnumerator AttackOccupiedCastleUpdaterMainGroup()
        {
            List<CastelArmyUICount> armyAttackCastles = new List<CastelArmyUICount>();
            armyAttackCastles.Add(mainEnemyGroup.mainCastle.GetComponent<CastelArmyUICount>());
            foreach (SingleCastle castle in mainEnemyGroup.otherCastles)
            {
                armyAttackCastles.Add(castle.GetComponent<CastelArmyUICount>());
            }

            GateArmyUICount gateArmy = mainEnemyGroupGate.GetComponent<GateArmyUICount>();

            while (mainEnemyGroupAttack)
            {
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (CastelArmyUICount castleArmy in armyAttackCastles)
                {
                    if (castleArmy.singleCastle.OwnerType != OwnerType.Enemy) continue;
                    CreatGroupFromCastleToCastle(castleArmy.singleCastle, mainEnemyGroupOccupiedCastle, castleArmy.sward);
                }

                if (mainEnemyGroupGate.OwnerType == OwnerType.Enemy)
                {
                    CreatGroupFromCastleToCastle(mainEnemyGroupGate, mainEnemyGroupOccupiedCastle, gateArmy.sward);
                }

                yield return new WaitForSeconds(1f);
            }
        }

        private IEnumerator AttackOccupiedCastleUpdater(CastleGroup castleGroup, int soliderNumber, float upadateEvery)
        {
            castleGroup.groupStatus = CastleGroup.GroupStatus.Attack;

            List<CastelArmyUICount> armyAttackCastles = new List<CastelArmyUICount>();
            foreach (SingleCastle castle in castleGroup.otherCastles)
            {
                armyAttackCastles.Add(castle.GetComponent<CastelArmyUICount>());
            }
            armyAttackCastles.Add(castleGroup.mainCastle.GetComponent<CastelArmyUICount>());

            while (startAttackOccupiedCastle)
            {
                yield return new WaitForSeconds(upadateEvery);
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (CastelArmyUICount castle in armyAttackCastles)
                {
                    if (castle == null) continue;
                    if (castle.singleCastle.OwnerType != OwnerType.Enemy) continue;
                    CreatGroupFromCastleToCastle(castle.singleCastle, occupiedCastle, soliderNumber);
                }

            }

            castleGroup.groupStatus = CastleGroup.GroupStatus.Idle;
        }
    }
}