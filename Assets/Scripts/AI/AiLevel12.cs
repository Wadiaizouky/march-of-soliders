﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{

    public class AiLevel12 : AiLevel
    {
        public List<SingleCastle> mapCastles;

        public List<SingleCastle> mainEnemyCastles;
        public List<VillageArmyUICount> enemyVillages;

        public SingleCastle mainEnemyCastle;

        public CastleGroup enemyGroup1;
        public CastleGroup enemyGroup2;
        public CastleGroup enemyGroup3;
        public CastleGroup enemyGroup4;
        public CastleGroup enemyGroup5;
        public CastleGroup enemyGroup6;
        public CastleGroup enemyGroup7;
        public CastleGroup enemyGroup8;
        public CastleGroup enemyGroup9;
        public CastleGroup enemyGroup10;
        public CastleGroup enemyGroup11;
        public CastleGroup enemyGroup12;

        public CastleGroup enemyGroup13;
        public CastleGroup enemyGroup14;

        public CastleGroup enemyGroup15;

        public CastleGroup enemyGroup16;
        public CastleGroup enemyGroup17;


        private CastelArmyUICount mainEnemyCastleArmy;

        private bool startAttackOccupiedCastle = false;
        private SingleCastle occupiedCastle;

        private Coroutine mainEnemyCastleAttackUpdater;


        private void Awake()
        {
            mainEnemyCastleArmy = mainEnemyCastle.GetComponent<CastelArmyUICount>();
        }

        private void Start()
        {
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup1, 10, 8, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup2, 10, 16, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup3, 10, 24, 0));

            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup4, 10, 8, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup5, 10, 16, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup6, 10, 24, 0));

            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup7, 10, 8, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup8, 10, 16, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup9, 10, 24, 0));

            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup10, 10, 8, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup11, 10, 16, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup12, 10, 24, 0));

            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup13, 10, 10, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup14, 10, 10, 0));

            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup15, 10, 24, 0));

            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup16, 10, 6, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup17, 10, 6, 0));



            StartCoroutine(EnemyAllCastleUpdater());
            StartCoroutine(EnemyAllVillagesUpdater());

            StartCoroutine(MainEnemyCastleAttackUpdater());

        }

        private void Update()
        {
            if (PasueController.Instance.IsPasued) return;

            //if (mainEnemyCastleArmy.sward >= 450)
            //{
            //    if (mainEnemyCastleAttackUpdater != null)
            //    {
            //        StopCoroutine(mainEnemyCastleAttackUpdater);
            //    }
            //    mainEnemyCastleAttackUpdater = StartCoroutine(MainEnemyCastleAttackUpdater());

            //}

            occupiedCastle = CheckIfMainEnemyCastlesIsOccupied();
            if (occupiedCastle != null)
            {
                if (!startAttackOccupiedCastle)
                {
                    startAttackOccupiedCastle = true;
                    StartCoroutine(AttackOccupiedCastleUpdater());
                }
            }
            else
            {
                startAttackOccupiedCastle = false;
            }


            CheckWinnerPlayer(12, mapCastles);
        }

        private IEnumerator MainEnemyCastleAttackUpdater()
        {

            while (true)
            {
                while (PasueController.Instance.IsPasued) yield return null;
                while (mainEnemyCastleArmy.sward < 450) yield return null;
                while (mainEnemyCastleArmy.singleCastle.OwnerType == OwnerType.Player) yield return null;

                EnemyGroupRandomAttackWithVilliages(mainEnemyCastle, 150);
                yield return new WaitForSeconds(30);
            }
        }

        private IEnumerator EnemyCastleGroupUpdater(CastleGroup group, int limit, int soldierCount)
        {
            List<CastelArmyUICount> castlesArmy = new List<CastelArmyUICount>();
            foreach (SingleCastle castle in group.otherCastles)
            {
                castlesArmy.Add(castle.GetComponent<CastelArmyUICount>());
            }

            while (true)
            {
                yield return null;
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (CastelArmyUICount castle in castlesArmy)
                {
                    if (castle.sward >= limit)
                    {
                        CreatGroupFromCastleToCastle(castle.singleCastle, group.mainCastle, soldierCount);
                    }

                }
            }
        }

        private IEnumerator EnemyAllCastleUpdater()
        {
            List<CastelArmyUICount> castlesArmy = new List<CastelArmyUICount>();

            foreach (SingleCastle castle in mainEnemyCastles)
            {
                castlesArmy.Add(castle.GetComponent<CastelArmyUICount>());
            }

            while (true)
            {
                yield return new WaitForSeconds(10f);
                while (PasueController.Instance.IsPasued) yield return null;
                while (startAttackOccupiedCastle) yield return null;

                foreach (CastelArmyUICount castleArmy in castlesArmy)
                {
                    if (castleArmy.sward > 100)
                    {
                        CreatGroupFromCastleToCastle(castleArmy.singleCastle, mainEnemyCastle, 50);
                    }
                }
            }
        }

        private IEnumerator EnemyAllVillagesUpdater()
        {
            while (true)
            {
                yield return new WaitForSeconds(10f);
                while (PasueController.Instance.IsPasued) yield return null;
                while (startAttackOccupiedCastle) yield return null;

                foreach (VillageArmyUICount village in enemyVillages)
                {
                    if (village.sward > 300)
                    {
                        CreatGroupFromCastleToCastle(village.singleCastle, mainEnemyCastle, 100);
                    }
                }
            }
        }

        private IEnumerator EnemyCastleGroupUpdater(SingleCastle castle, SingleCastle target, int limit, int soldierCount)
        {
            CastelArmyUICount castleArmy = null;
            VillageArmyUICount villageArmy = null;
            if (castle.isVillage)
            {
                villageArmy = castle.GetComponent<VillageArmyUICount>();
            }
            else
            {
                castleArmy = castle.GetComponent<CastelArmyUICount>();
            }


            while (true)
            {
                yield return null;
                while (PasueController.Instance.IsPasued) yield return null;
                while (startAttackOccupiedCastle) yield return null;

                if (castle.isVillage)
                {
                    if (villageArmy.sward < limit) continue;
                }
                else
                {
                    if (castleArmy.sward < limit) continue;
                }

                CreatGroupFromCastleToCastle(castle, target, soldierCount);
            }
        }

        private SingleCastle CheckIfMainEnemyCastlesIsOccupied()
        {
            foreach (SingleCastle castle in mainEnemyCastles)
            {
                if (castle.OwnerType == OwnerType.Player) return castle;
            }

            if (mainEnemyCastle.OwnerType == OwnerType.Player) return mainEnemyCastle;

            return null;
        }


        private IEnumerator AttackOccupiedCastleUpdater()
        {

            ChangeGroupsState(CastleGroup.GroupStatus.Attack);

            List<CastelArmyUICount> armyAttackCastles = new List<CastelArmyUICount>();
            foreach (SingleCastle castle in mainEnemyCastles)
            {
                armyAttackCastles.Add(castle.GetComponent<CastelArmyUICount>());
            }

            while (startAttackOccupiedCastle)
            {
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (CastelArmyUICount castle in armyAttackCastles)
                {
                    if (castle.sward < 20) continue;
                    if (castle.singleCastle.OwnerType == OwnerType.Player) continue;
                    CreatGroupFromCastleToCastle(castle.singleCastle, occupiedCastle, 20);
                }

                foreach (VillageArmyUICount village in enemyVillages)
                {
                    if (village.sward < 50) continue;
                    if (village.singleCastle.OwnerType == OwnerType.Player) continue;
                    CreatGroupFromCastleToCastle(village.singleCastle, occupiedCastle, village.sward);
                }

                yield return null;
            }

            ChangeGroupsState(CastleGroup.GroupStatus.Idle);
        }

        private void ChangeGroupsState(CastleGroup.GroupStatus state)
        {
            enemyGroup1.groupStatus = state;
            enemyGroup2.groupStatus = state;
            enemyGroup3.groupStatus = state;
            enemyGroup4.groupStatus = state;
            enemyGroup5.groupStatus = state;
            enemyGroup6.groupStatus = state;
            enemyGroup7.groupStatus = state;
            enemyGroup8.groupStatus = state;
            enemyGroup9.groupStatus = state;
            enemyGroup10.groupStatus = state;
            enemyGroup11.groupStatus = state;
            enemyGroup12.groupStatus = state;
            enemyGroup13.groupStatus = state;
            enemyGroup14.groupStatus = state;
            enemyGroup15.groupStatus = state;
            enemyGroup16.groupStatus = state;
            enemyGroup17.groupStatus = state;

        }



    }
}