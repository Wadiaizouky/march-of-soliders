﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MarchOfSoldiers;

public class AiLevel7 : MonoBehaviour
{
    public GameObject Casmera;
    bool Cameramove;

    public List<GameObject> AllGameCastles;

    //All Castle in the section 1
    public List<GameObject> AllCastle;

    //Ai Castles
    private List<GameObject> AiCastles;

    //Other Castles
    private List<GameObject> OtherCastles;

    //First 5 Castle
    public List<GameObject> First5Castle;
    // first village
    public GameObject FirstVillage;

    //Start Soliders Position
    public GameObject StartPoint;

    bool StartAiRun;

    public AudioSource WinSound;
    public AudioSource LostSound;
    public AudioSource LevelSound;


    //Castle ai groups
    public List<GameObject> group1;
    public List<GameObject> group2;
    public List<GameObject> group3;

    //main Player Castle
    public GameObject MainPlayerCastle;

    //// Section 2
    bool StartRunSection2 = false;
    public List<GameObject> Sec2group1;
    public List<GameObject> Sec2group2;
    //main ai Castle in section 2
    public GameObject Sec2MainaiCastle;

    public List<GameObject> Sec2group3;
    public List<GameObject> Sec2group4;

    //ai Village
    public List<GameObject> AiVillages;

    //Win Panel
    public GameObject Win_Panel;
    //Defeat Panel
    public GameObject Defeat_Panel;

    public SaveLoadLevels Saveloadlevel;


    void Start()
    {
        Cameramove = true;
        StartAiRun = false;

        StartCoroutine(step1());

        StartCoroutine(SentSolidersAfter5S());

        StartCoroutine(SentSolidersAfter10S());

        foreach (GameObject village in AiVillages)
            village.GetComponent<VillageArmyUICount>().sward = 75;

    }

    private void Update()
    {
        if (!PasueController.Instance.IsPasued)
        {
            //Calculate Ai and other Castle
            CalCastles();

            AiControler();

            if (Cameramove)
            {
                if (Casmera.transform.position.x > -43.0f)
                    StartCoroutine(wait());
                else
                    Casmera.transform.Translate(0.5f, 0f, 0f);
            }
        }
    }

     void AiControler() { 
            
        if(!MainPlayerCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
            StartAiRun = true;

        if (StartAiRun)
            Step2();

        //Section 2
        if (!StartRunSection2)
        {
            if (StartSection2())
            {
                if (!StartRunSection2)
                    StartCoroutine(step4());
                StartRunSection2 = true;
            }
        }
        if (StartRunSection2)
            step3();
    }

    //wait some seconds befor game begin
    IEnumerator wait()
    {
        yield return new WaitForSeconds(14f);
        Cameramove = false;
        SelectionManager.Instance.GetComponent<MoveMouse>().enabled = true;
        SelectionManager.Instance.GetComponent<SelectionManager>().enabled = true;
        SelectionManager.Instance.GetComponent<DrawSelectionBox>().enabled = true;
    }

    //Sent 50 Player Soliders to 5 ai Castle and village
    IEnumerator step1()
    {
        int i = 0;
        while (i < 5)
        {
            CreatGroupFromPointToCastle(StartPoint,First5Castle[i],50);
            yield return new WaitForSeconds(1f);
            i++;
        }
        CreatGroupFromPointToCastle(StartPoint,FirstVillage,50);

    }

    //ai run
    void Step2()
    {
        if (group1[0].GetComponent<CastelArmyUICount>().sward >= 100 && group1[0].GetComponent<CastelArmyUICount>().isEnumyCastle && !MainPlayerCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
            CreatGroupFromCastleToCastle(group1[0], MainPlayerCastle, 100);
        if (group2[0].GetComponent<CastelArmyUICount>().sward >= 100 && group2[0].GetComponent<CastelArmyUICount>().isEnumyCastle && !MainPlayerCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
            CreatGroupFromCastleToCastle(group2[0], MainPlayerCastle, 100);
        if (group3[0].GetComponent<CastelArmyUICount>().sward >= 100 && group3[0].GetComponent<CastelArmyUICount>().isEnumyCastle && !MainPlayerCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
            CreatGroupFromCastleToCastle(group3[0], MainPlayerCastle, 100);

        if (MainPlayerCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
        {
            //sent 50 Soliders to Player Castle
            foreach (GameObject castl in AiCastles)
            {
                if (!castl.GetComponent<SingleCastle>().isVillage)
                {
                    if (castl.GetComponent<CastelArmyUICount>().sward >= 50)
                        creatgrouptoNearlyCastle(castl, 50);
                }
            }

        }

        CheckWinnerPlayer();

    }

    // section 2
    void step3()
    {



            if (Sec2group1[0].GetComponent<CastelArmyUICount>().sward >= 100 && Sec2group1[0].GetComponent<CastelArmyUICount>().isEnumyCastle && Sec2MainaiCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                CreatGroupFromCastleToCastle(Sec2group1[0], Sec2MainaiCastle, 100);
            if (Sec2group2[0].GetComponent<CastelArmyUICount>().sward >= 100 && Sec2group2[0].GetComponent<CastelArmyUICount>().isEnumyCastle && Sec2MainaiCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                CreatGroupFromCastleToCastle(Sec2group2[0], Sec2MainaiCastle, 100);

            if (Sec2MainaiCastle.GetComponent<CastelArmyUICount>().sward >= 600 && Sec2MainaiCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                RandomAttack();
    }

    IEnumerator step4()
    {
        while (true)
        {
            yield return new WaitForSeconds(51f);
            if (!PasueController.Instance.IsPasued)
                Sent50SoldiersFromCastleToCastle();
        }
    }

    //sent 50 Soldier ..
    void Sent50SoldiersFromCastleToCastle()
    {
        for(int i = 0; i < Sec2group3.Count; i++)
        {
            if(Sec2group3[i].GetComponent<CastelArmyUICount>().sward >=50 && Sec2group3[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
                CreatGroupFromCastleToCastle(Sec2group3[i], Sec2group1[i], 50);
            if (Sec2group4[i].GetComponent<CastelArmyUICount>().sward >= 50 && Sec2group4[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
                CreatGroupFromCastleToCastle(Sec2group4[i], Sec2group2[i], 50);
        }
    }

    // Sent 5 Soliders to main Castle after 5 seconds..
    void sent()
    {
        StartCoroutine(SentSolidersAfter5S());
    }
    IEnumerator SentSolidersAfter5S()
    {
        if (!PasueController.Instance.IsPasued)
        {
            if (group1[0].GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                for (int i = 1; i < group1.Count; i++)
                    if (group1[i].GetComponent<CastelArmyUICount>().sward >=5 && group1[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
                        CreatGroupFromCastleToCastle(group1[i], group1[0], 5);
            }
            if (group2[0].GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                for (int i = 1; i < group2.Count; i++)
                    if (group2[i].GetComponent<CastelArmyUICount>().sward >= 5 && group2[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
                        CreatGroupFromCastleToCastle(group2[i], group2[0], 5);
            }
            if (group3[0].GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                for (int i = 1; i < group3.Count; i++)
                    if (group3[i].GetComponent<CastelArmyUICount>().sward >= 5 && group3[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
                        CreatGroupFromCastleToCastle(group3[i], group3[0], 5);
            }
        }

        yield return new WaitForSeconds(5f);
        sent();
    }

    //sent 50 Soldiers to main casstle after 10 seconds..
    void Section2sent()
    {
        StartCoroutine(SentSolidersAfter10S());
    }
    IEnumerator SentSolidersAfter10S()
    {
        yield return new WaitForSeconds(10f);
        if (!PasueController.Instance.IsPasued && StartRunSection2)
        {
            if (Sec2group1[0].GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                for (int i = 1; i < Sec2group1.Count; i++)
                    if (Sec2group1[i].GetComponent<CastelArmyUICount>().sward >= 20 && Sec2group1[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
                        CreatGroupFromCastleToCastle(Sec2group1[i], Sec2group1[0], 20);
            }
            if (Sec2group2[0].GetComponent<CastelArmyUICount>().isEnumyCastle)
            {
                for (int i = 1; i < Sec2group2.Count; i++)
                    if (Sec2group2[i].GetComponent<CastelArmyUICount>().sward >= 20 && Sec2group2[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
                        CreatGroupFromCastleToCastle(Sec2group2[i], Sec2group2[0], 20);
            }
        }
        Section2sent();
    }


    //Random attack
    void RandomAttack()
    {
        for (int i = 0; i < 4; i++)
        {
            int RandomCastle = Random.Range(0, OtherCastles.Count);
            CreatGroupFromCastleToCastle(Sec2MainaiCastle, OtherCastles[RandomCastle], 150);
        }
    }

    //Condation for Start run Section 2 if all castles is player castle
    bool StartSection2()
    {
        bool Start = true;
        foreach(GameObject Castle in AllCastle)
        {
            if (Castle.GetComponent<SingleCastle>().isVillage)
            {
                if (Castle.GetComponent<VillageArmyUICount>().IsEnemyVillage)
                {
                    Start = false;
                    break;
                }
            }
            else
            {
                if (Castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                {
                    Start = false;
                    break;
                }
            }
        }
        return Start;
    }

    //Create group from Point toword Castle
    void CreatGroupFromPointToCastle(GameObject CurrentPoint, GameObject TargerCastle, int groupSize)
    {
        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(TargerCastle.transform.position);
        GroupOfUnits g;
        //int swordcount = CurrentCastle.GetComponent<CastelArmyUICount>().sward;
        g = UnitsSpawner.Instance.CreateGroup(groupSize, CirclePosition, GroupType.Sword, OwnerType.Player, 1, TargerCastle.transform.position, CurrentPoint.GetComponent<SingleCastle>(), TargerCastle.GetComponent<SingleCastle>());

        //CurrentCastle.GetComponent<CastelArmyUICount>().sward -= groupSize;
    }


    /// NearlyCastle to the main Castle
    /// </summary>
    public void creatgrouptoNearlyCastle(GameObject Castle, int Soliders_Number)
    {
        GameObject NearCastle = NearlyCastle(Castle);

        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(NearCastle.transform.position);
        GroupOfUnits g;
        int swordcount = Castle.GetComponent<CastelArmyUICount>().sward;
        g = UnitsSpawner.Instance.CreateGroup(swordcount, CirclePosition, GroupType.Sword, OwnerType.Enemy, -1, NearCastle.transform.position, Castle.GetComponent<SingleCastle>(), NearCastle.GetComponent<SingleCastle>());

        Castle.GetComponent<CastelArmyUICount>().sward -= Soliders_Number;
    }

    /// <summary>wade adds
    /// Calculate the near castle
    /// </summary>
    public GameObject NearlyCastle(GameObject Castl)
    {
        GameObject castle = OtherCastles[0];
        float dis = Vector3.Distance(Castl.transform.position, OtherCastles[0].transform.position);
        for (int i = 1; i < OtherCastles.Count; i++)
        {
            float distance = Vector3.Distance(Castl.transform.position, OtherCastles[i].transform.position);
            if (dis > distance)
            {
                dis = distance;
                castle = OtherCastles[i];
            }

        }
        return castle;
        // }

    }



    //Create group from Castle toword Castle
    void CreatGroupFromCastleToCastle(GameObject CurrentCastle, GameObject TargerCastle, int groupSize)
    {
        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(TargerCastle.transform.position);
        GroupOfUnits g;
        //int swordcount = CurrentCastle.GetComponent<CastelArmyUICount>().sward;
        g = UnitsSpawner.Instance.CreateGroup(groupSize, CirclePosition, GroupType.Sword, OwnerType.Enemy, -1, TargerCastle.transform.position, CurrentCastle.GetComponent<SingleCastle>(), TargerCastle.GetComponent<SingleCastle>());

        CurrentCastle.GetComponent<CastelArmyUICount>().sward -= groupSize;
    }

    //Calculat Casttle(Ai + other Castles) + villages
    void CalCastles()
    {
        List<GameObject> AiCas = new List<GameObject>();
        int Aicast = 0;
        List<GameObject> OtherCas = new List<GameObject>();
        int OtherCast = 0;

        foreach (GameObject cast in AllCastle)
        {
            if (!cast.GetComponent<SingleCastle>().isVillage)
            {
                if (cast.GetComponent<CastelArmyUICount>().isEnumyCastle)
                {
                    AiCas.Add(cast);
                    Aicast++;
                }
                else
                {
                    OtherCas.Add(cast);
                    OtherCast++;
                }
            }
            else
            {
                if (!cast.GetComponent<VillageArmyUICount>().IsEnemyVillage)
                {
                    OtherCas.Add(cast);
                    OtherCast++;
                }
                else
                {
                    AiCas.Add(cast);
                    Aicast++;
                }
            }
        }


        AiCastles = AiCas;
        OtherCastles = OtherCas;
    }

    /// <summary>wade adds
    /// load cicil level if Player have all Castles or loose All his Castles
    /// </summary>
    public void CheckWinnerPlayer()
    {
        //Defeat Player case
        if (OtherCastles.Count == 0)
        {
            Defeat_Panel.SetActive(true);
            PasueController.Instance.IsPasued = true;
            LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
            LostSound.Play();
        }
        //Win Player case
        List<GameObject> PlayerCastle = new List<GameObject>();
        for(int i=0;i<Sec2group1.Count;i++)
        {
            if (!Sec2group1[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
                PlayerCastle.Add(Sec2group1[i]);
            if (!Sec2group2[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
                PlayerCastle.Add(Sec2group2[i]);
            if (!Sec2group3[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
                PlayerCastle.Add(Sec2group3[i]);
            if (!Sec2group4[i].GetComponent<CastelArmyUICount>().isEnumyCastle)
                PlayerCastle.Add(Sec2group4[i]);
        }
        if(!Sec2MainaiCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
            PlayerCastle.Add(Sec2MainaiCastle);

        if (PlayerCastle.Count + OtherCastles.Count == 38)
        {
            Win_Panel.SetActive(true);
            PasueController.Instance.IsPasued = true;
            //Win_Effects.SetActive(true);
            int MapLevel = 7;
            Saveloadlevel.AddLevel(MapLevel);
            PlayerPrefs.SetInt(MapLevel.ToString(), 1);
            LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
            WinSound.Play();

        }

    }
}
