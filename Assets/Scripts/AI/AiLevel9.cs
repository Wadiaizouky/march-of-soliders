﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class AiLevel9 : AiLevel
    {
        public SingleCastle gate;
        public CastleGroup enemyGroup;


        private void Start()
        {
            StartCoroutine(UpdaterStateForEnemyGroup());
            Invoke("SetGateSoliderSize", 0.5f);
        }

        private void SetGateSoliderSize()
        {
            gate.GetComponent<GateArmyUICount>().sward = 750;
        }

        private IEnumerator UpdaterStateForEnemyGroup()
        {
            while (true)
            {
                yield return new WaitForSeconds(5f);
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (SingleCastle castle in enemyGroup.otherCastles)
                {
                    if (castle.OwnerType == OwnerType.Player) continue;
                    CreatGroupFromCastleToCastle(castle, enemyGroup.mainCastle, 5);
                }
            }
        }

        private void Update()
        {
            if (PasueController.Instance.IsPasued) return;

            if (enemyGroup.mainCastle.GetComponent<CastelArmyUICount>().sward >= 540)
            {
                EnemyGroupAttack();
            }

            CheckWinnerPlayer(9);
        }

        private void EnemyGroupAttack()
        {
            if (enemyGroup.mainCastle.OwnerType == OwnerType.Player) return;

            List<SingleCastle> playerCastles = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());

            SingleCastle randCastle1 = playerCastles[UnityEngine.Random.Range(0, playerCastles.Count)];
            SingleCastle randCastle2 = playerCastles[UnityEngine.Random.Range(0, playerCastles.Count)];
            SingleCastle randCastle3 = playerCastles[UnityEngine.Random.Range(0, playerCastles.Count)];

            CreatGroupFromCastleToCastle(enemyGroup.mainCastle, randCastle1, 150);
            CreatGroupFromCastleToCastle(enemyGroup.mainCastle, randCastle2, 150);
            CreatGroupFromCastleToCastle(enemyGroup.mainCastle, randCastle3, 150);

            CreatGroupFromCastleToCastle(enemyGroup.mainCastle, gate, 90);
        }
    }
}