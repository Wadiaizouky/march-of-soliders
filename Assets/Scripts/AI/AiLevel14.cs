﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class AiLevel14 : AiLevel
    {
        public List<SingleCastle> mapCastles;

        public SingleCastle mainEnemyCastle;
        public List<SingleCastle> mainEnemyCastles;

        public SingleCastle mainPlayerCastle;
        public List<SingleCastle> mainPlayerCastles;

        public CastleGroup enemyGroup1;
        public CastleGroup group2;

        private bool group2CastleIsForPlayer = false;

        private void Start()
        {
            StartCoroutine(EnemyCastleGroupUpdater(enemyGroup1, 10, 10));
            StartCoroutine(MainEnemyCastleAttackUpdater());
            StartCoroutine(EnemyVillageGroupUpdater());
            StartCoroutine(MainGroup1CastleAttackUpdater());

            foreach (SingleCastle castle in group2.otherCastles)
            {
                castle.gameObject.layer = LayerMask.NameToLayer("Default");
            }
        }

        private void Update()
        {
            if (PasueController.Instance.IsPasued) return;
            CheckWinnerPlayer(14, mapCastles);
        }

        private IEnumerator MainGroup1CastleAttackUpdater()
        {
            CastelArmyUICount mainCastleArmy = group2.mainCastle.GetComponent<CastelArmyUICount>();

            while (true)
            {
                yield return null;
                while (PasueController.Instance.IsPasued) yield return null;
                while (group2.mainCastle.OwnerType == OwnerType.Enemy) yield return null;


                if (mainCastleArmy.sward > 400)
                {
                    int numOfAttack = 2;
                    List<SingleCastle> suffleList = Shuffle(new List<SingleCastle>(mainEnemyCastles));

                    foreach (SingleCastle castle in suffleList)
                    {
                        if (castle.OwnerType == OwnerType.Enemy && numOfAttack > 0)
                        {
                            CreatGroupFromCastleToCastle(group2.mainCastle, castle, 200);
                            numOfAttack--;
                        }
                    }
                }
            }
        }

        private IEnumerator MainEnemyCastleAttackUpdater()
        {
            CastelArmyUICount mainEnemyCastleArmy = mainEnemyCastle.GetComponent<CastelArmyUICount>();

            while (true)
            {
                yield return null;
                while (PasueController.Instance.IsPasued) yield return null;
                while (mainEnemyCastle.OwnerType == OwnerType.Player) yield return null;


                if (mainEnemyCastleArmy.sward > 400)
                {
                    int numOfAttack = 2;
                    List<SingleCastle> suffleList = Shuffle(new List<SingleCastle>(mainPlayerCastles));

                    foreach (SingleCastle castle in suffleList)
                    {
                        if (castle.OwnerType == OwnerType.Player && numOfAttack > 0)
                        {
                            CreatGroupFromCastleToCastle(mainEnemyCastle, castle, 200);
                            numOfAttack--;
                        }
                    }
                }
            }
        }

        private IEnumerator EnemyVillageGroupUpdater()
        {
            CastelArmyUICount mainCastleGroupArmy = group2.mainCastle.GetComponent<CastelArmyUICount>();
            List<VillageArmyUICount> villagesArmy = new List<VillageArmyUICount>();
            foreach (SingleCastle village in group2.otherCastles)
            {
                villagesArmy.Add(village.GetComponent<VillageArmyUICount>());
            }

            while (true)
            {
                yield return new WaitForSeconds(1);
                while (PasueController.Instance.IsPasued) yield return null;

                if (group2.mainCastle.OwnerType == OwnerType.Player)
                {
                    if (!group2CastleIsForPlayer)
                    {
                        group2CastleIsForPlayer = true;
                        foreach (VillageArmyUICount village in villagesArmy)
                        {
                            int tempCastleSoldier = village.sward;
                            village.ChangeCastleSprite("Player", 1);
                            village.sward = tempCastleSoldier;
                        }
                    }

                    foreach (VillageArmyUICount village in villagesArmy)
                    {
                        if (village.singleCastle.OwnerType == OwnerType.Player)
                        {
                            village.sward += 2;
                        }
                        if(village.sward > 20)
                        {
                            CreatGroupFromCastleToCastle(village.singleCastle, group2.mainCastle, 20);
                        }
                    }
                }
                else
                {
                    if (group2CastleIsForPlayer)
                    {
                        group2CastleIsForPlayer = false;

                        foreach (VillageArmyUICount village in villagesArmy)
                        {
                            int tempCastleSoldier = village.sward;
                            village.ChangeCastleSprite("Enemy", -1);
                            village.sward = tempCastleSoldier;
                        }
                    }
                }
            }
        }

        private IEnumerator EnemyCastleGroupUpdater(CastleGroup group, int limit, int soldierCount)
        {
            List<CastelArmyUICount> castlesArmy = new List<CastelArmyUICount>();
            foreach (SingleCastle castle in group.otherCastles)
            {
                castlesArmy.Add(castle.GetComponent<CastelArmyUICount>());
            }

            while (true)
            {
                yield return null;
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (CastelArmyUICount castle in castlesArmy)
                {
                    if (castle.singleCastle.OwnerType == OwnerType.Player) continue;
                    if (castle.sward >= limit)
                    {
                        CreatGroupFromCastleToCastle(castle.singleCastle, group.mainCastle, soldierCount);
                    }
                }
            }
        }


        public List<SingleCastle> Shuffle(List<SingleCastle> list)
        {
            System.Random rng = new System.Random();

            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                SingleCastle value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            return list;
        }
    }
}