﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class AiLevel10 : AiLevel
    {
        public SingleCastle village;
        public SingleCastle mainPlayerCastle;
        public List<SingleCastle> playersCastles;
        public List<SingleCastle> mainEnemyCastles;

        public CastleGroup enemyGroup1;
        public CastleGroup enemyGroup2;
        public CastleGroup enemyGroup3;
        public CastleGroup enemyGroup4;
        public CastleGroup enemyGroup5;
        public CastleGroup enemyGroup6;
        public CastleGroup enemyGroup7;

        private bool startAttackOccupiedCastle = false;
        private SingleCastle occupiedCastle;

        private void Start()
        {
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup1, 5f, 5, 100));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup2, 5f, 5, 100));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup3, 10f, 10, 100));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup4, 10f, 10, 100));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup5, 15f, 15, 100));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup6, 10f, 8, 50));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup7, 10f, 5, 100));

            Invoke("SetCastlesSoliderNumber", 1f);
            Invoke("IncreaseSoliderCounter", 2f);
        }

        private void SetCastlesSoliderNumber()
        {
            mainPlayerCastle.GetComponent<CastelArmyUICount>().sward = 50;
            village.GetComponent<VillageArmyUICount>().sward = 75;
            enemyGroup7.mainCastle.GetComponent<CastelArmyUICount>().sward = 500;
        }

        private void IncreaseSoliderCounter()
        {
            enemyGroup7.mainCastle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
        }

        private void Update()
        {
            if (PasueController.Instance.IsPasued) return;

            //UpdatePlayerCastleState();

            if (enemyGroup5.mainCastle.GetComponent<CastelArmyUICount>().sward >= 150)
            {
                EnemyGroupRandomAttackWithVilliages(enemyGroup5, 150);
            }

            if (enemyGroup6.mainCastle.GetComponent<CastelArmyUICount>().sward >= 200)
            {
                EnemyGroupRandomAttackWithVilliages(enemyGroup6, 200);
            }

            if (enemyGroup7.mainCastle.GetComponent<CastelArmyUICount>().sward >= 1200)
            {
                CreatGroupFromCastleToCastle(enemyGroup7.mainCastle, village, 200);
            }

            if (enemyGroup7.mainCastle.OwnerType == OwnerType.Enemy)
            {
                occupiedCastle = CheckIfMainEnemyCastlesIsOccupied();
                if (occupiedCastle != null)
                {
                    if (!startAttackOccupiedCastle)
                    {
                        startAttackOccupiedCastle = true;
                        StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup7.mainCastle, 200, 200, 10f));
                        StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup1, 100, 100, 10f));
                        StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup2, 100, 100, 10f));
                        StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup3, 100, 100, 5f));
                        StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup4, 100, 100, 5f));
                        StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup5, 100, 100, 5f));
                        StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup6, 201, 100, 10f));
                        StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup6, 50, 200, 50, 5f));
                        StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup7, 100, 100, 10f));
                    }
                }
                else
                {
                    startAttackOccupiedCastle = false;
                }

            }

            CheckWinnerPlayer(10);
        }

        //private void UpdatePlayerCastleState()
        //{
        //    if (village.OwnerType == OwnerType.Player)
        //    {
        //        foreach (SingleCastle castle in playersCastles)
        //        {
        //            if (castle.OwnerType == OwnerType.Player)
        //                castle.GetComponent<CastelArmyUICount>().SoldierPlayerCounter = 2;
        //            else
        //                castle.GetComponent<CastelArmyUICount>().SoldierPlayerCounter = 1;
        //        }
        //    }
        //    else if (village.OwnerType != OwnerType.Player)
        //    {
        //        foreach (SingleCastle castle in playersCastles)
        //        {
        //            castle.GetComponent<CastelArmyUICount>().SoldierPlayerCounter = 1;
        //        }
        //    }
        //}

        private SingleCastle CheckIfMainEnemyCastlesIsOccupied()
        {
            foreach (SingleCastle castle in mainEnemyCastles)
            {
                if (castle.OwnerType == OwnerType.Player) return castle;
            }

            return null;
        }

        private IEnumerator AttackOccupiedCastleUpdater(SingleCastle mainAttackCastle, int limitAttack, int soliderNumber, float upadateEvery)
        {
            CastelArmyUICount mainAttackCastleArmy = mainAttackCastle.GetComponent<CastelArmyUICount>();

            while (startAttackOccupiedCastle)
            {
                while (PasueController.Instance.IsPasued) yield return null;
                while (mainAttackCastleArmy.sward < limitAttack) yield return null;

                if (mainAttackCastle.OwnerType != OwnerType.Player)
                {
                    CreatGroupFromCastleToCastle(mainAttackCastle, occupiedCastle, soliderNumber);
                }

                yield return new WaitForSeconds(upadateEvery);
            }
        }

        private IEnumerator AttackOccupiedCastleUpdater(CastleGroup castleGroup, int limitAttack, int soliderNumber, float upadateEvery)
        {
            castleGroup.groupStatus = CastleGroup.GroupStatus.Attack;

            List<CastelArmyUICount> armyAttackCastles = new List<CastelArmyUICount>();
            foreach (SingleCastle castle in castleGroup.otherCastles)
            {
                armyAttackCastles.Add(castle.GetComponent<CastelArmyUICount>());
            }

            while (startAttackOccupiedCastle)
            {
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (CastelArmyUICount castle in armyAttackCastles)
                {
                    if (castle.sward < limitAttack) continue;
                    if (castle.singleCastle.OwnerType == OwnerType.Player) continue;
                    CreatGroupFromCastleToCastle(castle.singleCastle, occupiedCastle, soliderNumber);
                }

                yield return new WaitForSeconds(upadateEvery);
            }

            castleGroup.groupStatus = CastleGroup.GroupStatus.Idle;
        }

        private IEnumerator AttackOccupiedCastleUpdater(CastleGroup castleGroup, int limitAttack, int maxLimitAttack, int soliderNumber, float upadateEvery)
        {
            castleGroup.groupStatus = CastleGroup.GroupStatus.Attack;

            List<CastelArmyUICount> armyAttackCastles = new List<CastelArmyUICount>();
            foreach (SingleCastle castle in castleGroup.otherCastles)
            {
                armyAttackCastles.Add(castle.GetComponent<CastelArmyUICount>());
            }

            while (startAttackOccupiedCastle)
            {
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (CastelArmyUICount castle in armyAttackCastles)
                {
                    if (castle.sward < limitAttack) continue;
                    if (castle.sward > maxLimitAttack) continue;
                    if (castle.singleCastle.OwnerType == OwnerType.Player) continue;
                    CreatGroupFromCastleToCastle(castle.singleCastle, occupiedCastle, soliderNumber);
                }

                yield return new WaitForSeconds(upadateEvery);
            }

            castleGroup.groupStatus = CastleGroup.GroupStatus.Idle;
        }

    }
}