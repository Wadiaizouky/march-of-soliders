﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;

public class AiLevel13 : AiLevel
{
    public List<SingleCastle> mapCastles;

    public List<SingleCastle> mainEnemyCastles;

    public GameObject MainAiCastle;

    public CastleGroup enemyGroup1;
    public CastleGroup enemyGroup2;
    public CastleGroup enemyGroup3;
    public CastleGroup enemyGroup4;
    public CastleGroup enemyGroup5;
    public CastleGroup enemyGroup6;
    public CastleGroup enemyGroup7;
    public CastleGroup enemyGroup8;
    public CastleGroup enemyGroup9;

    private bool startAttackOccupiedCastle = false;
    private SingleCastle occupiedCastle;

    private void Start()
    {
        StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup1, 10, 8,0));
        StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup2, 10, 8,0));

        StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup3, 10, 16,0));
        StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup4, 10, 16,0));

        StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup5, 10, 24,0));
        StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup6, 10, 24,0));

        StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup7, 10, 32,0));

        StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup8, 10, 8,0));

        StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup9, 10, 8,0));

        StartCoroutine(EnemyAllCastleUpdater());

    }

    private void Update()
    {
        if (PasueController.Instance.IsPasued) return;


        //Randowm Attack From ai main Castle
        if (MainAiCastle.GetComponent<CastelArmyUICount>().sward >= 450)
        {
            if (MainAiCastle.GetComponent<SingleCastle>().OwnerType == OwnerType.Enemy)
            {
                StartCoroutine(RandowmAttack(MainAiCastle.GetComponent<SingleCastle>(),3,150));
            }
        }
        if(enemyGroup9.mainCastle.GetComponent<VillageArmyUICount>().sward >= 150)
        {
            if(enemyGroup9.mainCastle.OwnerType == OwnerType.Enemy)
            {
                StartCoroutine(RandowmAttack(enemyGroup9.mainCastle,1,150));
            }
        }

        //Deffens
        occupiedCastle = CheckIfMainEnemyCastlesIsOccupied();
        if (occupiedCastle != null)
        {
            if (!startAttackOccupiedCastle)
            {
                startAttackOccupiedCastle = true;
                StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup1, 20, 20, 1f));
                StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup2, 20, 20, 1f));
                StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup3, 20, 20, 1f));
                StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup4, 20, 20, 1f));
                StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup5, 20, 20, 1f));
                StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup6, 20, 20, 1f));
                StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup7, 20, 20, 1f));
                StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup8, 20, 20, 1f));
                StartCoroutine(AttackOccupiedCastleUpdater(enemyGroup9, 20, 20, 1f));

            }
        }
        else
        {
            startAttackOccupiedCastle = false;
        }


        CheckWinnerPlayer(13, mapCastles);
    }

    private IEnumerator EnemyCastleGroupUpdater(CastleGroup group, int limit, int soldierCount)
    {
        List<CastelArmyUICount> castlesArmy = new List<CastelArmyUICount>();
        foreach (SingleCastle castle in group.otherCastles)
        {
            castlesArmy.Add(castle.GetComponent<CastelArmyUICount>());
        }

        while (true)
        {
            yield return new WaitForSeconds(10f);
            while (PasueController.Instance.IsPasued) yield return null;

            foreach (CastelArmyUICount castle in castlesArmy)
            {
                if (castle.sward >= limit)
                {
                    CreatGroupFromCastleToCastle(castle.singleCastle, group.mainCastle, soldierCount);
                }

            }
        }
    }

    private IEnumerator EnemyAllCastleUpdater()
    {
        List<CastelArmyUICount> castlesArmy = new List<CastelArmyUICount>();

        foreach (SingleCastle castle in mainEnemyCastles)
        {
            castlesArmy.Add(castle.GetComponent<CastelArmyUICount>());
        }

        while (true)
        {
            yield return new WaitForSeconds(10f);
            while (PasueController.Instance.IsPasued) yield return null;

            foreach (CastelArmyUICount castleArmy in castlesArmy)
            {
                if (castleArmy.sward > 101 && castleArmy.isEnumyCastle)
                {
                    CreatGroupFromCastleToCastle(castleArmy.singleCastle, MainAiCastle.GetComponent<SingleCastle>(), 50);
                }
            }
        }
    }

    private IEnumerator EnemyCastleGroupUpdater(SingleCastle castle, SingleCastle target, int limit, int soldierCount)
    {
        CastelArmyUICount castleArmy = null;
        VillageArmyUICount villageArmy = null;
        if (castle.isVillage)
        {
            villageArmy = castle.GetComponent<VillageArmyUICount>();
        }
        else
        {
            castleArmy = castle.GetComponent<CastelArmyUICount>();
        }


        while (true)
        {
            yield return null;
            while (PasueController.Instance.IsPasued) yield return null;

            if (castle.isVillage)
            {
                if (villageArmy.sward < limit) continue;
            }
            else
            {
                if (castleArmy.sward < limit) continue;
            }

            CreatGroupFromCastleToCastle(castle, target, soldierCount);
        }
    }

    private IEnumerator RandowmAttack(SingleCastle Castle,int limit, int SoliderNumber)
    {
        for (int i = 0; i < limit; i++)
        {
            if (!Castle.isVillage)
                EnemyCastleRandomAttack(Castle, SoliderNumber);
            else
                EnemyGroupRandomAttackWithVilliages(Castle, SoliderNumber);
            yield return new WaitForSeconds(1.5f);

        }
    }

    private SingleCastle CheckIfMainEnemyCastlesIsOccupied()
    {
        foreach (SingleCastle castle in mainEnemyCastles)
        {
            if (castle.OwnerType == OwnerType.Player) return castle;
        }

        if (MainAiCastle.GetComponent<SingleCastle>().OwnerType == OwnerType.Player) return MainAiCastle.GetComponent<SingleCastle>();

        return null;
    }

    private IEnumerator AttackOccupiedCastleUpdater(CastleGroup castleGroup, int limitAttack, int soliderNumber, float upadateEvery)
    {
        castleGroup.groupStatus = CastleGroup.GroupStatus.Attack;

        List<CastelArmyUICount> armyAttackCastles = new List<CastelArmyUICount>();
        foreach (SingleCastle castle in castleGroup.otherCastles)
        {
            armyAttackCastles.Add(castle.GetComponent<CastelArmyUICount>());
        }

        while (startAttackOccupiedCastle)
        {
            while (PasueController.Instance.IsPasued) yield return null;

            foreach (CastelArmyUICount castle in armyAttackCastles)
            {
                if (castle.sward < limitAttack) continue;
                if (castle.singleCastle.OwnerType == OwnerType.Player) continue;
                CreatGroupFromCastleToCastle(castle.singleCastle, occupiedCastle, soliderNumber);
            }

            yield return new WaitForSeconds(upadateEvery);
        }

        castleGroup.groupStatus = CastleGroup.GroupStatus.Idle;
    }
}
