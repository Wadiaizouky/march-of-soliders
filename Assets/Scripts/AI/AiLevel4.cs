﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;

public class AiLevel4 : MonoBehaviour
{
    //All Castle in the game
    public List<GameObject> AllCastle;

    //Ai Castles
    public List<GameObject> AiCastles;

    //Other Castles
    public List<GameObject> OtherCastles;

    //first Caslte for atack 10 sliders
    public List<GameObject> FirstCastles;

    //Main ai Castle
    public GameObject MainAiCastle;

    private bool AddSoliderforfirsttime;

    //Win Panel
    public GameObject Win_Panel;
    //Defeat Panel
    public GameObject Defeat_Panel;

    //Win Effects
    public GameObject Win_Effects;

    public SaveLoadLevels Saveloadlevel;

    public AudioSource WinSound;
    public AudioSource LostSound;


    void Start()
    {
        AddSoliderforfirsttime = false;
    }

    //Cotrol Ai in the game
    public void AiControler()
    {
        if (!TestFirstCastles(FirstCastles) && MainAiCastle.GetComponent<CastelArmyUICount>().isEnumyCastle)
        {
            //foreach (GameObject castle in AiCastles)
            //if (castle.GetComponent<CastelArmyUICount>().sward >= 10 && castle != SoloCastle && castle == MainAiCastle)
            if (MainAiCastle.GetComponent<CastelArmyUICount>().sward >= 10)
                creatgrouptoNearlyCastle(MainAiCastle, 10);
        }
        //else
        //{
            foreach (GameObject castl in AiCastles)
                if (castl.GetComponent<CastelArmyUICount>().sward >= 50)
                    creatgrouptoNearlyCastle(castl, 50);
        //}

        //check if Number of Ai Castle 4
        if (AiCastles.Count == 4 && !AddSoliderforfirsttime)
            AddSoliderforfirsttim();
    }

    /// NearlyCastle to the main Castle
    /// </summary>
    public void creatgrouptoNearlyCastle(GameObject Castle, int Soliders_Number)
    {
        GameObject NearCastle = NearlyCastle(Castle);

        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(NearCastle.transform.position);
        GroupOfUnits g;
        int swordcount = Castle.GetComponent<CastelArmyUICount>().sward;
        g = UnitsSpawner.Instance.CreateGroup(swordcount, CirclePosition, GroupType.Sword, OwnerType.Enemy, -1, NearCastle.transform.position, Castle.GetComponent<SingleCastle>(), NearCastle.GetComponent<SingleCastle>());

        Castle.GetComponent<CastelArmyUICount>().sward -= Soliders_Number;
    }
    /// <summary>wade adds
    /// Calculate the near castle
    /// </summary>
    public GameObject NearlyCastle(GameObject Castle)
    {
        GameObject castle = OtherCastles[0];
        float dis = Vector3.Distance(Castle.transform.position, OtherCastles[0].transform.position);
        for (int i = 1; i < OtherCastles.Count; i++)
        {
            float distance = Vector3.Distance(Castle.transform.position, OtherCastles[i].transform.position);
            if (dis > distance)
            {
                dis = distance;
                castle = OtherCastles[i];
            }

        }
        return castle;
    }

    //Turn on AddSoliderforfirsttime
    void AddSoliderforfirsttim() {

        AddSoliderforfirsttime = true;
        foreach (GameObject cast in AiCastles)
        {
            CastelArmyUICount ca = cast.GetComponent<CastelArmyUICount>();
            cast.transform.GetChild(2).gameObject.transform.GetChild(2).transform.GetChild(2).gameObject.active = true;
            ca.IsExtraSoliders = true;
            cast.GetComponent<CastelArmyUICount>().ExtraiSoliders = 100;
        }
    }

    //check if all first castles is ai castle
    bool TestFirstCastles(List<GameObject> Castles)
    {
        bool result = true;
        foreach (GameObject castl in FirstCastles)
        {
            if (!castl.GetComponent<CastelArmyUICount>().isEnumyCastle)
                result = false;
        }
        return result;
    }

    /// <summary>wade adds
    /// load cicil level if Player have all Castles or loose All his Castles
    /// </summary>
    public void CheckWinnerPlayer()
    {
        //Defeat Player case
        if (AiCastles.Count == AllCastle.Count)
        {
            Defeat_Panel.SetActive(true);
            PasueController.Instance.IsPasued = true;
            LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
            LostSound.Play();

        }
        //Win Player case
        else if (AiCastles.Count == 0)
        {
            Win_Panel.SetActive(true);
            PasueController.Instance.IsPasued = true;
            //Win_Effects.SetActive(true);
            int MapLevel = 4;
            Saveloadlevel.AddLevel(MapLevel);
            PlayerPrefs.SetInt(MapLevel.ToString(), 1);
            LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();
            WinSound.Play();

        }
        //Debug.Log(CastlesSelection.Instance.selectableCastles.Count + "," + Allcastle.Count);
    }

}
