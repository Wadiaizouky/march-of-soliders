﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class AiLevel15 : AiLevel
    {
        public List<SingleCastle> mapCastles;

        public List<SingleCastle> mainEnemyCastles;
        public SingleCastle playerMainCastle;
        public SingleCastle enemyMainCastle;

        public List<VillageArmyUICount> villages_GoldSystem;

        public CastleGroup enemyGroup1;
        public CastleGroup enemyGroup2;
        public CastleGroup enemyGroup3;
        public CastleGroup enemyGroup4;
        public CastleGroup enemyGroup5;

        public CastleGroup enemyGroup6;
        public CastleGroup enemyGroup7;


        public CastleGroup enemyGroup8;
        public CastleGroup enemyGroup9;

        public CastleGroup enemyGroup10;

        private bool startAttackOccupiedCastle = false;
        private SingleCastle occupiedCastle;


        private void Start()
        {
            StartCoroutine(EnemyCastleGroupUpdater(enemyGroup1, 10, 10));
            StartCoroutine(EnemyCastleGroupUpdater(enemyGroup2, 10, 10));
            StartCoroutine(EnemyCastleGroupUpdater(enemyGroup3, 10, 10));
            StartCoroutine(EnemyCastleGroupUpdater(enemyGroup4, 10, 10));
            StartCoroutine(EnemyCastleGroupUpdater(enemyGroup5, 10, 10));

            StartCoroutine(UpdateStateForEnemyGroup(enemyGroup6, 10, 20));
            StartCoroutine(UpdateStateForEnemyGroup(enemyGroup7, 10, 20));


            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup8, 10, 20, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup9, 10, 20, 0));
            StartCoroutine(UpdateStateForEnemyGroupWithAttackStatus(enemyGroup10, 10, 20, 0));

            StartCoroutine(EnemyVillageGroupUpdaterRandomAttack(enemyGroup6, 150, 150, 0));
            StartCoroutine(EnemyVillageGroupUpdaterRandomAttack(enemyGroup7, 150, 150, 0));

            StartCoroutine(EnemyVillageGroupUpdaterRandomAttack(enemyGroup8, 200, 200, 30));
            StartCoroutine(EnemyVillageGroupUpdaterRandomAttack(enemyGroup9, 200, 200, 30));

            StartCoroutine(EnemyGroupUpdater(enemyGroup6));
            StartCoroutine(EnemyGroupUpdater(enemyGroup7));
            StartCoroutine(EnemyGroupUpdater(enemyGroup8));
            StartCoroutine(EnemyGroupUpdater(enemyGroup9));
            StartCoroutine(EnemyGroupUpdater(enemyGroup10));


            Invoke("IncreaseSoliderCounter", 2f);
        }


        private void IncreaseSoliderCounter()
        {
            foreach (SingleCastle castle in enemyGroup1.otherCastles)
            {
                castle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
                castle.GetComponent<CastelArmyUICount>().SoldierPlayerCounter = 2;
            }

            foreach (SingleCastle castle in enemyGroup4.otherCastles)
            {
                castle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
                castle.GetComponent<CastelArmyUICount>().SoldierPlayerCounter = 2;
            }

            foreach (SingleCastle castle in enemyGroup6.otherCastles)
            {
                castle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
            }

            foreach (SingleCastle castle in enemyGroup7.otherCastles)
            {
                castle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
            }

            foreach (SingleCastle castle in enemyGroup8.otherCastles)
            {
                castle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
            }

            foreach (SingleCastle castle in enemyGroup9.otherCastles)
            {
                castle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
            }

            foreach (SingleCastle castle in enemyGroup10.otherCastles)
            {
                castle.GetComponent<CastelArmyUICount>().SoldierEnemyCounter = 2;
            }
        }

        private void Update()
        {
            if (PasueController.Instance.IsPasued) return;

            foreach (VillageArmyUICount village in villages_GoldSystem)
            {
                if (village.sward > 200)
                {
                    if (enemyGroup2.mainCastle.OwnerType == OwnerType.Enemy)
                    {
                        CreatGroupFromCastleToCastle(village.singleCastle, enemyMainCastle, 200);
                    }
                    else
                    {
                        CreatGroupFromCastleToCastle(village.singleCastle, playerMainCastle, 200);
                    }
                }
            }


            if (enemyMainCastle.GetComponent<CastelArmyUICount>().sward >= 600)
            {
                if (mainEnemyCastleAttackUpdater != null)
                {
                    StopCoroutine(mainEnemyCastleAttackUpdater);
                }
                mainEnemyCastleAttackUpdater = StartCoroutine(MainEnemyCastleAttackUpdater());
            }

            occupiedCastle = CheckIfMainEnemyCastlesIsOccupied();
            if (occupiedCastle != null)
            {
                if (!startAttackOccupiedCastle)
                {
                    startAttackOccupiedCastle = true;
                    StartCoroutine(AttackOccupiedCastleUpdater());
                    StartCoroutine(AttackOccupiedCastleUpdaterVillages());
                }
            }
            else
            {
                startAttackOccupiedCastle = false;
            }



            CheckWinnerPlayer(15, mapCastles);
        }

        private Coroutine mainEnemyCastleAttackUpdater;


        private IEnumerator MainEnemyCastleAttackUpdater()
        {
            int i = 3;
            while (i > 0)
            {
                while (PasueController.Instance.IsPasued) yield return null;
                while (enemyMainCastle.OwnerType == OwnerType.Player) yield return null;

                if (CastlesSelection.Instance.GetSelectableTowersByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()).Count == 0)
                {
                    EnemyCastleRandomAttack(enemyMainCastle, 200);
                    yield return new WaitForSeconds(30);
                }
                else
                {
                    EnemyGroupRandomAttackTowers(enemyMainCastle, 200);
                    yield return new WaitForSeconds(60);
                }

                i--;
            }
        }

        private SingleCastle CheckIfMainEnemyCastlesIsOccupied()
        {
            foreach (SingleCastle castle in mainEnemyCastles)
            {
                if (castle.OwnerType == OwnerType.Player) return castle;
            }

            if (enemyMainCastle.OwnerType == OwnerType.Player) return enemyMainCastle;

            return null;
        }

        private IEnumerator EnemyGroupUpdater(CastleGroup group)
        {
            List<CastelArmyUICount> castlesArmy = new List<CastelArmyUICount>();

            foreach (SingleCastle castle in mainEnemyCastles)
            {
                castlesArmy.Add(castle.GetComponent<CastelArmyUICount>());
            }

            while (true)
            {
                yield return null;
                while (PasueController.Instance.IsPasued) yield return null;
                while (group.groupStatus == CastleGroup.GroupStatus.Attack) yield return null;
                while (startAttackOccupiedCastle) yield return null;

                foreach (CastelArmyUICount castleArmy in castlesArmy)
                {
                    if (castleArmy.sward > 100)
                    {
                        CreatGroupFromCastleToCastle(castleArmy.singleCastle, group.mainCastle, 50);
                    }
                }
                yield return new WaitForSeconds(10);
            }
        }

        private IEnumerator EnemyVillageGroupUpdaterRandomAttack(CastleGroup group, int limit, int soldierCount, float rate)
        {
            VillageArmyUICount villageArmy = group.mainCastle.GetComponent<VillageArmyUICount>();

            while (true)
            {
                yield return new WaitForSeconds(rate);
                while (PasueController.Instance.IsPasued) yield return null;
                while (group.groupStatus == CastleGroup.GroupStatus.Attack) yield return null;
                while (startAttackOccupiedCastle) yield return null;

                if (villageArmy.sward >= limit)
                {
                    EnemyCastleRandomAttack(villageArmy.singleCastle, soldierCount);
                }

            }
        }



        private IEnumerator EnemyCastleGroupUpdater(CastleGroup group, int limit, int soldierCount)
        {
            List<CastelArmyUICount> castlesArmy = new List<CastelArmyUICount>();
            foreach (SingleCastle castle in group.otherCastles)
            {
                castlesArmy.Add(castle.GetComponent<CastelArmyUICount>());
            }

            while (true)
            {
                yield return null;
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (CastelArmyUICount castle in castlesArmy)
                {
                    if (castle.sward >= limit)
                    {
                        CreatGroupFromCastleToCastle(castle.singleCastle, group.mainCastle, soldierCount);
                    }

                }
            }
        }

        private IEnumerator AttackOccupiedCastleUpdater()
        {

            ChangeGroupsState(CastleGroup.GroupStatus.Attack);

            List<CastelArmyUICount> armyAttackCastles = new List<CastelArmyUICount>();
            foreach (SingleCastle castle in mainEnemyCastles)
            {
                armyAttackCastles.Add(castle.GetComponent<CastelArmyUICount>());
            }

            while (startAttackOccupiedCastle)
            {
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (CastelArmyUICount castle in armyAttackCastles)
                {
                    if (castle.sward < 20) continue;
                    if (castle.singleCastle.OwnerType == OwnerType.Player) continue;
                    CreatGroupFromCastleToCastle(castle.singleCastle, occupiedCastle, 20);
                }

                yield return null;
            }

            ChangeGroupsState(CastleGroup.GroupStatus.Idle);
        }

        private IEnumerator AttackOccupiedCastleUpdaterVillages()
        {
            ChangeGroupsState(CastleGroup.GroupStatus.Attack);

            List<VillageArmyUICount> armyAttackVillages = new List<VillageArmyUICount>();
            armyAttackVillages.Add(enemyGroup8.mainCastle.GetComponent<VillageArmyUICount>());
            armyAttackVillages.Add(enemyGroup9.mainCastle.GetComponent<VillageArmyUICount>());

            while (startAttackOccupiedCastle)
            {
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (VillageArmyUICount village in armyAttackVillages)
                {
                    if (village.sward < 200) continue;
                    if (village.singleCastle.OwnerType == OwnerType.Player) continue;
                    CreatGroupFromCastleToCastle(village.singleCastle, occupiedCastle, 200);
                }

                yield return new WaitForSeconds(5);
            }

            ChangeGroupsState(CastleGroup.GroupStatus.Idle);
        }

        private void ChangeGroupsState(CastleGroup.GroupStatus state)
        {
            enemyGroup8.groupStatus = state;
            enemyGroup9.groupStatus = state;
            enemyGroup10.groupStatus = state;
        }
    }
}