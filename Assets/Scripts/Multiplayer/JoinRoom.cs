﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;

public class JoinRoom : MonoBehaviourPunCallbacks
{
    public Text RoomName;
    bool IsJoinroom = false;

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby();
    }
    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "ru";
        PhotonNetwork.AutomaticallySyncScene = true;

        StartCoroutine(Incris());
        if (IsJoinroom)
        {
            string roomname = RoomName.text;
            PhotonNetwork.JoinRoom(roomname);
            //PhotonNetwork.LoadLevel("Ready For Playing");
            //Debug.Log("Join To Romm : " + RoomName.text + "seccess !.");
            //IsJoinroom = false;
        }
    }

    public void OnJoinRoom_Click()
    {
        IsJoinroom = true;
        //string roomname = RoomName.text;
        //PhotonNetwork.JoinRoom(roomname);
         PhotonNetwork.LeaveRoom();
    }
    //public override void OnJoinedRoom()
    //{
    //    base.OnJoinedRoom();
    //    PhotonNetwork.LoadLevel("Ready For Playing");
    //    Debug.Log("Join To Romm : " + RoomName.text + "seccess !.");
    //}

    public override void OnJoinedRoom()
    {

        if (PhotonNetwork.CurrentRoom.Name != "Lobby")
        {
            base.OnJoinedRoom();  
                Debug.Log("Join To Romm : " + RoomName.text + "seccess !.");
                IsJoinroom = false;
                PhotonNetwork.LoadLevel("Ready For Playing");
        }
    }

    public void MyCorotine()
    {
        StartCoroutine(Incris());
    }

    //this for incris Solider in Castle after 1 second and repeat that ..
    IEnumerator Incris()
    {
        yield return new WaitForSecondsRealtime(3);
        if (IsJoinroom)
        {
            string roomname = RoomName.text;
            PhotonNetwork.JoinRoom(roomname);
            //PhotonNetwork.LoadLevel("Ready For Playing");
            //Debug.Log("Join To Romm : " + RoomName.text + "seccess !.");
            //IsJoinroom = false;
        }

    }
}
