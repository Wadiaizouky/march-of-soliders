﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ButtonReadyPress : MonoBehaviour
{
    //public Button Button;
    public Sprite NotReady;
    public Sprite Ready;

    bool Ispress = false;

    int Player_Number;
    //public List<Image> AllReadyImage;

    GameObject Player;

    public void Onpress()
    {
        Player = GameObject.FindGameObjectWithTag("PlayerP");
        Player_Number = Player.GetComponent<UIMultiplayerPlayer>().Player_number;

        Debug.Log("Player Number ="+ Player_Number);
        //Set Lookally ..
        if (!Ispress)
            UIMultiplayerManger.Instance.AllReady[Player_Number].sprite = Ready;
        else
            UIMultiplayerManger.Instance.AllReady[Player_Number].sprite = NotReady;

        Ispress = !Ispress;

        //Sent Remotly ..
        Player.GetComponent<UIMultiplayerPlayer>().ReceiveReadyorNot(Ispress,Player_Number);
    }
}
