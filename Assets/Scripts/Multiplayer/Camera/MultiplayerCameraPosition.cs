﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace MarchOfSoldiers
{

    public class MultiplayerCameraPosition : MonoBehaviour
    {
        public Camera playerCamera;
        public Transform[] positons;

        private void Start()
        {
            if (!PhotonNetwork.IsConnected || playerCamera == null) return;
            AssignCameraPosition();
        }

        private void AssignCameraPosition()
        {
            int playerNumber = PlayersManager.Instance.GetLocalPlayerNumber();
            playerCamera.transform.position = positons[playerNumber - 1].position;
        }
    }
}