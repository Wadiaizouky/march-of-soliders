﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;

public class DropdownValueSelected : MonoBehaviourPunCallbacks
{
    public Dropdown Dropdownlist;
    public Image image;
    public int Image_Number;

    GameObject Player;
    void Start()
    {
        //if(this.gameObject.tag == "UIColor")
        //    Dropdownlist.options = DropDownManger.Instance.AllDropDownColorData;
        //else
        //    if (this.gameObject.tag == "UITeam")
        //    Dropdownlist.options = DropDownManger.Instance.AllDropDownTeamsData;

    }

    public void OnDropdownvaluechangeForColor()
    {

        //image.sprite = Dropdownlist.GetComponent<Dropdown>().itemImage.sprite;
        int value = Dropdownlist.value;
        List<Dropdown.OptionData> DropData = new List<Dropdown.OptionData>();
        DropData = Dropdownlist.options;

        image.color = Color.white;
        image.sprite = DropData[value].image;

        Image_Number = value;
        //Dropdown.OptionData Data = DropData[value];
        //Update all other DropDownlist value
        //DropDownManger.Instance.DeleteFromlist(value);
        // DropDownManger.Instance.SetAllDropDown();

        Player = GameObject.FindGameObjectWithTag("PlayerP");
        Player.GetComponent<UIMultiplayerPlayer>().RecieveImage(Image_Number);


    }

    public void OnDropdownvaluechangeFprTeam()
    {

        //image.sprite = Dropdownlist.GetComponent<Dropdown>().itemImage.sprite;
        int value = Dropdownlist.value;
        List<Dropdown.OptionData> DropData = new List<Dropdown.OptionData>();
        DropData = Dropdownlist.options;

        image.color = Color.white;
        image.sprite = DropData[value].image;

        Image_Number = value;
        //Dropdown.OptionData Data = DropData[value];
        //Update all other DropDownlist value
        //DropDownManger.Instance.DeleteFromlist(value);
        // DropDownManger.Instance.SetAllDropDown();

        Player = GameObject.FindGameObjectWithTag("PlayerP");
        Player.GetComponent<UIMultiplayerPlayer>().RecieveTeam(Image_Number);


    }


}

