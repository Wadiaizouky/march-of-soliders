﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;

public class GameLobby1 : MonoBehaviourPunCallbacks
{
    string gameverstion = "0.9";
    // public GameObject Panel;

    public InputField CreatRooNname;
    public InputField JoinRooNname;

    bool joiningRoom = false;
    string roomName = "Room 1";
    string playerName = "Player 1";
    string gameVersion = "0.9";
    //The list of created rooms
    List<RoomInfo> createdRooms = new List<RoomInfo>();
    int i = 0;
    Vector2 roomListScroll = Vector2.zero;

    Player[] PlayerConnected;
    List<Player> PlayerConn = new List<Player>();

    string roomname;

    bool IsFindConnectedPlayers = false;
    bool IsFindCRooms = false;

    public GameObject StatusContent;
    public GameObject StatusPlayername;

    public GameObject GameNameContent;
    public GameObject GameNamePlayername;

    public GameObject PlayersContent;
    public Text PlayersPlayername;


    public void Update()
    {
        
        if (!IsFindConnectedPlayers)
        {
            // Debug.Log("PlayerConnected"+PlayerConnected.Length);
            AllPlayerConnecte();
            if (PlayerConnected.Length > 0)
                IsFindConnectedPlayers = true;
        }
        if (!IsFindCRooms)
        {
            Debug.Log(createdRooms.Count);
            //DeleteAllGameName();
            //AllGameNam();
            //DeleteAllPlayers();
            //AllPlayers();
            if (createdRooms.Count > 0)
                IsFindCRooms = true;
        }
        //PlayerConnected = PhotonNetwork.PlayerList;
        //DeleteAllPlayerConnecte();
        //if (Input.GetKeyDown(KeyCode.A))
    }

    //
    //All Players Connected to Photon Network
    void AllPlayerConnecte()
    {
        // DeleteAllPlayerConnecte();
        for (int i = 0; i < PlayerConnected.Length; i++)
        {
            GameObject Copy = Instantiate(StatusPlayername);
            Copy.transform.SetParent(StatusContent.transform);
            // GameObject CopyText = Instantiate(StatusText);
            // CopyText.text = PlayerConnected[i].NickName;
            // CopyText.transform.SetParent(StatusContent.transform);
            Copy.name = PlayerConnected[i].NickName;
            //Copy.transform.parent = StatusContent.transform;
            StatusContent.transform.localPosition = Vector3.zero;

        }
    }
    void DeleteAllPlayerConnecte()
    {
        for (int i = StatusContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(StatusContent.transform.GetChild(i).gameObject);
        }
    }

    //All Game Rooms(All Games)
    void AllGameNam()
    {
        //DeleteAllGameName();
        for (int i = 0; i < createdRooms.Count; i++)
        {
            if (createdRooms[i].Name != "Lobby")
            {
                GameObject Copy = Instantiate(GameNamePlayername);
                Copy.transform.SetParent(GameNameContent.transform);
                // GameObject CopyText = Instantiate(StatusText);
                // CopyText.text = PlayerConnected[i].NickName;
                // CopyText.transform.SetParent(StatusContent.transform);
                Copy.name = createdRooms[i].Name;
                //Copy.transform.parent = StatusContent.transform;
                GameNameContent.transform.localPosition = Vector3.zero;
            }

        }
    }
    void DeleteAllGameName()
    {
        for (int i = GameNameContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(GameNameContent.transform.GetChild(i).gameObject);
        }
    }

    //All Game info
    void AllPlayers()
    {
        // DeleteAllPlayers();
        for (int i = 0; i < createdRooms.Count; i++)
        {
            if (createdRooms[i].Name != "Lobby")
            {
                Text Copy = Instantiate(PlayersPlayername);
                Copy.transform.SetParent(PlayersContent.transform);
                // GameObject CopyText = Instantiate(StatusText);
                // CopyText.text = PlayerConnected[i].NickName;
                // CopyText.transform.SetParent(StatusContent.transform);
                Copy.text = createdRooms[i].PlayerCount + "/" + createdRooms[i].MaxPlayers;
                //Copy.transform.parent = StatusContent.transform;
                PlayersContent.transform.localPosition = Vector3.zero;
            }

        }
    }
    void DeleteAllPlayers()
    {
        for (int i = PlayersContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(PlayersContent.transform.GetChild(i).gameObject);
        }
    }
    //

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);

        createdRooms = roomList;
        Debug.Log(createdRooms.Count);
        DeleteAllGameName();
        DeleteAllPlayers();
        AllGameNam();
        AllPlayers();
        IsFindCRooms = false;
        Debug.Log("RoomList Run");
    }
}
