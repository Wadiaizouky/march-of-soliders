﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenAndCloseDropdown : MonoBehaviour
{
    public Dropdown Dropdownlist;
    GameObject Player;

    public int Drop_Number;

    public void Onchangevaluee()
    {
        Player = GameObject.FindGameObjectWithTag("PlayerP");

        int value = Dropdownlist.value;
        List<Dropdown.OptionData> DropData = new List<Dropdown.OptionData>();
        DropData = Dropdownlist.options;

        Dropdownlist.gameObject.transform.GetChild(1).GetComponent<Image>().sprite = DropData[value].image;

        //if Player Change value Sent Value To all Clients
        if(value == 1)
            Player.GetComponent<UIMultiplayerPlayer>().ReceiveOpen(Drop_Number);
        else
            if(value == 0)
            Player.GetComponent<UIMultiplayerPlayer>().ReceiveNotOpen(Drop_Number);
    }
}
