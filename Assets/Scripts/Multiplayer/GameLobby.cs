﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameLobby : MonoBehaviourPunCallbacks
{
    public bool IsLobby = false;
    bool Refresh = false;

    string gameverstion = "0.9";
   // public GameObject Panel;

    public InputField CreatRooNname;
    public InputField JoinRooNname;

    bool joiningRoom = false;
    string roomName = "Room 1";
    string playerName = "Player 1";
    string gameVersion = "0.9";
    //The list of created rooms
    List<RoomInfo> createdRooms = new List<RoomInfo>();
    int i = 0;
    Vector2 roomListScroll = Vector2.zero;

     Player[] PlayerConnected;
    List<Player> PlayerConn = new List<Player>();

    string roomname;

    bool IsFindConnectedPlayers = false;
    bool IsFindCRooms = false;

    public GameObject StatusContent;
    public GameObject StatusPlayername;

    public GameObject GameNameContent;
    public GameObject GameNamePlayername;

    public GameObject PlayersContent;
    public Text PlayersPlayername;

    void Start()
    {
        //if (!PhotonNetwork.IsConnected)
        //    PhotonNetwork.ConnectUsingSettings();
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    //public override void OnConnectedToMaster()
    //{
    //    base.OnConnectedToMaster();
    //    Debug.Log("Connect Succes to Master Server ..");
    //    PhotonNetwork.JoinLobby();
    //}

    //public override void OnJoinedLobby()
    //{
    //    base.OnJoinedLobby();
    //    Debug.Log("Lobby joined ..");
    //    // Panel.SetActive(true);
    //    PlayerConnected = PhotonNetwork.PlayerList;
    //    textt.text = PlayerConnected.Length.ToString();
    //    Debug.Log(PlayerConnected.Length);
    //    joiningRoom = true;
    //}

    //public void OnCreatRoom_Click()
    //{
    //    string roomname = CreatRooNname.text;
    //    RoomOptions roomOptions = new RoomOptions();
    //    roomOptions.IsOpen = true;
    //    roomOptions.IsVisible = true;
    //    roomOptions.MaxPlayers = (byte)4; //Set any number

    //    PhotonNetwork.JoinOrCreateRoom(roomname, roomOptions, TypedLobby.Default);
    //    //PhotonNetwork.CreateRoom(roomname);
    //}
    //public override void OnCreatedRoom()
    //{
    //    base.OnCreatedRoom();
    //    PhotonNetwork.LoadLevel("Room1");
    //    Debug.Log("Room : "+CreatRooNname.text+" Created !");
    //}

    //public void OnJoinRoom_Click()
    //{
    //    string roomname = JoinRooNname.text;
    //    PhotonNetwork.JoinRoom(roomname);
    //}
    //public override void OnJoinedRoom()
    //{
    //    base.OnJoinedRoom();
    //    PhotonNetwork.LoadLevel("Room1");
    //    Debug.Log("Join To Romm : "+JoinRooNname.text+"seccess !.");
    //}

    //public override void OnJoinRoomFailed(short returnCode, string message)
    //{
    //    base.OnJoinRoomFailed(returnCode, message);
    //    Debug.Log("JoinRoomFailed....");
    //}

    public void Update()
    {
         PlayerConnected = PhotonNetwork.PlayerList;
         GetAllConnectionPlayers();
        if (Input.GetKeyUp(KeyCode.A))
            Debug.Log(PlayerConn.Count);

        if (!IsFindConnectedPlayers)
        {
           // Debug.Log("PlayerConnected"+PlayerConnected.Length);
            AllPlayerConnecte();
            if (PlayerConnected.Length > 0)
                IsFindConnectedPlayers = true;
        }
        if(!IsFindCRooms)
        {
            Debug.Log(createdRooms.Count);
            DeleteAllGameName();
            DeleteAllPlayers();
            AllGameNam();
            AllPlayers();
            if (createdRooms.Count > 0)
                IsFindCRooms = true;
        }
        //PlayerConnected = PhotonNetwork.PlayerList;
        //DeleteAllPlayerConnecte();
        //if (Input.GetKeyDown(KeyCode.A))
    }

    void OnGUI()
    {
        //PlayerConnected = PhotonNetwork.PlayerList;
        ////DeleteAllPlayerConnecte();
        //if(Input.GetKeyDown(KeyCode.A))
        //AllPlayerConnecte();
        //GUI.Window(0, new Rect(Screen.width / 2 - 610, Screen.height / 2 - 180, 240, 180), AllPlayerConnected, "");

        //GUI.Window(1, new Rect(Screen.width / 2 - 370, Screen.height / 2 - 175, 380, 180), AllGameName, "");

        //GUI.Window(2, new Rect(Screen.width / 2 +10, Screen.height / 2 - 175, 120, 180), AllCurrentPlayersInRooms, "");
    }
    void AllPlayerConnected(int index)
    {

        //Connection Status and Room creation Button
        GUILayout.BeginHorizontal();

        //GUILayout.Label("Status: " + PhotonNetwork.connectionStateDetailed);

        if (/*joiningRoom || */!PhotonNetwork.IsConnected)
        {
            GUI.enabled = false;
        }

        GUILayout.FlexibleSpace();

        //Room name text field
        // roomName = GUILayout.TextField(roomName, GUILayout.Width(250));

        //if (GUILayout.Button("Create Room", GUILayout.Width(125)))
        //{
        //    if (roomName != "")
        //    {
        //        joiningRoom = true;

        //        RoomOptions roomOptions = new RoomOptions();
        //        roomOptions.IsOpen = true;
        //        roomOptions.IsVisible = true;
        //        roomOptions.MaxPlayers = (byte)10; //Set any number

        //        PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
        //    }
        //}

        GUILayout.EndHorizontal();

        //Scroll through available rooms
        roomListScroll = GUILayout.BeginScrollView(roomListScroll, false, true);

        if (PlayerConnected.Length == 0)
        {
            GUILayout.Label("No Players were created yet...");
        }
        else
        {
            for (int i = 0; i < PlayerConnected.Length; i++)
            {
               // GUILayout.BeginHorizontal("box");
                GUILayout.Label(PlayerConnected[i].NickName, GUILayout.Width(100));
                //GUILayout.Label(createdRooms[i].PlayerCount + "/" + createdRooms[i].MaxPlayers);

                //GUILayout.FlexibleSpace();

                //if (GUILayout.Button("Join Room"))
                //{
                //    joiningRoom = true;

                //    //Set our Player name
                //    PhotonNetwork.NickName = playerName;

                //    //Join the Room
                //    PhotonNetwork.JoinRoom(createdRooms[i].Name);
                //}
                //GUILayout.EndHorizontal();
            }
        }

        GUILayout.EndScrollView();

        //Set player name and Refresh Room button
        //GUILayout.BeginHorizontal();

        //GUILayout.Label("Player Name: ", GUILayout.Width(85));
        ////Player name text field
        //playerName = GUILayout.TextField(playerName, GUILayout.Width(250));

        //GUILayout.FlexibleSpace();

        //// GUI.enabled = /*PhotonNetwork.connectionState != ConnectionState.Connecting &&*/ !joiningRoom;
        //if (GUILayout.Button("Refresh", GUILayout.Width(100)))
        //{
        //    if (PhotonNetwork.IsConnected)
        //    {
        //        //We are already connected, simply update the Room list
        //        //createdRooms = PhotonNetwork.GetRoomList();
        //        ;
        //    }
        //    else
        //    {
        //        //We are not connected, estabilish a new connection
        //        PhotonNetwork.ConnectUsingSettings();
        //    }
        //}

        //GUILayout.EndHorizontal();

        //if (joiningRoom)
        //{
        //    GUI.enabled = true;
        //    GUI.Label(new Rect(900 / 2 - 50, 400 / 2 - 10, 100, 20), "Connecting...");
        //}
    }

    void AllGameName(int index) {
        //Connection Status and Room creation Button
        GUILayout.BeginHorizontal();

        //GUILayout.Label("Status: " + PhotonNetwork.connectionStateDetailed);

        if (/*joiningRoom || */!PhotonNetwork.IsConnected)
        {
            GUI.enabled = false;
        }

        GUILayout.FlexibleSpace();

        //Room name text field
        // roomName = GUILayout.TextField(roomName, GUILayout.Width(250));

        //if (GUILayout.Button("Create Room", GUILayout.Width(125)))
        //{
        //    if (roomName != "")
        //    {
        //        joiningRoom = true;

        //        RoomOptions roomOptions = new RoomOptions();
        //        roomOptions.IsOpen = true;
        //        roomOptions.IsVisible = true;
        //        roomOptions.MaxPlayers = (byte)10; //Set any number

        //        PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
        //    }
        //}

        GUILayout.EndHorizontal();

        //Scroll through available rooms
        roomListScroll = GUILayout.BeginScrollView(roomListScroll, false, true);

        if (createdRooms.Count == 0)
        {
            GUILayout.Label("No Rooms were created yet...");
        }
        else
        {
            
            for (int i = 0; i < createdRooms.Count; i++)
            {
                if (createdRooms[i].Name != "Lobby")
                {
                     GUILayout.BeginHorizontal("box");
                    GUILayout.Label(createdRooms[i].Name, GUILayout.Width(100));
                    //GUILayout.Label(createdRooms[i].PlayerCount + "/" + createdRooms[i].MaxPlayers);

                    GUILayout.FlexibleSpace();

                    if (GUILayout.Button("Join Room", GUILayout.Width(100)))
                    {
                        roomname = createdRooms[i].Name;
                        PhotonNetwork.LeaveRoom();
                        //    joiningRoom = true;

                        //    //Set our Player name
                        //    PhotonNetwork.NickName = playerName;

                        //    //Join the Room
                        //    PhotonNetwork.JoinRoom(createdRooms[i].Name);
                    }
                    GUILayout.EndHorizontal();
                }
            }
        }

        GUILayout.EndScrollView();

        //Set player name and Refresh Room button
        //GUILayout.BeginHorizontal();

        //GUILayout.Label("Player Name: ", GUILayout.Width(85));
        ////Player name text field
        //playerName = GUILayout.TextField(playerName, GUILayout.Width(250));

        //GUILayout.FlexibleSpace();

        //// GUI.enabled = /*PhotonNetwork.connectionState != ConnectionState.Connecting &&*/ !joiningRoom;
        //if (GUILayout.Button("Refresh", GUILayout.Width(100)))
        //{
        //    if (PhotonNetwork.IsConnected)
        //    {
        //        //We are already connected, simply update the Room list
        //        //createdRooms = PhotonNetwork.GetRoomList();
        //        ;
        //    }
        //    else
        //    {
        //        //We are not connected, estabilish a new connection
        //        PhotonNetwork.ConnectUsingSettings();
        //    }
        //}

        //GUILayout.EndHorizontal();

        //if (joiningRoom)
        //{
        //    GUI.enabled = true;
        //    GUI.Label(new Rect(900 / 2 - 50, 400 / 2 - 10, 100, 20), "Connecting...");
        //}
    }

    void AllCurrentPlayersInRooms(int index)
    {
        //Connection Status and Room creation Button
        GUILayout.BeginHorizontal();

        //GUILayout.Label("Status: " + PhotonNetwork.connectionStateDetailed);

        if (/*joiningRoom || */!PhotonNetwork.IsConnected)
        {
            GUI.enabled = false;
        }

        GUILayout.FlexibleSpace();

        //Room name text field
        // roomName = GUILayout.TextField(roomName, GUILayout.Width(250));

        //if (GUILayout.Button("Create Room", GUILayout.Width(125)))
        //{
        //    if (roomName != "")
        //    {
        //        joiningRoom = true;

        //        RoomOptions roomOptions = new RoomOptions();
        //        roomOptions.IsOpen = true;
        //        roomOptions.IsVisible = true;
        //        roomOptions.MaxPlayers = (byte)10; //Set any number

        //        PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
        //    }
        //}

        GUILayout.EndHorizontal();

        //Scroll through available rooms
        roomListScroll = GUILayout.BeginScrollView(roomListScroll, false, true);

        if (createdRooms.Count == 0)
        {
            GUILayout.Label("No Rooms..");
        }
        else
        {
            for (int i = 0; i < createdRooms.Count; i++)
            {
                if (createdRooms[i].Name != "Lobby")
                {
                    // GUILayout.BeginHorizontal("box");
                    //GUILayout.Label(createdRooms[i].Name, GUILayout.Width(100));
                    GUILayout.Label(createdRooms[i].PlayerCount + "/" + createdRooms[i].MaxPlayers);

                    //GUILayout.FlexibleSpace();

                    //if (GUILayout.Button("Join Room"))
                    //{
                    //    joiningRoom = true;

                    //    //Set our Player name
                    //    PhotonNetwork.NickName = playerName;

                    //    //Join the Room
                    //    PhotonNetwork.JoinRoom(createdRooms[i].Name);
                    //}
                    //GUILayout.EndHorizontal();
                }
            }
        }

        GUILayout.EndScrollView();

        //Set player name and Refresh Room button
        //GUILayout.BeginHorizontal();

        //GUILayout.Label("Player Name: ", GUILayout.Width(85));
        ////Player name text field
        //playerName = GUILayout.TextField(playerName, GUILayout.Width(250));

        //GUILayout.FlexibleSpace();

        //// GUI.enabled = /*PhotonNetwork.connectionState != ConnectionState.Connecting &&*/ !joiningRoom;
        //if (GUILayout.Button("Refresh", GUILayout.Width(100)))
        //{
        //    if (PhotonNetwork.IsConnected)
        //    {
        //        //We are already connected, simply update the Room list
        //        //createdRooms = PhotonNetwork.GetRoomList();
        //        ;
        //    }
        //    else
        //    {
        //        //We are not connected, estabilish a new connection
        //        PhotonNetwork.ConnectUsingSettings();
        //    }
        //}

        //GUILayout.EndHorizontal();

        //if (joiningRoom)
        //{
        //    GUI.enabled = true;
        //    GUI.Label(new Rect(900 / 2 - 50, 400 / 2 - 10, 100, 20), "Connecting...");
        //}
    }

    //
    //All Players Connected to Photon Network
    void AllPlayerConnecte()
    {
       // DeleteAllPlayerConnecte();
      for (int i = 0; i < PlayerConnected.Length; i++)
        { 
            GameObject Copy = Instantiate(StatusPlayername);
            Copy.transform.SetParent(StatusContent.transform);
            // GameObject CopyText = Instantiate(StatusText);
            // CopyText.text = PlayerConnected[i].NickName;
            // CopyText.transform.SetParent(StatusContent.transform);
            Copy.name = PlayerConnected[i].NickName;
            //Copy.transform.parent = StatusContent.transform;
            StatusContent.transform.localPosition = Vector3.zero;

        }
    }
    void DeleteAllPlayerConnecte()
    {
        for (int i = StatusContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(StatusContent.transform.GetChild(i).gameObject);
        }
    }

    //All Game Rooms(All Games)
    void AllGameNam()
    {
        //DeleteAllGameName();
        for (int i = 0; i < createdRooms.Count; i++)
        {
            if (createdRooms[i].Name != "Lobby")
            {
                GameObject Copy = Instantiate(GameNamePlayername);
                Copy.transform.SetParent(GameNameContent.transform);
                // GameObject CopyText = Instantiate(StatusText);
                // CopyText.text = PlayerConnected[i].NickName;
                // CopyText.transform.SetParent(StatusContent.transform);
                Copy.name = createdRooms[i].Name;
            //Copy.transform.parent = StatusContent.transform;
            GameNameContent.transform.localPosition = Vector3.zero;
            }

        }
    }
    void DeleteAllGameName()
    {
        for (int i = GameNameContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(GameNameContent.transform.GetChild(i).gameObject);
        }
    }
    
    //All Game info
    void AllPlayers()
    {
       // DeleteAllPlayers();
        for (int i = 0; i < createdRooms.Count; i++)
        {
             if (createdRooms[i].Name != "Lobby")
             {
            Text Copy = Instantiate(PlayersPlayername);
            Copy.transform.SetParent(PlayersContent.transform);
            // GameObject CopyText = Instantiate(StatusText);
            // CopyText.text = PlayerConnected[i].NickName;
            // CopyText.transform.SetParent(StatusContent.transform);
            Copy.text = createdRooms[i].PlayerCount+"/"+ createdRooms[i].MaxPlayers;
            //Copy.transform.parent = StatusContent.transform;
            PlayersContent.transform.localPosition = Vector3.zero;
             }

        }
    }
    void DeleteAllPlayers()
    {
        for (int i = PlayersContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(PlayersContent.transform.GetChild(i).gameObject);
        }
    }
    //

    public void GetAllConnectionPlayers()
    {
        //Player[] AllConnection_Players;
        //List<Player> PlayerCon = new List<Player>();
           // PlayerConnected = PhotonNetwork.PlayerList;
        for(int i = 0; i < PlayerConnected.Length; i++)
        {
            if(!PlayerConn.Contains(PlayerConnected[i]))
               PlayerConn.Add(PlayerConnected[i]);
        }

    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);
        createdRooms = roomList;
        //DeleteAllGameName();
        //DeleteAllPlayers();
        //AllGameNam();
        //AllPlayers();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        //if(PlayerConnected.Length  > 1 && otherPlayer != PhotonNetwork.LocalPlayer)
        
        if (otherPlayer.NickName != PhotonNetwork.LocalPlayer.NickName)
        {
            PlayerConnected = PhotonNetwork.PlayerList;
            DeleteAllPlayerConnecte();
           // DeleteAllGameName();
           // DeleteAllPlayers();
            AllPlayerConnecte();
            //AllGameNam();
            //AllPlayers();
            
        }

        StartCoroutine(Incris());
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        // StartCoroutine(Incris());

        PlayerConnected = PhotonNetwork.PlayerList;
        DeleteAllPlayerConnecte();
        //DeleteAllGameName();
        //DeleteAllPlayers();
        AllPlayerConnecte();
        //AllGameNam();
        //AllPlayers();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("Connect Succes to Master Server ..");
        PhotonNetwork.JoinLobby();
    }
    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        Lobbychange(IsLobby);
        //Debug.Log("Lobby joined ..");
        //RoomOptions roomOptions = new RoomOptions();

        //PhotonNetwork.JoinOrCreateRoom("Lobby", roomOptions, TypedLobby.Default);
        ////PhotonNetwork.NickName = Nickname.text;
        //PhotonNetwork.LoadLevel("Lobby Multiplayer");

    }

    void Lobbychange(bool inlobby)
    {
        if (inlobby)
        {
            Debug.Log("Lobby joined ..");
            RoomOptions roomOptions = new RoomOptions();

            PhotonNetwork.JoinOrCreateRoom("Lobby", roomOptions, TypedLobby.Default);
            IsLobby =false;
            StartCoroutine(Incris1());

            //PhotonNetwork.NickName = Nickname.text;
            //PhotonNetwork.LoadLevel("Lobby Multiplayer");
            //DeleteAllGameName();
            //DeleteAllPlayers();
            //AllGameNam();
            //AllPlayers();
        }
    }

    IEnumerator Incris()
    {
        yield return new WaitForSecondsRealtime(7);
        IsLobby = true;
        //if (!Refresh)
        //{
            PhotonNetwork.LeaveRoom();
        //    Refresh = true;
        //}
    }

    IEnumerator Incris1()
    {
        yield return new WaitForSecondsRealtime(4);
        DeleteAllGameName();
        DeleteAllPlayers();
        AllGameNam();
        AllPlayers();
    }

    //Exit Multiplayer and Go to main Scene
    public void OnExitMultiplayer()
    {
        PhotonNetwork.Disconnect();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        SceneManager.LoadScene("Start scene");
    }
}