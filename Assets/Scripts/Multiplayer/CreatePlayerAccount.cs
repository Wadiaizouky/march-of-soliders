﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;

public class CreatePlayerAccount : MonoBehaviourPunCallbacks
{
    public InputField NickName;
    public InputField Password;
    public InputField ConfPassword;
    public InputField Email;
    public InputField ConfEmail;

    //void Start()
    //{
    //    if (!PhotonNetwork.IsConnected)
    //        PhotonNetwork.ConnectUsingSettings();
    //}

    //public override void OnConnectedToMaster()
    //{
    //    base.OnConnectedToMaster();
    //    Debug.Log("Connect Succes to Master Server ..");
    //    PhotonNetwork.JoinLobby();
    //}
    //public override void OnJoinedLobby()
    //{
    //    base.OnJoinedLobby();
    //    Debug.Log("Lobby joined ..");
    //    RoomOptions roomOptions = new RoomOptions();

    //    PhotonNetwork.JoinOrCreateRoom("Lobby", roomOptions, TypedLobby.Default);
    //    PhotonNetwork.NickName = "Player Name";
    //    PhotonNetwork.LoadLevel("Lobby Multiplayer");
    //}

    //void Update() {
    //    if (!PhotonNetwork.IsConnected)
    //        PhotonNetwork.ConnectUsingSettings();
    //}

    public void OnCreate()
    {
        if(NickName.text != "" && Password.text != "")
        {
            if(Password.text == ConfPassword.text && Email.text == ConfEmail.text)
            {
                    PhotonNetwork.NickName = NickName.text;
                    //PhotonNetwork.AuthValues.UserId = Password.text;

                PhotonNetwork.LoadLevel("login");
            }
        }
    }
}
