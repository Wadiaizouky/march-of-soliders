﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class PlayersManager : Singleton_Photon<PlayersManager>
    {
        public bool networkMap = false;
        public GameObject playerPrefab;


        public List<Player> Players { get; protected set; }
        public Dictionary<int, Player> PlayersMap { get; protected set; }
        public Player LocalPlayer { get; protected set; }


        public Dictionary<int, List<Player>> TeamsPlayers { get; protected set; }
        public List<int> Teams { get; protected set; }
        public int TeamsNumber { get; protected set; }


        private void Awake()
        {
            Players = new List<Player>();
            PlayersMap = new Dictionary<int, Player>();
            TeamsPlayers = new Dictionary<int, List<Player>>();
            Teams = new List<int>();

            if (!networkMap)
                SetSinglePlayerConfig();
            else
                SpawnPlayer();
        }

        public void SpawnPlayer()
        {
            PhotonNetwork.Instantiate(playerPrefab.name, Vector3.zero, Quaternion.identity);
        }

        public void AddPlayer(Player player, int actorNumber)
        {
            Photon.Realtime.Player photonPlayer = GetPhotonPlayerByActorNumber(actorNumber);
            player.playerNumber = photonPlayer.ActorNumber;
            player.playerTeam = photonPlayer.ActorNumber;
            player.playerName = photonPlayer.NickName;
            player.playerColor = photonPlayer.CustomProperties["Player_Color"].ToString();
            player.isMasterClient = photonPlayer.IsMasterClient;
            player.isLocalPlayer = photonPlayer.IsLocal;

            Players.Add(player);

            if (photonPlayer.IsLocal)
            {
                LocalPlayer = player;
            }

            TeamsNumber++;

            PlayersMap.Add(player.playerNumber, player);

            if (!TeamsPlayers.ContainsKey(player.playerTeam))
            {
                List<Player> Team = new List<Player>();

                TeamsPlayers.Add(player.playerTeam, Team);
                Teams.Add(player.playerTeam);
            }

            TeamsPlayers[player.playerTeam].Add(player);
        }

        private Photon.Realtime.Player GetPhotonPlayerByActorNumber(int actorNumber)
        {
            foreach (Photon.Realtime.Player photonPlayer in PhotonNetwork.PlayerList)
            {
                if (photonPlayer.ActorNumber == actorNumber)
                {
                    return photonPlayer;
                }
            }
            return null;
        }

        private void SetSinglePlayerConfig()
        {
            Player player = Instantiate(playerPrefab, transform).GetComponent<Player>();
            player.playerNumber = 1;
            player.playerTeam = 1;
            player.isMasterClient = true;
            player.isLocalPlayer = true;
            player.playerColor = "Red";
            Player enemy = Instantiate(playerPrefab, transform).GetComponent<Player>();
            enemy.playerNumber = -1;
            enemy.playerTeam = -1;
            enemy.isMasterClient = false;
            enemy.isLocalPlayer = false;
            enemy.playerColor = "Blue";

            Players.Add(player);
            Players.Add(enemy);

            LocalPlayer = player;
            TeamsNumber = 2;

            PlayersMap.Add(player.playerNumber, player);
            PlayersMap.Add(enemy.playerNumber, enemy);

            List<Player> Team1 = new List<Player>();
            List<Player> Team2 = new List<Player>();

            Team1.Add(player);
            Team2.Add(enemy);

            TeamsPlayers.Add(1, Team1);
            TeamsPlayers.Add(-1, Team2);

            Teams.Add(1);
            Teams.Add(-1);
        }

        public int GetLocalPlayerNumber()
        {
            return LocalPlayer.playerNumber;
        }

        public Player GetPlayer(int playerNumber)
        {
            if (!PlayersMap.ContainsKey(playerNumber)) return null;
            return PlayersMap[playerNumber];
        }

        public int GetPlayerTeamNumber(int playerNumber)
        {
            return PlayersMap[playerNumber].playerTeam;
        }

        public List<Player> GetTeamPlayers(int teamNumber)
        {
            if (!TeamsPlayers.ContainsKey(teamNumber)) return null;
            return TeamsPlayers[teamNumber];
        }

    }
}
