﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DropDownManger : MonoBehaviour
{
    public List<Dropdown.OptionData> AllDropDownColorData;
    public List<Dropdown.OptionData> AllDropDownTeamsData;
    public List<Dropdown> AllDropDown;

    private static DropDownManger _instance = null;


    public static DropDownManger Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<DropDownManger>();
            return _instance;
        }
    }

    public void DeleteFromlist(int val)
    {

        AllDropDownColorData.RemoveAt(val);
    }

    public void SetAllDropDown()
    {
        foreach(Dropdown dow in AllDropDown)
        {
            dow.options = AllDropDownColorData;
        }
    }

}
