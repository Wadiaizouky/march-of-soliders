﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KichButtonClick : MonoBehaviour
{
    public int PlayerKicher_Number;
    GameObject Player;

    public void OnKichClicked()
    {
        Player = GameObject.FindGameObjectWithTag("PlayerP");

        Player.GetComponent<UIMultiplayerPlayer>().ReceiveKich(PlayerKicher_Number);

    }
}
