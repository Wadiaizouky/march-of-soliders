﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;


public class RoomController : MonoBehaviourPunCallbacks
{
    //Player instance prefab, must be located in the Resources folder
    public GameObject playerPrefab;
    //Player spawn point
    public Transform spawnPoint;

    private Player[] playerList;

    // Use this for initialization
    void Start()
    {


        playerList = PhotonNetwork.PlayerList;
        //In case we started this demo with the wrong scene being active, simply load the menu scene
        if (!PhotonNetwork.IsConnected)
        {
            //UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
            return;
        }

        //We're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
        PhotonNetwork.Instantiate(playerPrefab.name, spawnPoint.position, Quaternion.identity, 0);
        //Debug.Log(playerList.Length);
    }

    private void OnGameStart()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel("Lamosa Forest (1V1)");
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel("Lamosa Forest (1V1)");
        }

    }

    //void OnGUI()
    //{
    //    playerList = PhotonNetwork.PlayerList;

    //    if (PhotonNetwork.CurrentRoom.PlayerCount == (byte)0)
    //        return;

    //    //Leave this Room
    //    if (GUI.Button(new Rect(5, 5, 125, 25), "Leave Room"))
    //    {
    //        PhotonNetwork.LeaveRoom();
    //    }

    //    //Show the Room name
    //    GUI.Label(new Rect(135, 5, 200, 25), PhotonNetwork.CurrentRoom.Name);

    //    //Show the list of the players connected to this Room
    //    for (int i = 0; i < PhotonNetwork.CurrentRoom.PlayerCount; i++)
    //    {
    //        //Show if this player is a Master Client. There can only be one Master Client per Room so use this to define the authoritative logic etc.)
    //        string isMasterClient = (playerList[i].IsMasterClient ? ": MasterClient" : "");
    //        GUI.Label(new Rect(5, 35 + 30 * i, 200, 25), playerList[i].NickName + isMasterClient);
    //    }
    //}


    //public override void OnLeftRoom()
    //{
    //    base.OnLeftRoom();
    //    Debug.Log("Player left Room "+PhotonNetwork.CurrentRoom.Name);
    //    Application.Quit();
    //}

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("Connect Succes to Master Server ..");
        PhotonNetwork.JoinLobby();
    }
    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        Debug.Log("Lobby joined ..");
        RoomOptions roomOptions = new RoomOptions();

        PhotonNetwork.JoinOrCreateRoom("Lobby", roomOptions, TypedLobby.Default);
        //PhotonNetwork.NickName = Nickname.text;
        PhotonNetwork.LoadLevel("Lobby Multiplayer");

    }

    public void OnBackToLobby()
    {
        PhotonNetwork.LeaveRoom();
    }

}