﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;

public class UIMultiplayerPlayer : MonoBehaviourPun, IPunObservable
{
    public int Player_number;
    public string Player_Name;
    public string Color;
    public int Team;
    bool IsReady;

    public Sprite Ready_Image;
    public Sprite NotReady_Image;
    Player[] Players;
    //public PhotonView pv;

    void Start()
    {
        Players = PhotonNetwork.PlayerList;
        //pv.ViewID = Random.Range(1,999);
        //this.name = PhotonNetwork.NickName;

        //Allplayers = new Text[8];
        //if (PhotonNetwork.IsMasterClient)
        //    Player_number = 0;
        //else
        //{
        //    Player_number = Random.Range(1, 7);
        //}
        Player_number = UIMultiplayerManger.Instance.GetPlayer_Number();

        if (this.photonView.IsMine)
        {
            Player_Name = PhotonNetwork.NickName;

            UIMultiplayerManger.Instance.Set_name(PhotonNetwork.NickName, Player_number);
            UIMultiplayerManger.Instance.Active_Drop(Player_number);
            //UIMultiplayerManger.Instance.AllReady[Player_number].GetComponent<Button>().enabled = true;

            this.photonView.RPC("Receive_Name", RpcTarget.OthersBuffered, new object[] { PhotonNetwork.NickName, Player_number });

            //Activa All Open And Close DroupDown When Client is Master Server...
            if (PhotonNetwork.IsMasterClient)
            {
                for (int i = 0; i < UIMultiplayerManger.Instance.AllOpenAndClose.Count; i++)
                    UIMultiplayerManger.Instance.AllOpenAndClose[i].GetComponent<Dropdown>().enabled = true;

                for (int i = 0; i < UIMultiplayerManger.Instance.AllOpenAndClose.Count; i++)
                    UIMultiplayerManger.Instance.AllKich[i].GetComponent<Button>().enabled = true;

                for (int i = 0; i < UIMultiplayerManger.Instance.AllMapButtons.Count; i++)
                    UIMultiplayerManger.Instance.AllMapButtons[i].GetComponent<Button>().enabled = true;

                UIMultiplayerManger.Instance.StartGame.GetComponent<Button>().enabled = true;
                UIMultiplayerManger.Instance.StartGame.transform.GetChild(3).gameObject.SetActive(false);
            }
        }

        // pv.RPC("Active_Droup", RpcTarget.OthersBuffered, new object[] { Player_number });

    }

    void Update()
    {
        if (this.photonView.IsMine)
        {
            //Sent Player Sprite to all other players
            //if (UIMultiplayerManger.Instance.AllColors[Player_number].sprite != null)
            //{
            //    this.photonView.RPC("Receive_Color", RpcTarget.OthersBuffered, new object[] { UIMultiplayerManger.Instance.AllColors[Player_number].GetComponent<DropdownValueSelected>().Image_Number, Player_number });
            //    Set_Player_Color(UIMultiplayerManger.Instance.AllColors[Player_number].GetComponent<DropdownValueSelected>().Image_Number);
            //}

            //Sent Player is Ready or not to all Players
            //if (UIMultiplayerManger.Instance.AllReady[Player_number].sprite == Ready_Image)
            //{
            //    UIMultiplayerManger.Instance.AllReadyOrNotReady[Player_number] = true;
            //    this.photonView.RPC("Receive_Ready", RpcTarget.OthersBuffered, Player_number);
            //}

            //if (UIMultiplayerManger.Instance.AllReady[Player_number].image.sprite == NotReady_Image)
            //    this.photonView.RPC("Receive_Not_Ready", RpcTarget.OthersBuffered, new object[] { Player_number });
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            Debug.Log(Player_Name + ":" + Player_number);
        }

    }

    [PunRPC]
    public void Receive_Name(string Player_Nam, int Player_N)
    {
        UIMultiplayerManger.Instance.Set_name(Player_Nam, Player_N);
    }


    public void RecieveImage(int ImageNumber)
    {
        this.photonView.RPC("Receive_Color", RpcTarget.OthersBuffered, new object[] { ImageNumber, Player_number });
        Set_Player_Color(ImageNumber);
    }

    [PunRPC]
    public void Receive_Color(int sprite_Number, int Player_N)
    {
        UIMultiplayerManger.Instance.Set_Color(sprite_Number, Player_N);
        //UIMultiplayerManger.Instance.DeletSelectedPlayerSprite(sprite_Number);
    }


    public void RecieveTeam(int TeamNumber)
    {
        this.photonView.RPC("Receive_Team", RpcTarget.OthersBuffered, new object[] { TeamNumber, Player_number });
        Set_Player_Team(TeamNumber);
    }

    [PunRPC]
    public void Receive_Team(int sprite_Number, int Player_N)
    {
        UIMultiplayerManger.Instance.Set_team(sprite_Number, Player_N);
    }


    public void ReceiveReadyorNot(bool IsReady, int Player_N)
    {
        if (IsReady)
            this.photonView.RPC("Receive_Ready", RpcTarget.OthersBuffered, new object[] { Player_number });
        else
            this.photonView.RPC("Receive_Not_Ready", RpcTarget.OthersBuffered, new object[] { Player_number });
    }

    [PunRPC]
    public void Receive_Ready(int Player_N)
    {
        UIMultiplayerManger.Instance.Set_Ready(Player_N);
    }

    [PunRPC]
    public void Receive_Not_Ready(int Player_N)
    {
        UIMultiplayerManger.Instance.Set_NotReady(Player_N);
    }



    public void ReceiveOpen(int Dropdown_Numner)
    {
        this.photonView.RPC("Receive_Open", RpcTarget.OthersBuffered, Dropdown_Numner);
    }

    [PunRPC]
    public void Receive_Open(int Dropdown_Numner)
    {
        UIMultiplayerManger.Instance.Set_Open(Dropdown_Numner);
    }

    public void ReceiveNotOpen(int Dropdown_Numner)
    {
        this.photonView.RPC("Receive_Not_Open", RpcTarget.OthersBuffered, Dropdown_Numner);
    }

    [PunRPC]
    public void Receive_Not_Open(int Dropdown_Numner)
    {
        UIMultiplayerManger.Instance.Set_NotOpen(Dropdown_Numner);
    }



    public void ReceiveKich(int Player_N)
    {
        this.photonView.RPC("Receive_KichFromMaster", RpcTarget.All, Player_N);

    }

    [PunRPC]
    public void Receive_KichFromMaster(int Player_N)
    {
        KichFromMaster(Player_N);
        if (Player_number == Player_N)
        {
            PhotonNetwork.Disconnect();
            UnityEngine.SceneManagement.SceneManager.LoadScene("Connecting Scene");
        }
    }

    //Change map
    public void MapChange(int Map_Number)
    {
        this.photonView.RPC("Map_Change", RpcTarget.All, Map_Number);

    }
    [PunRPC]
    public void Map_Change(int Map_Number)
    {
        UIMultiplayerManger.Instance.ChangeMap(Map_Number);
    }


    public void KichFromMaster(int Player_N)
    {
        UIMultiplayerManger.Instance.PlayerNames[Player_N].text = "";
        UIMultiplayerManger.Instance.AllColors[Player_N].sprite = null;
        UIMultiplayerManger.Instance.AllTeams[Player_N].sprite = null;
        UIMultiplayerManger.Instance.AllReady[Player_N].GetComponent<Button>().image.sprite = NotReady_Image;
        UIMultiplayerManger.Instance.AllOpenAndClose[Player_N].transform.GetChild(1).GetComponent<Dropdown>().image.sprite = UIMultiplayerManger.Instance.Not_Open;
    }

    void Set_Player_Team(int Team_N)
    {
        switch (Team_N)
        {
            case (0): Team = 1; break;
            case (1): Team = 2; break;
            case (2): Team = 3; break;
            case (3): Team = 4; break;
            case (4): Team = 5; break;
            case (5): Team = 6; break;
            case (6): Team = 7; break;
            case (7): Team = 8; break;
        }
    }

    void Set_Player_Color(int Color_N)
    {
        switch (Color_N)
        {
            case (0): Color = "Red"; break;
            case (1): Color = "Orange"; break;
            case (2): Color = "Yellow"; break;
            case (3): Color = "Green"; break;
            case (4): Color = "Sky Blue"; break;
            case (5): Color = "Blue"; break;
            case (6): Color = "Purple"; break;
            case (7): Color = "Pink"; break;
        }
        PhotonNetwork.PlayerList[Player_number].CustomProperties["Player_Color"] = Color;
    }




    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            //We own this player: send the others our data
            //stream.SendNext(Player_number);
            Debug.Log("write");
        }
        else
        {
            //Network player, receive data
            //Player_number1 = (int)stream.ReceiveNext();
            Debug.Log("Read");
        }
    }
}
