﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeMap : MonoBehaviour
{
    public Image MapImage;
    public Sprite Map;
    public int MapNumber;

    public int PlayerCountInMap;

    GameObject Player;

    public void OnChangeMap()
    {
        MapImage.sprite = Map;

        Player = GameObject.FindGameObjectWithTag("PlayerP");
        Player.GetComponent<UIMultiplayerPlayer>().MapChange(MapNumber);

        UIMultiplayerManger.Instance.ChangeRoomSetting(PlayerCountInMap);
    }
}
