﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;

public class UIMultiplayerManger : MonoBehaviour
{
    public List<Text> PlayerNames;
    public List<Dropdown> AllDropdowns;
    public List<Dropdown> AllDropdownsTeams;
    public List<Image> AllColors;
    public List<Image> AllTeams;

    public List<Sprite> AllSprite;
    public List<Sprite> AllSpriteTeams;

    public List<Image> AllReady;
    public List<bool> AllReadyOrNotReady;
    public Sprite Ready;
    public Sprite Not_Ready;

    public List<Dropdown> AllOpenAndClose;
    public Sprite Open;
    public Sprite Not_Open;


    public List<Button> AllKich;

    public Button StartGame;

    public List<Sprite> AllMaps;
    public Image MapImage;
    public List<Button> AllMapButtons;

    private static UIMultiplayerManger _instance = null;


    public static UIMultiplayerManger Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<UIMultiplayerManger>();
            return _instance;
        }
    }


    public int GetPlayer_Number()
    {
        int result = 1;
        if (PhotonNetwork.IsMasterClient)
            return 0;
        else
        {
            for (int i = 1; i < PlayerNames.Count; i++)
            {
                if (PlayerNames[i].text == "" && AllOpenAndClose[i].transform.GetChild(1).GetComponent<Image>().sprite == Open)
                    return i;
            }
            return 0;
        }

    }

    public void Set_name(string name,int P_number)
    {
        PlayerNames[P_number].text = name;
    }

    public void Active_Drop(int P_number)
    {
        AllDropdowns[P_number].GetComponent<Dropdown>().enabled = true;
        AllDropdownsTeams[P_number].GetComponent<Dropdown>().enabled = true;
    }

    public void Set_Color(int sprite_Number,int P_number)
    {
        AllColors[P_number].sprite = AllSprite[sprite_Number];
    }

    public void Set_team(int sprite_Number, int P_number)
    {
        AllTeams[P_number].sprite = AllSpriteTeams[sprite_Number];
    }

    public void Set_Ready(int P_number)
    {
        //AllReady[P_number].GetComponent<Button>().enabled = true;
        AllReady[P_number].sprite = Ready;
        AllReadyOrNotReady[P_number] = true; 
    }

    public void Set_NotReady(int P_number)
    {
        AllReady[P_number].sprite = Not_Ready;
        AllReadyOrNotReady[P_number] = false;
    }

    public void Set_Open(int Dropdown_Numner)
    {
        AllOpenAndClose[Dropdown_Numner].transform.GetChild(1).GetComponent<Image>().sprite = Open;
    }

    public void Set_NotOpen(int Dropdown_Numner)
    {
        AllOpenAndClose[Dropdown_Numner].transform.GetChild(1).GetComponent<Image>().sprite = Not_Open;
    }

    public void ChangeMap(int Map_Number)
    {
        MapImage.sprite = AllMaps[Map_Number];
    }

    public void DeletSelectedPlayerSprite(int Sprite_Number)
    {
        DropDownManger.Instance.DeleteFromlist(Sprite_Number);
        DropDownManger.Instance.SetAllDropDown();
    }

    //Change MaxPlayers when Player change Map..
    public void ChangeRoomSetting(int PlayerCount)
    {
        PhotonNetwork.CurrentRoom.MaxPlayers = (byte)PlayerCount;
    }

    /////Start Game Button
    public void OnStartGame()
    {
        bool IsAllPlayerReady = true;

        int PlayerCount = PhotonNetwork.PlayerList.Length;
        for (int i = 0; i < PlayerCount; i++)
        {
            if (!AllReadyOrNotReady[i])
                IsAllPlayerReady = false;
        }

        if(IsAllPlayerReady)
            PhotonNetwork.LoadLevel("Lamosa Forest (1V1)");
    }
}
