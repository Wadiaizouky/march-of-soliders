﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;

public class UIMultiplayer : MonoBehaviourPunCallbacks
{
    public InputField CreatRooNname;
    // Start is called before the first frame update
    void Start()
    {
    }


    public void OnCreatRoom_Click()
    {
        PhotonNetwork.LeaveRoom();

        //PhotonNetwork.CreateRoom(roomname);
    }
    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        PhotonNetwork.LoadLevel("Ready For Playing");
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        Debug.Log("Faild Room Create..");
    }
    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby();
    }
    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        Debug.Log("Lobby joined ..");
        string roomname = CreatRooNname.text;
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = (byte)8; //Set any number

        PhotonNetwork.JoinOrCreateRoom(roomname, roomOptions, TypedLobby.Default);
    }
    //public void OnJoinRoom_Click()
    //{
    //    string roomname = JoinRooNname.text;
    //    PhotonNetwork.JoinRoom(roomname);
    //}
    //public override void OnJoinedRoom()
    //{
    //    base.OnJoinedRoom();
    //    PhotonNetwork.LoadLevel("Room1");
    //    Debug.Log("Join To Romm : " + JoinRooNname.text + "seccess !.");
    //}

    //public override void OnJoinRoomFailed(short returnCode, string message)
    //{
    //    base.OnJoinRoomFailed(returnCode, message);
    //    Debug.Log("JoinRoomFailed....");
    //}

}
