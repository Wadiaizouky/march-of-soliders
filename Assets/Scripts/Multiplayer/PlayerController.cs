﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;
using ExitGames.Client.Photon;

public class PlayerController : MonoBehaviourPun/*IPunObservable*/
{
    // Update is called once per frame
    private PhotonView pv;
    Vector3 latestPos;
    

     void Start()
    {
        pv = GetComponent<PhotonView>();
        //if (!photonView.IsMine)
        //    GetComponent<PlayerController>().enabled = false;
    }

    void Update()
    {
        if (photonView.IsMine)
        {
            //Move Front/Back
            if (Input.GetKey(KeyCode.D))
            {
                //transform.Translate(transform.forward * Time.deltaTime * 2.45f, Space.World);
                transform.Translate(0.1f, 0f, 0f,Space.World);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                //transform.Translate(-transform.forward * Time.deltaTime * 2.45f, Space.World);
                transform.Translate(-0.1f , 0f, 0f,Space.World);
            }
            //pv.RPC("translation", RpcTarget.AllBuffered, null);
            if (Input.GetKey(KeyCode.Space))
            {
                pv.RPC("translation", RpcTarget.All, null);
            }
        }
        //else
            //pv.RPC("translation", RpcTarget.AllBuffered,null);
            //transform.position = Vector3.Lerp(transform.position, latestPos, Time.deltaTime * 10);


    }

    // void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    if (stream.IsWriting)
    //    {
    //        //We own this player: send the others our data
    //        stream.SendNext(transform.position);
    //        Debug.Log("Hiiii");

    //    }
    //    else
    //    {
    //        //Network player, receive data
    //        latestPos = (Vector3)stream.ReceiveNext();

    //    }
    //}

    //void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    //throw new System.NotImplementedException();
    //    if (stream.IsWriting)
    //    {
    //        //We own this player: send the others our data
    //        stream.SendNext(transform.position);
    //    }
    //    else
    //    {
    //        //Network player, receive data
    //        latestPos = (Vector3)stream.ReceiveNext();
    //    }
    //}

    [PunRPC]
    void translation() {
        //transform.position = Vector3.Lerp(transform.position, latestPos, Time.deltaTime * 10);
        transform.GetChild(0).gameObject.SetActive(true);
    }


}