﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using UnityEngine.UI;

public class TestRPC : MonoBehaviourPun,IPunObservable
{
    int Player_Number;
    public PhotonView pv;
    //public InputField Name;

    public Text[] Allplayers;
    public List<Dropdown> AllInputField;
    public List<Image> AllColors;

    public List<Sprite> AllSprite;

    Player[] Players;


    void Start()
    {


        Players = PhotonNetwork.PlayerList;
            //pv.ViewID = Random.Range(1,999);
            this.name = PhotonNetwork.NickName;

            //Allplayers = new Text[8];
            if (PhotonNetwork.IsMasterClient)
                Player_Number = 0;
            else
            {
                Player_Number = Random.Range(1, 7);
            }
            Allplayers[Player_Number].text = PhotonNetwork.NickName;
            //AllInputField[Player_Number].GetComponent<Dropdown>().enabled = true;

           // pv.RPC("Sent_Name", RpcTarget.OthersBuffered, new object[] { PhotonNetwork.NickName, Player_Number });
       
        
    }
    // Update is called once per frame
    void Update()
    {
        if (pv.IsMine)
        {
            //Move Front/Back
            if (Input.GetKey(KeyCode.D))
            {
                //Debug.Log(PhotonNetwork.PlayerList.Length);
                //transform.Translate(transform.forward * Time.deltaTime * 2.45f, Space.World);
                transform.Translate(0.1f, 0f, 0f, Space.World);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                //transform.Translate(-transform.forward * Time.deltaTime * 2.45f, Space.World);
                transform.Translate(-0.1f, 0f, 0f, Space.World);
            }
            //pv.RPC("translation", RpcTarget.AllBuffered, null);
            if (Input.GetKey(KeyCode.Space))
            {
                Debug.Log(PhotonNetwork.CountOfRooms + "Room");
                Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount + "Player");
                pv.RPC("Sent_Name", RpcTarget.AllBuffered, new object[] { PhotonNetwork.NickName, Player_Number });
            }

            if (Input.GetKey(KeyCode.Tab))
            {
                Allplayers[7].text = Player_Number + "," + PhotonNetwork.CurrentRoom.PlayerCount.ToString();
            }
        }
            if (AllColors[Player_Number].sprite != null)
                pv.RPC("Sent_Color", RpcTarget.OthersBuffered, new object[] { Player_Number });
        //}
        // PhotonNetwork.NickName = Name.text;

        //Players = PhotonNetwork.PlayerList;
        //Allplayers.Clear();
        //foreach (Player p in Players)
        //{

        //    Allplayers.Add(p.NickName);
        //}
        //    if (pv.IsMine)
        //{
        //    if (Input.GetKeyDown(KeyCode.A))
        //    {
        //        Debug.Log("Sent To Remote...");
        //        pv.RPC("Sent_Name", RpcTarget.All,PhotonNetwork.NickName);
        //    }
        //}
    }

    [PunRPC]
    public void Sent_Name(string name,int P_number)
    {
            //Debug.Log(name+" : "+ P_number);
           this.Allplayers[P_number].text = name;
    }


    [PunRPC]
    public void Sent_Color(int P_number)
    {
        this.AllColors[P_number].sprite = AllSprite[P_number];
    }

    [PunRPC]
    public void Sent_ToMaster(Text[] Allpla)
    {
        for (int i = 0; i < Allplayers.Length; i++)
            Debug.Log(Allpla[i].text);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            //We own this player: send the others our data
            //stream.SendNext(Player_number);
            Debug.Log("write");
        }
        else
        {
            //Network player, receive data
            //Player_number1 = (int)stream.ReceiveNext();
            Debug.Log("Read");
        }
    }
}
