﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
//using Photon.Chat;
using UnityEngine.UI;

public class Login : MonoBehaviourPunCallbacks
{
    public InputField Nickname;
    public InputField Password;

    void Start()
    {
        //PhotonNetwork.AutomaticallySyncScene = true;
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("Connect Succes to Master Server ..");
        PhotonNetwork.JoinLobby();
    }
    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        Debug.Log("Lobby joined ..");
        RoomOptions roomOptions = new RoomOptions();

        PhotonNetwork.JoinOrCreateRoom("Lobby", roomOptions, TypedLobby.Default);
        PhotonNetwork.NickName = Nickname.text;
        PhotonNetwork.LoadLevel("Lobby Multiplayer");


    }

    public void OnEentre()
    {
        if (/*(PhotonNetwork.AuthValues.UserId == Password.text) && */(PhotonNetwork.NickName == Nickname.text))
        {
            
            if (!PhotonNetwork.IsConnected)
                PhotonNetwork.ConnectUsingSettings();
        }
    }
}
