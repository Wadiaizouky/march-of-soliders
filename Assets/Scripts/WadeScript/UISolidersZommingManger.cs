﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISolidersZommingManger : MonoBehaviour
{

    //public GameObject UISolier;
    void Start()
    {
        //UISolier = transform.GetChild(2).transform.GetChild(2).gameObject;
    }

    //zoom UI in when zoom mouse in
    public void zoomUIin() {
        transform.localScale = new Vector3(transform.localScale .x+ 0.000800f, transform.localScale.y + 0.000800f, 1f);
    }

    //zoom UI out when zoom mouse out
    public void zoomUIout() {
        transform.localScale = new Vector3(transform.localScale.x - 0.000800f, transform.localScale.y - 0.000800f, 1f);
    }

}
