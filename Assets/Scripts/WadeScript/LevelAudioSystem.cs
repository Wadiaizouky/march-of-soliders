﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelAudioSystem : MonoBehaviour
{
    public bool IsMusicPlay = true;

    private static LevelAudioSystem instance = null;
    public static LevelAudioSystem Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;

       gameObject.GetComponent<AudioSource>().Pause();
        DontDestroyOnLoad(this.gameObject);

    }

}
