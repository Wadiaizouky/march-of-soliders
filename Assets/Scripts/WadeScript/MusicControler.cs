﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicControler : MonoBehaviour
{
    public bool IsMusicPlay = true;

    private static MusicControler instance = null;
    public static MusicControler Instance
    {
        get { return instance; }
    }

     void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;

        DontDestroyOnLoad(this.gameObject);

    }
}
