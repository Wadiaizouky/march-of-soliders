﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine;
using UnityEngine.UI;


public class videocreation : MonoBehaviour
{
    public RawImage Rawimag;
    public VideoPlayer vp;
    public Text LoadText;

    // Update is called once per frame
    void Start()
    {
        StartCoroutine(playv());
    }

    IEnumerator playv()
    {
        //vp.Prepare();
        //yield return new WaitForSeconds(0.5f);
        while (!vp.isPrepared)
        {
            yield return new WaitForSeconds(1f);
            break;
        }
        Rawimag.texture = vp.texture;
        vp.Play();
        LoadText.gameObject.SetActive(false);
    }


}
