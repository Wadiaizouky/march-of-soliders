﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using MarchOfSoldiers;

public class MoveMouse : MonoBehaviour
{

    // Use this for initialization
    /* *********************************************** */
    [SerializeField] private Vector2 _cameraXRange;
    [SerializeField] private Vector2 _cameraYRange;

    [SerializeField] public float _mousespeed;

    private static MoveMouse _instance = null;

    /* *********************************************** */
    public GameObject cam;

    public Texture2D cursor;//main cursor 
    public Texture2D cursorTexture;//cursor for Right move
    public Texture2D cursorTexture1;//cursor for Left move
    public Texture2D cursorTexture2;//cursor for Up move
    public Texture2D cursorTexture3;//cursor for Down move
    public Texture2D cursorDefendTexture;//cursor for Defend 
    public Texture2D cursorAttackTexture;//cursor for Attack 
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

    //All UI Castles
    public List<GameObject> AllUICastles;

    public bool IsSummon = false;
    public Texture2D SummonCursor;//cursor for Summon

    public static MoveMouse Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<MoveMouse>();
            return _instance;
        }
    }

    void Start() { }

    // Update is called once per frame
    void Update()
    {
        #region cursor
        /////////////////for cursor UI////////////////
        List<GroupOfUnits> SelectedSoliders = SelectionManager.Instance.GetSelectedSolidersForLocalPlayer();
        List<SingleCastle> SelectedCastles = SelectionManager.Instance.GetSelectedCastlesForLocalPlayer();

        if (!PasueController.Instance.IsPasued && !SelectionManager.Instance.IsMouseOverUI())
        {
            if (!IsSummon)
            {
                if (SelectedSoliders.Count > 0)
                {
                    Vector2 destination = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                    // check if player choose to move toward a castle
                    RaycastHit2D hit = Physics2D.Raycast(
                        origin: destination,
                        direction: Vector2.zero,
                        distance: 100);

                    // if destination is CASTLE: don't add offset to destination, just move to Center of castle
                    if (hit)
                        if (hit.collider.gameObject.transform.childCount > 4 && hit.collider.gameObject.tag == "Player")
                            Cursor.SetCursor(cursorDefendTexture, hotSpot, cursorMode);
                        else if (hit.collider.gameObject.transform.childCount > 4 && !(hit.collider.gameObject.tag == "Player"))
                            Cursor.SetCursor(cursorAttackTexture, hotSpot, cursorMode);
                        else
                            Cursor.SetCursor(cursor, hotSpot, cursorMode);
                    else
                        Cursor.SetCursor(cursor, hotSpot, cursorMode);
                }
                else
                if (SelectedCastles.Count > 0)
                {
                    Vector2 destination = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                    // check if player choose to move toward a castle
                    RaycastHit2D hit = Physics2D.Raycast(
                        origin: destination,
                        direction: Vector2.zero,
                        distance: 100);

                    // if destination is CASTLE: don't add offset to destination, just move to Center of castle
                    if (hit)
                        if (hit.collider.gameObject.transform.childCount > 4 && hit.collider.gameObject.tag == "Player")
                            Cursor.SetCursor(cursorDefendTexture, hotSpot, cursorMode);
                        else if (hit.collider.gameObject.transform.childCount > 4 && !(hit.collider.gameObject.tag == "Player"))
                            Cursor.SetCursor(cursorAttackTexture, hotSpot, cursorMode);
                        else
                            Cursor.SetCursor(cursor, hotSpot, cursorMode);
                    else
                        Cursor.SetCursor(cursor, hotSpot, cursorMode);
                }
                else
                    Cursor.SetCursor(cursor, hotSpot, cursorMode);
            }
            else
                Cursor.SetCursor(SummonCursor, hotSpot, cursorMode);
        }
        else
            Cursor.SetCursor(cursor, hotSpot, cursorMode);


        #endregion
        /////////////////end cursor UI///////////////

        bool DrawSelectionBox = SelectionManager.Instance.DrawSelectionBox;
        bool DrawBoxBositions =   DrawBoxPosition.Instance.IsDrawingByPlayers[PlayersManager.Instance.GetLocalPlayerNumber()];
        bool IsPasued = PasueController.Instance.IsPasued;

        //Enable Moving scene when not drawing box
        if (!DrawSelectionBox && !DrawBoxBositions && !IsPasued)
        {
            ////////////* Moving Using Mouse *///////////
            //mouve camera right 
            if (Input.mousePosition.x >= Screen.width - 20)
            {
                Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
                if (cam.transform.position.x <= /*30f*/ _cameraXRange.y)
                {
                    cam.transform.Translate(_mousespeed * Time.deltaTime, 0f, 0f);
                    //GameObject mapplayerposition = GameObject.Find("Playermap");
                    //mapplayerposition.transform.Translate(1f, 0f, 0f);
                }
            }
            //mouve camera left
            if (Input.mousePosition.x <= 20)
            {
                Cursor.SetCursor(cursorTexture1, hotSpot, cursorMode);
                if (cam.transform.position.x >= /*-1.6f*/_cameraXRange.x)
                {
                    cam.transform.Translate(-_mousespeed * Time.deltaTime, 0f, 0f);
                    //GameObject mapplayerposition = GameObject.Find("Playermap");
                    //mapplayerposition.transform.Translate(-1f, 0f, 0f);
                }
            }
            //mouve camera up
            if (Input.mousePosition.y >= Screen.height - 20)
            {
                Cursor.SetCursor(cursorTexture2, hotSpot, cursorMode);
                if (cam.transform.position.y <= /*-2f*/_cameraYRange.y)
                {
                    cam.transform.Translate(0f, _mousespeed * Time.deltaTime, 0f);
                    //GameObject mapplayerposition = GameObject.Find("Playermap");
                    //mapplayerposition.transform.Translate(0f, 1.3f, 0f);
                }
            }
            //mouve camera Down
            if (Input.mousePosition.y <= 20)
            {
                if (Input.GetAxis("Mouse Y") < 100)//Down
                {
                    Cursor.SetCursor(cursorTexture3, hotSpot, cursorMode);
                    if (cam.transform.position.y >= /*-12f*/_cameraYRange.x)
                    {
                        cam.transform.Translate(0f, -_mousespeed * Time.deltaTime, 0f);
                        //GameObject mapplayerposition = GameObject.Find("Playermap");
                        //mapplayerposition.transform.Translate(0f, -1.3f, 0f);
                    }
                }
            }
            //zoom in and  zoom out
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            { // forward
              //if (cam.transform.position.z <= -6f)
              //    cam.transform.Translate(0f, 0f, 1.5f);
                Camera c = cam.GetComponent<Camera>();
                if (c.orthographicSize >= 9)
                {
                    c.orthographicSize -= 1.5f;
                    //if (c.orthographicSize > 17)
                    //{
                    //    for (int i = 0; i < AllUICastles.Count; i++)
                    //        AllUICastles[i].GetComponent<UISolidersZommingManger>().zoomUIout();
                    //}
                }
            }
            if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            { // backwards
              //if (cam.transform.position.z >= -25f)
              //    cam.transform.Translate(0f, 0f, -1.5f);
                Camera c = cam.GetComponent<Camera>();
                if (c.orthographicSize <= 50)
                {
                    c.orthographicSize += 1.5f;
                    //for (int i = 0; i < AllUICastles.Count; i++)
                    //    AllUICastles[i].GetComponent<UISolidersZommingManger>().zoomUIin();

                }
            }

            ////////////*End Moving Using Mouse *///////////



            ////////////* Moving Using Keybord *///////////
            //mouve camera right 
            if (Input.GetKey(KeyCode.D))
            {
                Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
                if (cam.transform.position.x <= /*30f*/ _cameraXRange.y)
                {
                    cam.transform.Translate(_mousespeed * Time.deltaTime, 0f, 0f);
                    //GameObject mapplayerposition = GameObject.Find("Playermap");
                    //mapplayerposition.transform.Translate(1f, 0f, 0f);
                }
            }
            //mouve camera left
            if (Input.GetKey(KeyCode.A))
            {
                Cursor.SetCursor(cursorTexture1, hotSpot, cursorMode);
                if (cam.transform.position.x >= /*-1.6f*/_cameraXRange.x)
                {
                    cam.transform.Translate(-_mousespeed * Time.deltaTime, 0f, 0f);
                    //GameObject mapplayerposition = GameObject.Find("Playermap");
                    //mapplayerposition.transform.Translate(-1f, 0f, 0f);
                }
            }
            //mouve camera up
            if (Input.GetKey(KeyCode.W))
            {
                Cursor.SetCursor(cursorTexture2, hotSpot, cursorMode);
                if (cam.transform.position.y <= /*-2f*/_cameraYRange.y)
                {
                    cam.transform.Translate(0f, _mousespeed * Time.deltaTime, 0f);
                    //GameObject mapplayerposition = GameObject.Find("Playermap");
                    //mapplayerposition.transform.Translate(0f, 1.3f, 0f);
                }
            }
            //mouve camera Down
            if (Input.GetKey(KeyCode.S))
            {
                if (Input.GetAxis("Mouse Y") < 100)//Down
                {
                    Cursor.SetCursor(cursorTexture3, hotSpot, cursorMode);
                    if (cam.transform.position.y >= /*-12f*/_cameraYRange.x)
                    {
                        cam.transform.Translate(0f, -_mousespeed * Time.deltaTime, 0f);
                        //GameObject mapplayerposition = GameObject.Find("Playermap");
                        //mapplayerposition.transform.Translate(0f, -1.3f, 0f);
                    }
                }
            }
            ////////////*End Moving Using Mouse *///////////
        }

    }
}

