﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioSystem : MonoBehaviour
{
    public Toggle InterfaceCheck;
    public Toggle GamePlayCheck;


    private void Start()
    {
        if (MusicControler.Instance.IsMusicPlay)
        {
            InterfaceCheck.isOn = true;
            MusicControler.Instance.GetComponent<AudioSource>().UnPause();
            MusicControler.Instance.IsMusicPlay = true;

        }
        else
        {
            InterfaceCheck.isOn = false;
            MusicControler.Instance.GetComponent<AudioSource>().Pause();
            MusicControler.Instance.IsMusicPlay = false;


        }

        if (LevelAudioSystem.Instance.IsMusicPlay)
        {
            GamePlayCheck.isOn = true;
            LevelAudioSystem.Instance.GetComponent<AudioSource>().UnPause();
            LevelAudioSystem.Instance.IsMusicPlay = true;

        }
        else
        {
            GamePlayCheck.isOn = false;
            LevelAudioSystem.Instance.GetComponent<AudioSource>().Pause();
            LevelAudioSystem.Instance.IsMusicPlay = false;


        }


        //if(LevelMusic.GetComponent<LevelAudioSystem>().isMusicPlay)
        //{
        //    GamePlayCheck.isOn = true;
        //    LevelMusic.GetComponent<LevelAudioSystem>().isMusicPlay = true;
        //}
        //else
        //{
        //    GamePlayCheck.isOn = false;
        //    LevelMusic.GetComponent<LevelAudioSystem>().isMusicPlay = false;
        //}

    }

    public void OnInterfaceSound()
    {
        if (MusicControler.Instance.IsMusicPlay)
        {
            MusicControler.Instance.GetComponent<AudioSource>().Pause();
            MusicControler.Instance.IsMusicPlay = false;
        }
        else
        {
            MusicControler.Instance.GetComponent<AudioSource>().UnPause();
            MusicControler.Instance.IsMusicPlay = true;
        }
    }

    public void OnGamePlaySound()
    {
        if (LevelAudioSystem.Instance.GetComponent<LevelAudioSystem>().IsMusicPlay)
            LevelAudioSystem.Instance.GetComponent<LevelAudioSystem>().IsMusicPlay = false;
        else
            LevelAudioSystem.Instance.GetComponent<LevelAudioSystem>().IsMusicPlay = true;
    }
}
