﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;
using UnityEngine.UI;

public class CastleFarmButtom : MonoBehaviour
{
    //
    //public Text FarmC;

    public List<GameObject> FarmElements;

    private void Update()
    {
        //FarmC.text = GetComponent<CastelArmyUICount>().FarmCount + "/" + PlayersManager.Instance.LocalPlayer.KingdomLevel;
    }

    public void OnFarmButtomClick()
    {
        SingleCastle singleCastle = GetComponent<SingleCastle>();
        int Farmcout = GetComponent<CastelArmyUICount>().FarmCount;
        int FarmCountvalid = GetComponent<CastelArmyUICount>().FarmCountvalid;

        if (Farmcout < FarmCountvalid)
        {
            GetComponent<CastelArmyUICount>().FarmCount++;
            PlayersManager.Instance.GetPlayer(singleCastle.PlayerOwnerNumber).Gold -= 5;
            //FarmC.text = GetComponent<CastelArmyUICount>().FarmCount + "/" + GetComponent<CastelArmyUICount>().FarmCountvalid;
        }
    }

    public void ChangeFarmUI()
    {
        SingleCastle singleCastle = GetComponent<SingleCastle>();

        if (singleCastle.disableSelectable) return;

        int Farmcout = GetComponent<CastelArmyUICount>().FarmCount;
        int FarmCountvalid = GetComponent<CastelArmyUICount>().FarmCountvalid;
        int FarmAvailable = GetComponent<CastelArmyUICount>().FarmAvailable;

        if (FarmCountvalid != 0)
        {
            for (int i = 0; i < FarmCountvalid; i++)
            {
                FarmElements[i].GetComponent<Farms_Castle_Interactable>().OnStatic();
            }

            for (int i = 0; i < Farmcout; i++)
            {
                FarmElements[i].GetComponent<Farms_Castle_Interactable>().OnActive();
            }
        }
        else
        {
            for (int i = 0; i < 3; i++)
            {
                FarmElements[i].GetComponent<Farms_Castle_Interactable>().OnLoced();
            }
        }
    }
}
