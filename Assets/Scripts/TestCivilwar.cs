﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TestCivilwar : MonoBehaviour
{
    public InputField text;

    public void Loadlevel()
    {
        int n = int.Parse(text.text);
        SceneManager.LoadSceneAsync("Level "+n);
    }
}
