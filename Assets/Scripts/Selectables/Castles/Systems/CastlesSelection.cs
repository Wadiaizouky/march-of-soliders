﻿using System.Collections.Generic;
using UnityEngine;


namespace MarchOfSoldiers
{
    public class CastlesSelection : Singleton<CastlesSelection>
    {

        [HideInInspector] public List<SingleCastle> allSelectable = new List<SingleCastle>();
        [HideInInspector] public List<SingleCastle> selectableGates = new List<SingleCastle>();


        private Dictionary<int, List<SingleCastle>> unSelectableVillages = new Dictionary<int, List<SingleCastle>>();


        private Dictionary<int, List<SingleCastle>> playersSelectableCastles = new Dictionary<int, List<SingleCastle>>();
        private Dictionary<int, List<SingleCastle>> playersSelectableVillages = new Dictionary<int, List<SingleCastle>>();
        private Dictionary<int, List<SingleCastle>> playersSelectableTowers = new Dictionary<int, List<SingleCastle>>();

        private Dictionary<int, List<SingleCastle>> playersAllSelected = new Dictionary<int, List<SingleCastle>>();
        private Dictionary<int, List<SingleCastle>> playersSelectedCastles = new Dictionary<int, List<SingleCastle>>();
        private Dictionary<int, List<SingleCastle>> playersSelectedGates = new Dictionary<int, List<SingleCastle>>();
        private Dictionary<int, List<SingleCastle>> playersSelectedVillages = new Dictionary<int, List<SingleCastle>>();
        private Dictionary<int, List<SingleCastle>> playersSelectedTowers = new Dictionary<int, List<SingleCastle>>();


        public List<SingleCastle> GetSelectableCastlesByPlayerNum(int playerNumber)
        {
            return (playersSelectableCastles.ContainsKey(playerNumber))
                 ? playersSelectableCastles[playerNumber] : new List<SingleCastle>();
        }

        public List<SingleCastle> GetUnSelectableVillagesByPlayerNum(int playerNumber)
        {
            return (unSelectableVillages.ContainsKey(playerNumber))
                 ? unSelectableVillages[playerNumber] : new List<SingleCastle>();
        }

        public List<SingleCastle> GetSelectableVillagesByPlayerNum(int playerNumber)
        {
            return (playersSelectableVillages.ContainsKey(playerNumber))
                 ? playersSelectableVillages[playerNumber] : new List<SingleCastle>();
        }

        public List<SingleCastle> GetSelectableTowersByPlayerNum(int playerNumber)
        {
            return (playersSelectableTowers.ContainsKey(playerNumber))
                 ? playersSelectableTowers[playerNumber] : new List<SingleCastle>();
        }

        public List<SingleCastle> GetAllSelectedByPlayerNum(int playerNumber)
        {
            return (playersAllSelected.ContainsKey(playerNumber))
                ? playersAllSelected[playerNumber] : new List<SingleCastle>();
        }

        public List<SingleCastle> GetSelectedCastlesByPlayerNum(int playerNumber)
        {
            return (playersSelectedCastles.ContainsKey(playerNumber))
                ? playersSelectedCastles[playerNumber] : new List<SingleCastle>();
        }

        public List<SingleCastle> GetSelectedGatesByPlayerNum(int playerNumber)
        {
            return (playersSelectedGates.ContainsKey(playerNumber))
                ? playersSelectedGates[playerNumber] : new List<SingleCastle>();
        }

        public List<SingleCastle> GetSelectedVillagesByPlayerNum(int playerNumber)
        {
            return (playersSelectedVillages.ContainsKey(playerNumber))
                ? playersSelectedVillages[playerNumber] : new List<SingleCastle>();
        }

        public List<SingleCastle> GetSelectedTowersByPlayerNum(int playerNumber)
        {
            return (playersSelectedTowers.ContainsKey(playerNumber))
                ? playersSelectedTowers[playerNumber] : new List<SingleCastle>();
        }

        private void AddToSelectableCastles(SingleCastle castle)
        {
            if (!playersSelectableCastles.ContainsKey(castle.PlayerOwnerNumber))
            {
                playersSelectableCastles.Add(castle.PlayerOwnerNumber, new List<SingleCastle>());
            }
            playersSelectableCastles[castle.PlayerOwnerNumber].Add(castle);
        }

        private void AddToSelectableVillages(SingleCastle village)
        {
            if (!playersSelectableVillages.ContainsKey(village.PlayerOwnerNumber))
            {
                playersSelectableVillages.Add(village.PlayerOwnerNumber, new List<SingleCastle>());
            }
            playersSelectableVillages[village.PlayerOwnerNumber].Add(village);
        }

        private void AddToUnSelectableVillages(SingleCastle village)
        {
            if (!unSelectableVillages.ContainsKey(village.PlayerOwnerNumber))
            {
                unSelectableVillages.Add(village.PlayerOwnerNumber, new List<SingleCastle>());
            }
            unSelectableVillages[village.PlayerOwnerNumber].Add(village);
        }

        private void AddToSelectableTowers(SingleCastle tower)
        {
            if (!playersSelectableTowers.ContainsKey(tower.PlayerOwnerNumber))
            {
                playersSelectableTowers.Add(tower.PlayerOwnerNumber, new List<SingleCastle>());
            }
            playersSelectableTowers[tower.PlayerOwnerNumber].Add(tower);
        }

        private void AddToAllSelected(SingleCastle castle)
        {
            if (!playersAllSelected.ContainsKey(castle.PlayerOwnerNumber))
            {
                playersAllSelected.Add(castle.PlayerOwnerNumber, new List<SingleCastle>());
            }
            playersAllSelected[castle.PlayerOwnerNumber].Add(castle);
        }

        private void AddToAllSelectedCastles(SingleCastle castle)
        {
            if (!playersSelectedCastles.ContainsKey(castle.PlayerOwnerNumber))
            {
                playersSelectedCastles.Add(castle.PlayerOwnerNumber, new List<SingleCastle>());
            }
            playersSelectedCastles[castle.PlayerOwnerNumber].Add(castle);
        }

        private void AddToAllSelectedGates(SingleCastle castle)
        {
            if (!playersSelectedGates.ContainsKey(castle.PlayerOwnerNumber))
            {
                playersSelectedGates.Add(castle.PlayerOwnerNumber, new List<SingleCastle>());
            }
            playersSelectedGates[castle.PlayerOwnerNumber].Add(castle);
        }

        private void AddToAllSelectedVillages(SingleCastle castle)
        {
            if (!playersSelectedVillages.ContainsKey(castle.PlayerOwnerNumber))
            {
                playersSelectedVillages.Add(castle.PlayerOwnerNumber, new List<SingleCastle>());
            }
            playersSelectedVillages[castle.PlayerOwnerNumber].Add(castle);
        }

        private void AddToAllSelectedTowers(SingleCastle castle)
        {
            if (!playersSelectedTowers.ContainsKey(castle.PlayerOwnerNumber))
            {
                playersSelectedTowers.Add(castle.PlayerOwnerNumber, new List<SingleCastle>());
            }
            playersSelectedTowers[castle.PlayerOwnerNumber].Add(castle);
        }

        public void AddSelectable(SingleCastle selectable)
        {
            if (selectable.disableSelectable && selectable.calculateSilverWhenDisabled)
            {
                if (selectable.isVillage)
                {
                    AddToUnSelectableVillages(selectable);
                }
                return;
            }
            else if (selectable.disableSelectable)
            {
                return;
            }

            allSelectable.Add(selectable);
            if (selectable.isVillage)
            {
                AddToSelectableVillages(selectable);
            }
            else if (selectable.isGate)
            {
                selectableGates.Add(selectable);
            }
            else if (selectable.isTower)
            {
                AddToSelectableTowers(selectable);
            }
            else
            {
                AddToSelectableCastles(selectable);
            }
        }

        public void RemoveSelectable(SingleCastle selectable)
        {
            if (selectable.disableSelectable && selectable.calculateSilverWhenDisabled)
            {
                if (selectable.isVillage)
                {
                    if (unSelectableVillages.ContainsKey(selectable.PlayerOwnerNumber))
                        unSelectableVillages[selectable.PlayerOwnerNumber].Remove(selectable);
                }
                return;
            }
            else if (selectable.disableSelectable)
            {
                return;
            }

            allSelectable.Remove(selectable);
            if (selectable.isVillage)
            {
                if (playersSelectableVillages.ContainsKey(selectable.PlayerOwnerNumber))
                    playersSelectableVillages[selectable.PlayerOwnerNumber].Remove(selectable);
            }
            else if (selectable.isGate)
            {
                selectableGates.Remove(selectable);
            }
            else if (selectable.isTower)
            {
                if (playersSelectableTowers.ContainsKey(selectable.PlayerOwnerNumber))
                    playersSelectableTowers[selectable.PlayerOwnerNumber].Remove(selectable);
            }
            else
            {
                if (playersSelectableCastles.ContainsKey(selectable.PlayerOwnerNumber))
                    playersSelectableCastles[selectable.PlayerOwnerNumber].Remove(selectable);
            }
        }

        public void RemoveSelected(SingleCastle castle)
        {
            if (playersAllSelected.ContainsKey(castle.PlayerOwnerNumber))
            {
                playersAllSelected[castle.PlayerOwnerNumber].Remove(castle);
            }
            if (castle.isVillage)
            {
                if (playersSelectedVillages.ContainsKey(castle.PlayerOwnerNumber))
                {
                    playersSelectedVillages[castle.PlayerOwnerNumber].Remove(castle);
                }
            }
            else if (castle.isGate)
            {
                if (playersSelectedGates.ContainsKey(castle.PlayerOwnerNumber))
                {
                    playersSelectedGates[castle.PlayerOwnerNumber].Remove(castle);
                }

            }
            else if (castle.isTower)
            {
                if (playersSelectedTowers.ContainsKey(castle.PlayerOwnerNumber))
                {
                    playersSelectedTowers[castle.PlayerOwnerNumber].Remove(castle);
                }
            }
            else
            {
                if (playersSelectedCastles.ContainsKey(castle.PlayerOwnerNumber))
                {
                    playersSelectedCastles[castle.PlayerOwnerNumber].Remove(castle);
                }
            }
        }

        public void OnSelectionCastle(Selectable selectableObject, int playerNumber)
        {
            SingleCastle castle = selectableObject as SingleCastle;
            
            if (!castle) return;

            if (castle.OwnerType != OwnerType.Player) return;
            if (castle.PlayerOwnerNumber != playerNumber) return;

            if (DetectDoubleClick())
            {
                foreach (SingleCastle singleCastle in GetSelectableCastlesByPlayerNum(playerNumber))
                {
                    if (!GetAllSelectedByPlayerNum(playerNumber).Contains(singleCastle)
                        && singleCastle.isGate == castle.isGate && singleCastle.isVillage == castle.isVillage
                        && singleCastle.isTower == castle.isTower
                        && singleCastle.PlayerOwnerNumber == playerNumber)
                        SelectSingleCastle(singleCastle);
                }
                return;
            }
            if (DetectMultiSelect())
            {
                if ((SelectionManager.Instance.currentSelectedType == SelectableType.None
                    || SelectionManager.Instance.currentSelectedType == SelectableType.Castle)
                    && GetAllSelectedByPlayerNum(playerNumber)[0].isGate == castle.isGate
                    && GetAllSelectedByPlayerNum(playerNumber)[0].isVillage == castle.isVillage
                    && GetAllSelectedByPlayerNum(playerNumber)[0].isTower == castle.isTower
                    && !GetAllSelectedByPlayerNum(playerNumber).Contains(castle))
                    SelectSingleCastle(castle);
            }
            else
            {
                SelectionManager.Instance.ClearSelection(playerNumber);
                SelectSingleCastle(castle);
            }

            SelectionManager.Instance.lastClickTime = Time.time;
        }

        private bool DetectDoubleClick()
        {
            float timeSinceLastClick = Time.time - SelectionManager.Instance.lastClickTime;
            return timeSinceLastClick <= SelectionManager.DOUBLE_CLICK_DEFERENCE;
        }

        private bool DetectMultiSelect()
        {
            return Input.GetKey("left ctrl");
        }

        public void ClearCastlesSelection(int playerNumber)
        {
            if (GetAllSelectedByPlayerNum(playerNumber).Count > 0)
            {
                List<SingleCastle> castleToDeselect = new List<SingleCastle>();
                foreach (var castle in GetAllSelectedByPlayerNum(playerNumber))
                {
                    if (castle.PlayerOwnerNumber == playerNumber)
                        castleToDeselect.Add(castle);
                }

                foreach (SingleCastle castle in castleToDeselect)
                {
                    DeselectSingleCastle(castle);
                }

                if (playersAllSelected.ContainsKey(playerNumber))
                    playersAllSelected[playerNumber].Clear();
                if (playersSelectedCastles.ContainsKey(playerNumber))
                    playersSelectedCastles[playerNumber].Clear();
                if (playersSelectedGates.ContainsKey(playerNumber))
                    playersSelectedGates[playerNumber].Clear();
                if (playersSelectedVillages.ContainsKey(playerNumber))
                    playersSelectedVillages[playerNumber].Clear();
                if (playersSelectedTowers.ContainsKey(playerNumber))
                    playersSelectedTowers[playerNumber].Clear();
            }
        }

        public void SelectSingleCastle(SingleCastle castle)
        {
            if (!castle) return;

            if (castle.OwnerType == OwnerType.Player)
            {
                AddToAllSelected(castle);
    
                if (castle.isVillage)
                {
                    AddToAllSelectedVillages(castle);
                }
                else if (castle.isGate)
                {
                    AddToAllSelectedGates(castle);
                }
                else if (castle.isTower)
                {
                    AddToAllSelectedTowers(castle);
                }
                else
                {
                    AddToAllSelectedCastles(castle);
                }
                SelectionManager.Instance.currentSelectedType = castle.SelectableType;
                castle.OnSelected();
            }
        }

        public void DeselectSingleCastle(SingleCastle castle)
        {
            if (!castle) return;
            RemoveSelected(castle);
            castle.OnDeselected();
        }

        public bool SelectMultipleCastles(ref Rect selectionBox, Vector3 pos1, Vector3 pos2, int playerNumber)
        {
            List<SingleCastle> castleToSelect = new List<SingleCastle>();
            List<SingleCastle> removeCastles = new List<SingleCastle>();

            foreach (SingleCastle castle in GetSelectableCastlesByPlayerNum(playerNumber))
            {
                if (castle.PlayerOwnerNumber != playerNumber) continue;

                if (castle)
                {
                    if (pos1.x < castle.transform.position.x && castle.transform.position.x < pos2.x
                        && pos1.y > castle.transform.position.y && castle.transform.position.y > pos2.y)
                        castleToSelect.Add(castle);
                }
                else
                {
                    removeCastles.Add(castle);
                }
            }

            if (castleToSelect.Count == 0)
            {
                foreach (SingleCastle village in GetSelectableVillagesByPlayerNum(playerNumber))
                {
                    if (village.PlayerOwnerNumber != playerNumber) continue;

                    if (village)
                    {
                        if (pos1.x < village.transform.position.x && village.transform.position.x < pos2.x
                              && pos1.y > village.transform.position.y && village.transform.position.y > pos2.y)
                            castleToSelect.Add(village);
                    }
                    else
                    {
                        removeCastles.Add(village);
                    }
                }
            }

            if (castleToSelect.Count == 0)
            {
                foreach (SingleCastle tower in GetSelectableTowersByPlayerNum(playerNumber))
                {
                    if (tower.PlayerOwnerNumber != playerNumber) continue;

                    if (tower)
                    {
                        if (pos1.x < tower.transform.position.x && tower.transform.position.x < pos2.x
                              && pos1.y > tower.transform.position.y && tower.transform.position.y > pos2.y)
                            castleToSelect.Add(tower);
                    }
                    else
                    {
                        removeCastles.Add(tower);
                    }
                }
            }

            if (castleToSelect.Count == 0)
            {
                foreach (SingleCastle gate in selectableGates)
                {
                    if (gate.PlayerOwnerNumber != playerNumber) continue;

                    if (gate)
                    {
                        if (pos1.x < gate.transform.position.x && gate.transform.position.x < pos2.x
                              && pos1.y > gate.transform.position.y && gate.transform.position.y > pos2.y)
                            castleToSelect.Add(gate);
                    }
                    else
                    {
                        removeCastles.Add(gate);
                    }
                }
            }

            foreach (SingleCastle castle in removeCastles)
            {
                allSelectable.Remove(castle);
                if (castle.isVillage)
                {
                    if (playersSelectableVillages.ContainsKey(castle.PlayerOwnerNumber))
                        playersSelectableVillages[castle.PlayerOwnerNumber].Remove(castle);
                }
                else if (castle.isGate)
                {
                    selectableGates.Remove(castle);
                }
                else if (castle.isTower)
                {
                    if (playersSelectableTowers.ContainsKey(castle.PlayerOwnerNumber))
                        playersSelectableTowers[castle.PlayerOwnerNumber].Remove(castle);
                }
                else
                {
                    if (playersSelectableCastles.ContainsKey(castle.PlayerOwnerNumber))
                        playersSelectableCastles[castle.PlayerOwnerNumber].Remove(castle);
                }
            }

            removeCastles.Clear();

            if (castleToSelect.Count > 0)
            {
                SelectionManager.Instance.ClearSelection(playerNumber);
                foreach (SingleCastle castle in castleToSelect)
                    SelectSingleCastle(castle);
            }

            return castleToSelect.Count > 0;
        }

    }
}
