﻿using Photon.Pun;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class SingleCastle : Selectable
    {
        [Tooltip("Used to offset castle center when group move toward the castle.")]
        [SerializeField] private float _castleOffset = 2.5f;

        [Tooltip("Gameobject represent castle door position")]
        [SerializeField] private Transform _Door = null;

        public float CastleOffset => _castleOffset;
        public Transform Door => _Door;

        public bool isGate = false;
        public bool isVillage = false;
        public bool isTower = false;

        public GameObject villageInfo;

        //silver product
        public int VillageSilverProduct;

        private void Awake() //temp
        {
            if (PhotonNetwork.IsConnected)
            {
                OwnerType = OwnerType.Player;
            }

            if (isVillage)
            {
                VillageArmyUICount villageArmyUI = GetComponent<VillageArmyUICount>();
                VillageSilverProduct = villageArmyUI.SilverProduction;

                if (villageArmyUI.IsPlayerVillage) // Player
                {
                    OwnerType = OwnerType.Player;
                    if (!PhotonNetwork.IsConnected)
                    {
                        PlayerOwnerNumber = 1;
                    }
                }
                else if (villageArmyUI.IsEnemyVillage) //Enemy
                {
                    OwnerType = OwnerType.Enemy;
                    PlayerOwnerNumber = -1;
                }
            }
            else if(isTower)
            {
                TowerArmyUICount towerArmyUI = GetComponent<TowerArmyUICount>();

                if (towerArmyUI.IsPlayerTower) // Player
                {
                    OwnerType = OwnerType.Player;
                    if (!PhotonNetwork.IsConnected)
                    {
                        PlayerOwnerNumber = 1;
                    }
                }
                else if (towerArmyUI.IsEnemyTower) //Enemy
                {
                    OwnerType = OwnerType.Enemy;
                    PlayerOwnerNumber = -1;
                }
            }
            else if (!isGate)
            {
                CastelArmyUICount castelArmyUI = GetComponent<CastelArmyUICount>();

                if (castelArmyUI.iswhitecastel)
                {
                    OwnerType = OwnerType.None;
                }
                if (!castelArmyUI.iswhitecastel && !castelArmyUI.isEnumyCastle) // Player
                {
                    OwnerType = OwnerType.Player;
                    if (!PhotonNetwork.IsConnected)
                    {
                        PlayerOwnerNumber = 1;
                    }
                }
                if (!castelArmyUI.iswhitecastel && castelArmyUI.isEnumyCastle) //Enemy
                {
                    OwnerType = OwnerType.Enemy;
                    PlayerOwnerNumber = -1;
                }
            }
            else
            {
                GateArmyUICount gateArmyUI = GetComponent<GateArmyUICount>();

                if (gateArmyUI.IsPlayerGate) // Player
                {
                    OwnerType = OwnerType.Player;
                    if (!PhotonNetwork.IsConnected)
                    {
                        PlayerOwnerNumber = 1;
                    }
                }
                if (gateArmyUI.IsEnemyGate) //Enemy
                {
                    OwnerType = OwnerType.Enemy;
                    PlayerOwnerNumber = -1;
                }
            }

        }

        //public override void OnSelected()
        //{
        //    base.OnSelected();
        //    if (isVillage)
        //        villageInfo.SetActive(true);
        //}

        //public override void OnDeselected()
        //{
        //    base.OnDeselected();
        //    if (isVillage)
        //        villageInfo.SetActive(false);
        //}
    }
}