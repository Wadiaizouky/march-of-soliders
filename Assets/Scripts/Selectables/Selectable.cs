﻿using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

namespace MarchOfSoldiers
{
    public enum SelectableType
    {
        None,
        Soldier,
        Castle,
        Gate
    }

    public enum OwnerType
    {
        None, Player, Enemy
    }

    public class Selectable : MonoBehaviour
    {
        [SerializeField] private GameObject _selectionMark;
        [SerializeField] private SelectableType _selectableType;

        public SelectableType SelectableType { get => _selectableType; set => _selectableType = value; }
        public bool IsSelected { get; set; }
        public bool disableSelectable = false;
        public bool calculateSilverWhenDisabled = false;

        public OwnerType OwnerType { get; set; }
        public int PlayerOwnerNumber;

        public virtual void OnSelected()
        {
            IsSelected = true;

            if (PlayerOwnerNumber != PlayersManager.Instance.GetLocalPlayerNumber()) return;

            if (_selectionMark)
                _selectionMark.SetActive(true);
            else
                Debug.LogError("no selection mark attached");
        }

        public virtual void OnDeselected()
        {
            IsSelected = false;
            if (_selectionMark)
                _selectionMark.SetActive(false);
            else
                Debug.LogError("no selection mark attached");
        }

        public virtual void OnEnable()
        {
            if (SelectionManager.Instance != null)
            {
                if (disableSelectable) return;

                if (!PhotonNetwork.IsConnected)
                {
                    if (gameObject.CompareTag("Player") || SelectionManager.Instance.testRecord)
                        SelectionManager.Instance.AddSelectable(this, SelectableType);
                }
                else
                {
                    SelectionManager.Instance.AddSelectable(this, SelectableType);
                }

            }

            else
                Debug.LogError(gameObject.name + " there is no active SelectionManager in the scene");
        }

        public virtual void OnDisable()
        {
            // remove this object from selectable list so it can't be controlled while disabled.
            if (SelectionManager.Instance != null)
            {
                if (disableSelectable) return;

                if (gameObject.CompareTag("Player"))
                    SelectionManager.Instance.RemoveSelectable(this, SelectableType);
            }
        }
    }
}