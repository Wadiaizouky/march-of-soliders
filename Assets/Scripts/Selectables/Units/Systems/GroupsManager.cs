﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MarchOfSoldiers
{
    public class GroupsManager : Singleton<GroupsManager>
    {
        private List<GroupOfUnits> currentActiveGroups = new List<GroupOfUnits>();
        private Dictionary<OwnerType, List<GroupOfUnits>> currentActiveGroupMap;
        private Dictionary<int, List<GroupOfUnits>> playersGroups = new Dictionary<int, List<GroupOfUnits>>();

        private void Awake()
        {
            AssignGroupMap();
        }

        private void AssignGroupMap()
        {
            currentActiveGroupMap = new Dictionary<OwnerType, List<GroupOfUnits>>();
            currentActiveGroupMap.Add(OwnerType.Player, new List<GroupOfUnits>());
            currentActiveGroupMap.Add(OwnerType.Enemy, new List<GroupOfUnits>());
        }

        public void AddGroup(GroupOfUnits group)
        {
            if (currentActiveGroupMap == null)
            {
                AssignGroupMap();
            }
            currentActiveGroupMap[group.OwnerType].Add(group);
            currentActiveGroups.Add(group);


            if (!playersGroups.ContainsKey(group.PlayerOwnerNumber))
            {
                playersGroups.Add(group.PlayerOwnerNumber, new List<GroupOfUnits>());
            }

            playersGroups[group.PlayerOwnerNumber].Add(group);
        }

        public void RemoveGroup(GroupOfUnits group)
        {
            if (currentActiveGroupMap == null)
            {
                AssignGroupMap();
            }
            currentActiveGroupMap[group.OwnerType].Remove(group);
            currentActiveGroups.Remove(group);

            playersGroups[group.PlayerOwnerNumber].Remove(group);
        }

        public List<GroupOfUnits> GetAllGroups()
        {
            return currentActiveGroups;
        }

        public List<GroupOfUnits> GetOwnerGroups(OwnerType owner)
        {
            return currentActiveGroupMap[owner];
        }

        public List<GroupOfUnits> GetPlayerGroups(int playerNumber)
        {
            if (!playersGroups.ContainsKey(playerNumber)) return new List<GroupOfUnits>();
            return playersGroups[playerNumber];
        }

        public List<GroupOfUnits> GetTeamGroups(int teamNumber)
        {
            List<GroupOfUnits> teamGroups = new List<GroupOfUnits>();
            foreach (Player player in PlayersManager.Instance.GetTeamPlayers(teamNumber))
            {
                teamGroups.AddRange(GetPlayerGroups(player.playerNumber));
            }
            return teamGroups;
        }

        public List<GroupOfUnits> GetTheRemainingGroups(int basePlayer)
        {
            List<GroupOfUnits> remainingGroups = new List<GroupOfUnits>();
            foreach (int num in playersGroups.Keys)
            {
                if (num != basePlayer)
                    remainingGroups.AddRange(playersGroups[num]);
            }
            return remainingGroups;
        }

        public void ConvertSwordUnitsToRam(List<GroupOfUnits> groups)
        {
            int groupsSize = 0;
            for (int i = 0; i < groups.Count; i++)
            {
                groupsSize += groups[i].groupSize;
            }

            //if (groupsSize < 200) return;

            int sizeToDelete = 200;

            for (int i = 0; i < groups.Count; i++)
            {
                if (groups[i].groupSize - sizeToDelete >= 0)
                {
                    int temp = sizeToDelete;
                    sizeToDelete -= groups[i].groupSize;
                    groups[i].groupSize -= temp;
                }
                else
                {
                    sizeToDelete -= groups[i].groupSize;
                    if (i != 0)
                    {
                        Destroy(groups[i].gameObject);
                    }
                }
            }

            foreach (SingleUnit u in groups[0].units)
            {
                Destroy(u.gameObject);
            }

            groups[0].groupType = GroupType.Ram;

            groups[0].units.Clear();
            groups[0].groupSize = 200;

            SingleUnit movementCommander = Instantiate(groups[0].spawnProp.unitRamPrefab, groups[0].groupOfUnits_Movement.movementCommander.transform.position
                     , Quaternion.identity, groups[0].transform).GetComponent<SingleUnit>();

            movementCommander.Group = groups[0];
            movementCommander.PlayerOwnerNumber = groups[0].PlayerOwnerNumber;
            movementCommander.VirtualValue = 200;

            groups[0].AddUnit(movementCommander);

            movementCommander.InvisibleMovementCommander = true;

            if (UnitsSelection.Instance.selectableSoldiers.Contains(movementCommander))
            {
                UnitsSelection.Instance.soliderCount--;
            }

            Destroy(groups[0].groupOfUnits_Movement.movementCommander.gameObject);
            groups[0].groupOfUnits_Movement.movementCommander = movementCommander;
        }

    }
}