﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class UnitsOperations : Singleton<UnitsOperations>
    {
        private Camera _mainCamera;

        private void Start()
        {
            _mainCamera = Camera.main;
        }

        public void MoveGroups(GroupOfUnits group, List<Vector2> destinations, SingleCastle targetCastle)
        {
            if (targetCastle)
            {
                group.groupOfUnits_Movement.MoveToCastle(targetCastle);
            }
            else
            {
                group.groupOfUnits_Movement.Move(destinations, false);
            }

            group.targetCastle = targetCastle;
        }

        private void MoveMultipleGroups(int playerNumber,Vector3 destination, bool offset, SingleCastle targetCastle)
        {
            List<GroupOfUnits> selectedGroups = UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(playerNumber);

            Vector3 averagePosition = CalculateAveragePosition(selectedGroups);

            foreach (var group in selectedGroups)
            {
                group.targetCastle = targetCastle;
                group.goal = averagePosition - group.centerOfGroup;

                if (!offset)
                {
                    //Vector2 distanceVector = (destination - group.centerOfGroup);
                    //distanceVector.Normalize();
                    //distanceVector *= -targetCastle.CastleOffset;
                    //group.groupOfUnits_Movement.Move((Vector2)targetCastle.transform.position + distanceVector, false);
                    group.groupOfUnits_Movement.MoveToCastle(targetCastle);
                }
                else
                    group.groupOfUnits_Movement.Move(destination + group.goal, true);
            }
        }

        private Vector3 CalculateAveragePosition(List<GroupOfUnits> selectedGroups)
        {
            Vector3 averagePosition = Vector3.zero;
            foreach (var group in selectedGroups)
                averagePosition += group.centerOfGroup;
            averagePosition /= selectedGroups.Count;
            return averagePosition;
        }


        //////////////////////////////// UnUsed /////////////////////////////////
        //private void MoveGroups()
        //{
        //    if (!_mainCamera) return;

        //    Vector2 destination = _mainCamera.ScreenToWorldPoint(Input.mousePosition);

        //    RaycastHit2D hit = Physics2D.Raycast(
        //        origin: destination,
        //        direction: Vector2.zero,
        //        distance: 100,
        //        layerMask: SelectionManager.Instance._selectableObjectsLayer);

        //    if (hit)
        //    {
        //        Selectable selectable = hit.collider.GetComponent<Selectable>();

        //        if (selectable.SelectableType == SelectableType.Castle)
        //            MoveMultipleGroups(hit.transform.position, false, selectable as SingleCastle);
        //    }
        //    else
        //        MoveMultipleGroups(destination, true, null);
        //}

        //public void SeparateUnits(int percent)
        //{
        //    if (UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(1).Count != 1)
        //    {
        //        Debug.LogAssertion("select exactly One group to separate!");
        //        return;
        //    }

        //    // just for test, choose static group and first selected group.
        //    // TODO: remove static values.
        //    GroupOfUnits group = UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(1)[0];

        //    // make sure 
        //    if (group.groupSize < 2)
        //    {
        //        Debug.LogError("can't separate group that has less than 2 units");
        //        return;
        //    }

        //    // make sure there is valid group to separate.
        //    if (group == null)
        //    {
        //        Debug.LogError("their is no group to separate");
        //        return;
        //    }

        //    // calculate old and new group sizes.
        //    int newGroupSize = group.groupSize * percent / 100;
        //    int oldGroupSize = group.groupSize - newGroupSize;

        //    if (!group.commander)
        //    {
        //        Debug.LogError("this group don't have commander!");
        //        return;
        //    }

        //    // get commander position to instantiate new group at it's position.
        //    Vector2 groupPos = group.commander.transform.position;

        //    // destroy original group and remove it from selected group.
        //    Destroy(UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(1)[0].gameObject);
        //    UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(1).Remove(group);

        //    //TODO: fix create units 

        //    // create first group.
        //    //CreateGroup(oldGroupSize, groupPos, group.GroupType);

        //    // create second group at different position.
        //    groupPos.x += 0.5f;
        //    //CreateGroup(newGroupSize, groupPos, group.GroupType);
        //}
    }
}
