﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;


namespace MarchOfSoldiers
{
    public class CreateGroupParameters
    {
        public int GroupSize = 0;
        public Vector3 Position = Vector3.zero;
        public GroupType GroupType = GroupType.Sword;
        public OwnerType GroupOwner = OwnerType.Player;
        public SingleCastle SourceCastle = null;
        public SingleCastle TargetCastle = null;
    }

    public class UnitsSpawner : Singleton<UnitsSpawner>
    {

        public GroupOfUnits CreateGroup(int groupSize, List<Vector2> positions, GroupType groupType,
                                        OwnerType groupOwner, int playerNumber, Vector2 mousePosition,
                                        SingleCastle sourceCastle = null, SingleCastle targetCastle = null)
        {
            if (groupSize <= 0 || positions == null || positions.Count.Equals(0)) return null;

            GroupOfUnits groupPrefab = GetGroupPrefab(groupOwner, playerNumber, groupType);

            if (!groupPrefab) return null;

            GroupOfUnits group = Instantiate(groupPrefab, mousePosition, Quaternion.identity) as GroupOfUnits;

            int unitsToCreate = Mathf.Min(groupSize, SelectionManager.MAX_UNITS_PER_GROUP);
            group.OwnerType = groupOwner;
            group.PlayerOwnerNumber = playerNumber;

            CreateUnits(group, unitsToCreate, sourceCastle, targetCastle, positions, mousePosition);

            group.groupSize = unitsToCreate;
            group.SetGroupCommander();

            group.sourceCastle = sourceCastle;
            group.targetCastle = targetCastle;

            foreach (SingleUnit unit in group.units)
            {
                unit.PlayerOwnerNumber = playerNumber;
            }

            return group;
        }

        private GroupOfUnits GetGroupPrefab(OwnerType groupOwner, int playerNumber, GroupType groupType)
        {
            GroupOfUnits groupPrefab = null;
            switch (groupType)
            {
                case GroupType.Sword:
                    if (PhotonNetwork.IsConnected)
                    {
                        switch (playerNumber)
                        {
                            case 1:
                                groupPrefab = SelectionManager.Instance._groupOfPlayerSowrdUnits;
                                break;
                            case 2:
                                groupPrefab = SelectionManager.Instance._groupOfEnemySowrdUnits;
                                break;
                        }
                    }
                    else
                    {
                        groupPrefab = groupOwner == OwnerType.Player
                               ? SelectionManager.Instance._groupOfPlayerSowrdUnits
                               : SelectionManager.Instance._groupOfEnemySowrdUnits;
                    }

                    break;
                case GroupType.Spear:
                    Debug.LogAssertion("NO Spear Units exist!");
                    break;
                case GroupType.Horse:
                    Debug.LogAssertion("NO Horse Units exist!");
                    break;
                default:
                    break;
            }

            return groupPrefab;

        }

        private void CreateUnits(GroupOfUnits group, int groupSize, SingleCastle sourceCastle,
            SingleCastle targetCastle, List<Vector2> positions, Vector2 mousePosition)
        {
            if (!group || groupSize <= 0 || positions == null || positions.Count.Equals(0)) return;

            int unitsToCreate = Mathf.CeilToInt(groupSize / SelectionManager.MAX_VIRTUAL_SIZE);

            List<SingleUnit> createdUnits = new List<SingleUnit>();

            int unitCounter = 0;
            int counter = 0;
            foreach (var unitPosition in positions)
            {
                if (unitCounter < unitsToCreate)
                {
                    SingleUnit unit = CreateUnit(unitPosition, group, sourceCastle, mousePosition);
                    createdUnits.Add(unit);
                    //group.SetGroupCommander();

                    unit.name += " " + unitCounter;

                    unit.VirtualValue = Mathf.Min(groupSize, (int)SelectionManager.MAX_VIRTUAL_SIZE);
                    groupSize -= unit.VirtualValue;

                    unitCounter++;
                    counter++;

                }
            }

            group.SetGroupCommander();
            unitCounter = 0;

            group.groupOfUnits_Movement.BuildMovementCommander(sourceCastle, targetCastle, CalculateUnitPosition(sourceCastle, mousePosition), positions[0]);

            for (int i = 0; i < positions.Count; i++)
            {
                if (unitCounter < unitsToCreate)
                {
                    Vector2 unitPosition = positions[i];
                    SingleUnit unit = createdUnits[i];

                    unit.Agent.enabled = true;

                    if (sourceCastle)
                    {
                        if (unit.Agent.isOnNavMesh)
                        {
                            // move toward target castle? offset the destination.
                            if (targetCastle)
                            {
                                unit.Unit_Movement.MoveToCastle(targetCastle);
                            }
                            else
                                unit.Unit_Movement.MoveToPoint(unitPosition, false);
                        }
                        else
                        {
                            Debug.LogError("Can't move unit, make sure to put the unit on active NavMesh");
                            return;
                        }

                    }
                    unitCounter++;
                }
            }

            group.centerOfGroup /= unitCounter;
            group.SetUnitsGoal(positions);
        }


        private SingleUnit CreateUnit(Vector3 unitPosition, GroupOfUnits group
                , SingleCastle sourceCastle, Vector2 mousePosition)
        {
            SingleUnit unit;

            // there is no castle? create units in there positions directly.
            if (!sourceCastle)
            {
                unit = Instantiate(group.spawnProp.unitPrefab, group.transform);
                unit.transform.localPosition = unitPosition;
            }
            else
            {
                Vector2 instantiatePosition = CalculateUnitPosition(sourceCastle, mousePosition);
                unit = Instantiate(group.spawnProp.unitPrefab, instantiatePosition
                                    , Quaternion.identity, group.transform);
            }

            group.centerOfGroup += unitPosition;
            group.AddUnit(unit);

            if (MinimapManger.Instance.TowerNumber >= 3)
            {
                unit.minimapMark.SetActive(true);
            }

            return unit;
        }

        private Vector3 CalculateUnitPosition(SingleCastle sourceCastle, Vector2 mousePosition)
        {
            Vector2 instantiatePosition = (mousePosition - (Vector2)sourceCastle.transform.position);
            instantiatePosition.Normalize();
            instantiatePosition *= sourceCastle.CastleOffset;
            return (Vector2)sourceCastle.transform.position + instantiatePosition;
        }


        // ---------------------------------- Unused --------------------------------
        public GroupOfUnits CreateGroup(CreateGroupParameters parameters)
        {
            if (parameters.GroupSize <= 0) return null;

            GroupOfUnits groupPrefab = GetGroupPrefab(parameters.GroupOwner, 1, parameters.GroupType);

            if (!groupPrefab) return null;

            GroupOfUnits group = Instantiate(groupPrefab, parameters.Position, Quaternion.identity) as GroupOfUnits;

            int unitsToCreate = Mathf.Min(parameters.GroupSize
                , SelectionManager.MAX_UNITS_PER_GROUP);

            CreateUnits(group, unitsToCreate, parameters.SourceCastle);

            group.groupSize = unitsToCreate;
            group.SetGroupCommander();

            return group;
        }

        private void CreateUnits(GroupOfUnits group, int groupSize, SingleCastle sourceCastle)
        {
            if (!group || groupSize <= 0) return;

            int unitsToCreate = Mathf.CeilToInt(groupSize / SelectionManager.MAX_VIRTUAL_SIZE);

            int unitCounter = 0;

            // list of positions that units will use to calculate it's goal bases on it.
            List<Vector2> unitPositions = new List<Vector2>();

            // instantiate units inside the group. 
            for (int i = 0; i < group.spawnProp.unitInstantiatePositions.GetLength(0); i++)
            {
                for (int j = 0; j < group.spawnProp.unitInstantiatePositions.GetLength(1); j++)
                {
                    if (unitCounter < unitsToCreate)
                    {
                        SingleUnit unit;

                        // there is no castle? create units in there positions directly.
                        if (!sourceCastle)
                        {
                            unit = Instantiate(group.spawnProp.unitPrefab, group.transform);
                            unit.transform.localPosition = group.spawnProp.unitInstantiatePositions[i, j];
                        }

                        // otherwise, start there position at castle door.
                        else
                            unit = Instantiate(group.spawnProp.unitPrefab, sourceCastle.Door.position, Quaternion.identity, group.transform);

                        // change unit name to differentiate between them.
                        unit.name += " " + unitCounter;

                        // enable navMesh agent and move it from door to it's position
                        if (unit.Agent)
                            unit.Agent.enabled = true;
                        else
                            Debug.LogError("there is no agent associated with this unit");

                        // fetch unit position (from group initial positions)
                        unitPositions.Add(group.transform.TransformPoint(group.spawnProp.unitInstantiatePositions[i, j]));

                        // add unit position to calculate group center
                        group.centerOfGroup += (Vector3)unitPositions[unitCounter];

                        // add it to the group.
                        group.AddUnit(unit);

                        // set unit virtual value
                        unit.VirtualValue = groupSize >= SelectionManager.MAX_VIRTUAL_SIZE ? (int)SelectionManager.MAX_VIRTUAL_SIZE : groupSize;

                        // decrease group size
                        groupSize -= (int)SelectionManager.MAX_VIRTUAL_SIZE;

                        unitCounter++;
                    }

                    else
                        break;
                }
            }

            group.centerOfGroup /= unitCounter;

            // set goal for every unit.
            group.SetUnitsGoal(unitPositions);

            // Move every unit to it's opposite position. 
            for (int i = 0; i < group.units.Count; i++)
            {
                SingleUnit unit = group.units[group.units.Count - 1 - i];
                unit.Unit_Movement.MoveToPoint(unitPositions[i], false);
            }
        }
    }
}