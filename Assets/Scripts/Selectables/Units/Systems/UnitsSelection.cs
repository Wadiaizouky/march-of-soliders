﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MarchOfSoldiers
{
    public class UnitsSelection : Singleton<UnitsSelection>
    {
        [HideInInspector] public List<SingleUnit> selectableSoldiers = new List<SingleUnit>();
        [HideInInspector] public List<GroupOfUnits> selectableGroups = new List<GroupOfUnits>();

        private Dictionary<int, List<GroupOfUnits>> playersSelectedGroups = new Dictionary<int, List<GroupOfUnits>>();

        [HideInInspector] public int soliderCount = 0;


        public List<GroupOfUnits> GetSelectedGroupsByPlayerNum(int playerNumber)
        {
            return (playersSelectedGroups.ContainsKey(playerNumber))
                ? playersSelectedGroups[playerNumber] : new List<GroupOfUnits>();
        }

        private void AddToSelectedGroups(GroupOfUnits group)
        {
            if (!playersSelectedGroups.ContainsKey(group.PlayerOwnerNumber))
            {
                playersSelectedGroups.Add(group.PlayerOwnerNumber, new List<GroupOfUnits>());
            }
            playersSelectedGroups[group.PlayerOwnerNumber].Add(group);
        }

        public void AddSelectable(SingleUnit unit)
        {
            selectableSoldiers.Add(unit);
            soliderCount++;
        }

        public void RemoveSelectable(SingleUnit unit)
        {
            selectableSoldiers.Remove(unit);
            if (!unit.InvisibleMovementCommander)
                soliderCount--;
        }

        public void OnSelectionSoldier(Selectable selectableObject, int playerNumber)
        {
            SingleUnit singleUnit = selectableObject as SingleUnit;
            if (!singleUnit) return;

            if (singleUnit.PlayerOwnerNumber != playerNumber) return;

            if (DetectMultiSelect())
            {
                if (SelectionManager.Instance.currentSelectedType == SelectableType.None
                    || SelectionManager.Instance.currentSelectedType == SelectableType.Soldier)
                {
                    if (singleUnit.IsSelected)
                        DeselectSingleUnit(singleUnit);
                    else
                        SelectSingleUnit(singleUnit);
                }
            }
            else
            {
                SelectionManager.Instance.ClearSelection(playerNumber);
                SelectSingleUnit(singleUnit);
            }

            UnitsSlots_UI.Instance.UpdateUISlots();
        }

        private bool DetectMultiSelect()
        {
            return Input.GetKey("left ctrl");
        }

        public void ClearUnitsSelection(int playerNumber)
        {
            if (GetSelectedGroupsByPlayerNum(playerNumber).Count > 0)
            {
                foreach (var group in GetSelectedGroupsByPlayerNum(playerNumber))
                    group.OnDeselected();

                if (playersSelectedGroups.ContainsKey(playerNumber))
                    playersSelectedGroups[playerNumber].Clear();

                UnitsSlots_UI.Instance.UpdateUISlots();
            }
        }

        private void SelectSingleUnit(SingleUnit unit)
        {
            if (!unit) return;

            if (unit.Group && !unit.Group.isSelected)
            {
                SelectGroup(unit.Group); 
            }

            SelectionManager.Instance.currentSelectedType = unit.SelectableType;
        }

        private void DeselectSingleUnit(SingleUnit unit)
        {
            if (!unit) return;

            DeselectGroup(unit.Group);
        }

        public void SelectGroup(GroupOfUnits group)
        {
            if (!group) return;
            if (GetSelectedGroupsByPlayerNum(group.PlayerOwnerNumber).Count >= 12) return;

            AddToSelectedGroups(group);
            group.OnSelected();
        }

        private void DeselectGroup(GroupOfUnits group)
        {
            if (!group) return;

            if (playersSelectedGroups.ContainsKey(group.PlayerOwnerNumber))
                playersSelectedGroups[group.PlayerOwnerNumber].Remove(group)
;
            group.OnDeselected();
            UnitsSlots_UI.Instance.UpdateUISlots();
        }

        public bool SelectMultipleSoldiers(ref Rect selectionBox, Vector3 pos1, Vector3 pos2, int playerNumber)
        {
            List<SingleUnit> unitsToSelect = new List<SingleUnit>();
            HashSet<GroupOfUnits> groupsToSelect = new HashSet<GroupOfUnits>();
            List<SingleUnit> removeUnits = new List<SingleUnit>();

            foreach (SingleUnit solider in selectableSoldiers)
            {
                if (solider.PlayerOwnerNumber != playerNumber) continue;
       
                if (solider)
                {
                    if (CanSelectSolider(ref selectionBox, pos1, pos2, solider, groupsToSelect))
                    {
                        unitsToSelect.Add(solider);
                        groupsToSelect.Add(solider.Group);
                    }
                }
                else
                {
                    removeUnits.Add(solider);
                }
            }

            foreach (SingleUnit selectedObject in removeUnits)
                selectableSoldiers.Remove(selectedObject);
            removeUnits.Clear();

            if (unitsToSelect.Count > 0)
            {
                SelectionManager.Instance.ClearSelection(playerNumber);
                for (int i = 0; i < unitsToSelect.Count; i++)
                    SelectSingleUnit(unitsToSelect[i]);
            }

            if (GetSelectedGroupsByPlayerNum(playerNumber).Count > 0)
                UnitsSlots_UI.Instance.UpdateUISlots();

            return unitsToSelect.Count > 0;
        }

        private bool CanSelectSolider(ref Rect selectionBox, Vector3 pos1, Vector3 pos2, SingleUnit solider, HashSet<GroupOfUnits> groupsToSelects)
        {
            return pos1.x < solider.transform.position.x && solider.transform.position.x < pos2.x &&
                   pos1.y > solider.transform.position.y && solider.transform.position.y > pos2.y
                                     && (groupsToSelects.Contains(solider.Group)
                                     || groupsToSelects.Count <= SelectionManager.MAX_SELECTABLE_GROUP);
        }

    }
}