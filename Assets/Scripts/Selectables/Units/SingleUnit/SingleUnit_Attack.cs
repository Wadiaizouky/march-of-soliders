﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MarchOfSoldiers
{
    [RequireComponent(typeof(SingleUnit))]
    public class SingleUnit_Attack : MonoBehaviour
    {

        private SingleUnit singleUnit;
        private bool canAttack = true;


        private void Awake()
        {
            singleUnit = GetComponent<SingleUnit>();
        }

        public void OnCollisionAttack(SingleUnit targetUnit)
        {
            if (targetUnit)
            {
                UnitsSlots_UI.Instance.UpdateUISlots();

                if (!canAttack || !targetUnit.Unit_Attack.CanAttack()
                    || singleUnit.IsKilled || targetUnit.IsKilled) return;

                CollisionAttack(targetUnit);
            }
        }

        private void CollisionAttack(SingleUnit collidingUnit)
        {
            int singleUnitDamage = singleUnit.VirtualValue;
            int collidingUnitDamage = collidingUnit.VirtualValue;

            Attack(collidingUnit, singleUnitDamage);
            collidingUnit.Unit_Attack.Attack(singleUnit, collidingUnitDamage);

            UnitsSlots_UI.Instance.UpdateUISlots();
        }

        private void Attack(SingleUnit targetUnit, int damage)
        {
            canAttack = false;
            targetUnit.TakeDamage(damage);
            StartCoroutine(ReactiveAttacking());
        }

        private IEnumerator ReactiveAttacking()
        {
            yield return new WaitForEndOfFrame();
            canAttack = true;
        }

        public bool CanAttack()
        {
            return canAttack;
        }

    }
}