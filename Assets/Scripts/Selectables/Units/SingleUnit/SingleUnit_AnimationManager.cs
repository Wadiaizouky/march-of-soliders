﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MarchOfSoldiers
{
    public class SingleUnit_AnimationManager : MonoBehaviour
    {
        private SingleUnit unit;
        private Animator anim;
        private bool active = false;

        private void Awake()
        {
            unit = GetComponent<SingleUnit>();
            anim = GetComponent<Animator>();
            anim.enabled = false;
        }

        private void Start()
        {
            SetStartAnimation();
            lastPoint = transform.position;
        }

        private void SetStartAnimation()
        {
            Vector3 dir = unit.Unit_Movement.GetFinalDestination() - transform.position;
            anim.SetFloat("Direction_X", dir.x);
            anim.SetFloat("Direction_Y", dir.y);
            anim.SetBool("Walk", true);
        }

        private Vector3 lastPoint;

        private void Update()
        {
            if (PasueController.Instance.IsPasued)
            {
                anim.enabled = false;
            }
            else
            {
                anim.enabled = true;
            }
            UpdateAnimator();

            if (!active)
            {
                if (!unit.Agent.pathPending)
                {
                    anim.enabled = true;
                    active = true;
                }
            }

            lastPoint = transform.position;
        }

        private void UpdateAnimator()
        {
            Vector3 dir = transform.position - lastPoint;

            if (dir.sqrMagnitude > 0.05f * 0.05f)
            {
                anim.SetFloat("Direction_X", dir.x);
                anim.SetFloat("Direction_Y", dir.y);
            }

            anim.SetBool("Walk", unit.Unit_Movement.MovementAniamtion());
        }

        internal void PlayDeathAnimation()
        {
            anim.Play("Death Blend Tree");
        }

        public Animator GetAnimator()
        {
            return anim;
        }
    }
}