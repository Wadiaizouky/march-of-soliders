﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Photon.Pun;

namespace MarchOfSoldiers
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class SingleUnit : Selectable
    {
        public string targetTag = string.Empty;
        [SerializeField] private int _virtualValue = 10;

        public GameObject minimapMark;

        #region Properties

        public int VirtualValue { set => _virtualValue = value; get => _virtualValue; }

        public GroupOfUnits Group { get; set; }
        public NavMeshAgent Agent { get; private set; }
        public SingleUnit_Movement Unit_Movement { get; set; }
        public SingleUnit_Attack Unit_Attack { get; set; }
        public SingleUnit_AnimationManager Unit_AnimationManager { get; set; }

        public bool IsKilled { get; set; }
        public bool IsCommander { get; set; }
        public bool InvisibleMovementCommander { get; set; }

        #endregion

        private void Awake()
        {
            Agent = GetComponent<NavMeshAgent>();
            Unit_Movement = GetComponent<SingleUnit_Movement>();
            Unit_Attack = GetComponent<SingleUnit_Attack>();
            Unit_AnimationManager = GetComponent<SingleUnit_AnimationManager>();
        }

        private void Start()
        {
            NavMesh.avoidancePredictionTime = 0.5f;
            if (PhotonNetwork.IsConnected)
            {
                gameObject.tag = "Player";
            }
        }

        public void ApplyInvisible()
        {
            Destroy(Unit_Attack);
            Destroy(Unit_AnimationManager);
            Destroy(GetComponent<Animator>());
            InvisibleMovementCommander = true;
            if (UnitsSelection.Instance.selectableSoldiers.Contains(this))
            {
                UnitsSelection.Instance.soliderCount--;
            }
        }

        public void JoinGroup(GroupOfUnits group)
        {
            Group = group;
        }

        public void TakeDamage(int damage)
        {
            if (IsKilled) return;

            int lossingValue = Mathf.Min(VirtualValue, damage);
            Group.UpdateGroupSize(Group, lossingValue);

            VirtualValue -= damage;

            if (VirtualValue <= 0)
            {
                Death();
                return;
            }
        }

        private void OnDestroy()
        {
            if (Group == null) return;

            Group.RemoveUnit(this);

            if (UnitsSlots_UI.Instance)
            {
                UnitsSlots_UI.Instance.UpdateUISlots();
            }
        }

        private void Death()
        {
            IsKilled = true;
            Group.RemoveUnit(this);

            if (Agent.enabled)
                Agent.isStopped = true;
            Unit_AnimationManager.PlayDeathAnimation();
        }

        /// <summary>
        /// Called from Death Animation event.
        /// </summary>
        public void DestroySolider()
        {
            Destroy(gameObject);
        }
    }
}