﻿using UnityEngine;

namespace MarchOfSoldiers
{
    [RequireComponent(typeof(SingleUnit))]
    public class SingleUnit_Movement : MonoBehaviour
    {
        public Vector3 finalDestination;
        private Vector3 goal;
        private Vector3 currentDestination;
        private bool followCommander = false;
        [HideInInspector] public bool commanderReachGoal = false;
        private Vector3 followCommanderOffset = Vector2.one;
        private bool move = false;
        private bool movementAnimation = false;
        private SingleCastle targetCastle;
        private bool enterCastle;
        [HideInInspector] public float speed;
        private float stopMovementTimer;


        private SingleUnit singleUnit;


        private void Awake()
        {
            singleUnit = GetComponent<SingleUnit>();
        }

        private void Start()
        {
            PreventAgentFromRotation();
            speed = singleUnit.Agent.speed;

            //singleUnit.Group.OnCommanderChange += OnCommanderChange;

            SetNavAgentState();
        }

        private void PreventAgentFromRotation()
        {
            singleUnit.Agent.updateRotation = false;
            singleUnit.Agent.updateUpAxis = false;
        }

        //private void OnDisable()
        //{
        //    singleUnit.Group.OnCommanderChange -= OnCommanderChange;
        //}

        public void SetFollowCommanderOffset(Vector3 offset)
        {
            followCommanderOffset = offset;
        }

        //private void OnCommanderChange()
        //{
        //    SetNavAgentState();

        //    if (commanderReachGoal) return;

        //    if (enterCastle)
        //    {
        //        MoveToCastle(targetCastle);
        //    }
        //    else
        //    {
        //        MoveToPoint(finalDestination, false);
        //    }

        //    //followCommanderOffset = singleUnit.Group.commander.transform.position + transform.position;
        //}

        private void SetNavAgentState()
        {
            //if (singleUnit.IsCommander)
            if (singleUnit.InvisibleMovementCommander)
            {
                if (!singleUnit.Agent.enabled)
                    singleUnit.Agent.enabled = true;
            }
            else
            {
                if (singleUnit.Agent.enabled)
                    singleUnit.Agent.enabled = false;
            }
        }

        private void Update()
        {
            if (PasueController.Instance.IsPasued) return;
            if (singleUnit.IsKilled || !move) return;
            Move();
        }

        private void Move()
        {
            if (followCommander)
            {
                UpdateCurrentDestination();
                transform.position = Vector3.MoveTowards(transform.position, currentDestination, speed * Time.deltaTime);
            }

            UpdateMovementState();
        }

        private void UpdateCurrentDestination()
        {
            if (commanderReachGoal)
            {
                currentDestination = finalDestination;
            }
            else
            {
                //if (enterCastle)
                //{
                //    Vector3 commanderDiretion = singleUnit.Group.commander.Unit_Movement.GetDirectionVector();
                //    currentDestination = singleUnit.Group.commander.transform.position;
                //    currentDestination += (commanderDiretion * followCommanderOffset.y);
                //    currentDestination += (Quaternion.Euler(0, 0, 90) * commanderDiretion) * followCommanderOffset.x;
                //}
                //else
                //{
                currentDestination = singleUnit.Group.groupOfUnits_Movement.movementCommander.transform.position - followCommanderOffset;
                //}

            }
        }

        private void UpdateMovementState()
        {
            if (CheckIfReachGoal())
            {
                ReachGoal();
            }
        }

        private bool CheckIfReachGoal()
        {
            float disFromGoal = MathUtility.SqrDistance(transform.position, finalDestination);

            return (enterCastle && disFromGoal < 30f)
                || (disFromGoal < 1 * 1);
        }

        private void ReachGoal()
        {
            if (enterCastle)
            {
                if (!commanderReachGoal)
                {
                    commanderReachGoal = true;
                    singleUnit.Group.groupOfUnits_Movement.OnCommanderReachGoal();
                }

                EnterTargetCastle();
            }

            if (singleUnit.InvisibleMovementCommander && !commanderReachGoal)
            {
                commanderReachGoal = true;
                singleUnit.Group.groupOfUnits_Movement.OnCommanderReachGoal();
            }

            stopMovementTimer += Time.deltaTime;
            if (stopMovementTimer > 0.1f)
            {
                movementAnimation = false;
            }
            if (stopMovementTimer > 1.0f)
            {
                stopMovementTimer = 0;
                StopMoving();
            }

        }

        private void EnterTargetCastle()
        {
            if (singleUnit.InvisibleMovementCommander && singleUnit.Group.groupType == GroupType.Sword)
            {
                Destroy(gameObject);
                return;
            }

            if (targetCastle.isVillage)
            {
                targetCastle.GetComponent<VillageArmyUICount>()
                   .OnCollisionWithUnit(gameObject);
            }
            else if (targetCastle.isGate)
            {
                targetCastle.GetComponent<GateArmyUICount>()
                                   .OnCollisionWithUnit(gameObject);
            }
            else if (targetCastle.isTower)
            {
                targetCastle.GetComponent<TowerArmyUICount>()
                                   .OnCollisionWithUnit(gameObject);
            }
            else
            {
                targetCastle.GetComponent<CastelArmyUICount>()
                                    .OnCollisionWithUnit(gameObject);
            }
        }

        private void StopMoving()
        {
            move = false;
            if (singleUnit.InvisibleMovementCommander)
            {
                singleUnit.Agent.isStopped = true;
            }
        }

        public void MoveToCastle(SingleCastle targetCastle)
        {
            if (singleUnit.IsKilled) return;
            enterCastle = true;
            this.targetCastle = targetCastle;

            UpdateMovementTarget(targetCastle.transform.position);
        }

        public void MoveToPoint(Vector3 destinationPoint, bool offset)
        {
            if (singleUnit.IsKilled) return;
            enterCastle = false;
            this.targetCastle = null;

            UpdateMovementTarget(offset ? destinationPoint + goal : destinationPoint);
        }

        private void UpdateMovementTarget(Vector3 destinationPoint)
        {
            if (singleUnit.IsKilled) return;
            move = true;
            movementAnimation = true;

            finalDestination = destinationPoint;

            if (singleUnit.InvisibleMovementCommander)
            {
                followCommander = false;
                singleUnit.Agent.isStopped = false;
                singleUnit.Agent.SetDestination(finalDestination);
            }
            else
            {
                followCommander = true;
                if (singleUnit.Agent.enabled)
                    singleUnit.Agent.isStopped = true;

                UpdateCurrentDestination();
            }
            commanderReachGoal = false;
        }

        public void OnCommanderReachTarget()
        {
            if (singleUnit.InvisibleMovementCommander) return;
            commanderReachGoal = true;
        }

        public void CalculatePositionGoal(Vector3 position)
        {
            goal = singleUnit.Group.centerOfGroup - position;
        }

        public Vector3 GetDirectionVector()
        {
            return (finalDestination - transform.position).normalized;
        }

        public bool IsMoving()
        {
            return move;
        }

        public bool MovementAniamtion()
        {
            return movementAnimation;
        }

        public Vector3 GetFinalDestination()
        {
            return finalDestination;
        }
    }
}