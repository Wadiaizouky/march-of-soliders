﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class CollisionManager : Singleton<CollisionManager>
    {

        private UnitsAttackDetection unitsAttackDetection;
        private UnitsCollisionDetection unitsCollisionDetection;

        private void Start()
        {
            unitsAttackDetection = UnitsAttackDetection.Instance;
            unitsCollisionDetection = UnitsCollisionDetection.Instance;
        }

        private int interval = 2;

        private void Update()
        {
            if (Time.frameCount % interval == 0)
            {
                unitsAttackDetection.OnUpdate();
            }
            else if (Time.frameCount % interval == 1)
            {
                unitsCollisionDetection.OnUpdate();
            }
        }


    }
}