﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class UnitsCollisionDetection : Singleton<UnitsCollisionDetection>
    {
        private const float DetectGroup_Radius = 20f;
        private const float DetectUnit_Radius = 4.7f;

        private List<CollisionGroupsWithCastle> activeCollisionGroups = new List<CollisionGroupsWithCastle>();


        private int interval = 35;

        public void OnUpdate()
        {
            if (Time.frameCount % interval == 0)
            {
                UpdateActiveCollisionGroups();
            }
            else
            {
                UpdateUnitsDetection();
            }
        }

        private void UpdateActiveCollisionGroups()
        {
            if (SelectionManager.Instance == null) return;

            activeCollisionGroups = new List<CollisionGroupsWithCastle>();

            List<GroupOfUnits> groups = GroupsManager.Instance.GetAllGroups();

            List<SingleCastle> castles = new List<SingleCastle>(SelectionManager.Instance.Allgate.Count);

            //castles.AddRange(SelectionManager.Instance.Allcastle);
            castles.AddRange(SelectionManager.Instance.Allgate);


            foreach (GroupOfUnits group in groups)
            {
                foreach (SingleCastle castle in castles)
                {
                    if (group == null || castle == null || group.commander == null) continue;

                    if (DetectCollisionGroupsWithCastle(group, castle))
                    {
                        activeCollisionGroups.Add(new CollisionGroupsWithCastle(group, castle));
                    }
                }
            }
        }

        private bool DetectCollisionGroupsWithCastle(GroupOfUnits group, Selectable castle)
        {
            float distance = MathUtility.SqrDistance(group.commander.transform.position
                , castle.transform.position);
            return (distance <= DetectGroup_Radius * DetectGroup_Radius) ? true : false;
        }

        private void UpdateUnitsDetection()
        {
            foreach (CollisionGroupsWithCastle collisionGroups in activeCollisionGroups)
            {
                foreach (SingleUnit unit in collisionGroups.group.units)
                {
                    if (unit.IsKilled) continue;

                    if (DetectCollisionUnitWithCastle(unit, collisionGroups.castle))
                    {
                        if (collisionGroups.castle.GetComponent<CastelArmyUICount>() != null)
                        {
                            collisionGroups.castle.GetComponent<CastelArmyUICount>()
                                                 .OnCollisionWithUnit(unit.gameObject);
                        }
                        else
                        {
                            collisionGroups.castle.GetComponent<GateArmyUICount>()
                                                .OnCollisionWithUnit(unit.gameObject);
                        }
                    }
                }
            }
        }

        private bool DetectCollisionUnitWithCastle(SingleUnit unit, SingleCastle castle)
        {
            float distance = MathUtility.SqrDistance(unit.transform.position
                , castle.transform.position);

            float detectRadius = DetectUnit_Radius;
            //if(castle.isGate)
            //{
            //    detectRadius = 15f;
            //}

            return (distance <= detectRadius * detectRadius) ? true : false;
        }












        public class CollisionGroupsWithCastle
        {
            public GroupOfUnits group;
            public SingleCastle castle;

            public CollisionGroupsWithCastle(GroupOfUnits group, SingleCastle castle)
            {
                this.group = group;
                this.castle = castle;
            }
        }


    }
}
