﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class UnitsAttackDetection : Singleton<UnitsAttackDetection>
    {

        private const float DetectGroup_Radius = 10f;
        private const float DetectUnit_Radius = 2.1f;

        private List<CollisionGroups> activeCollisionGroups = new List<CollisionGroups>();

        private int interval = 5;

        public void OnUpdate()
        {
            if (Time.frameCount % interval == 0)
            {
                UpdateActiveCollisionGroups();
            }
            else
            {
                UpdateUnitsDetection();
            }
        }

        private void UpdateActiveCollisionGroups()
        {
            activeCollisionGroups = new List<CollisionGroups>();

            List<List<GroupOfUnits>> teamsGroups = new List<List<GroupOfUnits>>();
            foreach (int teamNumber in PlayersManager.Instance.Teams)
            {
                teamsGroups.Add(GroupsManager.Instance.GetTeamGroups(teamNumber));
            }

            for (int i = 0; i < teamsGroups.Count; i++)
            {
                for (int j = i + 1; j < teamsGroups.Count; j++)
                {
                    UpdateActiveCollisionBetweenTeams(teamsGroups[i], teamsGroups[j]);
                }
            }
        }

        private void UpdateActiveCollisionBetweenTeams(List<GroupOfUnits> team1Groups, List<GroupOfUnits> team2Groups)
        {
            foreach (GroupOfUnits playerGroup in team1Groups)
            {
                foreach (GroupOfUnits remainingGroup in team2Groups)
                {
                    if (playerGroup == null || remainingGroup == null
                        || playerGroup.commander == null || remainingGroup.commander == null
                        || playerGroup.PlayerOwnerNumber == remainingGroup.PlayerOwnerNumber) continue;

                    if (DetectCollisionGroups(playerGroup, remainingGroup))
                    {
                        activeCollisionGroups.Add(new CollisionGroups(playerGroup, remainingGroup));
                    }
                }
            }
        }

        private bool DetectCollisionGroups(GroupOfUnits group1, GroupOfUnits group2)
        {
            float distance = MathUtility.SqrDistance(group1.commander.transform.position
                , group2.commander.transform.position);
            return (distance <= DetectGroup_Radius * DetectGroup_Radius) ? true : false;
        }

        private void UpdateUnitsDetection()
        {
            for (int i = 0; i < activeCollisionGroups.Count; i++)
            {
                CollisionGroups collisionGroups = activeCollisionGroups[i];

                if (collisionGroups.group1 == null || collisionGroups.group2 == null)
                {
                    activeCollisionGroups.Remove(collisionGroups);
                    continue;
                }
                for (int j = 0; j < collisionGroups.group1.units.Count; j++)
                {
                    SingleUnit playerUnit = collisionGroups.group1.units[j];
                    if (playerUnit == null || playerUnit.IsKilled
                        || !playerUnit.Unit_Attack.CanAttack()) continue;

                    SingleUnit closerUnit = GetCloserCollisionUnit(playerUnit, collisionGroups.group2.units);
                    if (closerUnit != null)
                    {
                        playerUnit.Unit_Attack.OnCollisionAttack(closerUnit);
                    }
                }
            }
        }

        private SingleUnit GetCloserCollisionUnit(SingleUnit playerUnit, List<SingleUnit> enemyUnits)
        {
            SingleUnit closerUnit = null;
            float closerUnitDis = float.MaxValue;
            foreach (SingleUnit enemyUnit in enemyUnits)
            {
                if (playerUnit == null || enemyUnit == null
                    || enemyUnit.IsKilled || !enemyUnit.Unit_Attack.CanAttack()) continue;

                float distance = MathUtility.SqrDistance(playerUnit.transform.position, enemyUnit.transform.position);
                if (distance < closerUnitDis)
                {
                    closerUnitDis = distance;
                    closerUnit = enemyUnit;
                }
            }
            return (closerUnitDis <= DetectUnit_Radius * DetectUnit_Radius) ? closerUnit : null;
        }

        private class CollisionGroups
        {
            public GroupOfUnits group1;
            public GroupOfUnits group2;

            public CollisionGroups(GroupOfUnits group1, GroupOfUnits group2)
            {
                this.group1 = group1;
                this.group2 = group2;
            }
        }


    }
}
