﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MarchOfSoldiers
{
    public class UnitsRaycasting : Singleton<UnitsRaycasting>
    {
        private const float HitUnit_Radius = 1.05f;

        public SingleUnit HitSolider(Vector3 pos)
        {
            List<GroupOfUnits> groups = GroupsManager.Instance.GetAllGroups();

            foreach (GroupOfUnits group in groups)
            {
                foreach (SingleUnit unit in group.units)
                {
                    float dis = MathUtility.SqrDistance(unit.transform.position, pos);
                    if (dis < HitUnit_Radius * HitUnit_Radius)
                    {
                        return unit;
                    }
                }
            }

            return null;
        }
    }
}