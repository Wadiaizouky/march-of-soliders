﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MarchOfSoldiers
{
    [RequireComponent(typeof(GroupOfUnits))]
    public class GroupOfUnits_Movement : MonoBehaviour
    {
        [HideInInspector] public SingleUnit movementCommander;

        private GroupOfUnits group;
        private bool setInitialFormationUnits = false;
        private bool setTempFormation = false;



        private void Awake()
        {
            group = GetComponent<GroupOfUnits>();
        }

        private void Start()
        {
            //group.OnCommanderChange += OnCommanderChange;
            //BuildMovementCommander();
            OnCommanderChange();
        }

        public void BuildMovementCommander(SingleCastle sourceCastle,
            SingleCastle targetCastle, Vector2 instantiatePosition, Vector2 unitPosition)
        {
            movementCommander = Instantiate(group.spawnProp.unitPrefab, instantiatePosition
                                , Quaternion.identity, group.transform).GetComponent<SingleUnit>();

            movementCommander.ApplyInvisible();
            movementCommander.Group = group;

            movementCommander.Agent.enabled = true;

            if (sourceCastle)
            {
                if (movementCommander.Agent.isOnNavMesh)
                {
                    // move toward target castle? offset the destination.
                    if (targetCastle)
                    {
                        movementCommander.Unit_Movement.MoveToCastle(targetCastle);
                    }
                    else
                        movementCommander.Unit_Movement.MoveToPoint(unitPosition, false);
                }
            }
        }

        private void OnCommanderChange()
        {
            //if (CheckIfPrevCommanderKilled())
            //{
            //    setTempFormation = true;
            //    SetTempFormationUnits();
            //}
            //else
            //{
            SetFormationOfUnits();
            //}

            setInitialFormationUnits = true;
        }

        //private bool CheckIfPrevCommanderKilled()
        //{
        //    return (!group.commander.Unit_Movement.commanderReachGoal && setInitialFormationUnits);
        //}

        //private void SetTempFormationUnits()
        //{
        //    foreach (SingleUnit unit in group.units)
        //    {
        //        unit.Unit_Movement.SetFollowCommanderOffset(-(unit.transform.position - group.commander.transform.position));
        //    }
        //}

        private void SetFormationOfUnits()
        {
            int currentColumNum = 1;
            int currentRowNum = 0;
            int rowNum = 5;
            int unitsNum = group.units.Count;
            int indexUnit = 0;
            while (indexUnit < unitsNum)
            {
                currentRowNum = -rowNum / 2;
                for (int j = 0; j < rowNum; j++)
                {
                    if (indexUnit >= unitsNum)
                    {
                        return;
                    }
                    if (!group.units[indexUnit].InvisibleMovementCommander)
                    {
                        float offset_Y = currentColumNum * 1f - 1.0f;
                        float offset_X = currentRowNum * 1f;
                        Vector3 offset = new Vector3(offset_X, -offset_Y, 0);
                        group.units[indexUnit].Unit_Movement.SetFollowCommanderOffset(offset);
                    }

                    currentRowNum++;
                    indexUnit++;
                }
                currentColumNum++;
                //rowNum += 2;
            }

        }

        public void Move(Vector3 destination, bool offset)
        {
            foreach (SingleUnit unit in group.units)
                unit.Unit_Movement.MoveToPoint(destination, offset);
        }

        public void Move(List<Vector2> destinations, bool offset)
        {
            SingleUnit commander = GetMovementCommander(destinations);
            movementCommander.Unit_Movement.MoveToPoint(destinations[0], offset);
            //group.SetGroupCommander(commander);

            for (int i = 0; i < group.units.Count; i++)
            {
                SingleUnit unit = group.units[i];

                //Vector2 offset_Commander = group.commander.transform.position - unit.transform.position;
                Vector2 offset_Commander = movementCommander.transform.position - unit.transform.position;
                unit.Unit_Movement.SetFollowCommanderOffset(offset_Commander);
                unit.Unit_Movement.MoveToPoint(destinations[i], offset);
            }

        }

        public void MoveToCastle(SingleCastle singleCastle)
        {
            movementCommander.Unit_Movement.MoveToCastle(singleCastle);
            for (int i = 0; i < group.units.Count; i++)
                group.units[i].Unit_Movement.MoveToCastle(singleCastle);
        }

        public void OnCommanderReachGoal()
        {
            if (setTempFormation)
            {
                setTempFormation = false;
                SetFormationOfUnits();
            }

            for (int i = 0; i < group.units.Count; i++)
                group.units[i].Unit_Movement.OnCommanderReachTarget();
        }

        private SingleUnit GetMovementCommander(Vector3 targetPoint)
        {
            //SingleUnit commander = null;
            float lowestDis = float.MaxValue;
            for (int i = 0; i < group.units.Count; i++)
            {
                float dis = MathUtility.SqrDistance(group.units[i].transform.position, targetPoint);
                if (dis < lowestDis)
                {
                    lowestDis = dis;
                    //commander = group.units[i];
                    movementCommander.transform.position = group.units[i].transform.position;
                }
            }

            //return commander;
            return movementCommander;
        }

        private SingleUnit GetMovementCommander(List<Vector2> targetPoints)
        {
            //SingleUnit commander = null;
            float lowestDis = float.MaxValue;
            for (int i = 0; i < group.units.Count; i++)
            {
                float dis = float.MaxValue;
                for (int j = 0; j < targetPoints.Count; j++)
                {
                    Vector3 t = new Vector3(targetPoints[j].x, targetPoints[j].y, 0);
                    float d = MathUtility.SqrDistance(group.units[i].transform.position, t);
                    if (d < dis)
                        dis = d;
                }
                //Vector3 targetPoint = new Vector3(targetPoints[i].x, targetPoints[i].y, 0);
                //float dis = MathUtility.SqrDistance(group.units[i].transform.position, targetPoint);
                if (dis < lowestDis)
                {
                    lowestDis = dis;
                    //commander = group.units[i];
                    movementCommander.transform.position = group.units[i].transform.position;
                }
            }

            //return commander;
            return movementCommander;
        }

    }
}