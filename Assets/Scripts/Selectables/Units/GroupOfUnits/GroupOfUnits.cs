﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace MarchOfSoldiers
{
    public enum GroupType { Sword, Spear, Horse , Ram }

    public enum GroupOwner { Player, Enemy }

    public class GroupOfUnits : MonoBehaviour
    {
        public OwnerType OwnerType { get; set; }
        public int PlayerOwnerNumber { get; set; }
        public GroupType groupType;

        [HideInInspector] public bool isSelected;
        [HideInInspector] public List<SingleUnit> units = new List<SingleUnit>();
        [HideInInspector] public int groupSize;
        [HideInInspector] public Vector2[,] unitInstantiatePositions;
        [HideInInspector] public Vector3 centerOfGroup;
        [HideInInspector] public Vector3 goal;
        [HideInInspector] public SingleUnit commander;
        [HideInInspector] public SingleCastle sourceCastle;
        [HideInInspector] public SingleCastle targetCastle;
        [HideInInspector] public GroupOfUnits_SpawnProp spawnProp;
        [HideInInspector] public GroupOfUnits_Movement groupOfUnits_Movement;

        public delegate void onCommanderChange();
        public event onCommanderChange OnCommanderChange;


        private void Awake()
        {
            spawnProp = GetComponent<GroupOfUnits_SpawnProp>();
            groupOfUnits_Movement = GetComponent<GroupOfUnits_Movement>();
        }

        private void Start()
        {
            GroupsManager.Instance.AddGroup(this);
        }

        private void OnDestroy()
        {
            if (UnitsSelection.Instance != null)
            {
                if (UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(PlayerOwnerNumber) != null)
                {
                    UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(PlayerOwnerNumber).Remove(this);
                }
            }
            if (UnitsSlots_UI.Instance != null)
            {
                UnitsSlots_UI.Instance.UpdateUISlots();
            }
            if (GroupsManager.Instance != null)
            {
                GroupsManager.Instance.RemoveGroup(this);
            }
        }

        public void AddUnit(SingleUnit unit)
        {
            units.Add(unit);
            unit.JoinGroup(this);
        }

        public void RemoveUnit(SingleUnit unit)
        {
            if (!units.Contains(unit)) return;

            if (units.Contains(unit))
            {
                units.Remove(unit);
            }

            if (units.Count <= 0)
            {
                Destroy(gameObject,1.5f);
            }

            SetGroupCommander();
            UpdateGroupSize(this, unit.VirtualValue);
        }

        public void UpdateGroupSize(GroupOfUnits group, int lossingSize)
        {
            if (group && lossingSize > 0)
            {
                group.groupSize -= lossingSize;
            }
        }

        public void SetGroupCommander()
        {
            if (units == null || units.Count == 0) return;

            foreach (SingleUnit unit in units)
                unit.IsCommander = false;
            // if count more than 5, commander will be the middle one in the first row
            commander = units.Count > 5 ? units[2] : units[units.Count / 2];

            commander.IsCommander = true;
            if (OnCommanderChange != null)
                OnCommanderChange();
        }
        public void SetGroupCommander(SingleUnit unit)
        {
            if (unit == null) return;

            foreach (SingleUnit u in units)
                u.IsCommander = false;

            commander = unit;
            unit.IsCommander = true;

            if (OnCommanderChange != null)
                OnCommanderChange();
        }

        public void SetUnitsGoal(List<Vector2> unitsPositions)
        {
            for (int i = 0; i < units.Count; i++)
                units[i].Unit_Movement.CalculatePositionGoal(unitsPositions[i]);
        }

        public void OnSelected()
        {
            isSelected = true;

            foreach (SingleUnit unit in units)
            {
                if (unit)
                    unit.OnSelected();
            }
        }

        public void OnDeselected()
        {
            isSelected = false;

            foreach (SingleUnit unit in units)
            {
                if (unit)
                    unit.OnDeselected();
            }
        }

    }
}