﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class GroupOfUnits_FlagController : MonoBehaviour
    {
        public Flag flag;
        public Vector3 offset;

        private GroupOfUnits group;
        private bool activeFlag = false;

        private void Awake()
        {
            group = GetComponent<GroupOfUnits>();
            if (group.OwnerType == OwnerType.Enemy) enabled = false;
        }

        private void Start()
        {
            SpawnFlag();
        }

        private void OnEnable()
        {
            KingdomManager.Instance.OnKingdomUpgrade += OnKingdomUpgrade;
        }

        private void OnDisable()
        {
            if (KingdomManager.Instance)
                KingdomManager.Instance.OnKingdomUpgrade -= OnKingdomUpgrade;
        }

        private void OnKingdomUpgrade()
        {
            int level = PlayersManager.Instance.LocalPlayer.KingdomLevel;
            flag.UpdateFlag(level);
        }

        private void SpawnFlag()
        {
            if (PlayersManager.Instance.LocalPlayer.KingdomLevel <= 0
                || group.groupSize < 10)
            {
                activeFlag = false;
                flag.gameObject.SetActive(false);
            }
            else
            {
                activeFlag = true;
                flag.gameObject.SetActive(true);
            }
        }

        private void Update()
        {
            if (!activeFlag) return;

            //flag.transform.position = group.commander.transform.position + offset;
        }
    }
}