﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class GroupOfUnits_SpawnProp : MonoBehaviour
    {
        public SingleUnit unitPrefab;
        public SingleUnit unitRamPrefab;

        [SerializeField, Tooltip("Used to separate units when they instantiated.")]
        private Vector2 _unitInstantiateOffset = new Vector2(0.2f, 0.23f);

        [SerializeField, Tooltip("how many units can be in each raw when group instantiated")]
        private int _unitGroupBoundaryRows = 4;

        [SerializeField, Tooltip("how many units can be in each Col when group instantiated")]
        private int _unitGroupBoundaryCols = 5;


        [HideInInspector] public Vector2[,] unitInstantiatePositions;

        private void Awake()
        {
            UnitsPositionInitializer();
        }

        private void UnitsPositionInitializer()
        {
            unitInstantiatePositions = new Vector2[_unitGroupBoundaryRows, _unitGroupBoundaryCols];

            for (int i = 0; i < unitInstantiatePositions.GetLength(0); i++)
            {
                for (int j = 0; j < unitInstantiatePositions.GetLength(1); j++)
                {
                    unitInstantiatePositions[i, j] = new Vector2(j * _unitInstantiateOffset.x, -i * _unitInstantiateOffset.y);
                }
            }
        }
    }

}