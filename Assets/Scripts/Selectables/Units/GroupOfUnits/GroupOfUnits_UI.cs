﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MarchOfSoldiers
{
    public class GroupOfUnits_UI : MonoBehaviour
    {
        [SerializeField] private GameObject groupSizeUIPrefab;

        private FollowTarget followTarget;
        private Text groupSizeText;
        private GroupOfUnits group;

        private void Awake()
        {
            group = GetComponent<GroupOfUnits>();
        }

        public void CreateUnitsUILabel(int groupSize)
        {
            GameObject groupUI = Instantiate(groupSizeUIPrefab, transform);
            if (groupUI)
            {
                followTarget = groupUI.GetComponent<FollowTarget>();
                followTarget.Target = group.commander.transform;

                groupSizeText = groupUI.GetComponentInChildren<Text>();
                groupSizeText.text = groupSize.ToString();
            }
        }
    }
}
