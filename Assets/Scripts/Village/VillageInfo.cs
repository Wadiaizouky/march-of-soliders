﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillageInfo : MonoBehaviour
{
    public GameObject uiInfo;

    private void OnMouseEnter()
    {
        if(!this.GetComponent<VillageArmyUICount>().IsEnemyVillage)
          uiInfo.SetActive(true);
    }

    private void OnMouseExit()
    {
        uiInfo.SetActive(false);
    }
}
