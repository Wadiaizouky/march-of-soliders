﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;
using UnityEngine.UI;

public class VillageArmyUICount : MonoBehaviour
{

    //number of Sword Solider
    public int sward { get; set; }
    //public GameObject p1;
    //bool sw = true;
    public Text SwardText;
    int ExtraSword;

    public bool IsPlayerVillage;
    public bool IsEnemyVillage;

    //what the Village result selver and gold in seconds..
    [SerializeField]
    public int Selver { get; private set; }
    [SerializeField]
    public double Gold { get; private set; }



    //Silver production per second
    public int SilverProduction;

    //Village Populations
    public int Populations;

    ////UI info
    //public Text UIInfoText;

    //public List<GameObject> AllOtherCastles;
    public SingleCastle singleCastle;

    private Animator VillageAnimator;

    //All Castle be to the Village
    public List<GameObject> VillageCastles;

    public GameObject UICastleBackground;
    //UIVillage Sprite
    public Sprite PlayerUISolider;
    public Sprite EnemyUISolider;
    public Sprite WhiteUISolider;


    //Minimap
    public GameObject SpriteMinimap;

    //Gold and Population
    public Text SilverText;
    public Text PopulationText;

    private static GateArmyUICount _instance = null;


    public static GateArmyUICount Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GateArmyUICount>();
            return _instance;
        }
    }

    void Start()
    {
        singleCastle = GetComponent<SingleCastle>();


        SilverProduction = 100;
        Populations = 360;

        VillageAnimator = GetComponent<Animator>();
        if (IsPlayerVillage)
        {
            VillageAnimator.SetBool("IsBlueVillage", false);
            SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.red;

            //Village Castles give 2 Soliders in 1s
            foreach (GameObject castle in VillageCastles)
            {
                castle.GetComponent<CastelArmyUICount>().SoldierPlayerCounter = 2;
                if (!castle.GetComponent<CastelArmyUICount>().iswhitecastel && !castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                    castle.transform.GetChild(4).gameObject.SetActive(true);
            }
        }
        else if(IsEnemyVillage)
        {
            VillageAnimator.SetBool("IsBlueVillage", true);

            SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        else
            SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.white;





        sward = 0;

        StartCoroutine(IncrisSilverAndPopulationProd());

        if (KingdomManager.Instance.MapLevel != 15)
            MinimapManger.Instance.ChangeMinimapStatus(3);

    }

    // Update is called once per frame
    void Update()
    {
        //UI Sward above the Castle
        SwardText.text = sward.ToString();
    }

    //when Solider collider hit castle collider
    public void OnCollisionWithUnit(GameObject other)
    {
        SingleUnit solider = other.gameObject.GetComponent<SingleUnit>();

        if (solider.Group.groupType == GroupType.Ram)
        {
            if (other.gameObject.tag == "Player" && solider != null && (IsEnemyVillage || (!IsEnemyVillage && !IsPlayerVillage)))
            {
                if (sward >= 500)
                    sward -= 500;
                else
                    sward = 0;

                Destroy(other.transform.gameObject);

            }
        }
        else
        {
            ////Get the castle pressed by player
            if (other.gameObject.tag == "Player" && solider != null)
            {
                GroupOfUnits curr = solider.Group;
                SingleCastle currentcastlepressed = curr.targetCastle;


                int size = solider.VirtualValue;
                ////check if the curent castle is castle pressed by player
                if (currentcastlepressed != null && currentcastlepressed.gameObject == this.gameObject)
                {

                    //Player Castle
                    if (IsEnemyVillage || (!IsEnemyVillage && !IsPlayerVillage))
                    {
                        sward -= size;
                    }

                    //Army Castle
                    else
                    {
                        sward += size;
                    }

                    //Destroy Solider

                    /* ************************************************** */
                    Destroy(other.transform.gameObject);
                    /* ************************************************** */

                    if (sward <= -1)
                        ChangeCastleSprite("Player", solider.PlayerOwnerNumber);
                }

            }
            else if (other.gameObject.tag == "Enemy" && solider != null)
            {
                GroupOfUnits curr = solider.Group;
                SingleCastle currentcastlepressed = curr.targetCastle;


                int size = solider.VirtualValue;
                ////check if the curent castle is castle pressed by player
                if (currentcastlepressed != null && currentcastlepressed.gameObject == this.gameObject)
                {
                    //Player Castle
                    if (IsPlayerVillage)
                        sward -= size;
                    //Army Castle
                    else
                        sward += size;


                    //Destroy Solider

                    /* ************************************************** */
                    Destroy(other.transform.gameObject);
                    /* ************************************************** */
                    if (sward <= -1)
                    {
                        ChangeCastleSprite("Enemy", -1);
                        //GatesSelection.Instance.DeselectSingleGate(GetComponent<SingleCastle>());
                        CastlesSelection.Instance.DeselectSingleCastle(GetComponent<SingleCastle>());
                    }
                }
            }
        }
    }



    //incris Silver per 60 second
    public void IncrisSilve()
    {
        StartCoroutine(IncrisSilverAndPopulationProd());
    }
    IEnumerator IncrisSilverAndPopulationProd()
    {
        yield return new WaitForSecondsRealtime(1f);
        if (!PasueController.Instance.IsPasued)
        {
            if (IsPlayerVillage)
            {
                Populations += 3;
                if (Populations == 540 || Populations == 720 || Populations == 900 || Populations == 1080 || Populations == 1260 || Populations == 1440
                    || Populations == 1620 || Populations == 1800 || Populations == 1980 || Populations == 2160)
                    SilverProduction += 25;
      //          UIInfoText.text = " (Village) \n" + "\n" +
      // "- Current tax rate(" + SilverProduction + ") Silvers \n" + "\n" +

      // "- Current Population(" + Populations + ") \n" + "\n" +

      //" - The growth + 3 Population increase for every second \n" + "  " + "\n" +


      // " -Every 180 population + 25 Silver tax" + "\n";

                PopulationText.text = Populations.ToString();
                SilverText.text = SilverProduction.ToString();
            }
        }

                if (SilverProduction != 350)
                    IncrisSilve();

    }
    /// <summary>wade adds
    /// return the percentage selected by player in UI
    /// </summary>
    public int Selectedpercentage()
    {
        return NumOfUnits_UI.Instance.GetNumOfUnits();
    }


    public void ChangeCastleSprite(string tag, int playerNumber)
    {
        if (tag == "Player")
        {
            transform.gameObject.tag = "Player";
            if (!singleCastle.disableSelectable)
                transform.gameObject.layer = 9;
            sward *= -1;
            IsEnemyVillage = false;
            IsPlayerVillage = true;
            // SpriteRenderer sp = GetComponent<SpriteRenderer>();
            // sp.sprite = PlayerVillagetex;


            singleCastle.OwnerType = OwnerType.Player;
            singleCastle.PlayerOwnerNumber = playerNumber;
            SelectionManager.Instance.AddSelectable(gameObject.GetComponent<SingleCastle>(), SelectableType.Castle);

            //Village Castles give 2 Soliders in 1s
            foreach (GameObject castle in VillageCastles)
            {
                castle.GetComponent<CastelArmyUICount>().SoldierPlayerCounter = 2;
                if(!castle.GetComponent<CastelArmyUICount>().iswhitecastel && !castle.GetComponent<CastelArmyUICount>().isEnumyCastle)
                   castle.transform.GetChild(4).gameObject.SetActive(true);
            }

            UICastleBackground.GetComponent<Image>().sprite = PlayerUISolider;

            //chamge village sprite
            VillageAnimator.SetBool("IsBlueVillage", false);


            //Change Sprite minimap
            ChangeMinimapVillage(MinimapManger.Instance.TowerNumber);

            //Start incris silver
            if (SilverProduction == 350)
                StartCoroutine(IncrisSilverAndPopulationProd());
            SilverProduction = 100;
            Populations = 360;
                //StartCoroutine(IncrisSilverAndPopulationProd());
            }
        else if (tag == "Enemy")
        {
            transform.gameObject.tag = "Enemy";
            if (!singleCastle.disableSelectable)
                transform.gameObject.layer = 9;
            sward *= -1;
            IsEnemyVillage = true;
            IsPlayerVillage = false;
           // SpriteRenderer sp = GetComponent<SpriteRenderer>();
           // sp.sprite = EnemyVillagetex;

            SelectionManager.Instance.RemoveSelectable(gameObject.GetComponent<SingleCastle>(), SelectableType.Castle);

            singleCastle.OwnerType = OwnerType.Enemy;
            singleCastle.PlayerOwnerNumber = -1;

            //Village Castles Not give 2 Soliders in 1s
            foreach (GameObject castle in VillageCastles)
            {
                castle.GetComponent<CastelArmyUICount>().SoldierPlayerCounter = 1;
                castle.transform.GetChild(4).gameObject.SetActive(false);
            }

            UICastleBackground.GetComponent<Image>().sprite = EnemyUISolider;

            VillageAnimator.SetBool("IsBlueVillage", true);

            //Change Sprite minimap
            ChangeMinimapVillage(MinimapManger.Instance.TowerNumber);

            //Stop product silver
            //StopCoroutine(IncrisSilverAndPopulationProd());

        }
        else
        {
            if (transform.tag == "Player")
            {
                //FarmCount = 0;
                //FarmAvailable = 0;
                //transform.GetChild(2).transform.GetChild(3).gameObject.SetActive(false);
                //GetComponent<CastleFarmButtom>().ChangeFarmUI();
                //GetComponent<CastleFarmButtom>().FarmC.text = "0/0";

                SelectionManager.Instance.RemoveSelectable(gameObject.GetComponent<SingleCastle>(), SelectableType.Castle);

                if (!singleCastle.disableSelectable)
                    transform.gameObject.layer = 9;

                singleCastle.OwnerType = OwnerType.None;
                singleCastle.PlayerOwnerNumber = 0;
                //SpriteRenderer sp = GetComponent<SpriteRenderer>();
                //sp.sprite = Enemycasteltex;

                UICastleBackground.GetComponent<Image>().sprite = WhiteUISolider;
                // UIExtraSolders.GetComponent<Image>().sprite = EnemyUIExtraSoldiers;
            }

            transform.gameObject.tag = "whiteCastle";
            //CastleAnimator.SetBool("IsNeutarl_Castle", true);
            IsPlayerVillage = false;
            IsEnemyVillage = false;

            //SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.white;

        }

    }
    public void ChangeMinimapVillage(int Tower_N)
    {
        if (Tower_N >= 2)
        {
            if (transform.gameObject.tag == "Player")
                SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.red;
            else if (transform.gameObject.tag == "Enemy")
                SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.blue;

        }
        else
            SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.white;

    }
}
