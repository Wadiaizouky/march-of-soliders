﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MarchOfSoldiers
{
    public class BarrelExplosionDetection : Singleton<BarrelExplosionDetection>
    {
        private const float DetectGroup_Radius = 20f;
        private const float DetectUnit_Radius = 3f;

        private List<CollisionGroupsWithBarrel> activeCollisionGroups = new List<CollisionGroupsWithBarrel>();
        private List<Barrel> activeBarrels = new List<Barrel>();

        private int interval = 35;

        public void AddBarrel(Barrel barrel)
        {
            activeBarrels.Add(barrel);
        }

        public void RemoveBarrel(Barrel barrel)
        {
            activeBarrels.Remove(barrel);
        }

        public List<Barrel> GetActiveBarrels()
        {
            return activeBarrels;
        }

        public void Update()
        {
            if (Time.frameCount % interval == 0)
            {
                UpdateActiveCollisionGroups();
            }
            else
            {
                UpdateUnitsDetection();
            }
        }


        private void UpdateActiveCollisionGroups()
        {
            if (SelectionManager.Instance == null) return;

            activeCollisionGroups = new List<CollisionGroupsWithBarrel>();

            List<GroupOfUnits> groups = GroupsManager.Instance.GetOwnerGroups(OwnerType.Enemy);

            foreach (GroupOfUnits group in groups)
            {
                foreach (Barrel barrel in activeBarrels)
                {
                    if (group == null || barrel == null || group.commander == null ||  barrel.isExpolde) continue;

                    if (DetectCollisionGroupsWithCastle(group, barrel))
                    {
                        activeCollisionGroups.Add(new CollisionGroupsWithBarrel(group, barrel));
                    }
                }
            }
        }

        private bool DetectCollisionGroupsWithCastle(GroupOfUnits group, Barrel barrel)
        {
            float distance = MathUtility.SqrDistance(group.commander.transform.position
                , barrel.transform.position);
            return (distance <= DetectGroup_Radius * DetectGroup_Radius) ? true : false;
        }

        private void UpdateUnitsDetection()
        {
            foreach (CollisionGroupsWithBarrel collisionGroups in activeCollisionGroups)
            {
                foreach (SingleUnit unit in collisionGroups.group.units)
                {
                    if (unit.IsKilled || collisionGroups.barrel.isExpolde) continue;

                    if (DetectCollisionUnitWithCastle(unit, collisionGroups.barrel))
                    {
                        collisionGroups.barrel.Explode(unit.Group);
                    }
                }
            }
        }

        private bool DetectCollisionUnitWithCastle(SingleUnit unit, Barrel barrel)
        {
            float distance = MathUtility.SqrDistance(unit.transform.position
                , barrel.transform.position);
            return (distance <= DetectUnit_Radius * DetectUnit_Radius) ? true : false;
        }


        public class CollisionGroupsWithBarrel
        {
            public GroupOfUnits group;
            public Barrel barrel;

            public CollisionGroupsWithBarrel(GroupOfUnits group, Barrel barrel)
            {
                this.group = group;
                this.barrel = barrel;
            }
        }
    }
}