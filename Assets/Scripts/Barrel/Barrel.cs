﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace MarchOfSoldiers
{
    public class Barrel : MonoBehaviour
    {
        public float explosionRadius = 40;

        [HideInInspector] public bool isExpolde = false;
        private Animator anim;

        public AudioSource ExplosionSound;

        private void Awake()
        {
            anim = GetComponent<Animator>();
        }

        private void OnEnable()
        {
            BarrelExplosionDetection.Instance.AddBarrel(this);
        }

        private void OnDisable()
        {
            BarrelExplosionDetection.Instance.RemoveBarrel(this);
        }

        public void Explode(GroupOfUnits collideGroup)
        {
            anim.SetBool("Explode", true);
            isExpolde = true;
            ExplosionSound.Play();
            StartCoroutine(ApplyDamage(collideGroup));

        }

        public IEnumerator ApplyDamage(GroupOfUnits collideGroup)
        {
            yield return new WaitForSeconds(.7f);

            List<GroupOfUnits> groups = GroupsManager.Instance.GetOwnerGroups(OwnerType.Enemy);
            List<GroupOfUnits> groupsInRadius = new List<GroupOfUnits>();

            foreach (GroupOfUnits group in groups)
            {
                if (group.commander == null) continue;
                if (MathUtility.SqrDistance(group.commander.transform.position, transform.position) <= explosionRadius)
                {
                    groupsInRadius.Add(group);
                }
            }

            if (groupsInRadius.Count > 0)
            {
                int damage = (int)(100.0f / groupsInRadius.Count);
                foreach (GroupOfUnits group in groupsInRadius)
                {
                    int currentDamage = damage;

                    foreach (SingleUnit unit in group.units.ToList())
                    {
                        if (unit.VirtualValue > currentDamage)
                        {
                            unit.TakeDamage(currentDamage);
                            currentDamage = 0;
                        }
                        else
                        {
                            currentDamage -= unit.VirtualValue;
                            unit.TakeDamage(unit.VirtualValue);
                        }

                        if (currentDamage <= 0)
                        {
                            break;
                        }
                    }
                }

            }

            Destroy(gameObject);
        }

    }
}