﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MarchOfSoldiers;

public class BarrelPositionManger : MonoBehaviour
{
    private bool BarrelinRightPosition = false;

    private bool IsBarrelMoving = true;

    private bool isActiveUIButton = false;

    //Bareel sprites
    public Sprite ValidPosition;
    public Sprite UnValidPosition;

    void Start()
    {
        SelectionManager.Instance.GetComponent<DrawBoxPosition>().enabled = false;
        SelectionManager.Instance.GetComponent<DrawSelectionBox>().enabled = false;
    }
    void Update()
    {

        if (Input.GetKeyUp(KeyCode.Mouse0) && IsRightPosition())
        {
            BarrelinRightPosition = true;
            IsBarrelMoving = false;
            SelectionManager.Instance.GetComponent<DrawBoxPosition>().enabled = true;
            SelectionManager.Instance.GetComponent<DrawSelectionBox>().enabled = true;
            GetComponent<Barrel>().enabled = true;
            GetComponent<Animator>().enabled = true;

            //PlayersManager.Instance.LocalPlayer.Gold -= 250.0f;

            if (!isActiveUIButton)
            {
                PlayersManager.Instance.LocalPlayer.Gold -= 250.0f;
                UIArmy.Instance.ActiveButton(3, 20);
                isActiveUIButton = true;
            }
        }
        else
            BarrelMovingWithMouse();

        if (Input.GetKeyUp(KeyCode.Mouse1) && IsBarrelMoving)
        {
            UIArmy.Instance.Army_Buttons[3].GetComponent<InteractableButton>().ActiveButton();

            SelectionManager.Instance.GetComponent<DrawBoxPosition>().enabled = true;
            SelectionManager.Instance.GetComponent<DrawSelectionBox>().enabled = true;

            Destroy(this.gameObject);
        }

        ChangeBarrelSprite();

    }

    //Move the Barrel with mouse
    public void BarrelMovingWithMouse()
    {
        if (!BarrelinRightPosition)
        {           
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 mousePosition = new Vector3(pos.x-0.5f, pos.y-1.0f,0f);
            transform.position = mousePosition;
        }
    }

    //Check if Barrel Position near the Player Castle
    bool IsRightPosition()
    {
        bool isright = false;
        List<SingleCastle> AllPlayerCastle = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(1);
        foreach (SingleCastle castle in AllPlayerCastle)
        {
            float dis = Vector3.Distance(transform.position, castle.transform.position);
            if (dis <= 12.0f && dis >= 6.0f)
             isright = true;
        }

        return isright;
    }

    //Change Barrel Sprite
    void ChangeBarrelSprite()
    {
        if (IsBarrelMoving)
        {
            if (IsRightPosition())
                GetComponent<SpriteRenderer>().sprite = ValidPosition;
            else
                GetComponent<SpriteRenderer>().sprite = UnValidPosition;
        }
    }

}
