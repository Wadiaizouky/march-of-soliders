﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxPosition : MonoBehaviour
{
    public List<Circle> BoxCirclesPositios;

    private static BoxPosition _instance = null;


    public static BoxPosition Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<BoxPosition>();
            return _instance;
        }
    }

    //valid if all Box circles all valid then u can sorting Group of Soliders
    public bool Drawable() {
        bool result = true;
        foreach(Circle c in BoxCirclesPositios)
        {
            if (!c.isvalid())
                result = false;
        }

        return result;
       
    }
}
