﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;

public class Circle : MonoBehaviour
{
    //Yelwo color main there are no objects in the same circle position
    public bool isYelwo = true;

    //Srite clors
    public Sprite YelowColor;
    public Sprite RedColor;

    private static Circle _instance = null;

    private SpriteRenderer circlespriterender;

    public GameObject Box;


    void Start()
    {
        isYelwo = true;
        circlespriterender = GetComponent<SpriteRenderer>();
    }

    public static Circle Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<Circle>();
            return _instance;
        }
    }

    void Update()
    {
        check();
    }

    //Run when hit other Collider ..Make the circle unvalid (or position).
    void OnTriggerEnter2D(Collider2D other)
    {
        if ((SelectionManager.Instance.GetSelectedSolidersForLocalPlayer().Count > 0 && other.gameObject.transform.childCount > 3) || SelectionManager.Instance.GetSelectedCastlesForLocalPlayer().Count > 0)
        {
            isYelwo = false;
            if (circlespriterender != null)
                circlespriterender.sprite = RedColor;
        }
    }

    //return true when u moke mouse ..
    public void check()
    {
        if (Input.GetAxis("Mouse X") < 0 || Input.GetAxis("Mouse X") > 0)
        {
            circlespriterender.sprite = YelowColor;
            isYelwo = true;
        }
    }
    //check is there are other objects in the same circle position
    public bool isvalid()
    {
        return isYelwo;
    }
}
