﻿using UnityEngine;

public class LayerSorter : MonoBehaviour
{

    private SpriteRenderer spriteRenderer;
    private int sortingOrderBase = 10000;
    private int interval = 10;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        if (gameObject.isStatic)
        {
            UpdateSortingOrder();
            Destroy(this);
        }
    }

    private void Update()
    {
        if (Time.frameCount % interval == 0)
        {
            UpdateSortingOrder();
        }
    }

    private void UpdateSortingOrder()
    {
        spriteRenderer.sortingOrder = GetLayerOrder(transform);
    }

    private int GetLayerOrder(Transform transform)
    {
        return (int)(sortingOrderBase - (transform.position.y * 30))
                  + (int)(transform.position.x * 10);
    }

}
