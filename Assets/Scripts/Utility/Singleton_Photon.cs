﻿using UnityEngine;
using Photon.Pun;

public class Singleton_Photon<T> : MonoBehaviourPunCallbacks where T : MonoBehaviourPunCallbacks
{
    private static T _Instance;
    public static T Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = (T)FindObjectOfType(typeof(T));
            }

            return _Instance;
        }
        set
        {
            if (_Instance == null) _Instance = value;
        }
    }

}