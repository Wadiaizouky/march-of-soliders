﻿using UnityEngine;

public class MoveWithCommander : MonoBehaviour
{
    public Transform Target { set; get; }
    private float _speed = 5;

    private void Update()
    {
        transform.Translate(Target.position * _speed * Time.deltaTime);
    }
}
