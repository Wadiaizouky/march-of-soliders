﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _Instance;
    public static T Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = (T)FindObjectOfType(typeof(T));

                //if (_Instance == null)
                //{
                //    GameObject obj = new GameObject("Singleton");
                //    _Instance = obj.AddComponent<T>();
                //}
                //if (_Instance == null)
                //{
                //    Debug.LogError("You Must Add Managers Prefab");
                //}
            }

            return _Instance;
        }
        set
        {
            if (_Instance == null) _Instance = value;
        }
    }

}
