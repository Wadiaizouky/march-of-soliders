﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MarchOfSoldiers;

public class SummonManger : MonoBehaviour
{
    //public bool IsSummon = false;

    private GameObject CastleClikedbymouse;

    [SerializeField]
    public GameObject SummonPoint;

    private bool IsSummonrun = false;

    void Start()
    {
        SummonPoint = GameObject.Find("SummonPoint");
    }
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0) && CastleCliked() && !IsSummonrun)
        {
            MoveMouse.Instance.IsSummon = false;
            StartCoroutine(CreatGroupsToCastleCliked());

            List<SingleCastle> AllPlayerCastle = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(1);
            foreach (SingleCastle castle in AllPlayerCastle)
            {
                castle.gameObject.transform.GetChild(7).gameObject.SetActive(false);
            }

           
            PlayersManager.Instance.LocalPlayer.Gold -= 1200.0f;

            UIArmy.Instance.ActiveButton(5, 300);

            IsSummonrun = true;
        }
    }

    //Check if Player Click to Player Castle by mouse
    bool CastleCliked()
    {
        bool CastleCliked = false;
        Vector2 destination = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // check if player choose to move toward a castle
        RaycastHit2D hit = Physics2D.Raycast(
            origin: destination,
            direction: Vector2.zero,
            distance: 100);

        // if destination is CASTLE: don't add offset to destination, just move to Center of castle
        if (hit)
        {
            if (hit.collider.gameObject.transform.childCount > 4 && hit.collider.gameObject.tag == "Player")
            {
                CastleClikedbymouse = hit.collider.gameObject;
                CastleCliked = true;
            }
        }
        return CastleCliked;
    }

    //Create 3Groups(600 Soldiers) from point towrd Castle Clicked by player
    IEnumerator CreatGroupsToCastleCliked()
    {
        int i = 0;
        while (i < 3)
        {
            CreatGroupFromPointToCastle(SummonPoint, CastleClikedbymouse, 200);
            yield return new WaitForSeconds(1f);
            i++;
        }
        Destroy(this.gameObject);
    }

    //Create group from Point toword Castle
    void CreatGroupFromPointToCastle(GameObject CurrentPoint, GameObject TargerCastle, int groupSize)
    {
        List<Vector2> CirclePosition = new List<Vector2>();
        for (int j = 0; j < 20; j++)
            CirclePosition.Add(TargerCastle.transform.position);
        GroupOfUnits g;
        //int swordcount = CurrentCastle.GetComponent<CastelArmyUICount>().sward;
        g = UnitsSpawner.Instance.CreateGroup(groupSize, CirclePosition, GroupType.Sword, OwnerType.Player, 1, TargerCastle.transform.position, CurrentPoint.GetComponent<SingleCastle>(), TargerCastle.GetComponent<SingleCastle>());

        //CurrentCastle.GetComponent<CastelArmyUICount>().sward -= groupSize;
    }
}
