﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class PhotonConnection : MonoBehaviourPunCallbacks
    {
        private void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        private void Start()
        {
            Debug.Log("Start Connect ...");
            PhotonNetwork.ConnectUsingSettings();

        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("Connected To Master");
            if (PhotonNetwork.CountOfRooms <= 0)
                PhotonNetwork.CreateRoom("as");
            else
                PhotonNetwork.JoinRandomRoom();
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("Room Joined");
            PlayersManager.Instance.SpawnPlayer();
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("Error " + message);
        }
        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.Log("Error " + message);
        }
    }
}