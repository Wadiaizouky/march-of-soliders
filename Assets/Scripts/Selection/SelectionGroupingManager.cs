﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class SelectionGroupingManager : Singleton<SelectionGroupingManager>
    {

        private Dictionary<int, SelectedGroup> selectionGroups = new Dictionary<int, SelectedGroup>();

        private KeyCode[] keyCodes = {
         KeyCode.Alpha1,
         KeyCode.Alpha2,
         KeyCode.Alpha3,
         KeyCode.Alpha4,
         KeyCode.Alpha5,
         KeyCode.Alpha6,
         KeyCode.Alpha7,
         KeyCode.Alpha8
        };

        private void Awake()
        {
            for (int i = 0; i <= 9; i++)
            {
                selectionGroups.Add(i, null);
            }
        }

        private void Update()
        {
            if (PasueController.Instance.IsPasued) return;

            for (int i = 0; i < keyCodes.Length; i++)
            {
                if (Input.GetKeyDown(keyCodes[i]))
                {
                    if (Input.GetKey("left ctrl") || Input.GetKey(KeyCode.G))
                    {
                        SaveSelectionGroup(i);
                    }
                    else
                    {
                        LoadSelectionGroup(i);
                    }
                }
            }

        }

        private void SaveSelectionGroup(int numOfGroup)
        {
            if (selectionGroups[numOfGroup] != null)
            {
                selectionGroups[numOfGroup] = null;
            }

            SelectableType currentSelectableType = SelectionManager.Instance.currentSelectedType;

            SelectedGroup selectedGroup = new SelectedGroup();
            selectedGroup.selectableType = currentSelectableType;

            switch (currentSelectableType)
            {
                case SelectableType.Soldier:
                    selectedGroup.selectedGroupsOfUnits = new List<GroupOfUnits>(UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()));
                    break;
                case SelectableType.Castle:
                    selectedGroup.selectedGroup = new List<Selectable>(CastlesSelection.Instance.GetSelectedCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()));
                    break;
                case SelectableType.Gate:
                    selectedGroup.selectedGroup = new List<Selectable>(CastlesSelection.Instance.GetSelectedGatesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()));
                    break;
            }

            selectionGroups[numOfGroup] = selectedGroup;
        }

        private void LoadSelectionGroup(int numOfGroup)
        {
            if (selectionGroups[numOfGroup] == null) return;

            SelectionManager.Instance.ClearSelection(PlayersManager.Instance.GetLocalPlayerNumber());

            switch (selectionGroups[numOfGroup].selectableType)
            {
                case SelectableType.Soldier:
                    foreach (GroupOfUnits groupOfUnits in selectionGroups[numOfGroup].selectedGroupsOfUnits)
                    {
                        UnitsSelection.Instance.SelectGroup(groupOfUnits);
                    }
                    break;
                case SelectableType.Castle:
                    foreach (Selectable selectable in selectionGroups[numOfGroup].selectedGroup)
                    {
                        CastlesSelection.Instance.SelectSingleCastle((SingleCastle)selectable);
                    }
                    break;
                case SelectableType.Gate:
                    foreach (Selectable selectable in selectionGroups[numOfGroup].selectedGroup)
                    {
                        CastlesSelection.Instance.SelectSingleCastle((SingleCastle)selectable);
                    }
                    break;
            }

            UnitsSlots_UI.Instance.UpdateUISlots();
        }

        public class SelectedGroup
        {
            public SelectableType selectableType;
            public List<Selectable> selectedGroup;
            public List<GroupOfUnits> selectedGroupsOfUnits;
        }
    }
}