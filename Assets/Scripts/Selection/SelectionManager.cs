﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Photon.Pun;
using System.Linq;

namespace MarchOfSoldiers
{
    public class SelectionManager : MonoBehaviourPunCallbacks
    {

        public bool testRecord = false;

        #region Fields

        #region Wade
        /* ************************************** */
        public List<SingleCastle> Allcastle;
        public List<SingleCastle> Allgate;
        public List<SingleCastle> AllVillage;
        public List<TowerArmyUICount> AllTower;


        private List<List<Vector2>> CircleBoxPosition;
        bool Drawablee;

        public List<SingleCastle> GetSelectedCastlesForLocalPlayer()
        {
            return CastlesSelection.Instance.GetAllSelectedByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
        }

        public List<GroupOfUnits> GetSelectedSolidersForLocalPlayer()
        {
            return UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
        }

        //Sounds
        public AudioSource BackToCastle;
        public AudioSource DefenTheCastle;
        public AudioSource SelectionSound;
        public AudioSource AttackSound;
        bool isPlayed = false;
        /* ************************************** */
        #endregion

        private static SelectionManager _instance = null;

        [Header("Groups")]
        public GroupOfUnits _groupOfPlayerSowrdUnits = null;
        public GroupOfUnits _groupOfEnemySowrdUnits = null;

        [Tooltip("Layers that contains Objects on screen that can be selected."), SerializeField]
        public LayerMask _selectableObjectsLayer;

        [HideInInspector] public Camera mainCamera = null;
        [HideInInspector] public SelectableType currentSelectedType;
        [HideInInspector] public Vector3 mousePos1, mousePos2;
        [HideInInspector] public float lastClickTime = 0;


        public const float MAX_VIRTUAL_SIZE = 10;
        public const int MAX_UNITS_PER_GROUP = 200;
        public const int MAX_SELECTABLE_GROUP = 12;
        public const float DOUBLE_CLICK_DEFERENCE = 0.2f;

        private Dictionary<int, GameObject> playersMouseObjectHit = new Dictionary<int, GameObject>();
        private bool canSelect = true;


        #endregion

        #region Properties

        public static SelectionManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<SelectionManager>();
                return _instance;
            }
        }

        /// <summary>
        /// percent of group used to separate group of units.
        /// </summary>
        public int Percent { get; set; }

        /// <summary>
        /// True while selection box in on drawn. 
        /// </summary>
        public bool DrawSelectionBox { get; set; }

        #endregion

        #region MonoBehavior Callbacks

        private void Awake()
        {
            mainCamera = Camera.main;

            LimitFrameRate();
        }

        private void LimitFrameRate()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;
        }

        private void Update()
        {

            if (Input.GetKey(KeyCode.P))
            {
                TestRecord();
                return;
            }

            if (PasueController.Instance.IsPasued) return;

            if (Input.GetKeyDown(KeyCode.T))
                print(UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()).Count);

            UpdateMousePosition();
            if (Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(0))
            {
                if (!PhotonNetwork.IsConnected)
                {
                    UpdateMouseObjectHit(PlayersManager.Instance.GetLocalPlayerNumber(), mainCamera.ScreenToWorldPoint(Input.mousePosition));
                }
                else
                {
                    photonView.RPC("UpdateMouseObjectHit", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber(), mainCamera.ScreenToWorldPoint(Input.mousePosition));
                }
            }
            UpdateSelection(mousePos1, mousePos2);



            if (Input.GetMouseButtonUp(0))
            {
                canSelect = true;
            }


            // wadi add:
            /* ******************************************************************* */
            //load cicil level if Player have all Castles or loose All his Castles
            //CheckWinnerPlayer();

            //Show UI Selver and Gold Coin
            //CalculatSelverAndGoldCoin();

            /////////////////For ai
            //if (!AddSoliderforfirsttime)
            //    AiAddExtraSoliders();

            //for outing soliders with mouse
            DrawBoxPosition drawBox = GetComponent<DrawBoxPosition>();
            Drawablee = drawBox.DrawableeByPlayers[PlayersManager.Instance.GetLocalPlayerNumber()];

            if (Drawablee && Input.GetMouseButtonUp(1) && !IsMouseOverUI() && !IsOutOfMap())
            {
                if (!PhotonNetwork.IsConnected)
                {
                    CkeckwhatSelecting(PlayersManager.Instance.GetLocalPlayerNumber());
                }
                else
                {
                    photonView.RPC("CkeckwhatSelecting", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber());
                }
            }

        }
        #region Test Record
        private void TestRecord()
        {
            UpdateMousePosition();
            if (Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(0))
            {
                UpdateMouseObjectHit(-1, mainCamera.ScreenToWorldPoint(Input.mousePosition));
            }
            UpdateSelectionTestRecord(mousePos1, mousePos2);

            if (Input.GetMouseButtonUp(0))
            {
                canSelect = true;
            }

            //for outing soliders with mouse
            DrawBoxPosition drawBox = GetComponent<DrawBoxPosition>();
            Drawablee = drawBox.DrawableeByPlayers[-1];

            if (Input.GetMouseButtonUp(1) && Drawablee && !IsMouseOverUI() && !IsOutOfMap())
            {
                CkeckwhatSelecting(-1);
            }
        }

        private void UpdateSelectionTestRecord(Vector3 mousePos1, Vector3 mousePos2)
        {
            if (Input.GetMouseButtonDown(0) && canSelect)
            {
                DrawSelectionBox = true;
                UpdateSelectionUnit(-1, mainCamera.ScreenToWorldPoint(Input.mousePosition));
            }
            if (Input.GetMouseButtonUp(0))
            {
                DrawSelectionBox = false;
                if (mousePos1 != mousePos2 && canSelect)
                {
                    SelectMultipleUnits(-1, mainCamera.ScreenToWorldPoint(mousePos1)
                        , mainCamera.ScreenToWorldPoint(mousePos2));
                }
            }
        }
        #endregion




        #endregion

        #region Selectable

        private void UpdateMousePosition()
        {
            if (Input.GetMouseButtonDown(0) && canSelect)
            {
                if (IsMouseOverUI())
                {
                    canSelect = false;
                }
                //mousePos1 = mainCamera.ScreenToViewportPoint(Input.mousePosition);
                mousePos1 = Input.mousePosition;
            }
            if (Input.GetMouseButtonUp(0))
            {
                //mousePos2 = mainCamera.ScreenToViewportPoint(Input.mousePosition);
                mousePos2 = Input.mousePosition;
            }
        }

        public bool IsMouseOverUI()
        {
            return EventSystem.current.IsPointerOverGameObject();
        }

        public bool IsOutOfMap()
        {
            GameObject objectHit = playersMouseObjectHit[PlayersManager.Instance.GetLocalPlayerNumber()];

            return objectHit != null && playersMouseObjectHit[PlayersManager.Instance.GetLocalPlayerNumber()].tag == "Map Border";
        }

        [PunRPC]
        private void UpdateMouseObjectHit(int playerNumber, Vector3 worldPoint)
        {
            if (!playersMouseObjectHit.ContainsKey(playerNumber))
            {
                playersMouseObjectHit.Add(playerNumber, null);
            }

            playersMouseObjectHit[playerNumber] = null;

            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero, Mathf.Infinity);
            if (hit)
            {
                playersMouseObjectHit[playerNumber] = hit.collider.gameObject;
            }
        }

        private void UpdateSelection(Vector3 mousePos1, Vector3 mousePos2)
        {
            if (Input.GetMouseButtonDown(0) && canSelect)
            {
                DrawSelectionBox = true;
                if (!PhotonNetwork.IsConnected)
                {
                    UpdateSelectionUnit(PlayersManager.Instance.GetLocalPlayerNumber(), mainCamera.ScreenToWorldPoint(Input.mousePosition));
                }
                else
                {
                    photonView.RPC("UpdateSelectionUnit", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber(), mainCamera.ScreenToWorldPoint(Input.mousePosition));
                }

            }
            if (Input.GetMouseButtonUp(0))
            {
                DrawSelectionBox = false;
                if (mousePos1 != mousePos2 && canSelect)
                {
                    if (!PhotonNetwork.IsConnected)
                    {
                        SelectMultipleUnits(PlayersManager.Instance.GetLocalPlayerNumber(), mainCamera.ScreenToWorldPoint(mousePos1)
                            , mainCamera.ScreenToWorldPoint(mousePos2));
                    }
                    else
                    {
                        photonView.RPC("SelectMultipleUnits", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber(), mainCamera.ScreenToWorldPoint(mousePos1)
                            , mainCamera.ScreenToWorldPoint(mousePos2));
                    }
                }
            }
        }

        [PunRPC]
        private void UpdateSelectionUnit(int playerNumber, Vector3 worldPoint)
        {
            GameObject hit = GetHit(worldPoint);
            if (hit)
            {
                SelectUnit(hit, playerNumber);
            }
        }

        private GameObject GetHit(Vector2 worldPoint)
        {

            SingleUnit unit = UnitsRaycasting.Instance.HitSolider(worldPoint);
            if (unit != null)
            {
                return unit.gameObject;
            }

            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero, Mathf.Infinity, _selectableObjectsLayer);
            if (hit)
            {
                return hit.collider.gameObject;
            }

            return null;
        }

        private void SelectUnit(GameObject hit, int playerNumber)
        {
            Selectable selectableObject = hit.GetComponent<Selectable>();

            if (!selectableObject) return;

            switch (selectableObject.SelectableType)
            {
                case SelectableType.Soldier:
                    {
                        UnitsSelection.Instance.OnSelectionSoldier(selectableObject, playerNumber);
                        if (currentSelectedType == SelectableType.Soldier)
                        {
                            // Add Sound : Select SingleUnit
                            if (!SelectionSound.isPlaying)
                                SelectionSound.Play();
                        }
                        break;
                    }
                case SelectableType.Castle:
                    {
                        CastlesSelection.Instance.OnSelectionCastle(selectableObject, playerNumber);
                        // Add Sound : Select SingleUnit
                        if (!SelectionSound.isPlaying && selectableObject.gameObject.tag == "Player")
                            SelectionSound.Play();
                        break;
                    }

                default:
                    break;
            }
        }

        [PunRPC]
        private void SelectMultipleUnits(int playerNumber, Vector3 mousePos1, Vector3 mousePos2)
        {
            // create rectangle to select objects.
            Rect selectionBox = new Rect(
                x: mousePos1.x,
                y: mousePos1.y,
                width: mousePos2.x - mousePos1.x,
                height: mousePos2.y - mousePos1.y);

            float width = Mathf.Abs(mousePos2.x - mousePos1.x);
            float height = Mathf.Abs(mousePos2.y - mousePos1.y);

            Vector3 pos1 = new Vector3(selectionBox.center.x - width / 2.0f, selectionBox.center.y + height / 2.0f);
            Vector3 pos2 = new Vector3(selectionBox.center.x + width / 2.0f, selectionBox.center.y - height / 2.0f);

            bool selectMultipleUnits = UnitsSelection.Instance.SelectMultipleSoldiers(ref selectionBox, pos1, pos2, playerNumber);

            if (selectMultipleUnits)
            {
                // Add Sound : Select Multiple Units
                SelectionSound.Play();
                return;
            }

            bool selectMultipleCastles = CastlesSelection.Instance.SelectMultipleCastles(ref selectionBox, pos1, pos2, playerNumber);

            if (selectMultipleCastles) return;
        }

        public void ClearSelection(int playerNumber)
        {
            UnitsSelection.Instance.ClearUnitsSelection(playerNumber);
            CastlesSelection.Instance.ClearCastlesSelection(playerNumber);

            currentSelectedType = SelectableType.None;
        }

        public void AddSelectable(Selectable unit, SelectableType type)
        {
            switch (type)
            {
                case SelectableType.None:
                    return;
                case SelectableType.Soldier:
                    UnitsSelection.Instance.AddSelectable(unit as SingleUnit);
                    break;
                case SelectableType.Castle:
                    CastlesSelection.Instance.AddSelectable(unit as SingleCastle);
                    break;
                default:
                    break;
            }
        }

        public void RemoveSelectable(Selectable unit, SelectableType type)
        {
            switch (type)
            {
                case SelectableType.None:
                    return;
                case SelectableType.Soldier:
                    if (UnitsSelection.Instance == null) return;
                    UnitsSelection.Instance.RemoveSelectable(unit as SingleUnit);
                    break;
                case SelectableType.Castle:
                    if (CastlesSelection.Instance == null) return;
                    CastlesSelection.Instance.RemoveSelectable(unit as SingleCastle);
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region WadeeFunctions

        /// <summary>wade adds
        /// Great Groups when select Castles and press I
        /// </summary>
        public void GreatGroupFromCastels(int playerNumber)
        {

            //if (AllBoxPositions.Count > 0)
            //{
            //List<GroupOfUnits> groupOfUnits = new List<GroupOfUnits>();

            List<SingleCastle> allSelected = CastlesSelection.Instance.GetAllSelectedByPlayerNum(playerNumber);
            List<SingleCastle> selectedCastles = CastlesSelection.Instance.GetSelectedCastlesByPlayerNum(playerNumber);
            for (int i = 0; i < selectedCastles.Count && (i < 12 || DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber] != null); i++)
            {
                isPlayed = false;

                SingleCastle CastleSelected = selectedCastles[i];

                if (DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber] != null)
                {
                    if (DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber] == CastleSelected)
                        continue;
                }

                //if (CastleSelected != AllBoxPositions[i].GetComponent<SingleCastle>())
                //{
                //int count = int.Parse(CastlesSelection.Instance.allSelected[i].transform.GetChild(1).gameObject.name);
                int count = CastleSelected.GetComponent<CastelArmyUICount>().sward;
                int groupsize = Selectedpercentage();

                //CreateGroupParameters group = new CreateGroupParameters();
                //group.GroupOwner = GroupOwner.Player;
                //group.GroupType = GroupType.Sword;
                //group.Position = AllBoxPositions[i].transform.position;
                //group.SourceCastle = _selectedCastles[i];
                //group.GroupSize = groupsize;
                //if (TargetCastle != null)
                //    group.TargetCastle = TargetCastle;

                ////////
                List<Vector2> CirclePosition = new List<Vector2>();
                if (!DrawBoxPosition.Instance.TargetCastleByPlayers.ContainsKey(playerNumber)
                    || DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber] == null)
                {
                    foreach (Circle circle in DrawBoxPosition.Instance.AllBoxPositionsByPlayers[playerNumber][i].GetComponent<BoxPosition>().BoxCirclesPositios)
                    {
                        CirclePosition.Add(circle.transform.position);
                    }
                }
                else
                {
                    for (int j = 0; j < 20; j++)
                        CirclePosition.Add(DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber].transform.position);
                    if (DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber].gameObject.tag != "Player")
                    {
                        if (!isPlayed && !AttackSound.isPlaying)
                            AttackSound.Play();
                    }
                    else
                    {
                        if (!isPlayed && !DefenTheCastle.isPlaying)
                            DefenTheCastle.Play();
                    }
                }

                Vector2 Mouseposition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                GroupOfUnits g;
                if (count > groupsize)
                {
                    //group.GroupSize = groupsize;
                    //CreateGroup(groupsize, AllBoxPositions[i].transform.position, GroupType.Sword, GroupOwner.Player, _selectedCastles[i]);
                    //g = CreateGroup(group);
                    //if (CastlesSelection.Instance.allSelected[i].tag == "Player")

                    g = UnitsSpawner.Instance.CreateGroup(groupsize, CirclePosition, GroupType.Sword, OwnerType.Player, playerNumber, Mouseposition, allSelected[i], DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber]);


                }
                else
                {
                    //group.GroupSize = count;
                    //CreateGroup(count, AllBoxPositions[i].transform.position, GroupType.Sword, GroupOwner.Player, _selectedCastles[i]);
                    //g= CreateGroup(group);
                    //if (CastlesSelection.Instance.allSelected[i].tag == "Player")

                    g = UnitsSpawner.Instance.CreateGroup(count, CirclePosition, GroupType.Sword, OwnerType.Player, playerNumber, Mouseposition, allSelected[i], DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber]);

                }
                if (count > groupsize)
                    selectedCastles[i].GetComponent<CastelArmyUICount>().sward -= groupsize;
                else
                    selectedCastles[i].GetComponent<CastelArmyUICount>().sward = 0;

                //groupOfUnits.Add(g);
                //MoveMultipleGroups(groupOfUnits, group.TargetCastle.transform.position, false, group.TargetCastle);


                //}
                isPlayed = true;
            }
            List<SingleCastle> selectedGates = CastlesSelection.Instance.GetSelectedGatesByPlayerNum(playerNumber);
            for (int i = 0; i < selectedGates.Count && (i < 12 || DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber] != null); i++)
            {
                SingleCastle CastleSelected = selectedGates[i];
                //if (CastleSelected != AllBoxPositions[i].GetComponent<SingleCastle>())
                //{
                //int count = int.Parse(CastlesSelection.Instance.allSelected[i].transform.GetChild(1).gameObject.name);
                int count = CastleSelected.GetComponent<GateArmyUICount>().sward;
                int groupsize = Selectedpercentage();

                //CreateGroupParameters group = new CreateGroupParameters();
                //group.GroupOwner = GroupOwner.Player;
                //group.GroupType = GroupType.Sword;
                //group.Position = AllBoxPositions[i].transform.position;
                //group.SourceCastle = _selectedCastles[i];
                //group.GroupSize = groupsize;
                //if (TargetCastle != null)
                //    group.TargetCastle = TargetCastle;

                ////////
                List<Vector2> CirclePosition = new List<Vector2>();
                if (DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber] == null)
                {
                    foreach (Circle circle in DrawBoxPosition.Instance.AllBoxPositionsByPlayers[playerNumber][i].GetComponent<BoxPosition>().BoxCirclesPositios)
                    {
                        CirclePosition.Add(circle.transform.position);
                    }
                }
                else
                {
                    for (int j = 0; j < 20; j++)
                        CirclePosition.Add(DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber].transform.position);
                }

                Vector2 Mouseposition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                GroupOfUnits g;
                if (count > groupsize)
                {
                    //group.GroupSize = groupsize;
                    //CreateGroup(groupsize, AllBoxPositions[i].transform.position, GroupType.Sword, GroupOwner.Player, _selectedCastles[i]);
                    //g = CreateGroup(group);
                    //if (CastlesSelection.Instance.allSelected[i].tag == "Player")
                    g = UnitsSpawner.Instance.CreateGroup(groupsize, CirclePosition, GroupType.Sword, OwnerType.Player, playerNumber, Mouseposition, allSelected[i], DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber]);
                }
                else
                {
                    //group.GroupSize = count;
                    //CreateGroup(count, AllBoxPositions[i].transform.position, GroupType.Sword, GroupOwner.Player, _selectedCastles[i]);
                    //g= CreateGroup(group);
                    //if (CastlesSelection.Instance.allSelected[i].tag == "Player")
                    g = UnitsSpawner.Instance.CreateGroup(count, CirclePosition, GroupType.Sword, OwnerType.Player, playerNumber, Mouseposition, allSelected[i], DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber]);

                }
                if (count > groupsize)
                    selectedGates[i].GetComponent<GateArmyUICount>().sward -= groupsize;
                else
                    selectedGates[i].GetComponent<GateArmyUICount>().sward = 0;

                //groupOfUnits.Add(g);
                //MoveMultipleGroups(groupOfUnits, group.TargetCastle.transform.position, false, group.TargetCastle);


                //}
            }
            List<SingleCastle> selectedVillage = CastlesSelection.Instance.GetSelectedVillagesByPlayerNum(playerNumber);
            for (int i = 0; i < selectedVillage.Count && (i < 12 || DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber] != null); i++)
            {
                SingleCastle CastleSelected = selectedVillage[i];
                //if (CastleSelected != AllBoxPositions[i].GetComponent<SingleCastle>())
                //{
                //int count = int.Parse(CastlesSelection.Instance.allSelected[i].transform.GetChild(1).gameObject.name);
                int count = CastleSelected.GetComponent<VillageArmyUICount>().sward;
                int groupsize = Selectedpercentage();

                //CreateGroupParameters group = new CreateGroupParameters();
                //group.GroupOwner = GroupOwner.Player;
                //group.GroupType = GroupType.Sword;
                //group.Position = AllBoxPositions[i].transform.position;
                //group.SourceCastle = _selectedCastles[i];
                //group.GroupSize = groupsize;
                //if (TargetCastle != null)
                //    group.TargetCastle = TargetCastle;

                ////////
                List<Vector2> CirclePosition = new List<Vector2>();
                if (DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber] == null)
                {
                    foreach (Circle circle in DrawBoxPosition.Instance.AllBoxPositionsByPlayers[playerNumber][i].GetComponent<BoxPosition>().BoxCirclesPositios)
                    {
                        CirclePosition.Add(circle.transform.position);
                    }
                }
                else
                {
                    for (int j = 0; j < 20; j++)
                        CirclePosition.Add(DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber].transform.position);
                }

                Vector2 Mouseposition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                GroupOfUnits g;
                if (count > groupsize)
                {
                    //group.GroupSize = groupsize;
                    //CreateGroup(groupsize, AllBoxPositions[i].transform.position, GroupType.Sword, GroupOwner.Player, _selectedCastles[i]);
                    //g = CreateGroup(group);
                    //if (CastlesSelection.Instance.allSelected[i].tag == "Player")
                    g = UnitsSpawner.Instance.CreateGroup(groupsize, CirclePosition, GroupType.Sword, OwnerType.Player, playerNumber, Mouseposition, allSelected[i], DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber]);
                }
                else
                {
                    //group.GroupSize = count;
                    //CreateGroup(count, AllBoxPositions[i].transform.position, GroupType.Sword, GroupOwner.Player, _selectedCastles[i]);
                    //g= CreateGroup(group);
                    //if (CastlesSelection.Instance.allSelected[i].tag == "Player")
                    g = UnitsSpawner.Instance.CreateGroup(count, CirclePosition, GroupType.Sword, OwnerType.Player, playerNumber, Mouseposition, allSelected[i], DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber]);

                }
                if (count > groupsize)
                    selectedVillage[i].GetComponent<VillageArmyUICount>().sward -= groupsize;
                else
                    selectedVillage[i].GetComponent<VillageArmyUICount>().sward = 0;

                //groupOfUnits.Add(g);
                //MoveMultipleGroups(groupOfUnits, group.TargetCastle.transform.position, false, group.TargetCastle);


                //}
            }
            List<SingleCastle> selectedTowers = CastlesSelection.Instance.GetSelectedTowersByPlayerNum(playerNumber);
            for (int i = 0; i < selectedTowers.Count && (i < 12 || DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber] != null); i++)
            {
                SingleCastle CastleSelected = selectedTowers[i];
                //if (CastleSelected != AllBoxPositions[i].GetComponent<SingleCastle>())
                //{
                //int count = int.Parse(CastlesSelection.Instance.allSelected[i].transform.GetChild(1).gameObject.name);
                int count = CastleSelected.GetComponent<TowerArmyUICount>().sward;
                int groupsize = Selectedpercentage();

                //CreateGroupParameters group = new CreateGroupParameters();
                //group.GroupOwner = GroupOwner.Player;
                //group.GroupType = GroupType.Sword;
                //group.Position = AllBoxPositions[i].transform.position;
                //group.SourceCastle = _selectedCastles[i];
                //group.GroupSize = groupsize;
                //if (TargetCastle != null)
                //    group.TargetCastle = TargetCastle;

                ////////
                List<Vector2> CirclePosition = new List<Vector2>();
                if (DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber] == null)
                {
                    foreach (Circle circle in DrawBoxPosition.Instance.AllBoxPositionsByPlayers[playerNumber][i].GetComponent<BoxPosition>().BoxCirclesPositios)
                    {
                        CirclePosition.Add(circle.transform.position);
                    }
                }
                else
                {
                    for (int j = 0; j < 20; j++)
                        CirclePosition.Add(DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber].transform.position);
                }

                Vector2 Mouseposition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                GroupOfUnits g;
                if (count > groupsize)
                {
                    //group.GroupSize = groupsize;
                    //CreateGroup(groupsize, AllBoxPositions[i].transform.position, GroupType.Sword, GroupOwner.Player, _selectedCastles[i]);
                    //g = CreateGroup(group);
                    //if (CastlesSelection.Instance.allSelected[i].tag == "Player")
                    g = UnitsSpawner.Instance.CreateGroup(groupsize, CirclePosition, GroupType.Sword, OwnerType.Player, playerNumber, Mouseposition, allSelected[i], DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber]);
                }
                else
                {
                    //group.GroupSize = count;
                    //CreateGroup(count, AllBoxPositions[i].transform.position, GroupType.Sword, GroupOwner.Player, _selectedCastles[i]);
                    //g= CreateGroup(group);
                    //if (CastlesSelection.Instance.allSelected[i].tag == "Player")
                    g = UnitsSpawner.Instance.CreateGroup(count, CirclePosition, GroupType.Sword, OwnerType.Player, playerNumber, Mouseposition, allSelected[i], DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber]);

                }
                if (count > groupsize)
                    selectedTowers[i].GetComponent<TowerArmyUICount>().sward -= groupsize;
                else
                    selectedTowers[i].GetComponent<TowerArmyUICount>().sward = 0;

                //groupOfUnits.Add(g);
                //MoveMultipleGroups(groupOfUnits, group.TargetCastle.transform.position, false, group.TargetCastle);


                //}
            }
            //if (TargetCastle != null)
            //{
            //    MoveMultipleGroups(groupOfUnits, TargetCastle.transform.position, false, TargetCastle);

            //}
            //}
        }

        /// <summary>wade adds
        /// Move All Selected Groups
        /// </summary>
        public void MoveAllSelectedGroups(int playerNumber)
        {
            isPlayed = false;
            List<GroupOfUnits> selectedGroups = UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(playerNumber);

            for (int i = 0; i < selectedGroups.Count; i++)
            {
                List<Vector2> CirclePosition = new List<Vector2>();
                if (DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber] == null)
                {
                    foreach (Circle circle in DrawBoxPosition.Instance.AllBoxPositionsByPlayers[playerNumber][i].GetComponent<BoxPosition>().BoxCirclesPositios)
                    {
                        CirclePosition.Add(circle.transform.position);
                    }
                    //if (!isPlayed)
                    //    MovingSound.Play();
                }
                else
                {
                    for (int j = 0; j < 20; j++)
                        CirclePosition.Add(DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber].transform.position);
                    if (DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber].gameObject.tag != "Player")
                    {
                        if (!isPlayed && !AttackSound.isPlaying)
                            AttackSound.Play();
                    }
                    else
                    {
                        if (!isPlayed && !BackToCastle.isPlaying)
                            BackToCastle.Play();
                    }
                }

                UnitsOperations.Instance.MoveGroups(selectedGroups[i], CirclePosition, DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber]);
                //Add Sound : Move Units to Castle or point Check (DrawBoxPosition.Instance.TargetCastleByPlayers[playerNumber]) if true move to castle 
                // and check if castle for enemy or not
                // or check if hit enemy units
                isPlayed = true;
            }
        }


        /// <summary>wade adds
        /// check if select Castle or Soliders
        /// </summary>
        ///
        [PunRPC]
        public void CkeckwhatSelecting(int playerNumber)
        {
            GameObject mouseObjectHit = null;
            if (playersMouseObjectHit.ContainsKey(playerNumber))
            {
                mouseObjectHit = playersMouseObjectHit[playerNumber];
            }

            if (UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(playerNumber).Count > 0)
                MoveAllSelectedGroups(playerNumber);
            else if (mouseObjectHit == null || mouseObjectHit.tag != "Map Border")
                GreatGroupFromCastels(playerNumber);
        }

        // <summary>wade adds
        // return the percentage selected by player in UI
        // </summary>
        public int Selectedpercentage()
        {
            return NumOfUnits_UI.Instance.GetNumOfUnits();
        }

        #endregion
    }
}