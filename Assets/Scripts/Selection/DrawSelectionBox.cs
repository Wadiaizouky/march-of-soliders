﻿using UnityEngine;
using UnityEngine.EventSystems;
using MarchOfSoldiers;

public class DrawSelectionBox : MonoBehaviour
{
    private bool isSelecting = false;
    private Vector3 startPosition;

    private void Update()
    {
        if (PasueController.Instance.IsPasued)
        {
            isSelecting = false;
            return;
        }

        if (Input.GetMouseButtonDown(0) && !IsMouseOverUI())
        {
            isSelecting = true;
            startPosition = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
            isSelecting = false;

        if (isSelecting)
        {
            SelectionBox.Instance.DrawScreenRect(startPosition, Input.mousePosition);
        }
    }

    private bool IsMouseOverUI()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }

    //private void OnGUI()
    //{
    //    if (isSelecting)
    //    {
    //        SelectionBox.Instance.DrawScreenRect(startPosition, Input.mousePosition);
    //    }
    //}
}
