﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class KingdomUpgrade_UI : MonoBehaviour
    {
        public GameObject[] buttonLevels;

        public AudioSource UpgradKingdom;

        private static KingdomUpgrade_UI _instance = null;
        public static KingdomUpgrade_UI Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<KingdomUpgrade_UI>();
                return _instance;
            }
        }

        private void Awake()
        {
            KingdomManager.Instance.OnKingdomUpgrade += OnKingdomUpgrade;
        }

        private void Start()
        {
            for (int i = 0; i < buttonLevels.Length; i++)
            {
                InteractableButton interactableButton = buttonLevels[i].GetComponent<InteractableButton>();
                if (i >= KingdomManager.Instance.maxUpgradeNum)
                {
                    interactableButton.LockButton();
                }
                interactableButton.LockButton();
            }
            if (KingdomManager.Instance.maxUpgradeNum == 0) return;
            buttonLevels[0].GetComponent<InteractableButton>().ActiveButton();
        }

        public void OnKingdomUpgradeButtonPress(int level)
        {
            if (!KingdomManager.Instance.CanUpgrade(level)) return;

            KingdomManager.Instance.UpgradeKingdom(true);

            buttonLevels[level - 1].GetComponent<InteractableButton>().DeactivateButton();

            UpgradKingdom.Play();
        }

        private void OnKingdomUpgrade()
        {
            int kingdomLevel = PlayersManager.Instance.LocalPlayer.KingdomLevel;
            if (kingdomLevel == 3 || kingdomLevel == KingdomManager.Instance.maxUpgradeNum) return;
            buttonLevels[kingdomLevel].GetComponent<InteractableButton>().ActiveButton();
        }

        public void Load(int kingdomLevel)
        {
            for (int i = 1; i <= 3 && i <= kingdomLevel; i++)
            {
                KingdomManager.Instance.UpgradeKingdom(false);
                buttonLevels[i - 1].GetComponent<InteractableButton>().DeactivateButton();
                UpgradKingdom.Play();
            }
            //for (int i = 0; i < buttonLevels.Length && i < kingdomLevel; i++)
            //{
            //    buttonLevels[i].GetComponent<InteractableButton>().DeactivateButton();
            //}
            //if (kingdomLevel == 3 || kingdomLevel >= KingdomManager.Instance.maxUpgradeNum) return;
            //buttonLevels[kingdomLevel].GetComponent<InteractableButton>().ActiveButton();
        }
    }
}