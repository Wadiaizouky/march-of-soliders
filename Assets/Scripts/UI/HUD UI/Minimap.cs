﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Minimap : MonoBehaviour, IPointerDownHandler
{
    private Camera minimapCam;

    private void Start()
    {
        if (Camera.allCameras.Length > 1)
            minimapCam = Camera.allCameras[1];
    }

    public void OnPointerDown(PointerEventData dat)
    {
        Vector2 localCursor;
        var rect1 = GetComponent<RectTransform>();
        var pos1 = dat.position;
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(rect1, pos1,
            null, out localCursor))
            return;

        int xpos = (int)(localCursor.x);
        int ypos = (int)(localCursor.y);

        if (xpos < 0) xpos = xpos + (int)rect1.rect.width / 2;
        else xpos += (int)rect1.rect.width / 2;

        if (ypos > 0) ypos = ypos + (int)rect1.rect.height / 2;
        else ypos += (int)rect1.rect.height / 2;

        Vector2 mouseClickPos = new Vector2(xpos, ypos);

        Vector3 mouseCliclPosRelativeToScreen;

        float height = 2f * minimapCam.orthographicSize;
        float width = height * minimapCam.aspect;

        mouseCliclPosRelativeToScreen.x = (mouseClickPos.x * width) / rect1.sizeDelta.x;
        mouseCliclPosRelativeToScreen.y = (mouseClickPos.y * height) / rect1.sizeDelta.y;
        mouseCliclPosRelativeToScreen.z = 0;

        Vector3 p = minimapCam.ViewportToWorldPoint(new Vector3(mouseCliclPosRelativeToScreen.x / width, mouseCliclPosRelativeToScreen.y / height, 0));
        Camera.main.transform.position = p;

    }

}
