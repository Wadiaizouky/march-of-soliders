﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MarchOfSoldiers
{
    public class MouseSpeedUp_UI : MonoBehaviour
    {
        public Text mouseSpeedText;

        public void OnChangeMouseSpeed()
        {
            switch (mouseSpeedText.text)
            {
                //case "Normal speed": MoveMouse.Instance._mousespeed = 0.7f;break;
                case "High speed": MoveMouse.Instance._mousespeed = 1.3f; break;
                case "very hight speed": MoveMouse.Instance._mousespeed = 2.3f; break;
            }
        }
    }
}
