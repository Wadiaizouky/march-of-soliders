﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Farms_Castle_Interactable : MonoBehaviour
{
    public GameObject Static;
    public GameObject Active;
    public GameObject Locked;

    public void OnStatic()
    {
        Static.SetActive(true);
        Active.SetActive(false);
        Locked.SetActive(false);
    }

    public void OnActive()
    {
        Static.SetActive(false);
        Active.SetActive(true);
        Locked.SetActive(false);
    }

    public void OnLoced()
    {
        Static.SetActive(false);
        Active.SetActive(false);
        Locked.SetActive(true);
    }
}
