﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumOfUnits_UI : Singleton<NumOfUnits_UI>
{
    public ButtonColor[] buttons;
    public Slider slider;
    public Text text;

    private bool ignoreSliderValueChange = true;

    public int GetNumOfUnits()
    {
        bool pressedButton = false;
        int numOfUnits = 200;

        for (int i = 0; i < 4; i++)
        {
            ButtonColor btn = buttons[i];
            if (btn.isPressed)
            {
                switch (i)
                {
                    case 0: numOfUnits = 10; break;
                    case 1: numOfUnits = 50; break;
                    case 2: numOfUnits = 100; break;
                    case 3: numOfUnits = 150; break;
                    case 4: numOfUnits = 200; break;
                }
                pressedButton = true;
                break;
            }
        }
        if (!pressedButton)
        {
            if (slider.value != 0)
            {
                numOfUnits = Mathf.RoundToInt(slider.value);
            }
        }
        return numOfUnits;
    }

    public void OnChangeSliderValue()
    {
        if (ignoreSliderValueChange)
        {
            ignoreSliderValueChange = false;
            return;
        }

        text.text = Mathf.RoundToInt(slider.value).ToString();

        foreach (ButtonColor btn in buttons)
        {
            btn.SetButtonState(false);
        }
    }

    public void OnButtonClick(int btnNum)
    {
        ButtonColor btnPressed = buttons[btnNum];
        if (btnPressed.isPressed)
        {
            btnPressed.SetButtonState(false);
            return;
        }

        foreach (ButtonColor btn in buttons)
        {
            btn.SetButtonState(false);
        }

        btnPressed.SetButtonState(true);

        ignoreSliderValueChange = true;
        slider.value = 0;
        text.text = Mathf.RoundToInt(slider.value).ToString();
    }

    public void OnUpPress() {
        if (slider.value < 200)
        {
            ignoreSliderValueChange = true;
            slider.value++;
            text.text = Mathf.RoundToInt(slider.value).ToString();
        }
    }

    public void OnDownPress() {
        if (slider.value > 0)
        {
            ignoreSliderValueChange = true;
            slider.value--;
            text.text = Mathf.RoundToInt(slider.value).ToString();
        }
    }
}
