﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MarchOfSoldiers
{
    public class Farms_UI : MonoBehaviourPunCallbacks
    {
        public InteractableButton buildFarmButton;

        public Text farmsNumText;
        public Text farmsBonusSilverNumText;
        public Text farmsLostNumText;

        private static Farms_UI _instance = null;
        public static Farms_UI Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<Farms_UI>();
                return _instance;
            }
        }

        private void Awake()
        {
            KingdomManager.Instance.OnKingdomUpgrade += OnKingdomUpgrade;
        }

        private void Start()
        {
            buildFarmButton.LockButton();
        }

        private void OnKingdomUpgrade()
        {
            if (KingdomManager.Instance.MapLevel > 3)
                buildFarmButton.ActiveButton();
        }

        private void FixedUpdate()
        {
            UpdateUI();
        }

        private void UpdateUI()
        {
            farmsNumText.text = PlayersManager.Instance.LocalPlayer.FarmsNumber.ToString();
            farmsBonusSilverNumText.text = PlayersManager.Instance.LocalPlayer.FarmsBonusSilver.ToString();
            farmsLostNumText.text = PlayersManager.Instance.LocalPlayer.FarmsLost.ToString();
        }

        private void Update()
        {
            int playerCastle = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()).Count;
            int farmsCount = PlayersManager.Instance.LocalPlayer.FarmsNumber;

            //buildFarmButton.DeactivateButton();
            foreach (SingleCastle castle in CastlesSelection.Instance.GetSelectedCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()))
            {
                CastelArmyUICount castelArmy = castle.GetComponent<CastelArmyUICount>();
                if (castelArmy.FarmCount < PlayersManager.Instance.GetPlayer(PlayersManager.Instance.GetLocalPlayerNumber()).KingdomLevel
                    && PlayersManager.Instance.GetPlayer(PlayersManager.Instance.GetLocalPlayerNumber()).Gold >= 5)
                {
                    buildFarmButton.ActiveButton();
                    continue;
                }
            }

            //if (farmsCount >= playerCastle * PlayersManager.Instance.LocalPlayer.KingdomLevel)
            //{
            //    buildFarmButton.DeactivateButton();
            //}
            //else
            //{
            //    buildFarmButton.ActiveButton();
            //}
        }

        public void OnBuildFarmButtonPressed()
        {
            if (PhotonNetwork.IsConnected)
            {
                photonView.RPC("BuildFarms", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber());
            }
            else
            {
                BuildFarms(PlayersManager.Instance.GetLocalPlayerNumber());
            }
        }

        [PunRPC]
        private void BuildFarms(int playerNumber)
        {
            foreach (SingleCastle castle in CastlesSelection.Instance.GetSelectedCastlesByPlayerNum(playerNumber))
            {
                CastelArmyUICount castelArmy = castle.GetComponent<CastelArmyUICount>();

                if (castelArmy.FarmCount < PlayersManager.Instance.GetPlayer(playerNumber).KingdomLevel && PlayersManager.Instance.GetPlayer(playerNumber).Gold >= 5)
                {
                    castelArmy.FarmCount++;
                    castelArmy.FarmAvailable = castelArmy.FarmCountvalid - castelArmy.FarmCount;
                    castelArmy.GetComponent<CastleFarmButtom>().ChangeFarmUI();

                    if (playerNumber == PlayersManager.Instance.GetLocalPlayerNumber())
                        PlayersManager.Instance.LocalPlayer.Gold -= 5;
                }
            }
        }

        public void Load(int kingdomLevel)
        {
            if (KingdomManager.Instance.MapLevel > 3 && kingdomLevel >= 1)
                buildFarmButton.ActiveButton();
        }

    }
}