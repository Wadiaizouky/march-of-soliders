﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MarchOfSoldiers
{
    public class Kingdom_UI : MonoBehaviour
    {
        public Text playerCastleNumber;
        public Text armyNumber;

        public Text goldText;
        public Text silverText;

        private void FixedUpdate()
        {
            UpdateUI();
        }

        private void UpdateUI()
        {
            UpdateCastlesCount();
            UpdateKingdomGoldAndSilver();
        }

        private void UpdateKingdomGoldAndSilver()
        {
            float gold = PlayersManager.Instance.LocalPlayer.Gold;
            if (gold >= 10000)
                goldText.text = (gold / 1000).ToString("F2") + " K";
            else
                goldText.text = gold.ToString("F2");

            float silver = PlayersManager.Instance.LocalPlayer.Silver;
            silverText.text = silver.ToString();
        }

        private void UpdateCastlesCount()
        {
            playerCastleNumber.text = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()).Count.ToString();
            //int NumberofArmyCastle = SelectionManager.Instance.Allcastle.Count - CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()).Count;
            armyNumber.text = GetArmyNumber().ToString();
        }

        private int GetArmyNumber()
        {
            int armyNumber = 0;
            List<SingleCastle> castles = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
            foreach (SingleCastle castle in castles)
            {
                armyNumber += Mathf.Max(0, castle.GetComponent<CastelArmyUICount>().sward);
            }
            List<SingleCastle> gates = CastlesSelection.Instance.selectableGates;
            foreach (SingleCastle gate in gates)
            {
                armyNumber += Mathf.Max(0, gate.GetComponent<GateArmyUICount>().sward);
            }
            List<SingleCastle> villiages = CastlesSelection.Instance.GetSelectableVillagesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
            foreach (SingleCastle villiage in villiages)
            {
                armyNumber += Mathf.Max(0, villiage.GetComponent<VillageArmyUICount>().sward);
            }

            List<GroupOfUnits> groups = GroupsManager.Instance.GetPlayerGroups(PlayersManager.Instance.GetLocalPlayerNumber());
            foreach (GroupOfUnits group in groups)
            {
                armyNumber += Mathf.Max(0, group.groupSize);
            }
            return armyNumber;
        }
    }
}