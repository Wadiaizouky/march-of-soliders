﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MarchOfSoldiers
{
    public class Reinforcement_UI : MonoBehaviour
    {
        public RunningCircle runningCircleButton;
        public GameObject[] buttonLevels;


        private static Reinforcement_UI _instance = null;
        public static Reinforcement_UI Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<Reinforcement_UI>();
                return _instance;
            }
        }

        private void Awake()
        {
            ReinforcementManager.Instance.OnReinforcementUpgrade += OnReinforcementUpgrade;
            KingdomManager.Instance.OnKingdomUpgrade += OnKingdomUpgrade;
        }

        private void Start()
        {
            for (int i = 0; i < buttonLevels.Length; i++)
            {
                InteractableButton interactableButton = buttonLevels[i].GetComponent<InteractableButton>();
                if (i >= ReinforcementManager.Instance.maxUpgradeNum)
                {
                    interactableButton.LockButton();
                }
                interactableButton.LockButton();
            }
            runningCircleButton.gameObject.SetActive(false);

            if (ReinforcementManager.Instance.maxUpgradeNum == 0) return;
            //buttonLevels[0].GetComponent<InteractableButton>().ActiveButton();
        }

        public void OnReinforcementUpgradeButtonPress(int level)
        {
            if (!ReinforcementManager.Instance.CanUpgrade(level)) return;

            ReinforcementManager.Instance.UpgradeReinforcementForLocalPlayer(true);

            buttonLevels[level - 1].GetComponent<InteractableButton>().DeactivateButton();
            runningCircleButton.gameObject.SetActive(true);
            runningCircleButton.transform.position = buttonLevels[level - 1].transform.position;
        }

        private void OnReinforcementUpgrade()
        {
            int kingdomLevel = PlayersManager.Instance.LocalPlayer.KingdomLevel;
            int reinforcementLevel = PlayersManager.Instance.LocalPlayer.ReinforcementLevel;
            if (reinforcementLevel == 3 || reinforcementLevel >= ReinforcementManager.Instance.maxUpgradeNum || reinforcementLevel >= kingdomLevel) return;
            buttonLevels[reinforcementLevel].GetComponent<InteractableButton>().ActiveButton();
        }

        private void OnKingdomUpgrade()
        {
            int kingdomLevel = PlayersManager.Instance.LocalPlayer.KingdomLevel;
            int reinforcementLevel = PlayersManager.Instance.LocalPlayer.ReinforcementLevel;
            if (kingdomLevel - 1 >= ReinforcementManager.Instance.maxUpgradeNum /*|| kingdomLevel - 1 > reinforcementLevel*/) return;
            buttonLevels[kingdomLevel - 1].GetComponent<InteractableButton>().ActiveButton();
        }

        public void OnRunningCirclePressed()
        {
            ReinforcementManager.Instance.ChangeReinforcementActiveStateLocalPlayer();
            runningCircleButton.ChangeActiveState();
        }

        public void Load(int reinforcementLevel)
        {
            for (int i = 0; i < buttonLevels.Length && i < reinforcementLevel; i++)
            {
                buttonLevels[i].GetComponent<InteractableButton>().DeactivateButton();
                ReinforcementManager.Instance.UpgradeReinforcementForLocalPlayer(false);
            }

            runningCircleButton.gameObject.SetActive(true);
            runningCircleButton.transform.position = buttonLevels[reinforcementLevel - 1].transform.position;
            OnRunningCirclePressed();

            if (reinforcementLevel == 3 || reinforcementLevel >= ReinforcementManager.Instance.maxUpgradeNum) return;
            buttonLevels[reinforcementLevel].GetComponent<InteractableButton>().ActiveButton();
        }
    }
}
