﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MarchOfSoldiers
{
    public class UnitsSlots_UI : Singleton<UnitsSlots_UI>
    {
        public List<GameObject> unitsSlots = new List<GameObject>();

        private List<Text> unitsSlotsTexts = new List<Text>();

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            for (int i = 0; i < unitsSlots.Count; i++)
            {
                unitsSlotsTexts.Add(unitsSlots[i].GetComponentInChildren<Text>());
            }
        }

        public void UpdateUISlots()
        {
            foreach (GameObject slot in unitsSlots)
            {
                if (slot != null)
                    slot.SetActive(false);
            }

            if (UnitsSelection.Instance == null) return;

            List<GroupOfUnits> selectedGroups = UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
            for (int i = 0; i < selectedGroups.Count && i < unitsSlots.Count; i++)
            {
                if (unitsSlots[i] != null
                    && selectedGroups[i] != null && unitsSlotsTexts[i])
                {
                    unitsSlots[i].SetActive(true);
                    unitsSlotsTexts[i].text = Mathf.Max(selectedGroups[i].groupSize, 0).ToString();
                }
            }
        }
    }
}