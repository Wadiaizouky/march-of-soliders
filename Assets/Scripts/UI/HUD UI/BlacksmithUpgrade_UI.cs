﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MarchOfSoldiers
{
    public class BlacksmithUpgrade_UI : MonoBehaviour
    {
        public GameObject[] buttonLevels;

        public AudioSource UpgradBlacksmith;


        private static BlacksmithUpgrade_UI _instance = null;
        public static BlacksmithUpgrade_UI Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<BlacksmithUpgrade_UI>();
                return _instance;
            }
        }

        private void Awake()
        {
            BlacksmithManager.Instance.OnBlacksmithUpgrade += OnBlacksmithUpgrade;
            KingdomManager.Instance.OnKingdomUpgrade += OnKingdomUpgrade;
        }

        private void Start()
        {
            for (int i = 0; i < buttonLevels.Length; i++)
            {
                InteractableButton interactableButton = buttonLevels[i].GetComponent<InteractableButton>();
                if (i >= BlacksmithManager.Instance.maxUpgradeNum)
                {
                    interactableButton.LockButton();
                }
                interactableButton.LockButton();
            }
            if (BlacksmithManager.Instance.maxUpgradeNum == 0) return;
            //buttonLevels[0].GetComponent<InteractableButton>().ActiveButton();
        }

        public void OnBlacksmithUpgradeButtonPress(int level)
        {
            if (!BlacksmithManager.Instance.CanUpgrade(level)) return;

            BlacksmithManager.Instance.UpgradeBlacksmith(true);

            buttonLevels[level - 1].GetComponent<InteractableButton>().DeactivateButton();

            UpgradBlacksmith.Play();
        }

        private void OnBlacksmithUpgrade()
        {
            int kingdomLevel = PlayersManager.Instance.LocalPlayer.KingdomLevel;
            int blacksmithLevel = PlayersManager.Instance.LocalPlayer.BlacksmithLevel;
            if (blacksmithLevel >= BlacksmithManager.Instance.maxUpgradeNum || blacksmithLevel >= kingdomLevel) return;
            buttonLevels[blacksmithLevel].GetComponent<InteractableButton>().ActiveButton();
        }

        private void OnKingdomUpgrade()
        {
            int kingdomLevel = PlayersManager.Instance.LocalPlayer.KingdomLevel;
            int blacksmithLevel = PlayersManager.Instance.LocalPlayer.BlacksmithLevel;
            if (kingdomLevel - 1 >= BlacksmithManager.Instance.maxUpgradeNum /*|| kingdomLevel - 1 > blacksmithLevel*/) return;
            buttonLevels[kingdomLevel - 1].GetComponent<InteractableButton>().ActiveButton();
        }

        public void Load(int kingdomLevel, int blacksmithLevel)
        {
            for (int i = 0; i < buttonLevels.Length && i < blacksmithLevel; i++)
            {
                buttonLevels[i].GetComponent<InteractableButton>().DeactivateButton();
                BlacksmithManager.Instance.UpgradeBlacksmith(false);
            }
            if (blacksmithLevel >= BlacksmithManager.Instance.maxUpgradeNum || blacksmithLevel >= kingdomLevel) return;
            if (kingdomLevel - 1 >= BlacksmithManager.Instance.maxUpgradeNum) return;
            buttonLevels[blacksmithLevel].GetComponent<InteractableButton>().ActiveButton();
        }
    }
}
