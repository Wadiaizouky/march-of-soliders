﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenesController : MonoBehaviour
{
    public List<GameObject> AllScenes;

    public void ActiveScene(int scene_N)
    {
        for(int i = 0; i < AllScenes.Count; i++)
        {
            if (i != scene_N)
                AllScenes[i].SetActive(false);
            else
                AllScenes[i].SetActive(true);
        }
    }

}
