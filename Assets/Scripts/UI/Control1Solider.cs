﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control1Solider : MonoBehaviour
{


    void Start()
    {
        StartCoroutine("Extra1SolidersIncris");
    }

    void Update()
    {
        transform.Translate(-0.2f,0.0f,0f);
    }


    IEnumerator Extra1SolidersIncris()
    {
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }
}
