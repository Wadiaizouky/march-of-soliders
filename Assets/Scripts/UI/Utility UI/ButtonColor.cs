﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonColor : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject onStatic;
    public GameObject onHover;
    public GameObject onActive;

    [HideInInspector] public bool isPressed;


    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        if (isPressed) return;
        onStatic.SetActive(false);
        onHover.SetActive(true);
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        if (isPressed) return;
        onStatic.SetActive(true);
        onHover.SetActive(false);
    }

    public void SetButtonState(bool state)
    {
        isPressed = state;
        ChangeButtonImage();
    }

    public void ChangeButtonState()
    {
        isPressed = !isPressed;
        ChangeButtonImage();
    }

    private void ChangeButtonImage()
    {
        if (isPressed)
        {
            onStatic.SetActive(false);
            onHover.SetActive(false);
            onActive.SetActive(true);
        }
        else
        {
            onStatic.SetActive(true);
            onHover.SetActive(false);
            onActive.SetActive(false);
        }
    }
}