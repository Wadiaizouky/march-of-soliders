﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MarchOfSoldiers;

public class SelectionBox : Singleton<SelectionBox>
{
    public Canvas canvas;
    public GameObject box;

    private RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    private void Start()
    {
        box.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0) || PasueController.Instance.IsPasued)
        {
            box.SetActive(false);
        }
    }

    public void DrawScreenRect(Vector3 startPos, Vector3 endPos)
    {
        if (PasueController.Instance.IsPasued) return;

        Vector3 center = (startPos + endPos) / 2f;
        rectTransform.position = center;

        float xSize = Mathf.Abs(startPos.x - endPos.x);
        float ySize = Mathf.Abs(startPos.y - endPos.y);

        rectTransform.sizeDelta = canvas.transform.InverseTransformVector(new Vector2(xSize, ySize));
        box.SetActive(true);
    }

}
