﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningCircle : MonoBehaviour
{
    public Animator anim;

    private bool isActive = true;

    public void ChangeActiveState()
    {
        isActive = !isActive;
        if (isActive)
        {
            anim.enabled = true;
        }
        else
        {
            anim.enabled = false;
        }

    }
}
