﻿using UnityEngine;
using MarchOfSoldiers;

public class UIButtonCheck : MonoBehaviour
{
    [SerializeField] private int _percent;

    /// <summary>
    /// Set SelectionManager units Percent.
    /// </summary>
    public void ValueCheck()
    {
        if (SelectionManager.Instance)
            SelectionManager.Instance.Percent = _percent;
        else
            UnityEngine.Debug.Log("make sure there is SelectionManager in the Scene");
    }
}
