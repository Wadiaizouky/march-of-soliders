﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CivilWarUIControler : MonoBehaviour
{
    public List<string> Levels_Name;
    public List<string> Levels_Info;
    public Text info;

    public List<Button> CivilWar_Buttons;

    private int Button_Clicked = 0;

    public ScrollRect ScrolView;

    public SaveLoadLevels Saveloadlevel;

    public GameObject LoadingPanel;
    public Slider LoadingSlider;

    void Start()
    {
        if (!MusicControler.Instance.GetComponent<AudioSource>().isPlaying && MusicControler.Instance.IsMusicPlay)
            MusicControler.Instance.GetComponent<AudioSource>().UnPause();

        LevelAudioSystem.Instance.GetComponent<AudioSource>().Stop();

        Saveloadlevel.Load();

        for (int i=1;i< Saveloadlevel.Levels_Enable.Count; i++)
        {
            if (Saveloadlevel.Levels_Enable[i]) {
                CivilWar_Buttons[i].GetComponent<InteractableButton>().enabled = true;
                CivilWar_Buttons[i].transform.GetChild(0).gameObject.SetActive(true);
                CivilWar_Buttons[i].transform.GetChild(2).gameObject.SetActive(false);
            }
        }
    }

    public void OnLevelClick(int Button_N)
    {
        CivilWar_Buttons[Button_Clicked].GetComponent<InteractableButton>().enabled = true;
        CivilWar_Buttons[Button_Clicked].transform.GetChild(0).gameObject.SetActive(true);
        CivilWar_Buttons[Button_Clicked].transform.GetChild(1).gameObject.SetActive(false);

        Button_Clicked = Button_N;
        info.text = Levels_Info[Button_N];

        CivilWar_Buttons[Button_N].transform.GetChild(0).gameObject.SetActive(false);
        CivilWar_Buttons[Button_N].transform.GetChild(1).gameObject.SetActive(true);
        CivilWar_Buttons[Button_N].GetComponent<InteractableButton>().enabled = false;
    }


    public void OnStartGame()
    {
        //SceneManager.LoadScene(Levels_Name[Button_Clicked]);
        LoadingPanel.SetActive(false);
        LoadingSlider.gameObject.SetActive(true);
        StartCoroutine(Loading_Scene(Levels_Name[Button_Clicked]));
    }

    IEnumerator Loading_Scene(string Scene_Name)
    {
        AsyncOperation Operation = SceneManager.LoadSceneAsync(Scene_Name);
        while (!Operation.isDone)
        {
            float progress = Mathf.Clamp01(Operation.progress / .9f);
            LoadingSlider.value = progress;

            yield return null;
        }
    }

    public void OnUp()
    {
        ScrolView.verticalScrollbar.value += 0.1f;
    }
    public void OnDown()
    {
        ScrolView.verticalScrollbar.value -= 0.1f;
    }
}
