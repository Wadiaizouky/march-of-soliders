﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SaveLoadScenes : MonoBehaviour
{
    public string Dir = "";
    public int MapLevell;
    public GameObject LoadPanel;
    public Slider LoadSlier;

    public void Load()
    {

        SceneManager.LoadScene("Level " + MapLevell);
    }

    //Delete All scenes saved
    public void Reset_Scenes()
    {
        SaveScenesManger.DeletDiracroryFils(Application.persistentDataPath + "/GameData/");
    }

    public void OnSliderLoadd()
    {
        StartCoroutine(OnSliderLoad());
    }
    IEnumerator OnSliderLoad()
    {
        float count = 0;
        while (count < 1)
        {
            count += 0.2f;
            LoadSlier.value += count;
            yield return null;
        }
        LoadPanel.SetActive(false);
    }
}
