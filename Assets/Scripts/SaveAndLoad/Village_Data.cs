﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Village_Data
{
    public int Sword;
    public bool IsEnemyVillage;
    public bool IsPlayerVillage;
}
