﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;

[System.Serializable]
public class Game_Data
{
    public int MalLevel;
    public int Silver;
    public float Gold;
    public Vector3 CameraPosition;
    public int TowerNumber;
}
