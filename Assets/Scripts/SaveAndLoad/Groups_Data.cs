﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum GroupTypee { Sword, Spear, Horse }

//public enum GroupOwnere { Player, Enemy }

[System.Serializable]
public class Groups_Data
{
    public string OwnerType;
    public int PlayerOwnerNumber;
    public string groupType;
    public bool isSelected;
    public int groupSize;
    public Vector3 goal;
    public int TargetCastle;
    public int SourceCastle;
    public Vector3 position;
    public List<Vector2> UnitsPositions;
}
