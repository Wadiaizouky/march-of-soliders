﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Castle_Data
{
    public int Sword;
    public bool IsEnemyCastle;
    public bool IswhitCastle;
    public int FarmCount;
    public int farmlose;
}
