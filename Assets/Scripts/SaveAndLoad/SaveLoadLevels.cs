﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SaveLoadLevels : MonoBehaviour
{
    public List<bool> Levels_Enable;

    
    public void AddLevel(int level_n)
    {
        Levels_Enable[level_n] = true;
       // Save();

    }

    public void Save() {
        for(int i=1;i<Levels_Enable.Count;i++)
        {
            if (Levels_Enable[i])
                PlayerPrefs.SetInt(i.ToString(), 1);
            else
                PlayerPrefs.SetInt(i.ToString(), 0);
        }
    }

    public void Load() {
        for (int i = 1; i < Levels_Enable.Count; i++)
        {
            int ii = PlayerPrefs.GetInt(i.ToString());
            if (ii == 1)
                Levels_Enable[i] = true;
            else
                Levels_Enable[i] = false;

        }
    }

    public void Reset_Levels()
    {
        for (int i = 1; i < Levels_Enable.Count; i++)
        {
            int ii = PlayerPrefs.GetInt(i.ToString());
            if (ii == 1)
            {
                PlayerPrefs.SetInt(i.ToString(), 0);
                Levels_Enable[i] = false;
            }
        }
    }
}
