﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;
using UnityEngine.UI;

public class LoadUIPanelControler : MonoBehaviour
{
    public GameObject ScrolviewContent;

    public Button SaveName;

    public GameObject Deletlevel;

    void Start()
    {
        for (int i = 0; i < ScrolviewContent.transform.childCount; i++)
        {
            Destroy(ScrolviewContent.transform.GetChild(i).gameObject);
        }


        //Return all Saved Levels
        string[] AllSaveLevels = SaveScenesManger.AllSavedLoad();
        for (int i = AllSaveLevels.Length - 1; i >= 0; i--)
        {
            Button Save_Name = Instantiate(SaveName);
            Save_Name.name = AllSaveLevels[i];
            int length = (Application.persistentDataPath + "/GameData/").Length;
            Save_Name.transform.GetChild(0).gameObject.name = AllSaveLevels[i].Substring(length);


            Save_Name.transform.SetParent(ScrolviewContent.transform);
        }
    }

    public void OnLoad()
    {
        SaveAndLoad_UI.Instance.OnLoad();
    } 
    public void OnDelete()
    {
        Deletlevel.SetActive(true);
    }
    public void OnYesDelet()
    {
        string SaveName_Clicked = PasueController.Instance.SaveName_Clicked;
        SaveScenesManger.DeletDiractory(SaveName_Clicked);

        Resetscrollview();
        Deletlevel.SetActive(false);
    }
    public void OnNoDelet()
    {
        Deletlevel.SetActive(false);
    }

    void Resetscrollview()
    {
        for (int i = 0; i < ScrolviewContent.transform.childCount; i++)
        {
            Destroy(ScrolviewContent.transform.GetChild(i).gameObject);
        }


        //Return all Saved Levels
        string[] AllSaveLevels = SaveScenesManger.AllSavedLoad();
        for (int i = 0; i < AllSaveLevels.Length; i++)
        {
            Button Save_Name = Instantiate(SaveName);
            Save_Name.name = AllSaveLevels[i];
            Save_Name.transform.GetChild(0).gameObject.name = AllSaveLevels[i].Substring(70);


            Save_Name.transform.SetParent(ScrolviewContent.transform);
        }
    }
}
