﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Tower_Data
{
    public int Sword;
    public bool IsEnemyTower;
    public bool IsPlayerTower;
}
