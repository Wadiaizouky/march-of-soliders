﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UI_Data
{
    public int KingdomLevel;
    public int BlacksmithLevel;
    public int ReiforcementLevel;
}
