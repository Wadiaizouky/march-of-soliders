﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using MarchOfSoldiers;

public class SaveLoadCheked : MonoBehaviour
{
    public GameObject MissionSaved;
    public GameObject Overwrite;
    public GameObject Deletlevel;
    public GameObject ResetUi;

    public SaveLoadLevels Saveloadlevel;
    public SaveLoadScenes SaveloadScene;

    public void OnOkMissionSaved()
    {
        MissionSaved.SetActive(false);
        Overwrite.SetActive(false);

        //Close UI Save
        PasueController.Instance.SaveButton.SetActive(false);
    }
    public void OnYesOverwrite()
    {
        SaveAndLoad_UI.Instance.SaveScene();
    }
    public void OnNoOverwrite()
    {
        Overwrite.SetActive(false);
    }

    public void OnYesDelet()
    {
        SaveAndLoad_UI.Instance.DeletScene();
        Deletlevel.SetActive(false);
    }
    public void OnNoDelet()
    {
        Deletlevel.SetActive(false);
    }

    public void OnYesReset()
    {
        Saveloadlevel.Reset_Levels();
        SaveloadScene.Reset_Scenes();

        ResetUi.SetActive(false);
    }
    public void OnNoReset()
    {
        ResetUi.SetActive(false);
    }
}
