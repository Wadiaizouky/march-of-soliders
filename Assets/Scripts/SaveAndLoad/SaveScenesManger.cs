﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using MarchOfSoldiers;

public static class SaveScenesManger
{
    public static string Diroctory = "/GameData/";
    //public static string FileName = "MyDatat.txt";

    public static List<Castle_Data> Castledata = new List<Castle_Data>();
    public static List<Gate_Data> Gatedata = new List<Gate_Data>();
    public static List<Village_Data> Villagedata = new List<Village_Data>();
    public static List<Tower_Data> Towerdata = new List<Tower_Data>();
    public static List<Groups_Data> Groupsedata = new List<Groups_Data>();


    public static void Save(List<Castle_Data> data,string Dir)
    {
        //Diroctory = Dir;

        int cout = 0;
        for (int i = 0; i < data.Count; i++)
        {
            MainSave(data[i], "CastleData"+cout+".txt",Dir);
            Castledata.Add(data[i]);
            cout++;
        }
        Debug.Log("save Castles ok");
    }
    public static void SaveGates(List<Gate_Data> data, string Dir)
    {
        //Diroctory = Dir;

        int cout = 0;
        for (int i = 0; i < data.Count; i++)
        {
            MainSaveGate(data[i], "GateData" + cout + ".txt", Dir);
            Gatedata.Add(data[i]);
            cout++;
        }
        Debug.Log("save Gates ok");
    }
    public static void SaveVillages(List<Village_Data> data, string Dir)
    {
        //Diroctory = Dir;

        int cout = 0;
        for (int i = 0; i < data.Count; i++)
        {
            MainSaveVillage(data[i], "VillageData" + cout + ".txt", Dir);
            Villagedata.Add(data[i]);
            cout++;
        }
        Debug.Log("save Villages ok");
    }
    public static void SaveTower(List<Tower_Data> data, string Dir)
    {
        //Diroctory = Dir;

        int cout = 0;
        for (int i = 0; i < data.Count; i++)
        {
            MainSaveTower(data[i], "TowerData" + cout + ".txt", Dir);
            Towerdata.Add(data[i]);
            cout++;
        }
        Debug.Log("save Villages ok");
    }

    static void MainSave(Castle_Data Data, string FileName,string Dir)
    {
        /*C:\Users\HP\AppData\LocalLow\DefaultCompany\RealTimeStratigy\CastleData*/
        string dir =  Dir;
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);

        string Json = JsonUtility.ToJson(Data);
        File.WriteAllText(dir + FileName, Json);

    }
    static void MainSaveGate(Gate_Data Data, string FileName, string Dir)
    {
        /*C:\Users\HP\AppData\LocalLow\DefaultCompany\RealTimeStratigy\CastleData*/
        string dir =  Dir;
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);

        string Json = JsonUtility.ToJson(Data);
        File.WriteAllText(dir + FileName, Json);

    }
    static void MainSaveVillage(Village_Data Data, string FileName, string Dir)
    {
        /*C:\Users\HP\AppData\LocalLow\DefaultCompany\RealTimeStratigy\CastleData*/
        string dir = Dir;
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);

        string Json = JsonUtility.ToJson(Data);
        File.WriteAllText(dir + FileName, Json);

    }
    static void MainSaveTower(Tower_Data Data, string FileName, string Dir)
    {
        /*C:\Users\HP\AppData\LocalLow\DefaultCompany\RealTimeStratigy\CastleData*/
        string dir = Dir;
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);

        string Json = JsonUtility.ToJson(Data);
        File.WriteAllText(dir + FileName, Json);

    }


    public static void SaveGameData(Game_Data Data, string Dir)
    {
        /*C:\Users\HP\AppData\LocalLow\DefaultCompany\RealTimeStratigy\CastleData*/
        //Diroctory = Dir;

        string dir =  Dir;
        string FileName = "GameData.txt";

        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);

        string Json = JsonUtility.ToJson(Data);
        File.WriteAllText(dir + FileName, Json);

        Debug.Log("save GameData ok");
    }

    public static void SaveUI(UI_Data Data, string Dir)
    {
        /*C:\Users\HP\AppData\LocalLow\DefaultCompany\RealTimeStratigy\CastleData*/
        //Diroctory = Dir;

        string dir =  Dir;
        string FileName = "UI_Data.txt";

        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);

        string Json = JsonUtility.ToJson(Data);
        File.WriteAllText(dir + FileName, Json);

        Debug.Log("save UI ok");
    }


    public static void SaveGroups(List<Groups_Data> data, string Dir)
    {
        //Diroctory = Dir;
        string dir =  Dir;
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);

        int cout = 0;
            for (int i = 0; i < data.Count; i++)
            {
                MainSaveGroups(data[i], "GroupsData" + cout + ".txt", Dir);
                Groupsedata.Add(data[i]);
                cout++;
            }
        Debug.Log("save Groups ok");
    }

    static void MainSaveGroups(Groups_Data Data, string FileName, string Dir)
    {
        /*C:\Users\HP\AppData\LocalLow\DefaultCompany\RealTimeStratigy\GroupData*/
        string dir = Dir;
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);

        string Json = JsonUtility.ToJson(Data);
        File.WriteAllText(dir + FileName, Json);
    }

    public static List<Castle_Data> Load(int CastleNumber,string Filename)
    {
        List<Castle_Data> Cas_d = new List<Castle_Data>();
        int cout = 0;
        for (int i = 0; i < CastleNumber; i++)
        {
            string FilePath = Filename + "/CastleData" + cout + ".txt";
            if (File.Exists(FilePath))
            {
                string Json = File.ReadAllText(FilePath);
                Castle_Data data;
                data = JsonUtility.FromJson<Castle_Data>(Json);
                Cas_d.Add(data);
                Debug.Log("Load file ok");
            }
            else
                Debug.Log("File not exist ..");
            cout++;
        }

        return Cas_d;
    }
    public static List<Gate_Data> LoadGates(int CastleNumber, string Filename)
    {
        List<Gate_Data> Cas_d = new List<Gate_Data>();
        int cout = 0;
        for (int i = 0; i < CastleNumber; i++)
        {
            string FilePath = Filename + "/GateData" + cout + ".txt";
            if (File.Exists(FilePath))
            {
                string Json = File.ReadAllText(FilePath);
                Gate_Data data;
                data = JsonUtility.FromJson<Gate_Data>(Json);
                Cas_d.Add(data);
                Debug.Log("Load file ok");
            }
            else
                Debug.Log("File not exist ..");
            cout++;
        }

        return Cas_d;
    }
    public static List<Village_Data> LoadVillages(int CastleNumber, string Filename)
    {
        List<Village_Data> Cas_d = new List<Village_Data>();
        int cout = 0;
        for (int i = 0; i < CastleNumber; i++)
        {
            string FilePath = Filename + "/VillageData" + cout + ".txt";
            if (File.Exists(FilePath))
            {
                string Json = File.ReadAllText(FilePath);
                Village_Data data;
                data = JsonUtility.FromJson<Village_Data>(Json);
                Cas_d.Add(data);
                Debug.Log("Load file ok");
            }
            else
                Debug.Log("File not exist ..");
            cout++;
        }

        return Cas_d;
    }
    public static List<Tower_Data> LoadTowers(int CastleNumber, string Filename)
    {
        List<Tower_Data> Cas_d = new List<Tower_Data>();
        int cout = 0;
        for (int i = 0; i < CastleNumber; i++)
        {
            string FilePath = Filename + "/TowerData" + cout + ".txt";
            if (File.Exists(FilePath))
            {
                string Json = File.ReadAllText(FilePath);
                Tower_Data data;
                data = JsonUtility.FromJson<Tower_Data>(Json);
                Cas_d.Add(data);
                Debug.Log("Load file ok");
            }
            else
                Debug.Log("File not exist ..");
            cout++;
        }

        return Cas_d;
    }




    public static Game_Data LoadGameData(string Filename)
    {
        Game_Data Cas_d = new Game_Data();
        string FilePath = Filename + "/GameData.txt";
        if (File.Exists(FilePath))
            {
                string Json = File.ReadAllText(FilePath);
                Cas_d = JsonUtility.FromJson<Game_Data>(Json);
                Debug.Log("Load file ok");
         }
         else
                Debug.Log("File not exist ..");

        return Cas_d;
    }

    public static UI_Data LoadUI(string Filename)
    {
        UI_Data Cas_d = new UI_Data();
        string FilePath = Filename + "/UI_Data.txt";
        if (File.Exists(FilePath))
        {
            string Json = File.ReadAllText(FilePath);
            Cas_d = JsonUtility.FromJson<UI_Data>(Json);
            Debug.Log("Load file ok");
        }
        else
            Debug.Log("File not exist ..");

        return Cas_d;
    }


    public static List<Groups_Data> LoadGroups(string Filename)
    {
        int FileCount = Directory.GetFiles(Filename).Length;
        List<Groups_Data> Cas_d = new List<Groups_Data>();
        int cout = 0;
        for (int i = 0; i < FileCount; i++)
        {
            string FilePath = Filename + "/GroupsData" + cout + ".txt";
            if (File.Exists(FilePath))
            {
                string Json = File.ReadAllText(FilePath);
                Groups_Data data;
                data = JsonUtility.FromJson<Groups_Data>(Json);
                Cas_d.Add(data);
                Debug.Log("Load file ok");
            }
            else
                Debug.Log("File not exist ..");
            cout++;
        }

        return Cas_d;
    }

    public static void DeletDiractory(string Dir)
    {
        Directory.Delete(Dir,true);
    }

    public static void DeletDiracroryFils(string Dir)
    {
        DirectoryInfo dirinfo = new DirectoryInfo(Dir);
        foreach (DirectoryInfo d in dirinfo.GetDirectories())
            DeletDiractoryFile(d,Dir+"/"+ d.Name + "/");

    }
    static void DeletDiractoryFile(DirectoryInfo dirinfo,string Dir)
    {
        foreach (DirectoryInfo d in dirinfo.GetDirectories())
            DeletDiractoryFile(d, Dir + "/" + d.Name + "/");
        foreach (FileInfo d in dirinfo.GetFiles())
            d.Delete();
        dirinfo.Delete();
    }

    public static string[] AllSavedLoad()
    {
        string DiractoryPath = Application.persistentDataPath + "/GameData/";
        string[] SaveLevels = new string[Directory.GetDirectories(DiractoryPath).Length];
        SaveLevels = Directory.GetDirectories(DiractoryPath);
        return SaveLevels;
    }
}
