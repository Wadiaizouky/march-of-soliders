﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Gate_Data
{
    public int Sword;
    public bool IsEnemyGate;
    public bool IsPlayerGate;
}