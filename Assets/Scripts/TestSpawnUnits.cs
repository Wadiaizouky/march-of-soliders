﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MarchOfSoldiers
{
    public class TestSpawnUnits : MonoBehaviour
    {
        private void Start()
        {
            Vector3 spawnPoint = transform.position; // Set Here the position of Units
            SingleCastle targetCastle = null; // Set Here the Target Castle 


            List<Vector2> CirclePosition = new List<Vector2>();

            for (int i = 0; i < 20; i++)
            {
                CirclePosition.Add(spawnPoint);
            }

            GameObject tempCastle = new GameObject("Test Castle");
            tempCastle.transform.position = spawnPoint;
            tempCastle.AddComponent<SingleCastle>();

            UnitsSpawner.Instance.CreateGroup(100, CirclePosition, GroupType.Sword,
                                        OwnerType.Player, 1, Input.mousePosition,
                                        tempCastle.GetComponent<SingleCastle>(), targetCastle);

            Destroy(tempCastle);
        }
    }
}