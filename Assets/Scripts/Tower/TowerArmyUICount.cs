﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;
using UnityEngine.UI;

public class TowerArmyUICount : MonoBehaviour
{



    //number of Sword Solider
    public int sward { get; set; }
    public GameObject p1;
    bool sw = true;
    public Text SwardText;
    int ExtraSword;

    public bool IsPlayerTower;
    public bool IsEnemyTower;

    public GameObject particSys;
    //public Texture whitecastelTex;

    public Sprite PlayerGatetex;
    public Sprite EnemyGatetex;

    public List<GameObject> AllOtherCastles;
    private SingleCastle singleCastle;

    public GameObject UICastleBackground;
    //UIGate Sprite
    public Sprite PlayerUISolider;
    public Sprite EnemyUISolider;
    public Sprite WhiteUISolider;

    //Minimap
    public GameObject SpriteMinimap;

    private static GateArmyUICount _instance = null;


    public static GateArmyUICount Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GateArmyUICount>();
            return _instance;
        }
    }

    void Start()
    {
        singleCastle = GetComponent<SingleCastle>();

        SpriteRenderer sp = GetComponent<SpriteRenderer>();

        if (IsEnemyTower)
        {
            sp.sprite = EnemyGatetex;
            UICastleBackground.GetComponent<Image>().sprite = EnemyUISolider;
            SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        else if (IsPlayerTower)
        {
            sp.sprite = PlayerGatetex;
            UICastleBackground.GetComponent<Image>().sprite = PlayerUISolider;
            SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.red;
        }
        else
            SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.white;

        sward = 0;
    }

    // Update is called once per frame
    void Update()
    {



        //UI Sward above the Castle
        SwardText.text = sward.ToString();


    }

    //when Solider collider hit castle collider
    public void OnCollisionWithUnit(GameObject other)
    {
        SingleUnit solider = other.gameObject.GetComponent<SingleUnit>();
        ////Get the castle pressed by player
        if (other.gameObject.tag == "Player" && solider != null)
        {
            GroupOfUnits curr = solider.Group;
            SingleCastle currentcastlepressed = curr.targetCastle;

            int size = solider.VirtualValue;
            ////check if the curent castle is castle pressed by player
            if ((currentcastlepressed != null && currentcastlepressed.gameObject == this.gameObject)
                || singleCastle.PlayerOwnerNumber != solider.PlayerOwnerNumber)
            {
                if (IsEnemyTower || (!IsEnemyTower && !IsPlayerTower))
                    sward -= size;

                //Army Castle
                else
                {
                    sward += size;
                }

                //Destroy Solider

                /* ************************************************** */
                Destroy(other.transform.gameObject);
                /* ************************************************** */

                if (sward <= -1)
                    ChangeCastleSprite("Player", solider.PlayerOwnerNumber);
            }

        }
        else if (other.gameObject.tag == "Enemy" && solider != null)
        {
            GroupOfUnits curr = solider.Group;
            SingleCastle currentcastlepressed = curr.targetCastle;


            int size = solider.VirtualValue;
            ////check if the curent castle is castle pressed by player
            if ((currentcastlepressed != null && currentcastlepressed.gameObject == this.gameObject)
               || singleCastle.PlayerOwnerNumber != solider.PlayerOwnerNumber)
            {

                if (IsPlayerTower)
                    sward -= size;
                //Army Castle
                else
                    sward += size;


                //Destroy Solider

                /* ************************************************** */
                Destroy(other.transform.gameObject);
                /* ************************************************** */
                if (sward <= -1)
                {
                    ChangeCastleSprite("Enemy", -1);
                    //GatesSelection.Instance.DeselectSingleGate(GetComponent<SingleCastle>());
                    CastlesSelection.Instance.DeselectSingleCastle(GetComponent<SingleCastle>());
                }
            }
        }
    }


    /// <summary>wade adds
    /// return the percentage selected by player in UI
    /// </summary>
    public int Selectedpercentage()
    {
        return NumOfUnits_UI.Instance.GetNumOfUnits();
    }


    public void ChangeCastleSprite(string tag, int playerNumber)
    {
        if (tag == "Player")
        {
            transform.gameObject.tag = "Player";
            if (!singleCastle.disableSelectable)
                transform.gameObject.layer = 9;
            sward *= -1;
            IsEnemyTower = false;
            IsPlayerTower = true;
            SpriteRenderer sp = GetComponent<SpriteRenderer>();
            sp.sprite = PlayerGatetex;


            singleCastle.OwnerType = OwnerType.Player;
            singleCastle.PlayerOwnerNumber = playerNumber;
            SelectionManager.Instance.AddSelectable(gameObject.GetComponent<SingleCastle>(), SelectableType.Castle);

            UICastleBackground.GetComponent<Image>().sprite = PlayerUISolider;

            //Change Sprite minimap
            ChangeMinimapTower(MinimapManger.Instance.TowerNumber);

        }
        else if (tag == "Enemy")
        {
            transform.gameObject.tag = "Enemy";
            if (!singleCastle.disableSelectable)
                transform.gameObject.layer = 9;
            sward *= -1;
            IsEnemyTower = true;
            IsPlayerTower = false;
            SpriteRenderer sp = GetComponent<SpriteRenderer>();
            sp.sprite = EnemyGatetex;

            SelectionManager.Instance.RemoveSelectable(gameObject.GetComponent<SingleCastle>(), SelectableType.Castle);

            singleCastle.OwnerType = OwnerType.Enemy;
            singleCastle.PlayerOwnerNumber = -1;

            UICastleBackground.GetComponent<Image>().sprite = EnemyUISolider;

            //Change Sprite minimap
            ChangeMinimapTower(MinimapManger.Instance.TowerNumber);

        }
          else
            {
            if (transform.tag == "Player")
            {
                //FarmCount = 0;
                //FarmAvailable = 0;
                //transform.GetChild(2).transform.GetChild(3).gameObject.SetActive(false);
                //GetComponent<CastleFarmButtom>().ChangeFarmUI();
                //GetComponent<CastleFarmButtom>().FarmC.text = "0/0";

                SelectionManager.Instance.RemoveSelectable(gameObject.GetComponent<SingleCastle>(), SelectableType.Castle);

                if (!singleCastle.disableSelectable)
                    transform.gameObject.layer = 9;

                singleCastle.OwnerType = OwnerType.None;
                singleCastle.PlayerOwnerNumber = 0;
                //SpriteRenderer sp = GetComponent<SpriteRenderer>();
                //sp.sprite = Enemycasteltex;

                UICastleBackground.GetComponent<Image>().sprite = WhiteUISolider;
                // UIExtraSolders.GetComponent<Image>().sprite = EnemyUIExtraSoldiers;
            }

            transform.gameObject.tag = "whiteCastle";
            //CastleAnimator.SetBool("IsNeutarl_Castle", true);
            IsPlayerTower = false;
            IsEnemyTower = false;

            //SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.white;

        }
    
    }

    public void ChangeMinimapTower(int Tower_N)
    {
        if (Tower_N >= 2)
        {
            if (transform.gameObject.tag == "Player")
                SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.red;
            else if (transform.gameObject.tag == "Enemy")
                SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.blue;

        }
        else
            SpriteMinimap.GetComponent<SpriteRenderer>().color = Color.white;

    }
}



