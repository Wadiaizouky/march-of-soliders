﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchOfSoldiers;

//public enum Minimapstatus { Locked, OpenWithWhiteColor, OpenWithCastleColor, OpenWithAllColor,OpenWithAtackPosition}
public class MinimapManger : MonoBehaviour
{
    public int TowerNumber = 0;
    public GameObject LockedMinimap;
    public GameObject UnLockedMinimap;

    private static MinimapManger _instance = null;
    public static MinimapManger Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<MinimapManger>();
            return _instance;
        }
    }

    private void Update()
    {
        if (KingdomManager.Instance.MapLevel == 15)
        {
            if (TowerNumber != TowerNumbers())
            {
                TowerNumber = TowerNumbers();
                ChangeMinimapStatus(TowerNumber);
            }
        }
    }

    public void ChangeMinimapStatus(int Tower_N)
    {
        switch (Tower_N)
        {
            case 0: LockedMinimap.SetActive(true);UnLockedMinimap.SetActive(false); break;
            case 1: LockedMinimap.SetActive(false); UnLockedMinimap.SetActive(true); ChangeCastleMinimap(Tower_N); ChangeSoldierMinimap(Tower_N); break;
            case 2: LockedMinimap.SetActive(false); UnLockedMinimap.SetActive(true); ChangeCastleMinimap(Tower_N); ChangeSoldierMinimap(Tower_N); break;
            case 3: LockedMinimap.SetActive(false); UnLockedMinimap.SetActive(true); ChangeCastleMinimap(Tower_N); ChangeSoldierMinimap(Tower_N); break;
            case 4: LockedMinimap.SetActive(false); UnLockedMinimap.SetActive(true); ChangeCastleMinimap(Tower_N); ChangeSoldierMinimap(Tower_N); break;
        }
    }

    private void ChangeCastleMinimap(int Tower_N)
    {
        List<SingleCastle> AllCastle = SelectionManager.Instance.Allcastle;
        List<SingleCastle> Allvillage = SelectionManager.Instance.AllVillage;
        List<TowerArmyUICount> AllTower = SelectionManager.Instance.AllTower;


        foreach (SingleCastle castle in AllCastle){
                castle.gameObject.GetComponent<CastelArmyUICount>().ChangeMinimapCastle(Tower_N);
        }
        foreach (SingleCastle castle in Allvillage)
        {
                castle.gameObject.GetComponent<VillageArmyUICount>().ChangeMinimapVillage(Tower_N);
        }
        foreach (TowerArmyUICount castle in AllTower)
        {
            castle.gameObject.GetComponent<TowerArmyUICount>().ChangeMinimapTower(Tower_N);
        }

    }
    private void ChangeSoldierMinimap(int Tower_N)
    {
        List<GroupOfUnits> Allgroup = GroupsManager.Instance.GetAllGroups();
        foreach (GroupOfUnits group in Allgroup)
        {
            List<SingleUnit> AllUnits = group.units;
            if (Tower_N >= 3)
            {
                foreach (SingleUnit Unit in AllUnits)
                    Unit.gameObject.transform.GetChild(1).gameObject.SetActive(true);
            }
            else
            {
                foreach (SingleUnit Unit in AllUnits)
                    Unit.gameObject.transform.GetChild(1).gameObject.SetActive(false);
            }
        }

    }

    private int TowerNumbers()
    {
        int Tower_N = 0;
        foreach (TowerArmyUICount Tower in SelectionManager.Instance.AllTower)
        {
            if (Tower.IsPlayerTower)
                Tower_N++;
        }
            
        return Tower_N;
    }
}
