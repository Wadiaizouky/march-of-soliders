﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace MarchOfSoldiers
{
    public class TestRecording : MonoBehaviour
    {
        public Sprite castleBlueUI;
        public RuntimeAnimatorController enemyAniamtor;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                switch (SelectionManager.Instance.currentSelectedType)
                {
                    case SelectableType.Castle:
                        List<SingleCastle> selectedCastles = CastlesSelection.Instance.GetSelectedCastlesByPlayerNum(1);
                        foreach (SingleCastle castle in selectedCastles)
                        {
                            Animator anim = castle.GetComponent<Animator>();
                            anim.SetBool("IsPlayer_Castle", false);
                            anim.SetBool("IsBlue_Castle", true);

                            castle.transform.Find("Castelclike").transform.Find("CastleUIText (2)").GetChild(0).GetComponent<Image>().sprite = castleBlueUI;
                        }
                        break;
                    case SelectableType.Soldier:
                        List<GroupOfUnits> selectedGroups = UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(1);
                        foreach (GroupOfUnits group in selectedGroups)
                        {
                            foreach (SingleUnit unit in group.units)
                            {
                                Animator anim = unit.GetComponent<Animator>();
                                anim.runtimeAnimatorController = enemyAniamtor;
                            }
                        }
                        break;

                }

            }
        }
    }
}

