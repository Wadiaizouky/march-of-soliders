﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MarchOfSoldiers;
using System;

public class UISpeedGroupCost : MonoBehaviour
{
    public Text CostText;

     void Update()
    {
        Cost();
    }

    public void Cost()
    {
        int Dis_Silver = 0;
        List<GroupOfUnits> selectedGroups = UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(1);
        if (selectedGroups.Count > 0)
        {
            foreach (GroupOfUnits group in selectedGroups)
            {
                int Grout_N = group.units.Count;
                Dis_Silver += 40 * Grout_N;
            }

            //float Dis_Gold = Dis_Silver / 100;
            CostText.text = Dis_Silver.ToString();
        }
        else
            CostText.text = "0";

    }
}
