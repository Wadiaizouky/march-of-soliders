﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MarchOfSoldiers;
using System;

public class UIArmy : MonoBehaviour
{
    public List<GameObject> Army_Buttons;

    public List<Text> Army_Buttons_Texts;

    //UI Number Sprite
    public Sprite Number20_Plus_Sprite;
    public Sprite Number20_min_Sprite;


    private static UIArmy _instance = null;
    public static UIArmy Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<UIArmy>();
            return _instance;
        }
    }
    //Barrel prefab Gameobject
    public GameObject Barrel;

    //private int counter;

    private void Awake()
    {
        BlacksmithManager.Instance.OnBlacksmithUpgrade += OnBlacksmithUpgrade;
    }

    private void Start()
    {
        for (int i = 0; i < Army_Buttons.Count; i++)
        {
            Army_Buttons[i].GetComponent<InteractableButton>().LockButton();
        }
    }

    private void OnBlacksmithUpgrade()
    {
        int blacksmithLevel = PlayersManager.Instance.LocalPlayer.BlacksmithLevel;
        switch (blacksmithLevel)
        {
            case 1:
                Army_Buttons[0].GetComponent<InteractableButton>().ActiveButton();
                Army_Buttons[1].GetComponent<InteractableButton>().ActiveButton();
                break;
            case 2:
                Army_Buttons[2].GetComponent<InteractableButton>().ActiveButton();
                if (KingdomManager.Instance.MapLevel > 6)
                    Army_Buttons[3].GetComponent<InteractableButton>().ActiveButton();
                break;
            case 3:
                if (KingdomManager.Instance.MapLevel > 6)
                {
                    Army_Buttons[4].GetComponent<InteractableButton>().ActiveButton();
                    Army_Buttons[5].GetComponent<InteractableButton>().ActiveButton();
                }
                break;
        }
    }

    //cell 20 Soldiers from Selected castle
    public void SellSoldiersFromCastle()
    {
        if (!PasueController.Instance.IsPasued)
        {
            List<SingleCastle> selectedCastles = CastlesSelection.Instance.GetSelectedCastlesByPlayerNum(1);
            if (selectedCastles.Count == 1)
            {
                if (selectedCastles[0].GetComponent<CastelArmyUICount>().sward >= 20)
                {
                    CastelArmyUICount castelArmy = selectedCastles[0].GetComponent<CastelArmyUICount>();
                    castelArmy.sward -= 20;
                    castelArmy.MyExraCorotine(Number20_min_Sprite);

                    PlayersManager.Instance.LocalPlayer.Gold += 20.0f;

                    Army_Buttons[0].GetComponent<InteractableButton>().DeactivateButton();
                    //counter = 15;
                    ActiveButton(0, 15);
                }
            }
        }
    }

    //Add Soldiers in selectde Castle
    public void AddSoldiersInCastle()
    {
        if (!PasueController.Instance.IsPasued)
        {
            List<SingleCastle> selectedCastles = CastlesSelection.Instance.GetSelectedCastlesByPlayerNum(1);
            if (selectedCastles.Count == 1)
            {
                if (PlayersManager.Instance.LocalPlayer.Gold >= 50f)
                {
                    CastelArmyUICount castelArmy = selectedCastles[0].GetComponent<CastelArmyUICount>();
                    PlayersManager.Instance.LocalPlayer.Gold -= 50f;
                    castelArmy.sward += 20;
                    castelArmy.MyExraCorotine(Number20_Plus_Sprite);

                    Army_Buttons[1].GetComponent<InteractableButton>().DeactivateButton();
                    //counter = 30;
                    ActiveButton(1, 30);
                }
            }
        }
    }

    //speed the Soldiers selected by Player
    public void SpeedSoldiersSelected()
    {
        if (!PasueController.Instance.IsPasued)
        {
            int Dis_Silver = 0;
            List<GroupOfUnits> selectedGroups = UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(1);
            if (selectedGroups.Count > 0)
            {
                foreach (GroupOfUnits group in selectedGroups)
                {
                    int Grout_N = group.units.Count;
                    Dis_Silver += 40 * Grout_N;
                }

                float Dis_Gold = Dis_Silver / 100;
                if (PlayersManager.Instance.LocalPlayer.Gold >= Dis_Gold)
                {
                    PlayersManager.Instance.LocalPlayer.Gold -= Dis_Gold;
                    StartCoroutine(SpeedUnits());

                    Army_Buttons[2].GetComponent<InteractableButton>().DeactivateButton();
                    //counter = 60;
                    ActiveButton(2, 60);
                }
            }
        }
    }

    //speed the Units Selected
    IEnumerator SpeedUnits()
    {
        List<GroupOfUnits> seletedGroups = UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
        List<SingleUnit> allSelectedUnits = new List<SingleUnit>();

        foreach (GroupOfUnits group in seletedGroups)
        {
            allSelectedUnits.AddRange(group.units);
        }

        float prevSpeed = 0;
        float animatorPrevSpeed = 1;

        foreach (SingleUnit unit in allSelectedUnits)
        {
            prevSpeed = unit.Agent.speed;
            animatorPrevSpeed = unit.Unit_AnimationManager.GetAnimator().speed;

            unit.Agent.speed = unit.Agent.speed * 1.5f;
            unit.Unit_Movement.speed = unit.Agent.speed * 1.5f;
            unit.Unit_AnimationManager.GetAnimator().speed = unit.Unit_AnimationManager.GetAnimator().speed * 1.5f;

            unit.Group.groupOfUnits_Movement.movementCommander.Agent.speed = unit.Agent.speed * 1.5f;
        }

        yield return new WaitForSeconds(15f);

        foreach (SingleUnit unit in allSelectedUnits)
        {
            unit.Agent.speed = prevSpeed;
            unit.Unit_Movement.speed = prevSpeed;
            unit.Unit_AnimationManager.GetAnimator().speed = animatorPrevSpeed;

            unit.Group.groupOfUnits_Movement.movementCommander.Agent.speed = prevSpeed;
        }

    }

    //Make Button Desactive for x second
    public void ActiveButton(int Button_number, int Counter)
    {
        if (!Army_Buttons_Texts[Button_number].IsActive())
            Army_Buttons_Texts[Button_number].gameObject.SetActive(true);


        if (Counter == 0)
        {
            Army_Buttons_Texts[Button_number].gameObject.SetActive(false);
            Army_Buttons[Button_number].GetComponent<InteractableButton>().ActiveButton();
        }

        else
        {
            Army_Buttons_Texts[Button_number].text = Counter.ToString();
            StartCoroutine(Active_Button(Button_number, Counter));
        }
    }
    IEnumerator Active_Button(int Button_number, int counter)
    {
        yield return new WaitForSeconds(1f);
        counter--;

        ActiveButton(Button_number, counter);

    }
    /// <summary>
    /// //////
    public void BarrelsClick()
    {
        if (!PasueController.Instance.IsPasued)
        {
            if (PlayersManager.Instance.LocalPlayer.Gold >= 250.0f)
            {
                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector3 mousePosition = new Vector3(pos.x, pos.y, 0f);
                Instantiate(Barrel, mousePosition, Quaternion.identity);

                // PlayersManager.Instance.LocalPlayer.Gold -= 250.0f;

                Army_Buttons[3].GetComponent<InteractableButton>().DeactivateButton();

                //ActiveButton(3, 20);
            }
        }
    }

    public void HideClick()
    {
        if (!PasueController.Instance.IsPasued)
        {
            List<GroupOfUnits> selectedGroups = UnitsSelection.Instance.GetSelectedGroupsByPlayerNum(1);

            if (PlayersManager.Instance.LocalPlayer.Gold >= 500)
            {
                GroupsManager.Instance.ConvertSwordUnitsToRam(selectedGroups);

                PlayersManager.Instance.LocalPlayer.Gold -= 500;

                //Army_Buttons[4].GetComponent<InteractableButton>().DeactivateButton();
            }
        }
    }

    public void SummonClick()
    {
        if (PlayersManager.Instance.LocalPlayer.Gold >= 1200.0f)
        {
            //PlayersManager.Instance.LocalPlayer.Gold -= 400.0f;
            MoveMouse.Instance.IsSummon = true;
            GameObject summon = new GameObject("Summon");
            summon.AddComponent<SummonManger>();

            List<SingleCastle> AllPlayerCastle = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(1);
            foreach (SingleCastle castle in AllPlayerCastle)
            {
                castle.gameObject.transform.GetChild(7).gameObject.SetActive(true);
            }

            Army_Buttons[5].GetComponent<InteractableButton>().DeactivateButton();
        }
    }

    public void Load(int blacksmithLevel)
    {
        //for (int i = 1; i <= blacksmithLevel; i++)
        //{
        //    switch (i)
        //    {
        //        case 1:
        //            Army_Buttons[0].GetComponent<InteractableButton>().ActiveButton();
        //            Army_Buttons[1].GetComponent<InteractableButton>().ActiveButton();
        //            break;
        //        case 2:
        //            Army_Buttons[2].GetComponent<InteractableButton>().ActiveButton();
        //            if (KingdomManager.Instance.MapLevel > 6)
        //                Army_Buttons[3].GetComponent<InteractableButton>().ActiveButton();
        //            break;
        //        case 3:
        //            if (KingdomManager.Instance.MapLevel > 6)
        //            {
        //                Army_Buttons[4].GetComponent<InteractableButton>().ActiveButton();
        //                Army_Buttons[5].GetComponent<InteractableButton>().ActiveButton();
        //            }
        //            break;
        //    }
        //}

    }
}
