﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchOfSoldiers
{
    public class VillageGoldSystem : MonoBehaviour
    {
        public SingleCastle village;
        public List<SingleCastle> castles;

        public int enemyGold;
        public int playerGold;

        public float enemyUpdaterTimer;
        public int enemyUpdaterAmountOfGold;

        private bool isPlayerWin = false;


        private VillageArmyUICount villageArmy;
        private List<CastelArmyUICount> castlesArmy = new List<CastelArmyUICount>();
        private List<VillageArmyUICount> villagesArmy = new List<VillageArmyUICount>();


        private VillageGoldSystem_UI villageGoldSystem_UI;


        private void Awake()
        {
            Set();
            Assign();
        }

        private void Set()
        {
            //village.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            //foreach (SingleCastle castle in castles)
            //{
            //    castle.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            //}
            village.gameObject.layer = LayerMask.NameToLayer("Default");
            foreach (SingleCastle castle in castles)
            {
                castle.gameObject.layer = LayerMask.NameToLayer("Default");
            }
        }

        private void Assign()
        {
            villageArmy = village.GetComponent<VillageArmyUICount>();
            foreach (SingleCastle castle in castles)
            {
                if (castle.isVillage)
                    villagesArmy.Add(castle.GetComponent<VillageArmyUICount>());
                else
                    castlesArmy.Add(castle.GetComponent<CastelArmyUICount>());
            }

            villageGoldSystem_UI = GetComponent<VillageGoldSystem_UI>();
        }

        private void Start()
        {
            Invoke("OnGoldChange", 1f);
            StartCoroutine(EnemyGoldUpdater());
        }

        private IEnumerator EnemyGoldUpdater()
        {
            while (true)
            {
                yield return new WaitForSeconds(enemyUpdaterTimer);
                while (PasueController.Instance.IsPasued) yield return null;
                enemyGold += enemyUpdaterAmountOfGold;
                OnGoldChange();
            }
        }

        public void IncreasePlayerGold(int amountOfGold)
        {
            if (PlayersManager.Instance.LocalPlayer.Gold >= amountOfGold)
            {
                PlayersManager.Instance.LocalPlayer.Gold -= amountOfGold;
                playerGold += amountOfGold;
                OnGoldChange();
            }

        }

        private void OnGoldChange()
        {
            if (enemyGold > playerGold && isPlayerWin)
            {
                isPlayerWin = false;
                Change("Enemy", -1);
            }
            else if (playerGold > enemyGold && !isPlayerWin)
            {
                isPlayerWin = true;
                Change("Player", 1);
            }
            villageGoldSystem_UI.UpdateUI();
        }

        private void Change(string tag, int playerNubmer)
        {
            int tempVillageSoldier = villageArmy.sward;
            villageArmy.ChangeCastleSprite(tag, playerNubmer);
            villageArmy.sward = tempVillageSoldier;

            foreach (CastelArmyUICount castleArmy in castlesArmy)
            {
                int tempCastleSoldier = castleArmy.sward;
                castleArmy.ChangeCastleSprite(tag, playerNubmer);
                castleArmy.sward = tempCastleSoldier;
            }

            foreach (VillageArmyUICount villageArmy in villagesArmy)
            {
                int tempCastleSoldier = villageArmy.sward;
                villageArmy.ChangeCastleSprite(tag, playerNubmer);
                villageArmy.sward = tempCastleSoldier;
            }
        }
    }
}