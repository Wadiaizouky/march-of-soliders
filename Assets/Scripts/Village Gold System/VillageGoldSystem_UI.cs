﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MarchOfSoldiers
{
    public class VillageGoldSystem_UI : MonoBehaviour
    {
        public Text enemyText;
        public Text playerText;

        private VillageGoldSystem villageGoldSystem;

        private void Awake()
        {
            villageGoldSystem = GetComponent<VillageGoldSystem>();
        }

        public void UpdateUI()
        {
            enemyText.text = villageGoldSystem.enemyGold.ToString();
            playerText.text = villageGoldSystem.playerGold.ToString();
        }
    }
}