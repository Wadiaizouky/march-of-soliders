﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

namespace MarchOfSoldiers
{
    public class BlacksmithManager : Singleton_Photon<BlacksmithManager>
    {
        public int maxUpgradeNum = 3;

        public delegate void onBlacksmithUpgrade();
        public event onBlacksmithUpgrade OnBlacksmithUpgrade;


        public bool CanUpgrade(int level)
        {
            if (PlayersManager.Instance.LocalPlayer.BlacksmithLevel >= level) return false;
            if (level - PlayersManager.Instance.LocalPlayer.BlacksmithLevel > 1) return false;

            int price = GetUpgradePricing(level);
            return PlayersManager.Instance.LocalPlayer.Gold >= price;
        }

        public void UpgradeBlacksmith(bool applyGold)
        {
            if (PlayersManager.Instance.LocalPlayer.BlacksmithLevel + 1 > maxUpgradeNum) return;
            //if (!CanUpgrade(PlayersManager.Instance.LocalPlayer.BlacksmithLevel + 1)) return;
            if (PlayersManager.Instance.LocalPlayer.BlacksmithLevel == 3) return;

            PlayersManager.Instance.LocalPlayer.BlacksmithLevel += 1;

            if (applyGold)
            {
                PlayersManager.Instance.LocalPlayer.Gold -= GetUpgradePricing(PlayersManager.Instance.LocalPlayer.BlacksmithLevel);
            }

            if (PhotonNetwork.IsConnected)
            {
                photonView.RPC("CallOnBlacksmithUpgradeEvent", RpcTarget.All);
            }
            else
            {
                CallOnBlacksmithUpgradeEvent();
            }
        }

        [PunRPC]
        private void CallOnBlacksmithUpgradeEvent()
        {
            if (OnBlacksmithUpgrade != null)
                OnBlacksmithUpgrade();
        }

        public int GetUpgradePricing(int level)
        {
            int price = 0;
            switch (level)
            {
                case 1:
                    price = 120;
                    break;
                case 2:
                    price = 275;
                    break;
                case 3:
                    price = 425;
                    break;
            }

            return price;
        }
    }
}
