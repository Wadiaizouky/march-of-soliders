﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace MarchOfSoldiers
{
    public class ReinforcementManager : Singleton_Photon<ReinforcementManager>
    {
        public int maxUpgradeNum = 3;

        private Dictionary<int, bool> firstStartReinforcementByPlayers = new Dictionary<int, bool>();
        private Dictionary<int, bool> startReinforcementByPlayers = new Dictionary<int, bool>();

        private Dictionary<int, Coroutine> UpdaterByPlayers = new Dictionary<int, Coroutine>();

        public delegate void onReinforcementUpgrade();
        public event onReinforcementUpgrade OnReinforcementUpgrade;

        //UI Number Sprite
        public Sprite Number_Sprite;

        private void Start()
        {
            firstStartReinforcementByPlayers.Add(1, false);
            firstStartReinforcementByPlayers.Add(2, false);

            startReinforcementByPlayers.Add(1, false);
            startReinforcementByPlayers.Add(2, false);

            UpdaterByPlayers.Add(1, null);
            UpdaterByPlayers.Add(2, null);
        }

        private IEnumerator ReinforcementUpdater(int playerNumber, float reinforcementTimer)
        {
            startReinforcementByPlayers[playerNumber] = true;
            while (startReinforcementByPlayers[playerNumber])
            {

                yield return new WaitForSeconds(reinforcementTimer);
                while (PasueController.Instance.IsPasued) yield return null;

                foreach (SingleCastle castle in CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(playerNumber))
                {
                    if (PlayersManager.Instance.GetPlayer(playerNumber).Gold >= 2)
                    {
                        if (!castle.isGate && castle.gameObject.tag == "Player")
                        {
                            CastelArmyUICount castelArmy = castle.GetComponent<CastelArmyUICount>();
                            castelArmy.MyExraCorotine(Number_Sprite);
                            castelArmy.sward++;
                            if (PlayersManager.Instance.GetLocalPlayerNumber() == playerNumber)
                                PlayersManager.Instance.GetPlayer(playerNumber).Gold -= 2;
                        }
                    }
                }
            }
        }

        public bool CanUpgrade(int level)
        {
            Debug.Log(PlayersManager.Instance.LocalPlayer.ReinforcementLevel);
            if (PlayersManager.Instance.LocalPlayer.ReinforcementLevel >= level) return false;
            if (level - PlayersManager.Instance.LocalPlayer.ReinforcementLevel > 1) return false;

            int price = GetUpgradePricing(level);
            return PlayersManager.Instance.LocalPlayer.Gold >= price;
        }

        public void UpgradeReinforcementForLocalPlayer(bool applyGold)
        {
            if (PlayersManager.Instance.LocalPlayer.ReinforcementLevel + 1 > maxUpgradeNum) return;
            //if (!CanUpgrade(PlayersManager.Instance.LocalPlayer.ReinforcementLevel + 1)) return;
            if (PlayersManager.Instance.LocalPlayer.ReinforcementLevel == 3) return;

            PlayersManager.Instance.LocalPlayer.ReinforcementLevel += 1;

            if (applyGold)
            {
                PlayersManager.Instance.LocalPlayer.Gold -= GetUpgradePricing(PlayersManager.Instance.LocalPlayer.ReinforcementLevel);
            }

            if (PhotonNetwork.IsConnected)
            {
                photonView.RPC("UpgradeReinforcement", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber());
            }
            else
            {
                UpgradeReinforcement(PlayersManager.Instance.GetLocalPlayerNumber());
            }

        }

        [PunRPC]
        private void UpgradeReinforcement(int playerNumber)
        {
            if (!firstStartReinforcementByPlayers[playerNumber])
            {
                firstStartReinforcementByPlayers[playerNumber] = true;
                UpdaterByPlayers[playerNumber] = StartCoroutine(ReinforcementUpdater(playerNumber, GetReinforcementTimer(PlayersManager.Instance.GetPlayer(playerNumber).ReinforcementLevel)));
            }
            else
            {
                if (startReinforcementByPlayers[playerNumber])
                {
                    StopCoroutine(UpdaterByPlayers[playerNumber]);
                    UpdaterByPlayers[playerNumber] = StartCoroutine(ReinforcementUpdater(playerNumber, GetReinforcementTimer(PlayersManager.Instance.GetPlayer(playerNumber).ReinforcementLevel)));
                }
            }

            if (OnReinforcementUpgrade != null)
                OnReinforcementUpgrade();
        }

        public void ChangeReinforcementActiveStateLocalPlayer()
        {
            if (PhotonNetwork.IsConnected)
            {
                photonView.RPC("ChangeReinforcementActiveState", RpcTarget.All, PlayersManager.Instance.GetLocalPlayerNumber());
            }
            else
            {
                ChangeReinforcementActiveState(PlayersManager.Instance.GetLocalPlayerNumber());
            }
        }

        [PunRPC]
        private void ChangeReinforcementActiveState(int playerNumber)
        {
            startReinforcementByPlayers[playerNumber] = !startReinforcementByPlayers[playerNumber];

            if (startReinforcementByPlayers[playerNumber])
            {
                UpdaterByPlayers[playerNumber] = StartCoroutine(ReinforcementUpdater(playerNumber, GetReinforcementTimer(PlayersManager.Instance.GetPlayer(playerNumber).ReinforcementLevel)));
            }
            else
            {
                StopCoroutine(UpdaterByPlayers[playerNumber]);
            }
        }

        public int GetUpgradePricing(int level)
        {
            int price = 0;
            switch (level)
            {
                case 1:
                    price = 225;
                    break;
                case 2:
                    price = 450;
                    break;
                case 3:
                    price = 675;
                    break;
            }

            return price;
        }

        private int GetReinforcementTimer(int reinforcementLevel)
        {
            switch (reinforcementLevel)
            {
                case 1:
                    return 8;
                case 2:
                    return 6;
                case 3:
                    return 4;
            }
            return 8;
        }
    }
}