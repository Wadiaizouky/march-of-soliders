﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MarchOfSoldiers;
using System;
using Photon.Pun;


namespace MarchOfSoldiers
{
    public class KingdomManager : Singleton_Photon<KingdomManager>
    {
        public int maxUpgradeNum = 3;

        private float updateGoldAndSilverEvery = 1.0f;

        public delegate void onKingdomUpgrade();
        public event onKingdomUpgrade OnKingdomUpgrade;

        //when Player mouse Hover in Silver Text
        public Text SilverExplainText;

        //Map Level
        public int MapLevel;

        void Start()
        {
            if (!PlayersManager.Instance.networkMap)
            {
                PlayersManager.Instance.LocalPlayer.KingdomLevel = 0;

                PlayersManager.Instance.LocalPlayer.Silver = 0;
                if (MapLevel == 4 | MapLevel == 5 | MapLevel == 6)
                    PlayersManager.Instance.LocalPlayer.Gold = 50.0f;
                else
                    PlayersManager.Instance.LocalPlayer.Gold = 0.0f;

                //stop Playing Music
                if (MusicControler.Instance != null)
                    MusicControler.Instance.GetComponent<AudioSource>().Pause();
            }


            StartCoroutine("UpdateGoldAndSilver");
        }

        private IEnumerator UpdateGoldAndSilver()
        {

            while (true)
            {

                if (!PasueController.Instance.IsPasued)
                {
                    List<SingleCastle> _selectableCastles = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());

                    float castleProduce = 0.0f;
                    switch (PlayersManager.Instance.LocalPlayer.KingdomLevel)
                    {
                        case 0:
                            castleProduce = 25.0f;
                            break;
                        case 1:
                            castleProduce = 35.0f;
                            break;
                        case 2:
                            castleProduce = 50.0f;
                            break;
                        case 3:
                            castleProduce = 75.0f;
                            break;
                    }

                    int playerVillagesSilver = GetPlayerVillagesSilver();

                    PlayersManager.Instance.LocalPlayer.FarmsBonusSilver = GetFarmsCount() * 15;
                    PlayersManager.Instance.LocalPlayer.Silver = (int)castleProduce * _selectableCastles.Count + playerVillagesSilver + PlayersManager.Instance.LocalPlayer.FarmsBonusSilver;
                    PlayersManager.Instance.LocalPlayer.Gold += ((castleProduce * (_selectableCastles.Count * 1.0f)) + playerVillagesSilver + (15 * (GetFarmsCount() * 1.0f))) / 100;

                    List<SingleCastle> _selectableVillages = CastlesSelection.Instance.GetSelectableVillagesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
                    int Village_N = _selectableVillages.Count;
                    if (MapLevel == 15)
                        Village_N += 5;
                    if (MapLevel == 11)
                        Village_N += 2;
                    SilverExplainHover(_selectableCastles.Count, (castleProduce * (_selectableCastles.Count * 1.0f)), PlayersManager.Instance.LocalPlayer.FarmsNumber, (15 * (GetFarmsCount() * 1.0f)), Village_N, playerVillagesSilver);
                }
                yield return new WaitForSeconds(updateGoldAndSilverEvery);
            }
        }

        public bool CanUpgrade(int level)
        {
            if (PlayersManager.Instance.LocalPlayer.KingdomLevel >= level) return false;
            if (level - PlayersManager.Instance.LocalPlayer.KingdomLevel > 1) return false;

            int price = GetUpgradePricing(level);
            return PlayersManager.Instance.LocalPlayer.Gold >= price;
        }

        public void UpgradeKingdom(bool applyGold)
        {

            //if (!CanUpgrade(PlayersManager.Instance.LocalPlayer.KingdomLevel + 1)) return;
            if (PlayersManager.Instance.LocalPlayer.KingdomLevel == 3) return;

            PlayersManager.Instance.LocalPlayer.KingdomLevel++;

            if (applyGold)
            {
                PlayersManager.Instance.LocalPlayer.Gold -= GetUpgradePricing(PlayersManager.Instance.LocalPlayer.KingdomLevel);
            }

            if (PhotonNetwork.IsConnected)
            {
                photonView.RPC("CallOnKingdomUpgradeEvent", RpcTarget.All);
            }
            else
            {
                CallOnKingdomUpgradeEvent();
            }

            ChangeCastlesSpriteLevel();

            ChangeFarmCountWithKingdomLevel();

            //Active UI Player Castle Farms
            List<SingleCastle> AllPlayerCastle = CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(1);
            foreach (SingleCastle castle in AllPlayerCastle)
                castle.GetComponent<CastleFarmButtom>().ChangeFarmUI();

        }

        [PunRPC]
        private void CallOnKingdomUpgradeEvent()
        {
            if (OnKingdomUpgrade != null)
                OnKingdomUpgrade();
        }

        public int GetUpgradePricing(int level)
        {
            int price = 0;
            switch (level)
            {
                case 1:
                    price = 150;
                    break;
                case 2:
                    price = 300;
                    break;
                case 3:
                    price = 750;
                    break;
            }

            return price;
        }

        //wade add
        //Change All Sprite Player Castle to new Level 
        public void ChangeCastlesSpriteLevel()
        {
            switch (PlayersManager.Instance.LocalPlayer.KingdomLevel)
            {
                case 1:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {
                        if (Castle.GetComponent<Animator>() == null) return;
                        Castle.GetComponent<Animator>().SetBool("Level0", false);
                        Castle.GetComponent<Animator>().SetBool("Level1", true);
                    }
                    break;
                case 2:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {
                        if (Castle.GetComponent<Animator>() == null) return;
                        Castle.GetComponent<Animator>().SetBool("Level1", false);
                        Castle.GetComponent<Animator>().SetBool("Level2", true);
                    }
                    break;
                case 3:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {
                        if (Castle.GetComponent<Animator>() == null) return;
                        Castle.GetComponent<Animator>().SetBool("Level2", false);
                        Castle.GetComponent<Animator>().SetBool("Level3", true);
                    }
                    break;

            }
        }

        private void Update()
        {
            PlayersManager.Instance.LocalPlayer.FarmsNumber = GetFarmsCount();
            PlayersManager.Instance.LocalPlayer.FarmsLost = GetFarmLosed();

            //if (Input.GetKey(KeyCode.I))
            //    PlayersManager.Instance.LocalPlayer.Gold += 1000;
        }


        private int GetPlayerVillagesSilver()
        {
            int silver = 0;
            List<SingleCastle> _selectableVillages = CastlesSelection.Instance.GetSelectableVillagesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
            foreach (SingleCastle village in _selectableVillages)
            {
                silver += village.GetComponent<VillageArmyUICount>().SilverProduction;
            }

            List<SingleCastle> _unSelectableVillages = CastlesSelection.Instance.GetUnSelectableVillagesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber());
            foreach (SingleCastle village in _unSelectableVillages)
            {
                silver += village.GetComponent<VillageArmyUICount>().SilverProduction;
            }
            return silver;
        }
        #region Farm

        //Change Number of Farms valid to Player Build it like ..(KindomLevel1 = No.Farm =1;KindomLevel2 = No.Farm=2...)
        void ChangeFarmCountWithKingdomLevel()
        {
            switch (PlayersManager.Instance.LocalPlayer.KingdomLevel)
            {
                case 1:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {
                        if (!Castle.GetComponent<CastelArmyUICount>().isEnumyCastle && !Castle.GetComponent<CastelArmyUICount>().iswhitecastel)
                            Castle.GetComponent<CastelArmyUICount>().FarmCountvalid = 1;

                        CastelArmyUICount castelArmy = Castle.GetComponent<CastelArmyUICount>();
                        castelArmy.FarmAvailable = castelArmy.FarmCountvalid - castelArmy.FarmCount;
                        ActivatFarmButtom();
                    }
                    break;
                case 2:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {
                        if (!Castle.GetComponent<CastelArmyUICount>().isEnumyCastle && !Castle.GetComponent<CastelArmyUICount>().iswhitecastel)
                            Castle.GetComponent<CastelArmyUICount>().FarmCountvalid = 2;

                        CastelArmyUICount castelArmy = Castle.GetComponent<CastelArmyUICount>();
                        castelArmy.FarmAvailable = castelArmy.FarmCountvalid - castelArmy.FarmCount;
                    }
                    break;
                case 3:
                    foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
                    {

                        if (!Castle.GetComponent<CastelArmyUICount>().isEnumyCastle && !Castle.GetComponent<CastelArmyUICount>().iswhitecastel)
                            Castle.GetComponent<CastelArmyUICount>().FarmCountvalid = 3;

                        CastelArmyUICount castelArmy = Castle.GetComponent<CastelArmyUICount>();
                        castelArmy.FarmAvailable = castelArmy.FarmCountvalid - castelArmy.FarmCount;
                    }
                    break;
            }
        }

        //Active Farm Buttom to allow Player Build Farms
        void ActivatFarmButtom()
        {
            foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
            {
                if (Castle.disableSelectable) continue;

                CastelArmyUICount castelArmy = Castle.GetComponent<CastelArmyUICount>();
                if (castelArmy == null) continue;
                if (!Castle.GetComponent<CastelArmyUICount>().isEnumyCastle && !Castle.GetComponent<CastelArmyUICount>().iswhitecastel)
                    Castle.transform.GetChild(2).transform.GetChild(3).gameObject.SetActive(true);
            }
        }

        //Return All Player Farms
        public int GetFarmsCount()
        {
            int FarmCountt = 0;
            foreach (SingleCastle Castle in CastlesSelection.Instance.GetSelectableCastlesByPlayerNum(PlayersManager.Instance.GetLocalPlayerNumber()))
            {
                CastelArmyUICount castelArmy = Castle.GetComponent<CastelArmyUICount>();
                //if (castelArmy == null) continue;
                //if (!castelArmy.isEnumyCastle && !castelArmy.iswhitecastel)
                FarmCountt += castelArmy.FarmCount;
            }
            return FarmCountt;
        }

        //Return All Player Farm Losed
        public int GetFarmLosed()
        {
            int FarmLosed = 0;
            foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
            {
                //CastelArmyUICount castelArmy = Castle.GetComponent<CastelArmyUICount>();
                //if (castelArmy == null) continue;
                FarmLosed += Castle.GetComponent<CastelArmyUICount>().FarmLosed;
            }
            return FarmLosed;

        }

        //Return All Available Farms
        public int GetAvailableFarms()
        {
            int AvailableFarms = 0;
            foreach (SingleCastle Castle in SelectionManager.Instance.Allcastle)
            {
                AvailableFarms += Castle.GetComponent<CastelArmyUICount>().FarmAvailable;
            }

            return AvailableFarms;
        }

        void SilverExplainHover(int CastleNumber, float CastleSilver, int FarmNumber, float FarmSilaver, int VillageNumber, int villageSilver)
        {

            float Total = CastleSilver + FarmSilaver + villageSilver;

            SilverExplainText.text = "- ( " + CastleNumber + " ) castles = ( " + CastleSilver + " ) silvers" + "\n" +
    "- ( " + FarmNumber + " ) Farms = ( " + FarmSilaver + " ) silvers" + "\n" +
    "- ( " + VillageNumber + " ) Villages = ( " + villageSilver + " ) silvers" + "\n" +

    "Total = ( " + Total + " ) silvers";
        }

        #endregion


    }
}