﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class GameController : MonoBehaviourPunCallbacks
{

    void Start()
    {
        CreatePlayer();
    }
    private void CreatePlayer()
    {
        print("Creating Player");
        PhotonNetwork.Instantiate(Path.Combine("test", "PlayerPrefabs"), Vector3.zero, Quaternion.identity);
    }

}
