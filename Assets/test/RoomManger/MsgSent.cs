﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class MsgSent : MonoBehaviourPun
{
    private const byte EventNum = 0;

    int Player_number;
    string Player_Name;
    string Color;
    int Team;
    public PhotonView pv;

    void Start()
    {
        if (PhotonNetwork.IsMasterClient)
            Player_number = 0;
        else
        {
            Player_number = Random.Range(1, 7);
        }

        UIMultiplayerManger.Instance.Set_name(PhotonNetwork.NickName, Player_number);
        UIMultiplayerManger.Instance.Active_Drop(Player_number);

        if(pv.IsMine)
            SendMsg(PhotonNetwork.NickName, Player_number);
    }

    // Update is called once per frame
    void Update()
    {
        if (base.photonView.IsMine && Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("You pressed A");
            UIMultiplayerManger.Instance.PlayerNames[7].text = PhotonNetwork.NickName + ":" + PhotonNetwork.CurrentRoom.PlayerCount.ToString();
            //SendMsg(PhotonNetwork.NickName, Player_number);
        }
    }
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += NetworkingClient_EventReceived;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= NetworkingClient_EventReceived;
    }

    private void NetworkingClient_EventReceived(EventData obj)
    {
        if (obj.Code == EventNum)
        {
            string name = (string)obj.CustomData;
            int P_n = (int)obj.CustomData;
            UIMultiplayerManger.Instance.Set_name(name, P_n);
            // Debug.Log(obj.CustomData);
        }
    }

    private void SendMsg(string msg,int P)
    {
        PhotonNetwork.RaiseEvent( EventNum,new object[] {msg,P}, RaiseEventOptions.Default, SendOptions.SendUnreliable);
    }
}
