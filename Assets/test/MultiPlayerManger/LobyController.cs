﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;


public class LobyController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject qStart;
    [SerializeField]
    private GameObject qCancel;
    [SerializeField]
    private int RoomSize;


    public override void OnConnectedToMaster()
    {
        //base.OnConnectedToMaster()
        PhotonNetwork.AutomaticallySyncScene = true;
        qStart.SetActive(true);
        print("we are connectd to " + PhotonNetwork.CloudRegion + " Server!");
    }
    public void QStart()
    {
        qStart.SetActive(false);
        qCancel.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
        print("Quick Start");
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {

        print("Failed to join room");
        CreateRoom();
    }

    void CreateRoom()
    {
        print("Creat room");
        int randRoomNum = Random.Range(0, 1000);
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)RoomSize };
        PhotonNetwork.CreateRoom("Room" + randRoomNum, roomOps);
        print(randRoomNum);

    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {

        print("failed to create room >>>");
        CreateRoom();

    }

    public void QCancel()
    {
        qCancel.SetActive(false);
        qStart.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }

}
