﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MinimapController : MonoBehaviour
{
    public Camera MinimapCamera;
    //public RenderTexture rt;
    public RawImage rm;

    void Start()
    {
        
    }


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {

            Vector3 pos = MinimapCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector3 mousePosition; /*= new Vector3(pos.x - 0.5f, pos.y - 1.0f, 0f);*/

            //RectTransformUtility.WorldToScreenPoint(MinimapCamera,);
            RectTransformUtility.ScreenPointToWorldPointInRectangle(rm.GetComponent<RectTransform>(), new Vector3(pos.x - 0.5f, pos.y - 1.0f, 0f), MinimapCamera,out mousePosition);

            //Camera.main.transform.position = MinimapCamera.ScreenToWorldPoint(mousePosition);  
            Vector3 mousePosition2 = new Vector3(mousePosition.x, mousePosition.y, 0f);
            Camera.main.transform.position = mousePosition2;

        }
    }
}
